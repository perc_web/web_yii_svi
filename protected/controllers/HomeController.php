<?php

class HomeController extends FrontController {

    /**
     * Declares class-based actions.
     */
    public function actions() {
        return array(
            // captcha action renders the CAPTCHA image displayed on the contact page
            'captcha' => array(
                'class' => 'CCaptchaAction',
                'backColor' => 0xFFFFFF,
            ),
        );
    }

    /**
     * This is the default 'index' action that is invoked
     * when an action is not explicitly requested by users.
     */
    public function actionIndex() {
        // renders the view file 'protected/views/site/index.php'
        // using the default layout 'protected/views/layouts/main.php'

       // $this->layout= ' ';
        $condition1 = 'active = 1 and new_deal = 1';
     //  $latest_items = Item::model()->findAll(array('condition' => $condition1, 'order' => 'id DESC', 'limit' => 5));
         // $latest_items = Menu::model()->findAll(array('condition' => $condition1, 'order' => 'id DESC', 'limit' => 5));
           $latest_items = Outlet::model()->findAll(array('condition' => $condition1, 'order' => 'id DESC', 'limit' => 5));
        
        $condition2 = 'active = 1 and featured = 1';
        $featured_brands = Brands::model()->findAll(array('condition' => $condition2));
       // $featured_deals = Item::model()->findAll(array('condition' => $condition2, 'order' => 'id DESC', 'limit' => 9));
     //  $featured_deals = Menu::model()->findAll(array('condition' => $condition2, 'order' => 'id DESC', 'limit' => 9));
        $featured_deals = Outlet::model()->findAll(array('condition' => $condition2, 'order' => 'id DESC', 'limit' => 9));
        
        
        $condition3 = 'active = 1 and  hot_deal = 1';
      //  $hot_deals = Item::model()->findAll(array('condition' => $condition3, 'order' => 'id DESC', 'limit' => 9));
       // $hot_deals = Menu::model()->findAll(array('condition' => $condition3, 'order' => 'id DESC', 'limit' => 9));
         $hot_deals = Outlet::model()->findAll(array('condition' => $condition3, 'order' => 'id DESC', 'limit' => 9));
        
        $condition4 = 'active = 1 and  recomended = 1';
      //  $recomended = Item::model()->findAll(array('condition' => $condition4, 'order' => 'RAND()', 'limit' => 4));    
        //  $recomended = Menu::model()->findAll(array('condition' => $condition4, 'order' => 'RAND()', 'limit' => 4));
           $recomended = Outlet::model()->findAll(array('condition' => $condition4, 'order' => 'RAND()', 'limit' => 4));
        
        $blogs = Blog::model()->findAll(array('order' => 'id DESC', 'limit' => 4));

       $informations=Information::model()->findAll(array('condition'=>'publish=1'));
       
          $condition5 = 'active = 1';    
          $outlets = Outlet::model()->findAll(array('condition' => $condition5, 'order' => 'RAND()', 'limit' => 8));

        $this->render('index', array('latest_items' => $latest_items,
            'featured_brands' => $featured_brands,
            'hot_deals' => $hot_deals,
            'recomended' => $recomended,
            'blogs' => $blogs,
            'informations' => $informations,
            'featured_deals' => $featured_deals,
            'outlets' => $outlets,
            ));
    }

    public function actionPartners() {
        
        $cond = 'active=1';
        $criteira = new CDbCriteria;
        $criteira->order = 'title desc';
        if (isset($_REQUEST['sort']) && $_REQUEST['sort']) {
            $criteira->order = $_REQUEST['sort'];
        }
        if(isset($_REQUEST['category']) && $_REQUEST['category']){
        
            $category = Category::model()->find(array('condition' => 'slug = "'.$_REQUEST['category'].'"'));
            $cond .= ' && cat_id = '.$category->id;
         
        }
        
        /*
         if(isset($_REQUEST['sub_cat']) && $_REQUEST['sub_cat']){
            $sub_category = Category::model()->find(array('condition' => 'slug = "'.$_REQUEST['sub_cat'].'"'));
            
             $cond .= ' and ( subcat_id ="'.$sub_category->id. '"';
            $cond .= ' || subcat_id LIKE "'.$sub_category->id. ',%"';
            $cond .= ' ||  subcat_id LIKE "%,'.$sub_category->id. ',%"';
            $cond .= ' ||  subcat_id LIKE "%,'.$sub_category->id. '" )';
          
        }
        */
        
        
        $criteira->condition = $cond;
        $partners = Partner::model()->findAll($criteira);
        $scroll_partners = new CPagination(count($partners));
        $scroll_partners->pageSize = 9;
        $scroll_partners->applyLimit($criteira);
        
        
        $categories = Category::model()->findAll();
        $this->render('grid', array('partners' => $partners, 'type' => 'partner', 'scroll_partners' => $scroll_partners, 'categories' => $categories));
    }
    
    public function actionOutletSearch() {
        $criteria = new CDbCriteria; 
        $criteria->condition='`t`.`active`=1';
        $criteria->order = 'title desc';
        if (isset($_REQUEST['sort']) && $_REQUEST['sort']) {
            $criteria->order = $_REQUEST['sort'];
        }
    
        
         if(isset($_REQUEST['category']) && $_REQUEST['category']){
        
            $category = Category::model()->find(array('condition' => 'slug = "'.$_REQUEST['category'].'"'));         
             $criteria->join = 'INNER JOIN  `partner` ON (`t`.`partner_id` = `partner`.`id` AND `partner`.`cat_id`="'.$category->id.'")';
        
            
        }
        
        $outlets = Outlet::model()->findAll($criteria);
        $scroll_outlets = new CPagination(count($outlets));
        $scroll_outlets->pageSize = 9;
        $scroll_outlets->applyLimit($criteria);
        
        $categories = Category::model()->findAll();
        $this->render('outlet_search', array('outlets' => $outlets, 'scroll_outlets' => $scroll_outlets, 'categories' => $categories));
    }

    public function actionTags() {
        $tag = $_REQUEST['slug'];
        $criteira = new CDbCriteria;
        $criteira->condition = 'tag_id = :tag';
        $criteira->params = array(':tag' => $tag);
       
        $tags = PartnerTag::model()->findAll($criteira);
        if ($tags) {
             $item_arr = array();
            foreach ($tags as $tag) {
                $item_arr[] = $tag->partner_id;
            }
            $cond = 'id in (' . implode(",", $item_arr) . ')';
            
            $partners = Partner::model()->findAll(array('condition' => $cond));
            $scroll_items = new CPagination(count($items));
            $scroll_items->pageSize = 9;
            $scroll_items->applyLimit($criteira);
        }
        
       

        $this->render('grid', array('partners' => $partners, 'type' => 'search', 'scroll_items' => $scroll_items));
    }

    public function actionCategories() {
        $cat = $_REQUEST['slug'];
        $category=Category::model()->findByAttributes(array('slug'=>$cat ));
        $criteira = new CDbCriteria;
        $criteira->condition = 'cat_id = :cat and active = 1';
        $criteira->params = array(':cat' => $category->id);
        $criteira->order = 'title desc';
        if (isset($_REQUEST['sort']) && $_REQUEST['sort']) {
            $criteira->order = $_REQUEST['sort'];
        }
        $items = Item::model()->findAll($criteira);
        $scroll_items = new CPagination(count($items));
        $scroll_items->pageSize = 9;
        $scroll_items->applyLimit($criteira);
        $this->render('grid', array('items' => $items, 'type' => 'search', 'scroll_items' => $scroll_items));
    }

    public function actionSearch() {
        $cond = 'active = 1';
        Yii::app()->user->setState('search_q', '');
        Yii::app()->user->setState('search_cat', '');
        if (isset($_POST['cat']) && $_POST['cat']) {
            $cond .= ' and cat_id = ' . $_POST['cat'];
            Yii::app()->user->setState('search_cat', $_POST['cat']);
        }
        
        if (isset($_POST['q']) && $_POST['q']) {
            $cond .= ' and title like "%' . $_POST['q'] . '%"';
            Yii::app()->user->setState('search_q', $_POST['q']);
            $tag_cond= "LOWER(`t`.`title`) ='" . strtolower($_POST['q'])."'" ;
           $tag = Tags::model()->find(array('condition'=>$tag_cond));
           if($tag){
               $tag->usage+=1;
               $tag->save();
                
           }
          
        }
        
        
        $criteria = new CDbCriteria;
        $criteria->condition = $cond;
        $criteria->order = 'title desc';
        if (isset($_REQUEST['sort']) && $_REQUEST['sort']) {
            $criteria->order = $_REQUEST['sort'];
        }
        $partners = Partner::model()->findAll($criteria);
        $scroll_items = new CPagination(count($items));
        $scroll_items->pageSize = 9;
        $scroll_items->applyLimit($criteria);
        $this->render('grid', array('partners' => $partners, 'type' => 'search'));
    }

    public function actionItemDetails() {
        $criteria = new CDbCriteria;
        $criteria->condition = 'active = 1 and slug = :slug';
        $criteria->params = array(':slug' => $_REQUEST['slug']);
        $item = Item::model()->find($criteria);

        $photos = GalleryPhoto::model()->findAllByAttributes(array('gallery_id' => $item->gallery_id));
        
            //similar items that in the same partner
            $criteria = new CDbCriteria;
            $cond2 = 'id != "'.$item->id .'" && partner_id="'.$item->partner_id .'"';
            $criteria = new CDbCriteria;
            $criteria->condition = $cond2;
            $criteria->order = 'id DESC';
            // $criteria->limit = 4;
            $similar_items = Item::model()->findAll($criteria);
            
        

        $tag_criteria = new CDbCriteria;
        $tag_criteria->condition = 'item_id = :item';
        $tag_criteria->params = array(':item' => $item->id);
        $tag_criteria->group = 'item_id';
        $item_tags = ItemTag::model()->findAll($tag_criteria);
        if (isset($_POST['productTagName'])) {
            $item_tag = new ItemTag;
            $item_tag->item_id = $item->id;
            if ($_POST['productTagName'] == 0 && isset($_POST['productNewTags'])) {
                $tag = new Tags;
                $tag->title = $_POST['productNewTags'];
                $tag->save();
                $item_tag->tag_id = $tag->id;
            } else {
                $item_tag->tag_id = $_POST['productTagName'];
            }
            $item_tag->save();
            $this->redirect(Yii::app()->request->urlReferrer);
        }

        $review_criteria = new CDbCriteria;
        $review_criteria->condition = 'item_id = :item';
        $review_criteria->order = 'id DESC';
        $review_criteria->params = array(':item' => $item->id);
        $limit = 5;
        if (isset($_POST['Pages'])) {
            $limit = $_POST['Pages'];
        }
        $review_criteria->limit = $limit;
        $reviews = Reviews::model()->findAll($review_criteria);
        if (isset($_POST['Reviews'])) {
            $review = new Reviews;
            $review->user_id = Yii::app()->user->id;
            $review->item_id = $item->id;
            $review->attributes = $_POST['Reviews'];
            $review->save();
            $this->redirect(Yii::app()->request->urlReferrer);
        }

        $this->render('details', array('item' => $item,
            'item_tags' => $item_tags,
            'reviews' => $reviews,
            'limit' => $limit,
            'photos' => $photos,
            'similar_items' => $similar_items,
        ));
    }
    
     public function actionItemDetailsFancy() {
        $this->layout= ' ' ;
        $criteria = new CDbCriteria;
        $criteria->condition = 'active = 1 and slug = :slug';
        $criteria->params = array(':slug' => $_REQUEST['slug']);
        $item = Item::model()->find($criteria);

        $photos = GalleryPhoto::model()->findAllByAttributes(array('gallery_id' => $item->gallery_id));

        
         //similar items that in the same partner
            $criteria = new CDbCriteria;
            $cond2 = 'id != "'.$item->id .'" && partner_id="'.$item->partner_id .'"';
            $criteria = new CDbCriteria;
            $criteria->condition = $cond2;
            $criteria->order = 'id DESC';
            // $criteria->limit = 4;
            $similar_items = Item::model()->findAll($criteria);
            
        $tag_criteria = new CDbCriteria;
        $tag_criteria->condition = 'item_id = :item';
        $tag_criteria->params = array(':item' => $item->id);
        $tag_criteria->group = 'item_id';
        $item_tags = ItemTag::model()->findAll($tag_criteria);
        if (isset($_POST['productTagName'])) {
            $item_tag = new ItemTag;
            $item_tag->item_id = $item->id;
            if ($_POST['productTagName'] == 0 && isset($_POST['productNewTags'])) {
                $tag = new Tags;
                $tag->title = $_POST['productNewTags'];
                $tag->save();
                $item_tag->tag_id = $tag->id;
            } else {
                $item_tag->tag_id = $_POST['productTagName'];
            }
            $item_tag->save();
            $this->redirect(Yii::app()->request->urlReferrer);
        }

        $review_criteria = new CDbCriteria;
        $review_criteria->condition = 'item_id = :item';
        $review_criteria->order = 'id DESC';
        $review_criteria->params = array(':item' => $item->id);
        $limit = 5;
        if (isset($_POST['Pages'])) {
            $limit = $_POST['Pages'];
        }
        $review_criteria->limit = $limit;
        $reviews = Reviews::model()->findAll($review_criteria);
        if (isset($_POST['Reviews'])) {
            $review = new Reviews;
            $review->user_id = Yii::app()->user->id;
            $review->item_id = $item->id;
            $review->attributes = $_POST['Reviews'];
            $review->save();
            $this->redirect(Yii::app()->request->urlReferrer);
        }

        $this->render('details2', array('item' => $item,
            'item_tags' => $item_tags,
            'reviews' => $reviews,
            'limit' => $limit,
            'photos' => $photos,
            'similar_items' =>  $similar_items ,
        ));
    }

    
     public function actionMenuDetails() {
        $criteria = new CDbCriteria;
        $criteria->condition = 'active = 1 and slug = :slug';
        $criteria->params = array(':slug' => $_REQUEST['slug']);
        $menu = Menu::model()->find($criteria);
        
       $photos = MenuItem::model()->findAllByAttributes(array('menu_id' => $menu->id));
       
          
       
        $tag_criteria = new CDbCriteria;
        $tag_criteria->condition = 'item_id = :item';
        $tag_criteria->params = array(':item' => $menu->id);
        $tag_criteria->group = 'item_id';
        $item_tags = ItemTag::model()->findAll($tag_criteria);
        if (isset($_POST['productTagName'])) {
            $item_tag = new ItemTag;
            $item_tag->item_id = $menu->id;
            if ($_POST['productTagName'] == 0 && isset($_POST['productNewTags'])) {
                $tag = new Tags;
                $tag->title = $_POST['productNewTags'];
                $tag->save();
                $item_tag->tag_id = $tag->id;
            } else {
                $item_tag->tag_id = $_POST['productTagName'];
            }
            $item_tag->save();
            $this->redirect(Yii::app()->request->urlReferrer);
        }

        $review_criteria = new CDbCriteria;
        $review_criteria->condition = 'approved = 1 && item_id = :item';
        $review_criteria->order = 'id DESC';
        $review_criteria->params = array(':item' => $menu->id);
        $limit = 5;
        if (isset($_REQUEST['page'])) {
            $limit = $_REQUEST['page'];
        }
        $review_criteria->limit = $limit;
        $reviews = Reviews::model()->findAll($review_criteria);
        if (isset($_POST['Reviews'])) {
            $review = new Reviews;
            $review->user_id = Yii::app()->user->id;
            $review->item_id = $menu->id;
            $review->attributes = $_POST['Reviews'];
            $review->save();
            $this->redirect(Yii::app()->request->urlReferrer);
        }

        
        $this->render('menu_details', array('menu' => $menu,
            'item_tags' => $item_tags,
            'reviews' => $reviews,
            'limit' => $limit,
            'photos' => $photos,
        ));
       
        
    }
    
    
      
     public function actionMenuDetailsfancy() {
         $this->layout= ' ' ;
        $criteria = new CDbCriteria;
        $criteria->condition = 'active = 1 and slug = :slug';
        $criteria->params = array(':slug' => $_REQUEST['slug']);
        $menu = Menu::model()->find($criteria);
        
       $photos = MenuItem::model()->findAllByAttributes(array('menu_id' => $menu->id));
       
        $tag_criteria = new CDbCriteria;
        $tag_criteria->condition = 'item_id = :item';
        $tag_criteria->params = array(':item' => $menu->id);
        $tag_criteria->group = 'item_id';
        $item_tags = ItemTag::model()->findAll($tag_criteria);
        if (isset($_POST['productTagName'])) {
            $item_tag = new ItemTag;
            $item_tag->item_id = $menu->id;
            if ($_POST['productTagName'] == 0 && isset($_POST['productNewTags'])) {
                $tag = new Tags;
                $tag->title = $_POST['productNewTags'];
                $tag->save();
                $item_tag->tag_id = $tag->id;
            } else {
                $item_tag->tag_id = $_POST['productTagName'];
            }
            $item_tag->save();
            $this->redirect(Yii::app()->request->urlReferrer);
        }

        $review_criteria = new CDbCriteria;
        $review_criteria->condition = 'item_id = :item';
        $review_criteria->order = 'id DESC';
        $review_criteria->params = array(':item' => $menu->id);
        $limit = 5;
        if (isset($_REQUEST['page'])) {
            $limit = $_REQUEST['page'];
        }
        $review_criteria->limit = $limit;
        $reviews = Reviews::model()->findAll($review_criteria);
        if (isset($_POST['Reviews'])) {
            $review = new Reviews;
            $review->user_id = Yii::app()->user->id;
            $review->item_id = $menu->id;
            $review->attributes = $_POST['Reviews'];
            $review->save();
            $this->redirect(Yii::app()->request->urlReferrer);
        }

        
        $this->render('menu_details2', array('menu' => $menu,
            'item_tags' => $item_tags,
            'reviews' => $reviews,
            'limit' => $limit,
            'photos' => $photos,
        ));
       
        
    }
    
    
    public function actionFaq() {
        $model = Faq::model()->findAll();
        $this->render('faq', array('faqs' => $model,));
    }

    /**
     * This is the action to handle external exceptions.
     */

      public function actionError() {
        $report = false;
        if (isset($_POST['Report'])) {
            $model = new Report;
            $model->attributes = $_POST['Report'];
            $model->date_create = date('Y-m-d H:i:s');
            $model->page = filter_input(INPUT_SERVER, 'HTTP_REFERER');
            if ($model->save())
                $report = true;
        }

        $error = Yii::app()->errorHandler->error;
        $error['report'] = $report;

        if (Yii::app()->request->isAjaxRequest) {
            echo $error['message'];
            return;
        }
        $this->render('error', $error);
    }

    /**
     * Displays the contact page
     */
    public function actionContact() {

        // $this->layout = ' ';
        $model = new ContactForm;
        if (isset($_POST['ContactForm'])) {
            $model->attributes = $_POST['ContactForm'];
            if ($model->validate()) {
                $name = '=?UTF-8?B?' . base64_encode($model->name) . '?=';
                $subject = '=?UTF-8?B?' . base64_encode($model->subject) . '?=';
                $headers = "From: $name <{$model->email}>\r\n" .
                        "Reply-To: {$model->email}\r\n" .
                        "MIME-Version: 1.0\r\n" .
                        "Content-type: text/plain; charset=UTF-8";
                mail(Helper::yiiparam('adminEmail'), $subject, $model->message, $headers);
                Yii::app()->user->setFlash('contact', 'Thank you for contacting us. We will respond to you as soon as possible.');
                $this->refresh();
            }
        }
        $this->render('Contact', array('model' => $model));
    }

    public function actionRegister() {
        $user_signUp = $this->user_signUp;
        $user_signUp->scenario = 'register';
        $list = '';
        if (isset($_POST['User'])) {
            $user_signUp->attributes = $_POST['User'];
            $user_signUp->groups_id = 3;
            $user_signUp->active = 1;
            $user_signUp->username = Helper::slugify($user_signUp->full_name);
            $user_signUp->last_login_ip = Yii::app()->request->getUserHostAddress();
            if(isset($_POST['categories'])){
                $user_signUp->interested_category = implode(',', $_POST['categories']);
            }
            if ($user_signUp->save()) {
                $user_signUp->username .= $user_signUp->id;
                $user_signUp->save();
                $user_login = $this->user_login;
                $user_login->username = $user_signUp->username;
                $user_login->password = User::simple_decrypt($user_signUp->password);
                // validate user input and redirect to the previous page if valid
                if ($user_login->validate() && $user_login->login()) {
                    if (Yii::app()->user->group == 3) {
                        echo "home";
                    } else if (Yii::app()->user->group == 2) {
                        echo "dashboard";
                    }
                }
            } else {
                foreach ($user_signUp->getErrors() as $error) {
                    $list.=$error[0] . '<br>';
                }
                echo 'wrong' . '*-*' . $list . '<br>';
                die;
            }
        }
    }

    /**
     * Displays the login page
     */
    public function actionLogin() {
        $user_login = $this->user_login;
        // if it is ajax validation request
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'login-form') {
            echo CActiveForm::validate($user_login);
            Yii::app()->end();
        }
        // collect user input data
        if (isset($_POST['LoginForm'])) {
            $user_login->attributes = $_POST['LoginForm'];
            // validate user input and redirect to the previous page if valid
            
            // User GeoLocation:
            $location = Yii::app()->geoip->lookupLocation($ip);
 
            $data = array(
                'ip' => $ip,
                'country' => $location ->countryCode3,
                'countryName' => $location ->countryName,
                'region' => $location ->region,
                'regionName' => $location ->regionName,
                'city' => $location ->city,
            );
    
            if ($user_login->validate() && $user_login->login()) {
                 $user=User::model()->findByPk(Yii::app()->user->id);
                 $user->last_login=date("Y-m-d H:i:s");
                 $user->last_login_ip = Yii::app()->request->getUserHostAddress();
                 $user->last_login_country = $data['countryName'];
                  
                  $user->save(false);
                if (Yii::app()->user->group == 3  ) {
                    echo "home";
                    $user->login=1 ;
                    $user->last_login_ip = Yii::app()->request->getUserHostAddress();
                    $user->last_login_country = $data['countryName'];
                    $user->save(false);
                } else if (Yii::app()->user->group == 2) {
                    echo "dashboard";
                     
                    $user->login=1 ;
                     $user->last_login_ip = Yii::app()->request->getUserHostAddress();
                     $user->last_login_country = $data['countryName'];
                    $user->save(false);
                }
            } else {
                 foreach ($user_login->getErrors() as $error) {
                    $list.=$error[0] . '<br>';
                }
                echo 'wrong' . '*-*' . $list . '<br>';
                die;
               // echo 'wrong';
            }
        }
    }
    
    public function actionloginFB() {
        $returnUrl = Yii::app()->request->baseUrl;
        $user = Yii::app()->facebook->getUser(); //id
        $accessToken = Yii::app()->facebook->getAccessToken();
        if (isset($_GET['error']) && $_GET['error'] == 'access_denied') {
            $this->redirect($returnUrl);
        }
        if ($user) {
            Yii::app()->facebook->setAccessToken($accessToken);
            $userinfo = Yii::app()->facebook->api('/me');
            // after we have the user informaition
            $userFBID = $userinfo['id'];
            $first_name = $userinfo['first_name'];
            $last_name = $userinfo['last_name'];
            $username = $userinfo['name'];
            ////// check if user exist before or not useing email or userid
            $criteria = new CDbCriteria;
            $criteria->condition = 'oauth_uid = ' . $userFBID;
            $Users = User::model()->find($criteria);
            ///// if user exist make session login
            if ($Users) {
                ////// login
                $model = new LoginForm;
                $model->username = $Users->username;
                $model->password = User::model()->simple_decrypt($Users->password);

                if ($model->login()) {
                    $userGroup = Yii::app()->user->getState('userGroup');
                    if ($userGroup == 2 || $userGroup == 1)
                        $this->redirect(array('/dashboard'));
                    else
                        $this->redirect(array('index'));
                }
                ///end login
            }///end login
            ////////  user not exist  then register and then login
            //registration and send emial with user name and password
            else {
                $password = Yii::app()->name;
                $model = new User('register');
                $model->username = $username;
                $model->password = $password;
                $model->fname = $first_name;
                $model->lname = $last_name;
                $model->oauth_uid = $userFBID;
                $model->groups_id = 3;
                if ($model->save(false)) {
                    ////// login
                    $model2 = new LoginForm;
                    $model2->username = $model->username;
                    $model2->password = User::model()->simple_decrypt($model->password);
                    if ($model2->login()) {
                        $userGroup = Yii::app()->user->getState('userGroup');
                        if ($userGroup == 2 || $userGroup == 1) {
                            $this->redirect(array('/dashboard'));
                        } else {
                            $this->redirect(array('index'));
                        }
                    }
                    //$this->redirect( array('/Profile'));
                }///// end registration and send emial with user name and password
            }/// end register user
        }/// end user access from FB
        else { //starting my app,cancel
            $loginUrl = Yii::app()->facebook->getLoginUrl(array('scope' => 'email', 'redirect-uri' => $returnUrl));
            $this->redirect($loginUrl);
        }
    }

    /**
     * Logs out the current user and redirect to homepage.
     */
    public function actionLogout() {
         $user=User::model()->findByPk(Yii::app()->user->id);
         if($user){
               $user->login=0;
                $user->save(false);
         }
       
         Yii::app()->user->logout();
          $this->redirect(Yii::app()->homeUrl);
         
    }

    public function actionForgotpass() {

        $model = new User;
        if (isset($_POST['User'])) {
            $model->attributes = $_POST['User'];

            $criteria = new CDbCriteria;
            $criteria->condition = 'email=:email';
            $criteria->params = array(':email' => $model->email);
            $usermodel = User::model()->find($criteria);
            if (count($usermodel) == 0) {

                Yii::app()->user->setFlash('ErrorMsg', 'Sorry, there\'s no account associated with that email address');
            } else {

                //create randomkey
                $key = Helper::GenerateRandomKey();
                $usermodel->pass_reset = 1;
                $usermodel->pass_code = $key;
                $usermodel->save(false);

                $mail = new YiiMailer();
                $mail->setFrom(Yii::app()->params['adminEmail'], ' EHRlinked Admininstrator');
                $mail->setTo($model->email);
                $mail->setSubject('EHRlinked Password reset');

                $message = 'Dear customer,

					Please follow this link to reset your password :
					Username:' . $usermodel->username . '
					URL: 	' . Yii::app()->params['webSite'] . '/home/reset/hash/' . $usermodel->pass_code . '

					';

                $mail->setBody($message);


                if ($mail->send()) {
                    Yii::app()->user->setFlash('register-success', 'Thank you! An activation email has been sent to your email address.');
                } else {
                    Yii::app()->user->setFlash('error', 'Error while sending email: ' . $mail->getError());
                }

                Yii::app()->user->setFlash('ErrorMsg', 'Check <b> ' . $usermodel->email . ' </b> for the confirmation email. It will have a link to reset your password..');
            }
        }



        $this->render('forgotpass', array('model' => $model));
    }

    public function actionReset($hash) {
        $criteria = new CDbCriteria;
        $criteria->condition = 'pass_code=:Hash and pass_reset=1';
        $criteria->params = array(':Hash' => $hash);
        $user_found = User::model()->find($criteria);

        if (count($user_found) == 0) {
            $flag = 0;
            Yii::app()->user->setFlash('ErrorMsg', 'Sorry you have followed a wrong link .');
        } else {
            $flag = 1;
        }

        $model = new User('passreset');


        if (isset($_POST['User']) and count($user_found) != 0) {
            $model->attributes = $_POST['User'];

            $user_found->pass_reset = 0;
            $user_found->pass_code = '';
            $user_found->password = $model->newpassword = $_POST['User']['newpassword'];

            $user_found->save(false);

            $notify = new Notification();
            $notify->type = 4;
            $notify->notify_from = 1; // admin user has static id
            $notify->notify_to = $user_found->id;
            $notify->title = 'Password changed  ';
            $notify->details = 'Your password has been changed successfully .';
            $notify->save(false);

            Yii::app()->user->setFlash('ErrorMsg', ' Please Login with your new credentials');

            $this->redirect(array('home/login'));
        }


        $this->render('resetpass', array('model' => $model, 'flag' => $flag));
    }

    public function actionStaticPages() {
        $url = $_REQUEST['slug'];
        $criteria = new CDbCriteria;
        $criteria->condition = 'publish = 1 and url = :url';
        $criteria->params = array(':url' => $url);
        $page = Pages::model()->find($criteria);
        $this->render('static_page', array('page' => $page));
    }

    public function actionSubscribe() {
        if (isset($_POST['email'])) {
            $subscribe = new Subscribtion;
            $subscribe->email = $_POST['email'];
            
            if ($subscribe->save()) {
               Yii::app()->user->setFlash('subscribe', ' Thank you for subscribing!.');
            }
            
        }
        $this->redirect(Yii::app()->request->urlReferrer);
    }

    public function actionCareers() {
        $criteria = new CDbCriteria;
        $criteria->condition = '';
        $jobs = Jobs::model()->findAll($criteria);
        $scroll_jobs = new CPagination(count($jobs));
        $scroll_jobs->pageSize = 9;
        $scroll_jobs->applyLimit($criteria);
        $this->render('careers', array('jobs' => $jobs, 'scroll_jobs' => $scroll_jobs));
    }

    public function actionReserve() {
          // $this->layout=' ';
        $model = new Reservations;
        $user_id = Yii::app()->user->id;
        $user = User::model()->findByPk($user_id);
        if ($user) {
            if (isset($_POST['Reservations'])) {
                $model->attributes = $_POST['Reservations'];
                $model->user_id = $user->id;
                if ($model->save()) {
                    Yii::app()->user->setFlash('reserve', 'Thanks For Your Reservation.');
                    $notification = new Notifications;
                    $notification->url = Yii::app()->getBaseUrl(true) . '/admin/reservations/view/id/' . $model->id;
                    $notification->notification = strip_tags(Yii::app()->params['notification_msg']) . ' "' . $user->username . ' "';
                    $notification->notifier_id = $user->id;
                    $notification->save();
                }
            }
            $this->render('reserve', array('model' => $model, 'cats' => $cats));
        } else {
            throw new CHttpException(404, 'The requested page does not exist.');
        }
    }

    public function actionCatalog() {
         $categories = Category::model()->findAll(array('condition' => 'parent is null'));
        $this->render('catalog', array('categories' => $categories));
    }

    public function actionAddCart() {
        if(isset($_REQUEST['Reservations']['menu_id']) && $_REQUEST['Reservations']['menu_id']){
            $id = $_REQUEST['Reservations']['menu_id'];
        }
        if(isset($_REQUEST['product'])&& $_REQUEST['product']){
            $id = $_REQUEST['product'];
        }
        $action = $_REQUEST['action'];
        $quantity=1; // when user click on  recommended items thathas not quantitiy in home page 
        
        $product = Menu::model()->findByPk($id);
        
        
      $outlet=  Outlet::model()->findByPk($product->outlet_id) ;
        if ($id && !$action) {
            Yii::app()->shoppingCart->put($product, $quantity);
        }
        if ($id && $action == 'remove') {
            Yii::app()->shoppingCart->remove($id); //no items
        }
        /*
          elseif($id && $action == 'update'){
          $p=Yii::app()->shoppingCart->itemAt($id);
          Yii::app()->shoppingCart->update($p, $quantity);
          } */
        
         /*
        if(isset($_REQUEST['flag'])&& $_REQUEST['flag']=="menu"){
            
             $this->redirect(Yii::app()->getBaseUrl(true) . '/home/calculate?category='.$outlet->partner->cat->id.'&partner='.$outlet->partner->id.'&outlet='.$product->outlet_id.'&menu='.$product->id);
        }else{
            // $this->redirect('calculate');
            
             $this->redirect(Yii::app()->getBaseUrl(true) . '/home/calculate?category='.$outlet->partner->cat->id.'&partner='.$outlet->partner->id.'&outlet='.$product->outlet_id.'&menu='.$product->id);
        }
        */
        
         $this->redirect(Yii::app()->getBaseUrl(true) . '/home/calculate?category='.$outlet->partner->cat->id.'&partner='.$outlet->partner->id.'&outlet='.$product->outlet_id.'&menu='.$product->id);
         
        
       
    }

    public function actionCalculate() {
        $model = new Reservations;
        $cart = Yii::app()->shoppingCart->getPositions();
        $this->render('calculate', array('cart' => $cart, 'model' => $model));
    }

    public function actiongetTags() {
        $term = trim($_GET['term']);
        if ($term != '') {
            $sql = 'SELECT id ,title AS label FROM tags WHERE title LIKE :name';
            $name = '%' . $term . '%';
            $result = Yii::app()->db->createCommand($sql)->queryAll(true, array(':name' => $name));
            $return_array = array();
            if ($result) {
                foreach ($result as $sub) {
                    // Set all of your view variables in this array
                    $return_array[] = array(
                        // This is the color name
                        'label' => $sub['label'],
                        'id' => $sub['id'],
                    );
                }
            } else {
                $return_array[0] = array(
                    // This is the color name
                    'label' => $term,
                    'id' => 0,
                );
            }

            echo CJSON::encode($return_array);
            Yii::app()->end();
        }
    }
    
    
     public function actionBlogdetail() {
         
        $slug = $_REQUEST['slug'];
        $criteria = new CDbCriteria;
        $criteria->condition = 'slug = :slug';
        $criteria->params = array(':slug' => $slug);
        $blog = Blog::model()->find($criteria);
        $this->render('blog_detail', array('blog' => $blog));
        
    }
    
    public function actionPartnerdetail() {
        $slug = $_REQUEST['slug'];
        $criteria = new CDbCriteria;
        $criteria->condition = 'slug = :slug';
        $criteria->params = array(':slug' => $slug);
        $partner = Partner::model()->find($criteria);
        
        if($partner){
             $photos = GalleryPhoto::model()->findAllByAttributes(array('gallery_id' => $partner->gallery_id));

        
            //similar partners
             $cond = 'id!='.$partner->id . ' && cat_id="'.$partner->cat_id .'"';
             $criteria = new CDbCriteria;
            $criteria->condition = $cond;
            $criteria->order = 'RAND()';
            $criteria->limit = 4;
            $similar_partners = Partner::model()->findAll($criteria);
            
            //partner items
             $criteria = new CDbCriteria;
             $criteria->condition='partner_id=:partner';
            $criteria->params = array(':partner' => $partner->id);
             $criteria->order = 'id DESC';
           $items=Item::model()->findAll($criteria) ;
           
           
                     //partner outlets
            $criteria = new CDbCriteria;
             $criteria->condition='partner_id=:partner';
             $criteria->params = array(':partner' => $partner->id);
             $criteria->order = 'id DESC';
           $outlets=Outlet::model()->findAll($criteria) ;
        
        $review_criteria = new CDbCriteria;
        $review_criteria->condition = 'approved = 1 && partner_id="'.$partner->id.'"';
        $limit = 5;
        if (isset($_REQUEST['page'])) {
            $limit = $_REQUEST['page'];
        }
        
         $review_criteria->limit = $limit;
        $reviews = PartnerReview::model()->findAll($review_criteria);
      
        if (isset($_POST['PartnerReview'])) {
            $review = new PartnerReview;
            $review->user_id = Yii::app()->user->id;
            $review->partner_id = $partner->id;
            $review->attributes = $_POST['PartnerReview'];
            $review->save();
            $this->redirect(Yii::app()->request->urlReferrer);
        }
        $this->render('partner_detail', array('partner' => $partner, 'reviews' => $reviews,'similar_partners' => $similar_partners,'outlets' => $outlets,'items' => $items,'photos' => $photos,'limit' => $limit));
            
        }else {
            throw new CHttpException(404, 'The requested page does not exist.');
        }
        
         
    }
    
    
    

    public function actionPartnerdetailfancy() {
        $this->layout=' ' ;
        $slug = $_REQUEST['slug'];
        $criteria = new CDbCriteria;
        $criteria->condition = 'slug = :slug';
        $criteria->params = array(':slug' => $slug);
        $partner = Partner::model()->find($criteria);
        
          $photos = GalleryPhoto::model()->findAllByAttributes(array('gallery_id' => $partner->gallery_id));
        
            //similar partners
             $cond = 'id!='.$partner->id . ' && cat_id="'.$partner->cat_id .'"';
             $criteria = new CDbCriteria;
            $criteria->condition = $cond;
            $criteria->order = 'RAND()';
            $criteria->limit = 4;
            $similar_partners = Partner::model()->findAll($criteria);
            
            //partner items
             $criteria = new CDbCriteria;
             $criteria->condition='partner_id=:partner';
            $criteria->params = array(':partner' => $partner->id);
             $criteria->order = 'id DESC';
           $items=Item::model()->findAll($criteria) ;
           
           
                     //partner outlets
            $criteria = new CDbCriteria;
             $criteria->condition='partner_id=:partner';
            $criteria->params = array(':partner' => $partner->id);
             $criteria->order = 'id DESC';
           $outlets=Outlet::model()->findAll($criteria) ;
        
        $review_criteria = new CDbCriteria;
        $review_criteria->condition = 'approved = 1 && partner_id="'.$partner->id.'"';
        $limit = 5;
       if (isset($_REQUEST['page'])) {
            $limit = $_REQUEST['page'];
        }
        
        $review_criteria->limit = $limit;
        $reviews = PartnerReview::model()->findAll($review_criteria);
        if (isset($_POST['PartnerReview'])) {
            $review = new PartnerReview;
            $review->user_id = Yii::app()->user->id;
            $review->partner_id = $partner->id;
            $review->attributes = $_POST['PartnerReview'];
            $review->save();
            $this->redirect(Yii::app()->request->urlReferrer);
        }
        $this->render('partner_detail2', array('partner' => $partner, 'reviews' => $reviews,'similar_partners' => $similar_partners,'outlets' => $outlets,'items' => $items,'photos' => $photos));
    }
    
      public function actionOutletdetail() {
         
        $slug = $_REQUEST['slug'];
        $criteria = new CDbCriteria;
        $criteria->condition = 'slug = :slug';
        $criteria->params = array(':slug' => $slug);
        $outlet = Outlet::model()->find($criteria);
         $partner=$outlet->partner ;
        
           $photos = GalleryPhoto::model()->findAllByAttributes(array('gallery_id' => $outlet->gallery_id));
            $outlet_menu = Menu::model()->findByAttributes(array('outlet_id' => $outlet->id));
            $menu_items = MenuItem::model()->findAllByAttributes(array('menu_id' => $outlet_menu->id));
           
           
            //similar outlet
             $cond = 'id!='.$outlet->id . ' && partner_id="'.$outlet->partner_id .'"';
             $criteria = new CDbCriteria;
            $criteria->condition = $cond;
            $criteria->order = 'RAND()';
            $criteria->limit = 4;
            $similar_outlets = Outlet::model()->findAll($criteria);
            
            
             //partner items
             $criteria = new CDbCriteria;
             $criteria->condition='partner_id=:partner';
            $criteria->params = array(':partner' => $partner->id);
             $criteria->order = 'id DESC';
           $items=Item::model()->findAll($criteria) ;
           
           
            //outlet menu
             $criteria = new CDbCriteria;
             $criteria->condition='outlet_id=:outlet';
            $criteria->params = array(':outlet' => $outlet->id);
             $criteria->order = 'id DESC';
           $menus=Menu::model()->findAll($criteria) ;
           

            
        $review_criteria = new CDbCriteria;
        $review_criteria->condition = 'approved = 1  && outlet_id="'.$outlet->id.'"';
        $limit = 5;
        if (isset($_REQUEST['page'])) {
            $limit = $_REQUEST['page'];
        }
        $review_criteria->limit = $limit;
        $reviews = OutletReview::model()->findAll($review_criteria);
        if (isset($_POST['OutletReview'])) {
            $review = new OutletReview;
            $review->user_id = Yii::app()->user->id;
            $review->outlet_id = $outlet->id;
            $review->attributes = $_POST['OutletReview'];
            $review->save();
            $this->redirect(Yii::app()->request->urlReferrer);
        }
        $this->render('outlet_detail', array('outlet' => $outlet, 'reviews' => $reviews, 'partner' => $partner, 'similar_outlets' => $similar_outlets,'items' => $items,'menus' => $menus,'photos' => $photos,'menu_items' => $menu_items));
    }
    
    public function actionOutletdetailfancy() {
          $this->layout=' ' ;
        $slug = $_REQUEST['slug'];
        $criteria = new CDbCriteria;
        $criteria->condition = 'slug = :slug';
        $criteria->params = array(':slug' => $slug);
        $outlet = Outlet::model()->find($criteria);
         $partner=$outlet->partner ;
         
            $photos = GalleryPhoto::model()->findAllByAttributes(array('gallery_id' => $outlet->gallery_id));
            $outlet_menu = Menu::model()->findByAttributes(array('outlet_id' => $outlet->id));
            $menu_items = MenuItem::model()->findAllByAttributes(array('menu_id' => $outlet_menu->id));
            //similar outlet
             $cond = 'id!='.$outlet->id . ' && partner_id="'.$outlet->partner_id .'"';
             $criteria = new CDbCriteria;
            $criteria->condition = $cond;
            $criteria->order = 'RAND()';
            $criteria->limit = 4;
            $similar_outlets = Outlet::model()->findAll($criteria);
            
            
             //partner items
             $criteria = new CDbCriteria;
             $criteria->condition='partner_id=:partner';
            $criteria->params = array(':partner' => $partner->id);
             $criteria->order = 'id DESC';
           $items=Item::model()->findAll($criteria) ;
           
           
            //outlet menu
             $criteria = new CDbCriteria;
             $criteria->condition='outlet_id=:outlet';
            $criteria->params = array(':outlet' => $outlet->id);
             $criteria->order = 'id DESC';
           $menus=Menu::model()->findAll($criteria) ;
           
           
            
        $review_criteria = new CDbCriteria;
        $review_criteria->condition = 'approved = 1  && outlet_id="'.$outlet->id.'"';

        $limit = 5;
        if (isset($_REQUEST['page'])) {
            $limit = $_REQUEST['page'];
        }
        $review_criteria->limit = $limit;
        $reviews = OutletReview::model()->findAll($review_criteria);
        if (isset($_POST['OutletReview'])) {
            $review = new OutletReview;
            $review->user_id = Yii::app()->user->id;
            $review->outlet_id = $outlet->id;
            $review->attributes = $_POST['OutletReview'];
            $review->save();
            $this->redirect(Yii::app()->request->urlReferrer);
        }
        $this->render('outlet_detail2', array('outlet' => $outlet, 'reviews' => $reviews, 'partner' => $partner, 'similar_outlets' => $similar_outlets,'items' => $items,'menus' => $menus,'photos' => $photos   ,'menu_items' => $menu_items));
    }
    
    public function actionLikeReview(){
        $id = $_POST['id'];
        $like = new ReviewLikes;
        $like->review_id = $id;
        $like->user_id = Yii::app()->user->id;
        if($like->save()){
            echo "done";
        }
    }
    
    public function actionDislikeReview(){
        $id = $_POST['id'];
        $criteria = new CDbCriteria;
        $criteria->condition = 'user_id = '.Yii::app()->user->id.' and review_id = '.$id;
        if(ReviewLikes::model()->delete($criteria)){
            echo "done";
        }
    }
    
   
    
    
    public function actionGetpartners()
    {
        $cat_id = $_REQUEST['cat_id'];
        $criteria = new CDbCriteria;
        $criteria->condition='cat_id=:cat';
        $criteria->params = array(':cat'=>$cat_id);
        echo CHtml::dropDownList('Reservations[partner_id]', 'partner_id',CHtml::listData(Partner::model()->findAll($criteria),'id', 'title'), 
                array('prompt' => 'Choose', 'class' => 'form-control col-md-8 jqtransformdone', 'onchange' => 'get_outlets(this.value)', 'id' => 'partner_drop'
                    ));
    }
    
    public function actionGetoutlets()
    {
        $partner_id = $_REQUEST['partner_id'];
        $criteria = new CDbCriteria;
        $criteria->condition='partner_id=:partner';
        $criteria->params = array(':partner' => $partner_id);
        echo CHtml::dropDownList('Reservations[outlet_id]', 'outlet_id',CHtml::listData(Outlet::model()->findAll($criteria),'id', 'title'), 
                array('prompt' => 'Choose', 'class' => 'form-control col-md-8 jqtransformdone', 'onchange' => 'get_menus(this.value)', 'id' => 'outlet_drop'

                    ));
    }
     
    public function actionGetmenus()
    {
        $outlet_id = $_REQUEST['outlet_id'];
        $criteria = new CDbCriteria;
        $criteria->condition='outlet_id=:outlet';
        $criteria->params = array(':outlet' => $outlet_id);
        echo CHtml::dropDownList('Reservations[menu_id]', 'menu_id',CHtml::listData(Menu::model()->findAll($criteria),'id', 'title'), 
                array('prompt' => 'Choose', 'class' => 'form-control col-md-8 jqtransformdone', 'onchange' => 'submit_form()'

                    ));
    }
    
    public function actionGetitems()
    {
        $menu_id = $_REQUEST['menu_id'];
        $criteria = new CDbCriteria;
        $criteria->condition='menu_id=:menu';
        $criteria->params = array(':menu' => $menu_id);
        $item_arr = array();
        foreach(MenuItem::model()->findAll($criteria) as $menu_item){
            $item_arr[] = $menu_item->item_id;
        }
        if($item_arr){
            $item_criteria = new CDbCriteria;
            $item_criteria->condition = 'id in ('.implode(",", $item_arr).')';
            echo CHtml::dropDownList('Reservations[item_id]', 'item_id',CHtml::listData(Item::model()->findAll($item_criteria),'id', 'title'), 
                array('prompt' => 'Choose', 'class' => 'form-control col-md-8 jqtransformdone', 'onchange' => 'submit_form()'

                    ));
        }else{
            echo CHtml::dropDownList('Reservations[item_id]', 'item_id', array(),
                array('prompt' => 'Choose', 'class' => 'form-control col-md-8 jqtransformdone'

                    ));
        }
    }
    
    
  

}

