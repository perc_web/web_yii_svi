<?php

class MenuController extends AdminController
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			/*'accessControl', // perform access control for CRUD operations*/
		);
	}

	public function actions() {
        return array(
            'order' => array(
            'class' => 'ext.yiisortablemodel.actions.AjaxSortingAction',
            ),
        );
    	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('allow', 'actions' => array('order'), 'users' => array('@')),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate($outlet_id= null)
	{
		$model=new Menu;

                  if($outlet_id){
                     $model->outlet_id = $outlet_id;  
                }
		
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Menu']))
		{
			$model->attributes=$_POST['Menu'];
                        /*
                        $rnd = time();  // generate random number between 0-9999
			$uploadedFile=CUploadedFile::getInstance($model,'image');

			if(! empty ($uploadedFile)){
				$fileName = "{$rnd}-{$uploadedFile}";  // random number + file name
				$model->image = $fileName;
				$uploadedFile->saveAs(Yii::app()->basePath.'/../media/menus/'.$fileName);
			} 
                        */
                        if($model->save()){
                           if (isset($_POST['item'])) {
                                for ($i = 0; $i < count($_POST['item']); $i++) {
                                    $menu_item = new MenuItem;
                                    $menu_item->menu_id = $model->id;
                                    $menu_item->item_id = $_POST['item'][$i];
                                    $menu_item->save();
                                }
                          }
                
                    $this->redirect(array('view','id'=>$model->id));
                        }
				
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Menu']))
		{
                    /*
                     if( $model->image != ''){
                        $_POST['Menu']['image'] = $model->image;
                    }
                    */
			$model->attributes=$_POST['Menu'];
                        /*
                         $uploadedFile=CUploadedFile::getInstance($model,'image');

                    if(! empty ($uploadedFile)){
                        if($model->image == ''){
                            $rnd = time();
                            $fileName = "{$rnd}-{$uploadedFile}";
                            $model->image=	$fileName;
                        }
                        $uploadedFile->saveAs(Yii::app()->basePath.'/../media/menus/'.$model->image);
                    }   
                    */
                            if($model->save()){
                                      MenuItem::model()->deleteAllByAttributes(array('menu_id' => $model->id));
                            if (isset($_POST['item'])) {
                                for ($i = 0; $i < count($_POST['item']); $i++) {
                                    $menu_item = new MenuItem;
                                    $menu_item->menu_id = $model->id;
                                    $menu_item->item_id = $_POST['item'][$i];
                                    $menu_item->save();
                                }
                          }
                               $this->redirect(array('view','id'=>$model->id)); 
                            }
				
		}
                
  

                 $items = MenuItem::model()->findAllByAttributes(array('menu_id' => $model->id));
		$this->render('update',array(
			'model'=>$model,
                       'items'=>$items,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex($outlet_id= null)
	{
		$model=new Menu('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Menu']))
			$model->attributes=$_GET['Menu'];
                
                    if($outlet_id){
                     $model->outlet_id = $outlet_id;  
                }

		$this->render('index',array(
			'model'=>$model,
		));

	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=Menu::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='menu-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
        
}
