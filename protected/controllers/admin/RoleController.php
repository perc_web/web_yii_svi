<?php

class RoleController extends AdminController
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			/*'accessControl', // perform access control for CRUD operations*/
		);
	}

	public function actions() {
        return array(
            'order' => array(
            'class' => 'ext.yiisortablemodel.actions.AjaxSortingAction',
            ),
        );
    	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('allow', 'actions' => array('order'), 'users' => array('@')),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
        $model = $this->loadModel($id);

        $modules = Helper::get_modules_access();
        $fields = RoleField::model()->findAll();

          $role_users = RoleUser::model()->findAllByAttributes(array('role_id' => $id));
        if ($role_users) {
            $users_id = array();
            foreach ($role_users as $ru) {
                $users_id[] = $ru->user_id;
            }
            $role_users = new RoleUser;
            $role_users->user_id = $users_id;
        } else {
            $role_users = new RoleUser;
        }
        
        
         if ($_POST['save']) {

            //get roles privliges
            $privs = array();
            foreach ($modules as $module) {
                for ($i = 0; $i < count($fields); $i++) {
                    $privs[$module->url][$fields[$i]->real_action] = $_POST['role_' . $fields[$i]->id . "_" . $module->id];
                }
            }

            $model->permissions = serialize($privs);
            $model->save(false);
            
            //assign role to users
            if ($_POST['RoleUser']) {
                RoleUser::model()->deleteAllByAttributes(array('role_id' => $id));
                if (is_array($_POST['RoleUser']['user_id'])) {
                    
                    foreach ($_POST['RoleUser']['user_id'] as $us) {

                        $role_user = new RoleUser;
                        $role_user->user_id = $us;
                        $role_user->role_id = $id;
                        $role_user->save(false);

                        /*
                        $user = User::model()->findByPk($us);
                        if ($user) {
                            $user->permission_updated = 1;
                           // $user->password = $user->simple_decrypt($user->password);
                            $user->save(false);
                        }
                        */
                    }
                }
            }

            Yii::app()->user->setFlash("roles_saved", "The data has been saved successfully.");

            $this->refresh();
        }
        
        
        
        
        

		$this->render('view',array(
			'model'=> $model,
                       'role_users' => $role_users,
                      'fields' => $fields,
                      'modules' => $modules,
                    
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Role;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Role']))
		{
			$model->attributes=$_POST['Role'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Role']))
		{
			$model->attributes=$_POST['Role'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
            
		$model=new Role('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Role']))
			$model->attributes=$_GET['Role'];
             
                
                 $this->render('index',array(
			'model'=>$model,
		));
                
         /*       
        if( Yii::app()->user->group == 2  )
        {
            //$this->render('dashboard');
            $this->render('index',array(
			'model'=>$model,
		));
        }else
	     throw new CHttpException(404," You don't have permission to access this part");

	*/	

	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=Role::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='role-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
