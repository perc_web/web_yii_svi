<?php

class OutletController extends AdminController
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			/*'accessControl', // perform access control for CRUD operations*/
		);
	}

	public function actions() {
        return array(
            'order' => array(
            'class' => 'ext.yiisortablemodel.actions.AjaxSortingAction',
            ),
        );
    	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('allow', 'actions' => array('order'), 'users' => array('@')),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate($partner_id=null)
	{
		$model=new Outlet;
                   if($partner_id){
                     $model->partner_id = $partner_id;  
                }
		

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Outlet']))
		{
                    $model->attributes=$_POST['Outlet'];
                    $rnd = time();  // generate random number between 0-9999
                    $uploadedFile=CUploadedFile::getInstance($model,'image');
                if(! empty ($uploadedFile)){
                    $fileName = "{$rnd}-{$uploadedFile}";  // random number + file name
                    $model->image = $fileName;
                    $uploadedFile->saveAs(Yii::app()->basePath.'/../media/outlet/'.$fileName);
                }
                    if($model->save())
                        $this->redirect(array('view','id'=>$model->id));
		}
                $gallery = new Gallery;
            $gallery->name = true;
            $gallery->description = false;
            $gallery->versions = array(
                'large' => array(
                    'resize' => array(1280, 854),
                ),
                'medium' => array(
                    'resize' => array(952, 351),
                ),
                'small' => array(
                    'resize' => array(140, 113),
                )
            );
            $gallery->save();
            $model->gallery_id = $gallery->id;
		$this->render('create',array(
			'model'=>$model, 'gallery' => $gallery
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
                if($model->gallery_id){
                    $gallery = Gallery::model()->findByPk($model->gallery_id);
                }else{
                    $gallery = new Gallery();
                    $gallery->name = true;
                    $gallery->description = false;
                    $gallery->versions = array(
                        'large' => array(
                            'resize' => array(1280, 854),
                        ),
                        'medium' => array(
                            'resize' => array(952, 351),
                        ),
                        'small' => array(
                            'resize' => array(140, 113),
                        )
                    );
                    $gallery->save();
                }
		if(isset($_POST['Outlet']))
		{
                    if( $model->image != ''){
                        $_POST['Outlet']['image'] = $model->image;
                    }
                    $model->attributes=$_POST['Outlet'];
                    $uploadedFile=CUploadedFile::getInstance($model,'image');
                    if(! empty ($uploadedFile)){
                        if($model->image =='')
                        {
                            $rnd = time();
                            $fileName = "{$rnd}-{$uploadedFile}";
                            $model->image=	$fileName;
                        }
                        $uploadedFile->saveAs(Yii::app()->basePath.'/../media/outlet/'.$model->image);
                    }
                    if($model->save())
                        $this->redirect(array('view','id'=>$model->id));
                    }
                    $this->render('update',array(
                            'model'=>$model, 'gallery' => $gallery
                    ));
                }

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex($partner_id=null)
	{
       
            $model=new Outlet('search');
            $model->unsetAttributes();  // clear any default values
            if(isset($_GET['Outlet']))
                $model->attributes=$_GET['Outlet'];
              if($partner_id){
                     $model->partner_id = $partner_id;  
                }

            $this->render('index',array(
                'model'=>$model,
            ));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=Outlet::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='outlet-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
        
}
