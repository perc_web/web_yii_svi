<?php

class m140903_130744_intialdatabse extends CDbMigration {

    public function up() {
        $this->execute("
DROP TABLE IF EXISTS `pages`;
CREATE TABLE `pages` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `intro` varchar(255) DEFAULT NULL,
  `details` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `video` varchar(255) DEFAULT NULL,
  `publish` varchar(255) DEFAULT NULL,
  `meta_keywords` varchar(255) DEFAULT NULL,
  `meta_desc` varchar(255) DEFAULT NULL,
  `meta_author` varchar(255) DEFAULT NULL,
  `field` varchar(255) DEFAULT NULL,
  `sort` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_id` (`id`),
  KEY `index_id2` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `banner`;
CREATE TABLE `banner` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `header` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `subheader` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `details` text COLLATE utf8_bin,
  `image` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_bin NOT NULL,
  `subtitle` varchar(255) COLLATE utf8_bin NOT NULL,
  `temp1` varchar(255) COLLATE utf8_bin NOT NULL,
  `temp2` varchar(255) COLLATE utf8_bin NOT NULL,
  `temp3` varchar(255) COLLATE utf8_bin NOT NULL,
  `temp4` varchar(255) COLLATE utf8_bin NOT NULL,
  `sort` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

DROP TABLE IF EXISTS `country`;
CREATE TABLE `country` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `country_code` varchar(2) NOT NULL,
  `title` varchar(100) NOT NULL,
  `cost_country` int(10) NOT NULL DEFAULT '100',
  `sort` int(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=240 DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `errormessage`;
CREATE TABLE `errormessage` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `error_home` varchar(255) NOT NULL,
  `error_homeactive` tinyint(2) NOT NULL,
  `error_image` varchar(255) NOT NULL,
  `error_prev` varchar(255) NOT NULL,
  `error_prevactive` tinyint(255) NOT NULL,
  `error_subhead` longtext NOT NULL,
  `error_heading` longtext NOT NULL,
  `error_message` varchar(255) NOT NULL,
  `error_body` longtext NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_id` (`id`),
  KEY `index_id` (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `faq`;
CREATE TABLE `faq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `question` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `answer` text COLLATE utf8_bin NOT NULL,
  `cat_id` int(255) DEFAULT NULL,
  `quest` varchar(255) COLLATE utf8_bin NOT NULL,
  `active` varchar(255) COLLATE utf8_bin NOT NULL,
  `sort` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

DROP TABLE IF EXISTS `gallery`;
CREATE TABLE `gallery` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `versions_data` text COLLATE utf8_bin NOT NULL,
  `name` tinyint(1) NOT NULL DEFAULT '1',
  `description` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

DROP TABLE IF EXISTS `gallery_photo`;
CREATE TABLE `gallery_photo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gallery_id` int(11) NOT NULL,
  `rank` int(11) NOT NULL DEFAULT '0',
  `name` varchar(512) COLLATE utf8_bin NOT NULL,
  `description` text COLLATE utf8_bin,
  `file_name` varchar(128) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_gallery_photo_gallery1` (`gallery_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;


DROP TABLE IF EXISTS `newsletter`;
CREATE TABLE `newsletter` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=45 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

DROP TABLE IF EXISTS `newsletter_message`;
CREATE TABLE `newsletter_message` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `message` text COLLATE utf8_bin,
  `subject` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `users_id` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `date_sent` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `start_flag` tinyint(4) DEFAULT NULL,
  `end_flag` tinyint(4) DEFAULT NULL,
  `temp1` tinyint(4) DEFAULT NULL,
  `temp2` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;


DROP TABLE IF EXISTS `product`;
CREATE TABLE `product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cat_id` int(11) DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `main_image` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `price` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `active` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `status` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_bin NOT NULL,
  `quantity` int(255) NOT NULL,
  `url` varchar(255) COLLATE utf8_bin NOT NULL,
  `sku` varchar(255) COLLATE utf8_bin NOT NULL,
  `short_desc` varchar(255) COLLATE utf8_bin NOT NULL,
  `desc` varchar(255) COLLATE utf8_bin NOT NULL,
  `gallery_id` varchar(255) COLLATE utf8_bin NOT NULL,
  `meta_title` varchar(255) COLLATE utf8_bin NOT NULL,
  `meta_key` varchar(255) COLLATE utf8_bin NOT NULL,
  `meta_desc` varchar(255) COLLATE utf8_bin NOT NULL,
  `date_created` varchar(255) COLLATE utf8_bin NOT NULL,
  `sort` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;



DROP TABLE IF EXISTS `settings`;
CREATE TABLE `settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `facebook` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `google` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `twitter` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `pinterest` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `press_email` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `support_email` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `blog_email` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `paypal_email` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `temp1` tinyint(4) DEFAULT NULL,
  `temp2` tinyint(4) DEFAULT NULL,
  `temp3` tinyint(4) DEFAULT NULL,
  `temp4` tinyint(4) DEFAULT NULL,
  `api_username` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `api_password` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `signature` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `paypal_fee` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `paypalextra_fee` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `site_commession` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `mobile` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `fax` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `address` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `paypal_live` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;


DROP TABLE IF EXISTS `tree`;
CREATE TABLE `tree` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` tinyint(3) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `link` varchar(255) NOT NULL,
  `icon` varchar(45) DEFAULT NULL,
  `level` int(11) NOT NULL,
  `position` int(10) unsigned DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `parent_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) COLLATE utf8_bin NOT NULL,
  `email` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `password` varchar(90) COLLATE utf8_bin DEFAULT NULL,
  `fname` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `lname` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `image` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `details` text COLLATE utf8_bin,
  `groups_id` int(11) DEFAULT NULL COMMENT '0 trainer  1  vendor   2 company  3 employee  4 stuff(EHR linked stuff)     6 Admin',
  `active` tinyint(1) DEFAULT '0',
  `pass_reset` tinyint(4) DEFAULT '0' COMMENT '1 requested reset password',
  `pass_code` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `last_login` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `sort` int(255) NOT NULL,
  `level` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

DROP TABLE IF EXISTS `user_details`;
CREATE TABLE `user_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `country_id` varchar(90) COLLATE utf8_bin DEFAULT '1',
  `state` varchar(90) COLLATE utf8_bin DEFAULT NULL,
  `city` varchar(90) COLLATE utf8_bin DEFAULT NULL,
  `county` varchar(90) COLLATE utf8_bin DEFAULT NULL,
  `address` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `address2` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `zipcode` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `lng` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `lat` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `zoom` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `last_login` datetime DEFAULT NULL,
  `phone_no` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `fax_no` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

DROP TABLE IF EXISTS `user_groups`;
CREATE TABLE `user_groups` (
  `id` int(11) NOT NULL,
  `group_title` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;


INSERT INTO `user` VALUES ('1', 'superadmin', 'superadmin@ukprosloutions.com', 'ZbahWZYXk1bv6MPwm1OUIg==', 'super', 'user', '', 0x7468652073757065722061646D696E2075736572, '1', '1', '0', '', '2014-08-21 10:23:48', '1', '2');
INSERT INTO `user` VALUES ('2', 'admin', 'admin@ukprosolutions.com', 'ZbahWZYXk1bv6MPwm1OUIg==', 'super', 'admin', null, 0x74657374, '2', '1', '0', '', '2014-09-01 14:50:26', '3', '0');

INSERT INTO `user_details` VALUES ('1', '6', '1', null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `user_details` VALUES ('2', '2', '1', null, null, null, '', null, null, null, null, null, null, null, null, null);

INSERT INTO `user_groups` VALUES ('1', 'Super Admin ');
INSERT INTO `user_groups` VALUES ('2', 'Admin');
INSERT INTO `user_groups` VALUES ('3', 'Normal User');
                 
                 ");
    }

    public function down() {
        echo "m140903_130744_pages does not support migration down.\n";
        return false;
    }

    /*
      // Use safeUp/safeDown to do migration with transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
