<?php

/**
 * This is the model class for table "user".
 *
 * The followings are the available columns in table 'user':
 * @property integer $id
 * @property string $username
 * @property string $email
 * @property string $password
 * @property string $fname
 * @property string $lname
 * @property string $image
 * @property string $details
 * @property integer $groups_id
 * @property integer $active
 * @property integer $pass_reset
 * @property string $pass_code
 * @property string $last_login
 * @property integer $sort
 * @property integer $level
 * @property string $phone
 * @property string $address
 * @property string $post_code
 * @property string $city
 * @property string $state
 * @property integer $country
 * @property string $oauth_uid
 *
 * The followings are the available model relations:
 * @property Favorite[] $favorites
 * @property Review[] $reviews
 * @property Shop[] $shops
 * @property UserGroups $groups
 * @property AllCountries $country0
 * @property Package $package
 * login
 * permission_updated
 */
class User extends CActiveRecord
{
    public $password_repeat;
    public $verifyCode;
    public $newpassword;
    public $newpassword_repeat;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'user';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
                    array('username', 'required'),
                    array('groups_id, active, pass_reset, level, country, payment_method', 'numerical', 'integerOnly'=>true),
                    array('username, last_login_ip', 'length', 'max'=>50),
                    array('last_login_country', 'length', 'max'=>100),
                    array('email, fname, lname, image, pass_code, last_login, phone, address, post_code, city, state, oauth_uid, full_name', 'length', 'max'=>255),
                    array('password', 'length', 'max'=>90),
                    array('details,age,login', 'safe'),

                    array('username,email', 'unique'),
                    array('email', 'email'),
                    array('newpassword', 'compare', 'compareAttribute' => 'newpassword_repeat', 'on' => 'update'),
                    array('details,groups_id,password_repeat,newpassword,newpassword_repeat,active', 'safe'),
                    array('email,username,fname,lname', 'required', 'on' => 'create ,update'),
                    array('details', 'safe', 'on' => 'create'),
                    // The following rule is used by search().
                    array('username, email, password, fname, lname, image, details, groups_id, active', 'safe', 'on' => 'search'),
                    array('email,password,password_repeat,username','required' ,'on'=>'register'),
                    array('password', 'compare','compareAttribute'=>'password_repeat' ,'on'=>'register' ),
                    array('newpassword_repeat', 'compare', 'compareAttribute' => 'newpassword', 'on' => 'passreset'),

                    // The following rule is used by search().
                    // @todo Please remove those attributes that should not be searched.
                    array('id, username, email, password, fname, lname, image, details, groups_id, active, pass_reset, pass_code, last_login, sort, level, phone, address, post_code, city, state, country, oauth_uid, last_login_ip,last_login_country', 'safe', 'on'=>'search'),
		);
	}
	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'reviews' => array(self::HAS_MANY, 'Review', 'user_id'),
			'usergroup' => array(self::BELONGS_TO, 'Groups', 'groups_id'),
			'country0' => array(self::BELONGS_TO, 'AllCountries', 'country'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'username' => 'Username',
			'email' => 'Email',
			'password' => 'Password',
			'fname' => 'Firstname',
			'lname' => 'Lastname',
			'image' => 'Image',
			'details' => 'Details',
			'groups_id' => 'Groups',
			'active' => 'Active',
			'pass_reset' => 'Pass Reset',
			'pass_code' => 'Pass Code',
			'last_login' => 'Last Login',			
			'level' => 'Level',
			'phone' => 'Phone',
			'address' => 'Address',
			'post_code' => 'Post Code',
			'city' => 'City',
			'state' => 'State',
			'country' => 'Country',
			'oauth_uid' => 'Oauth Uid',
                       'age' => 'Brith Date',
                       'last_login_ip' => 'Last Login IP',
                       'last_login_country' => 'Last Login Country',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('username',$this->username,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('fname',$this->fname,true);
		$criteria->compare('lname',$this->lname,true);
		$criteria->compare('image',$this->image,true);
		$criteria->compare('details',$this->details,true);
		$criteria->compare('groups_id',$this->groups_id);
		$criteria->compare('active',$this->active);
		$criteria->compare('pass_reset',$this->pass_reset);
		$criteria->compare('pass_code',$this->pass_code,true);
		$criteria->compare('last_login',$this->last_login,true);
		$criteria->compare('level',$this->level);
		$criteria->compare('phone',$this->phone,true);
		$criteria->compare('address',$this->address,true);
		$criteria->compare('post_code',$this->post_code,true);
		$criteria->compare('city',$this->city,true);
		$criteria->compare('state',$this->state,true);
		$criteria->compare('country',$this->country);
		$criteria->compare('oauth_uid',$this->oauth_uid,true);
		$criteria->compare('last_login_ip',$this->last_login_ip,true);
		$criteria->compare('last_login_country',$this->last_login_country,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return User the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        
        
          public function beforeSave() {
        if ($this->isNewRecord){
            //$this->created = new CDbExpression('NOW()'); // add action
            $this->password = User::hash($this->password);
        }
        
     return parent::beforeSave(); 
    }

    // Authentication methods
    public static function hash($value) {
        return User::simple_encrypt($value);
    }

    public static function simple_encrypt($text, $salt = "Ukprosol") {
        return trim(base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_128, $salt, $text, MCRYPT_MODE_ECB, mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_ECB), MCRYPT_RAND))));
    }

    public static function simple_decrypt($text, $salt = "Ukprosol") {
        return trim(mcrypt_decrypt(MCRYPT_RIJNDAEL_128, $salt, base64_decode($text), MCRYPT_MODE_ECB, mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_ECB), MCRYPT_RAND)));
    }

    public function check($value) {
        $new_hash = User::simple_encrypt($value);
        if ($new_hash == $this->password) {
            return true;
        }
        return false;
    }

    public static function getProfileType() {
        if (Yii::app()->user->group == 1) {
            return 'member';
        } else if (Yii::app()->user->group == 2) {
            return '2';
        } else if (Yii::app()->user->group == 3) {
            return '3';
        } else if (Yii::app()->user->group == 4) {
            return '4';
        } else if (Yii::app()->user->group == 6) {
            return 'dashboard';
        } else {
            return '#';
        }
    }

    public static function CheckAdmin() {
        if (( Yii::app()->user->isGuest)) {
            return false;
        } else if (Yii::app()->user->group == 1) {
            return true;
        } else if (Yii::app()->user->group == 2) {
            return true;
        } else {
            return false;
        }
    }
    
    
        public static function CheckAcessRole() {
           $user= User::model()->findByPk(Yii::app()->user->id) ;
        if (( Yii::app()->user->isGuest)) {
            return false;
        } else if ($user->groups_id == 1) {
            return true;
        } else if ($user->groups_id == 2) {
            return false;
        } else {
            return false;
        }
    }

    public static function CheckPermission($type) {
        if (( Yii::app()->user->isGuest)) {
            return false;
        }

        switch ($type) {
            case 'superadmin':
                if (Yii::app()->user->group == 1)
                    return true;
                break;
            case 'admin':
                if (Yii::app()->user->group == 2)
                    return true;
                break;
            case 'member':
                if (Yii::app()->user->group == 3)
                    return true;
                break;
            default:
                return false;
        } // switch
    }
    
     public static function RoleName($id) {
         $roleuser = RoleUser::model()->findByAttributes(array('user_id' => $id)); 
         return $roleuser->role->title ;
     }
}
