<?php

/**
 * This is the model class for table "advertise".
 *
 * The followings are the available columns in table 'advertise':
 * @property integer $id
 * @property string $image
 * @property integer $page
 * @property integer $size
 * @property integer $active
 * @property string $link
 */
class Advertise extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'advertise';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('page, size, active', 'numerical', 'integerOnly'=>true),
			array('image, link', 'length', 'max'=>255),
                         array('link', 'url'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, image, page, size, active, link', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'image' => 'Image',
			'page' => 'Page',
			'size' => 'Size',
			'active' => 'Active',
			'link' => 'Link',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('image',$this->image,true);
		$criteria->compare('page',$this->page);
		$criteria->compare('size',$this->size);
		$criteria->compare('active',$this->active);
		$criteria->compare('link',$this->link,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Advertise the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        
        
        public static function getPage($page) {
               switch ($page) {
            
            case 1:
              
                    return "home";
                break;
            case 2:
                
                    return "why svi card";
                break;
            
            case 3:
                
                    return "partner&reserve&catalog";
                break;
            
          
            
            case 4:
                
                    return "contact us";
                break;
            
            case 5:
                
                    return "faq";
                break;
            
             case 6:
                
                    return "blog";
                break;
            
            
            
            default:
                return " ";
        } 
    }
    
    
    
     public static function getSize($size) {
               switch ($size) {
                    case 0:
              
                    return "very small";
                break;
            
            case 1:
              
                    return "small";
                break;
            case 2:
                
                    return "medium";
                break;
            
            case 3:
                
                    return "large";
                break;
            
           
            default:
                return " ";
        } 
    }
    
    
}
