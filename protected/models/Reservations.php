<?php

/**
 * This is the model class for table "reservations".
 *
 * The followings are the available columns in table 'reservations':
 * @property integer $id
 * @property integer $user_id
 * @property string $date
 * @property integer $item_id
 * @property integer $partner_id
 * @property integer $outlet_id
 * @property string $from
 * @property string $to
 * @property integer $category_id
 * @property integer $num_customers
 *
 * The followings are the available model relations:
 * @property Item $item
 * @property Category $category
 * @property Partner $partner
 * @property Outlet $outlet
 */
class Reservations extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'reservations';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('user_id, item_id, partner_id, outlet_id, category_id, num_customers, menu_id', 'numerical', 'integerOnly'=>true),
			array('from, to', 'length', 'max'=>255),
			array('date', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, user_id, date, item_id, partner_id, outlet_id, from, to, category_id, num_customers', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'item' => array(self::BELONGS_TO, 'Item', 'item_id'),
			'category' => array(self::BELONGS_TO, 'Category', 'category_id'),
			'partner' => array(self::BELONGS_TO, 'Partner', 'partner_id'),
			'outlet' => array(self::BELONGS_TO, 'Outlet', 'outlet_id'),
                        'user' => array(self::BELONGS_TO, 'User', 'user_id'),
                        'menu' => array(self::BELONGS_TO, 'Menu', 'menu_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'user_id' => 'User',
			'date' => 'Date',
			'item_id' => 'Item',
			'partner_id' => 'Partner',
			'outlet_id' => 'Outlet',
			'from' => 'From',
			'to' => 'To',
			'category_id' => 'Category',
			'num_customers' => 'Num Customers',
                        'menu_id' => 'Menu',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('date',$this->date,true);
		$criteria->compare('item_id',$this->item_id);
		$criteria->compare('partner_id',$this->partner_id);
		$criteria->compare('outlet_id',$this->outlet_id);
		$criteria->compare('from',$this->from,true);
		$criteria->compare('to',$this->to,true);
		$criteria->compare('category_id',$this->category_id);
		$criteria->compare('num_customers',$this->num_customers);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Reservations the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
