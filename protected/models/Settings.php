<?php

/**
 * This is the model class for table "settings".
 *
 * The followings are the available columns in table 'settings':
 * @property integer $id
 * @property string $facebook
 * @property string $google
 * @property string $twitter
 * @property string $pinterest
 * @property string $email
 * @property string $press_email
 * @property string $support_email
 * @property string $phone
 * @property string $mobile
 * @property string $fax
 * @property string $address
 */
class Settings extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'settings';
	}
        
        public function __set($name, $value) 
        {
                if (EMHelper::WinnieThePooh($name, $this->behaviors())){
                        $this->{$name} = $value;
                }else{
                        parent::__set($name, $value);
                }
        }

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('facebook, google, twitter, pinterest, email, press_email, support_email, phone, mobile, fax, address', 'length', 'max'=>255),
			array('notification_msg,youtube,linkedin,currency', 'safe'),
                        // The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, facebook, google, twitter, pinterest, email, press_email, support_email, phone, mobile, fax, address', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'facebook' => 'Facebook',
			'google' => 'Google',
			'twitter' => 'Twitter',
			'pinterest' => 'Pinterest',
			'email' => 'Email',
			'press_email' => 'Press Email',
			'support_email' => 'Support Email',
			'phone' => 'Phone',
			'mobile' => 'Mobile',
			'fax' => 'Fax',
			'address' => 'Address',
                        'notification_msg' => 'Notification Message',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('facebook',$this->facebook,true);
		$criteria->compare('google',$this->google,true);
		$criteria->compare('twitter',$this->twitter,true);
		$criteria->compare('pinterest',$this->pinterest,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('press_email',$this->press_email,true);
		$criteria->compare('support_email',$this->support_email,true);
		$criteria->compare('phone',$this->phone,true);
		$criteria->compare('mobile',$this->mobile,true);
		$criteria->compare('fax',$this->fax,true);
		$criteria->compare('address',$this->address,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Settings the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        public function behaviors() {
            return array_merge(
                    parent::behaviors(), array(
                'EasyMultiLanguage' => array(
                    'class' => 'ext.EasyMultiLanguage.EasyMultiLanguageBehavior',
                    'translated_attributes' => array('notification_msg'),
                    'languages' => Yii::app()->params['languages'],
                    'default_language' => Yii::app()->params['default_language'],
                    'admin_routes' => array('admin/settings/index', 'admin/settings/update', 'admin/settings/create'),
                    'translations_table' => 'translations',
                ),
                    )
            );
    }
}
