<?php

/**
 * This is the model class for table "outlet".
 *
 * The followings are the available columns in table 'outlet':
 * @property integer $id
 * @property integer $partner_id
 * @property string $title
 * @property string $intro
 * @property string $description
 * @property string $address
 * @property string $slug
 * @property integer $active
 * image
 *
 * The followings are the available model relations:
 * @property Partner $partner
 * hot_deal,new_deal,recomended,featured
 */
class Outlet extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'outlet';
	}
        
        public function __set($name, $value) 
        {
                if (EMHelper::WinnieThePooh($name, $this->behaviors())){
                        $this->{$name} = $value;
                }else{
                        parent::__set($name, $value);
                }
        }

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
		        array('partner_id, title,intro,image', 'required'),
			array('partner_id, active, gallery_id,hot_deal,new_deal,recomended,featured', 'numerical', 'integerOnly'=>true),
			array('title, address, slug', 'length', 'max'=>255),
                        array('discount', 'match' ,
                        'pattern'=> '/^\s*-?[1-9]\d*(\.\d{1,2})?\s*$/u',    
                        'message'=> 'Discount must be number'
                        ),
			array('intro, description, image,hot_deal,new_deal,recomended,featured', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, partner_id, title, intro, description, address, slug, active', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'partner' => array(self::BELONGS_TO, 'Partner', 'partner_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'partner_id' => 'Partner',
			'title' => 'Title',
			'intro' => 'Intro',
			'description' => 'Description',
			'address' => 'Address',
			'slug' => 'Slug',
			'active' => 'Active',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('partner_id',$this->partner_id);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('intro',$this->intro,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('address',$this->address,true);
		$criteria->compare('slug',$this->slug,true);
		$criteria->compare('active',$this->active);
                $criteria->compare('hot_deal',$this->hot_deal);
                $criteria->compare('new_deal',$this->new_deal);
                $criteria->compare('recomended',$this->recomended);
                $criteria->compare('featured',$this->featured);
                
                        

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Outlet the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        
        protected function beforeSave(){
            $this->slug = Helper::slugify($this->title);
            $this->slug .= $this->id;
            return true;
        }
        
        public function behaviors() {
            return array_merge(
                    parent::behaviors(), array(
                'EasyMultiLanguage' => array(
                    'class' => 'ext.EasyMultiLanguage.EasyMultiLanguageBehavior',
                    'translated_attributes' => array('title', 'intro' ,'description'),
                    'languages' => Yii::app()->params['languages'],
                    'default_language' => Yii::app()->params['default_language'],
                    'admin_routes' => array('admin/outlet/index', 'admin/outlet/update', 'admin/outlet/create'),
                    'translations_table' => 'translations',
                ),
                    )
            );
    }
}
