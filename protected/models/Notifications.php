<?php

/**
 * This is the model class for table "notifications".
 *
 * The followings are the available columns in table 'notifications':
 * @property integer $id
 * @property integer $notification
 * @property integer $notifier_id
 * @property integer $notified_id
 * @property integer $seen
 * @property string $created_time
 */
class Notifications extends CActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Notifications the static model class
     */
    public static function model($className=__CLASS__)
    {
            return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
            return 'notifications';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
            // NOTE: you should only define rules for those attributes that
            // will receive user inputs.
            return array(
                    array('notifier_id, notified_id, seen', 'numerical', 'integerOnly'=>true),
                    array('created_time', 'length', 'max'=>255),
                    array('notification, url', 'safe'),

                    // The following rule is used by search().
                    // Please remove those attributes that should not be searched.
                    array('id, notification, notifier_id, notified_id, seen, created_time', 'safe', 'on'=>'search'),
            );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
            // NOTE: you may need to adjust the relation name and the related
            // class name for the relations automatically generated below.
            return array(
                    'notifier' => array(self::BELONGS_TO, 'User', 'notifier_id'),
                    'notified' => array(self::BELONGS_TO, 'User', 'notified_id'),
            );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'notification' => 'Notification',
            'notifier_id' => 'Notifier',
            'notified_id' => 'Notified',
            'seen' => 'Notified Seen',
            'created_time' => 'Created Time',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id);
        $criteria->compare('notification',$this->notification, true);
        $criteria->compare('notifier_id',$this->notifier_id);
        $criteria->compare('notified_id',$this->notified_id);
        $criteria->compare('seen',$this->seen);
        $criteria->compare('created_time',$this->created_time,true);
        return new CActiveDataProvider($this, array(
            'pagination' => array(
                'pageSize' => 40,
            ),
            'criteria'=>$criteria,
        ));
    }

    public function get_unseen($login_user){
        $criteria = new CDbCriteria;
        $criteria->condition = 'seen = 0 and notified_id = :user';
        $criteria->params = array(':user' => $login_user);
        return Notifications::model()->findAll($criteria);
    }

    public function get_All(){
        $criteria = new CDbCriteria;
        $criteria->order = 'id DESC';
        $criteria->condition = 'notified_id = :user';
        $criteria->params = array(':user' => Yii::app()->user->id);
        return Notifications::model()->findAll($criteria);
    }
}