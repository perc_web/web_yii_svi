<?php

/**
 * This is the model class for table "menu".
 *
 * The followings are the available columns in table 'menu':
 * @property integer $id
 * @property string $title
 * @property integer $outlet_id
 * @property string $description
 * @property string $image
 * @property integer $active
 * @property integer $featured
 * @property string $intro
 * @property string $discount
 * @property integer $hot_deal
 * @property integer $new_deal
 * @property integer $recomended
 * @property integer $brand_id
 * @property string $model
 * @property string $price
 *
 * The followings are the available model relations:
 * @property Outlet $outlet
 * @property MenuItem[] $menuItems
 * @property Reservations[] $reservations
 */
class Menu extends CActiveRecord implements IECartPosition
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'menu';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
                   // array('title,image,intro,price', 'required'),
			array('outlet_id, active, featured, hot_deal, new_deal, recomended, brand_id', 'numerical', 'integerOnly'=>true),
			array('title, model', 'length', 'max'=>255),
			// array('discount, price', 'length', 'max'=>10),
                        
//                        array('price,discount', 'match' ,
//                        'pattern'=> '/^\s*-?[1-9]\d*(\.\d{1,2})?\s*$/u',    
//                        'message'=> '{attribute}: must be number'
//                        ),
			array('description, intro,slug,discount_type,price', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, title, outlet_id, description, active, featured, intro, discount, hot_deal, new_deal, recomended, brand_id, model, price', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'outlet' => array(self::BELONGS_TO, 'Outlet', 'outlet_id'),
			'menuItems' => array(self::HAS_MANY, 'MenuItem', 'menu_id'),
			'reservations' => array(self::HAS_MANY, 'Reservations', 'menu_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'title' => 'Title',
			'outlet_id' => 'Outlet',
			'description' => 'Description',
			'image' => 'Image',
			'active' => 'Active',
			'featured' => 'Featured',
			'intro' => 'Intro',
			'discount' => 'Discount',
			'hot_deal' => 'Hot Deal',
			'new_deal' => 'New Deal',
			'recomended' => 'Recomended',
			'brand_id' => 'Brand',
			'model' => 'Model',
			'price' => 'Price',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('outlet_id',$this->outlet_id);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('image',$this->image,true);
		$criteria->compare('active',$this->active);
		$criteria->compare('featured',$this->featured);
		$criteria->compare('intro',$this->intro,true);
		$criteria->compare('discount',$this->discount,true);
		$criteria->compare('hot_deal',$this->hot_deal);
		$criteria->compare('new_deal',$this->new_deal);
		$criteria->compare('recomended',$this->recomended);
		$criteria->compare('brand_id',$this->brand_id);
		$criteria->compare('model',$this->model,true);
		$criteria->compare('price',$this->price,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Menu the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        
//         protected function beforeSave(){
//            $this->slug = Helper::slugify($this->title);
//            $this->slug .= $this->id;
//            return true;
//        }
        
         function getId(){
            return $this->id;
        }

        function getPrice(){
            return $this->price;
        }
}
