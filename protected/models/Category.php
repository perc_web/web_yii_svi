<?php

/**
 * This is the model class for table "category".
 *
 * The followings are the available columns in table 'category':
 * @property integer $id
 * @property string $title
 * @property string $slug
 * @property integer $parent
 * @property string $image
 *
 * The followings are the available model relations:
 * @property Product[] $products
 */
class Category extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'category';
	}

        
        public function __set($name, $value) 
        {
                if (EMHelper::WinnieThePooh($name, $this->behaviors())){
                        $this->{$name} = $value;
                }else{
                        parent::__set($name, $value);
                }
        }
        
	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('parent', 'numerical', 'integerOnly'=>true),
			array('title, slug, image', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, title, slug, parent, image', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
                        'cat' => array(self::BELONGS_TO, 'Category', 'parent'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'title' => 'Title',
			'slug' => 'Slug',
			'parent' => 'Parent Category',
			'image' => 'Image',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('slug',$this->slug,true);
		$criteria->compare('parent',$this->parent);
		$criteria->compare('image',$this->image,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Category the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        
        
         protected function beforeSave(){
            $this->slug = Helper::slugify($this->title).$this->id;
            return true; 
        }
        
        public function behaviors() {
            return array_merge(
                    parent::behaviors(), array(
                'EasyMultiLanguage' => array(
                    'class' => 'ext.EasyMultiLanguage.EasyMultiLanguageBehavior',
                    'translated_attributes' => array('title'),
                    'languages' => Yii::app()->params['languages'],
                    'default_language' => Yii::app()->params['default_language'],
                    'admin_routes' => array('admin/category/index', 'admin/category/update', 'admin/category/create'),
                    'translations_table' => 'translations',
                ),
                    )
            );
    }
 
        
}
