<form id="search_mini_form" action="<?= Yii::app()->getBaseUrl(true); ?>/home/search" method="post">
    <div class="form-search">
        <div class="search-cat">
            <div id="uniform-cat" class="selector" style="width: 215px;">
                
                <select id="cat" name="cat"  style="border: 1px ;" >
                    <option value="" ><?php echo Yii::t('default', 'All Categories');?></option>
                   
                    <?php
                    $categories = Category::model()->findAll(array('condition' => 'parent is null'));
                    foreach ($categories as $cat) {
                        $selected = '';
                        if (Yii::app()->user->getState('search_cat') && Yii::app()->user->getState('search_cat') == $cat->id && Yii::app()->controller->action->id == 'search') {
                            $selected = 'selected';
                        }
                        ?>
                        <option value="<?= $cat->id ?>" <?= $selected ?>><?= $cat->title; ?></option>
                        <?php
                    }
                    ?>
                </select>
            </div>
        </div>
        <div class="input-search-pro">
            <?php
            $value = '';
            if (Yii::app()->user->getState('search_q') && Yii::app()->controller->action->id == 'search') {
                $value = Yii::app()->user->getState('search_q');
            }
            ?>
            <input name="q" class="input-text" value="<?= $value ?>" placeholder="<?php echo Yii::t('default', 'Enter your keyword...');?>" />
            <button type="submit" title="<?php echo Yii::t('default', 'Search');?>" class="button form-button"><span><span><?php echo Yii::t('default', 'Search');?></span></span></button>
            <div id="search_autocomplete" class="search-autocomplete"></div>
        </div>
    </div>
</form>
