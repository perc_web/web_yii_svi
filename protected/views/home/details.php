         <?php
  $this->widget('application.extensions.fancybox.EFancyBox', array(
    'target'=>'.fancyElm',
    'config'=>array(
        'maxWidth'    => 1000,
        'maxHeight'   => 1000,
        'fitToView'   => false,
        'width'       => '90%',
        'height'      => '90%',
        'autoSize'    => false,
        'closeClick'  => false,
        'openEffect'  => 'none',
        'closeEffect' => 'none',
	'type'        =>'iframe',
    ),
));
  
?> 



<!-- BEGIN: content -->
<div id="yt_content" class="yt-content wrap">
    <div class="yt-content-inner">
        <div class="main-full">
            <div class="container">
                <div class="row">
                    <?= $this->renderPartial('left_side'); ?>

                    <div id="yt_main" class="yt-main-right yt-main col-main col-lg-9 col-md-9">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-9">
                                    <div id="messages_product_view">
                                        <!--<ul class="messages"><li class="notice-msg"><ul><li><span>///</span></li></ul></li></ul></div>-->
                                        <div class="yt-product-detail">
                                            <div class="yt-product-detail-inner">
                                                <div class="row product-essential">
                                                    <div class="box-1">
                                                        <div class="image-box-detail col-lg-5 col-md-5 col-sm-5 col-xs-12">

                                                        <div class="item-zoom">
                                                              <img      id="zoom_03" class="slider-zoom"  src="<?= Yii::app()->request->baseUrl ?>/media/items/<?= $item->image ?>"  data-zoom-image="<?= Yii::app()->request->baseUrl ?>/media/items/<?= $item->image ?>">
                                                           
                                                        <div id="gallery_01" class="gallery col-md-12 col-xs-12">
                                                            <?php
                                                            foreach($photos as $photo){ ?>
                                                                <a href="#" data-image="<?= Yii::app()->request->baseUrl; ?>/gallery/<?=$photo->rank?>medium.jpg" data-zoom-image="<?= Yii::app()->request->baseUrl; ?>/gallery/<?=$photo->rank?>large.jpg" class="col-md-3"> 
                                                                <img id="img_01" src="<?= Yii::app()->request->baseUrl; ?>/gallery/<?=$photo->rank?>small.jpg" /> 
                                                                </a>
                                                            <?php
                                                            }
                                                            ?> 
                                                        </div> 
                                                            </div>

                                                        </div>  

                                                        <script src="<?php echo Yii::app()->baseUrl; ?>/js/jquery.elevatezoom.js"></script>

                                                        <script>
                                                            $("#zoom_03").elevateZoom({gallery: 'gallery_01', zoomType: "inner"});
                                                        </script> 

                                                        <div class="yt-detail-right col-lg-7 col-md-7 col-sm-7 col-xs-12">          
                                                            <form action="<?= Yii::app()->getBaseUrl(true) ?>/home/addCart" method="post" id="product_addtocart_form">              
                                                                <div class="product-shop">
                                                                    <h2 class="product-name">
                                                                        <?= $item->title ?>
                                                                    </h2>

                                                                    <!--<div class="product-review">
                                                                        <div class="ratings">
                                                                            <div class="rating-box">
                                                                                <div class="rating" style="width:70%"></div>
                                                                            </div>
                                                                            <p class="rating-links">
                                                                                <a href="review/product/list/id/480/index.html">2 Review(s)</a>
                                                                                <span class="separator">|</span>
                                                                                <a href="review/product/list/id/480/index.html#review-form">Add Review</a>
                                                                            </p>
                                                                        </div>
                                                                    </div>--> 
                                                                    <div class="price-box">
                                                                        
                                                                       <span class="special-price">
                                                                        <span class="special-price" id="product-price-480"> <?php echo Yii::app()->params['currency'] ; ?>   <?php echo  $item->price ?> </span>
                                                                        </span>
                                                                        
                                                                        <?php
                                                                        /*
                                                                        if ($item->hot_deal == 1 && $item->discount_amount) {
                                                                            if ($item->discount_type == 1) {
                                                                                echo '<span class="special-price">
                                                                        <span class="special-price" id="product-price-480">' . $item->price . '</span>
                                                                        </span>';
                                                                            } else {
                                                                                echo '<span class="special-price">
                                                                        <span class="special-price" id="product-price-480">' . $item->discount_amount . ' off </span>
                                                                        </span>';
                                                                            }
                                                                        } else {
                                                                            echo '<span class="special-price">
                                                                        <span class="special-price" id="product-price-480">' . $item->price . '</span>
                                                                        </span>';
                                                                        }
                                                                        */
                                                                        ?>
                                                                    </div>
                                                                    <p class="availability in-stock">Availability: <span><?php
                                                                            if ($item->availability == 1) {
                                                                                echo Yii::t('default', 'In stock');
                                                                            } else {
                                                                                echo Yii::t('default', 'Out Of Stock');
                                                                            }
                                                                            ?></span></p>
                                                                    <!--<div class="like-social">
                                                                        <div class="addthis_toolbox addthis_default_style ">
                                                                            <a class="addthis_button_facebook_like" fb:like:layout="button_count"></a>
                                                                            <iframe allowtransparency="true" frameborder="0" scrolling="no" src="https://platform.twitter.com/widgets/tweet_button.html" style="width:95px; height:20px; float:left;"></iframe>                                                   
                                                                            <a class="addthis_button_pinterest_pinit" pi:pinit:layout="horizontal"></a>
                                                                            <a class="addthis_counter addthis_pill_style"></a>                      
                                                                        </div>
                                                                    </div>-->
                                                                    <div class="short-description">
                                                                        <h2 class="title-short-des"><?php echo Yii::t('default', 'QUICK OVERVIEW');?></h2>
                                                                        <p><?= $item->intro; ?></p>                         
                                                                    </div>
                                                                    <div class="clearer"></div>

                                                                    
                                                                    <!--<div class="Qty">
                                                                        Qty : 

                                                                        <input style="width: 150px;display: inline-block; margin: 0 5px;" class='form-input form-control day' id='day'  min='1' name="qty" type='number' value='1'>
                                                                    </div>-->
<!--                                                                    <input type="hidden" name="Reservations[item_id]" value="<?=$item->id?>">-->
<!--                                                                    <button class="button pull-right" type="submit">-->
<!--                                                                        <span>
                                                                            <span>Add To Calculator</span>
                                                                        </span>-->
                                                                    </button>
                                                                </div>
                                                            </form>
                                                        </div>

                                                        <script>

                                                            $('.form-input').keyup(function () {
                                                                // GET INPUT VALUE
                                                                var day, month, year;
                                                                day = $('.day').val();
                                                                month = $('.month').val();
                                                                year = $('.year').val();
                                                                // ORGANIZE VALUES
                                                                var date = year + '/' + month + '/' + day;
                                                                // GENERATE AGE
                                                                var birthdate, cur, diff, age;
                                                                birthdate = new Date(date);
                                                                cur = new Date();
                                                                diff = cur - birthdate;
                                                                age = Math.floor(diff / 31536000000);
                                                                // DISPLAY AGE
                                                                $('#age').html(age);
                                                            }).keyup();

                                                        </script>


                                                        <div id="yt_tab_products" class="col-md-12 tab-product-detail">
                                                            <ul class="yt-tab-navi">
                                                                <li class="active">
                                                                    <a data-toggle="tab" href="#decription"><?php echo Yii::t('default', 'Description');?></a>
                                                                </li>
                                                                 <li >
                                                                        <a data-toggle="tab" href="#recommended"><?php echo Yii::t('default', 'Recommended');?></a>
                                                                    </li>

<!--                                                                <li >
                                                                    <a data-toggle="tab" href="#yt_tab_reviewform">Reviews</a>
                                                                </li>-->
                                                                
<!--                                                                <li>
                                                                    <a data-toggle="tab" href="#yt_tab_additional">Additional Information</a>
                                                                </li>-->

                                                                <!--<li >
                                                                    <a data-toggle="tab" href="#yt_tab_custom">Custom tab</a>
                                                                </li>-->
                                                            </ul>
                                                            <div class="yt-tab-content tab-content">                        
                                                                <div id="decription" class="tab-pane fade active in">
                                                                    <!-- h2></h2-->
                                                                    <div class="std">
                                                                        <?php
                                                                        echo $item->description;
                                                                        ?>
                                                                    </div>
                                                                </div>
                                                                
                                                                 <div id="recommended" class="tab-pane fade">
        <!-- h2></h2-->
        <div class="std">
            <?php
            if ($similar_items) {
                foreach ($similar_items as $similar_item) {
                    ?>
            
              <div class="item col-lg-4 col-md-6 col-sm-6 respl-item itemsscroll">
                                                <div class="item-inner">
                                                    <div class="w-image-box">
                                                        <span class="hover-background"></span>
                                                        <div class="item-image">
                                                            <a href="<?= Yii::app()->request->baseUrl ?>/Product-<?php echo $similar_item->slug; ?>" title="<?=$similar_item->title?>"  class="product-image"><img src="<?= Yii::app()->request->baseUrl ?>/media/items/<?php echo $similar_item->image; ?>" alt="<?=$similar_item->title?>"     style="width:258px  !important;height:258px !important;"  ></a>
                                                            <a class="sm_quickview_handler fancyElm" title="Quick View" href="<?= Yii::app()->request->baseUrl ?>/itemfancy-<?= $similar_item->slug ?>"><?php echo Yii::t('default', 'Quick View');?></a>
                                                             
                                                                      
      <?php
      echo ' <span class="disc">price: '.Yii::app()->params['currency'].' '. $similar_item->price .' </span>';
    
?>  

                                                          
                                                           
                                                              <a class="fancyElm"  href="<?= Yii::app()->request->baseUrl ?>/itemfancy-<?php echo $similar_item->slug; ?>">
                                                                  <p >
                                                                <?= substr($similar_item->intro, 0, 50)?>
                                                            </p>
                                                                 
                                                              </a>
                                                            
                                                        </div>
                                                        
                                                        
                                                        
                                                    </div>
                                                    <div class="item-info">
                                                        <div class="item-title">
                                                            <a href="<?= Yii::app()->request->baseUrl ?>/Product-<?= $similar_item->slug ?>" title="<?=$similar_item->title;?>"><?php echo $similar_item->title; ?></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
            


              <?php
                }
            }  else {
                ?>
            <div> <?php echo Yii::t('default', 'No items found.');?></div>
            <?php
              }
            ?>
        </div>
    </div>
                                                                
                                                                <div id="tags" class="tab-pane fade">
                                                                    <div class="collateral-box">
                                                                        <div class="box-collateral box-tags">
                                                                            <h2><?php echo Yii::t('default', 'Product Tags');?></h2>
                                                                            <h3><?php echo Yii::t('default', 'Other people marked this product with these tags:');?></h3>
                                                                            <ul id="product-tags_78a2b29331e953127485d2f908b3a07d" class="product-tags">
                                                                                <?php
                                                                                foreach($item_tags as $tag){ 
                                                                                    $count = ItemTag::model()->count(array('condition' => 'item_id = '.$tag->item_id.' and tag_id = '.$tag->tag_id));
                                                                                    ?>
                                                                                    <li class="first"><?= $tag->tag->title; ?> (<?=$count?>)</li>
                                                                                <?php
                                                                                }
                                                                                ?>
                                                                            </ul>

                                                                            <form id="addTagForm" action="" method="post">
                                                                                <div class="form-add">
                                                                                    <label for="productTagName"><?php echo Yii::t('default', 'Add Your Tags:');?></label>
                                                                                    <div class="input-box">
                                                                                        <input type="hidden" class="input-text required-entry" name="productTagName" id="productTagName">
                                                                                        <input type="hidden" class="input-text required-entry" name="productNewTags" id="productNewTag">
                                                                                        <?php
                                                                                        $this->widget('zii.widgets.jui.CJuiAutoComplete', array(
                                                                                           'name' => 'autocompletesearch',
                                                                                           'source' => Yii::app()->request->baseUrl . '/home/getTags', // <- path to controller which returns dynamic data
                                                                                            'options' => array(
                                                                                               'minLength' => '1', // min chars to start search
                                                                                               'select' => 'js:function(event, ui)
                                                                                                { 
                                                                                                    $("#productTagName").val(ui.item.id);
                                                                                                    $("#productNewTag").val(ui.item.label);
                                                                                                }'
                                                                                            ),
                                                                                            'htmlOptions' => array(
                                                                                               'class' => 'form-control',
                                                                                               'placeholder' => 'Friend,page or event',
                                                                                            ),
                                                                                        ));
                                                                                        ?>
                                                                                    </div>
                                                                                    <button type="submit" title="Add Tag" class="button">
                                                                                        <span>
                                                                                            <span><?php echo Yii::t('default', 'Add Tag');?></span>
                                                                                        </span>
                                                                                    </button>
                                                                                </div>
                                                                            </form>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div id="yt_tab_reviewform" class="tab-pane fade">
                                                                    <div class="box-collateral box-reviews" id="customer-reviews">
                                                                        <h2><?php echo Yii::t('default', 'Customer Reviews');?></h2>
                                                                        <div class="pager">
                                                                            <p class="amount">
                                                                                <strong><?=count($reviews)?> <?php echo Yii::t('default', 'Item(s)');?></strong>
                                                                            </p>
                                                                            <div class="limiter">
                                                                                <label>Show</label>
                                                                                <form method="post" action="" id="pagesform">
                                                                                <select onchange="countReviews()" class="jqtransformdone" name="page">
                                                                                    <option value="5" <?php if($limit == 5){echo Yii::t('default', 'selected');}else{echo "";}?>>
                                                                                        5            
                                                                                    </option>
                                                                                    <option value="20" <?php if($limit == 20){echo Yii::t('default', 'selected');}else{echo "";}?>>
                                                                                        20           
                                                                                    </option>
                                                                                    <option value="50" <?php if($limit == 50){echo Yii::t('default', 'selected');}else{echo "";}?>>
                                                                                        50            
                                                                                    </option>
                                                                                </select> <?php echo Yii::t('default', 'per page');?> 
                                                                                </form>
                                                                            </div>
                                                                        </div>
                                                                        <dl>
                                                                            <?php
                                                                            foreach($reviews as $review){ 
                                                                                $price = ($review->price_rate/5)*100;
                                                                                $value = ($review->value_rate/5)*100;
                                                                                $quality = ($review->quality_rate/5)*100;
                                                                            ?>
                                                                            <dt>
                                                                            <?php echo Yii::t('default', 'first article Review by');?>  <span><?= $review->user->username; ?></span>            
                                                                            </dt>
                                                                            <dd>
                                                                                <table class="ratings-table">
                                                                                    <colgroup>
                                                                                        <col width="1">
                                                                                        <col>
                                                                                    </colgroup>
                                                                                    <tbody>
                                                                                        <tr>
                                                                                            <th><?php echo Yii::t('default', 'Price');?></th>
                                                                                            <td>
                                                                                                <div class="rating-box">
                                                                                                    <div class="rating" style="width:<?=$price?>%;"></div>
                                                                                                </div>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <th><?php echo Yii::t('default', 'Value');?></th>
                                                                                            <td>
                                                                                                <div class="rating-box">
                                                                                                    <div class="rating" style="width:<?=$value?>%;"></div>
                                                                                                </div>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <th><?php echo Yii::t('default', 'Quality');?></th>
                                                                                            <td>
                                                                                                <div class="rating-box">
                                                                                                    <div class="rating" style="width:<?=$quality?>%;"></div>
                                                                                                </div>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </tbody>
                                                                                </table>
                                                                                <?=$review->review;?>
                                                                                <small class="date">(<?php echo Yii::t('default', 'Posted on');?> <?= date('d/m/Y', strtotime($review->date_created)); ?>)</small>
                                                                                <?php
                                                                                if(Yii::app()->user->id && $review->user_id != Yii::app()->user->id){
                                                                                    if(ReviewLikes::model()->find(array('condition' => 'review_id = '.$review->id.' and user_id = '.Yii::app()->user->id))){
                                                                                        echo '<button class="button pull-right" style="margin-top: -21px;" onclick="dislike('.$review->id.')">Dislike</button>';
                                                                                    }else{
                                                                                        echo '<button class="button pull-right" style="margin-top: -21px;" onclick="like('.$review->id.')">Like</button>';
                                                                                    }
                                                                                }
                                                                                ?>
                                                                            </dd>
                                                                            <?php
                                                                            }
                                                                            ?>
                                                                        </dl>
                                                                    </div>
                                                                    <?php
                                                                    if(Yii::app()->user->id && ! Reviews::model()->find(array('condition' => 'user_id = '.Yii::app()->user->id.' and item_id = '.$item->id))){ ?>
                                                                        <div class="form-add">
                                                                            <!--h2></h2-->
                                                                            <form action="" method="post" id="review-form">
                                                                                <input name="form_key" type="hidden" value="0dFh25rijWrMUXuC">
                                                                                <fieldset>  
                                                                                    <div class="customer-review">
                                                                                        <h4><?php echo Yii::t('default', 'How do you rate this product?');?> <!--em class="required">*</em--></h4>
                                                                                        <span id="input-message-box"></span>                
                                                                                        <table class="data-table" id="product-review-table">                        
                                                                                            <thead>
                                                                                                <tr class="first last">
                                                                                                    <th>&nbsp;</th>
                                                                                                    <th><span class="nobr">1 <?php echo Yii::t('default', 'star');?></span></th>
                                                                                                    <th><span class="nobr">2 <?php echo Yii::t('default', 'stars');?></span></th>
                                                                                                    <th><span class="nobr">3 <?php echo Yii::t('default', 'stars');?></span></th>
                                                                                                    <th><span class="nobr">4 <?php echo Yii::t('default', 'stars');?></span></th>
                                                                                                    <th><span class="nobr">5 <?php echo Yii::t('default', 'stars');?></span></th>
                                                                                                </tr>
                                                                                            </thead>
                                                                                            <tbody>
                                                                                                <tr class="first odd">
                                                                                                    <th><?php echo Yii::t('default', 'Quality');?></th>
                                                                                                    <td class="value"><input type="radio" name="Reviews[quality_rate]" id="Quality_1" value="1" class="radio"></td>
                                                                                                    <td class="value"><input type="radio" name="Reviews[quality_rate]" id="Quality_2" value="2" class="radio"></td>
                                                                                                    <td class="value"><input type="radio" name="Reviews[quality_rate]" id="Quality_3" value="3" class="radio"></td>
                                                                                                    <td class="value"><input type="radio" name="Reviews[quality_rate]" id="Quality_4" value="4" class="radio"></td>
                                                                                                    <td class="value last"><input type="radio" name="Reviews[quality_rate]" id="Quality_5" value="5" class="radio"></td>
                                                                                                </tr>
                                                                                                <tr class="even">
                                                                                                    <th><?php echo Yii::t('default', 'Price');?></th>
                                                                                                    <td class="value"><input type="radio" name="Reviews[price_rate]" id="Price_1" value="1" class="radio"></td>
                                                                                                    <td class="value"><input type="radio" name="Reviews[price_rate]" id="Price_2" value="2" class="radio"></td>
                                                                                                    <td class="value"><input type="radio" name="Reviews[price_rate]" id="Price_3" value="3" class="radio"></td>
                                                                                                    <td class="value"><input type="radio" name="Reviews[price_rate]" id="Price_4" value="4" class="radio"></td>
                                                                                                    <td class="value last"><input type="radio" name="Reviews[price_rate]" id="Price_5" value="5" class="radio"></td>
                                                                                                </tr>
                                                                                                <tr class="last odd">
                                                                                                    <th><?php echo Yii::t('default', 'Value');?></th>
                                                                                                    <td class="value"><input type="radio" name="Reviews[value_rate]" id="Value_1" value="1" class="radio"></td>
                                                                                                    <td class="value"><input type="radio" name="Reviews[value_rate]" id="Value_2" value="2" class="radio"></td>
                                                                                                    <td class="value"><input type="radio" name="Reviews[value_rate]" id="Value_3" value="3" class="radio"></td>
                                                                                                    <td class="value"><input type="radio" name="Reviews[value_rate]" id="Value_4" value="4" class="radio"></td>
                                                                                                    <td class="value last"><input type="radio" name="Reviews[value_rate]" id="Value_5" value="5" class="radio"></td>
                                                                                                </tr>
                                                                                            </tbody>
                                                                                        </table>
                                                                                    </div>
                                                                                    <div class="write-your-review">         
                                                                                        <h4><?php echo Yii::t('default', 'Write your own review');?></h4>
                                                                                        <ul class="form-list">
                                                                                            <!--<li>
                                                                                                <label for="nickname_field" class="required">Nickname<em> *</em></label>
                                                                                                <div class="input-box">
                                                                                                    <input type="text" name="nickname" id="nickname_field" class="input-text required-entry" value="">
                                                                                                </div>
                                                                                            </li>-->
                                                                                            <li>
                                                                                                <label for="summary_field" class="required"><?php echo Yii::t('default', 'Summary of Your Review');?><em> *</em></label>
                                                                                                <div class="input-box">
                                                                                                    <input type="text" name="Reviews[title]" id="summary_field" class="input-text required-entry" value="">
                                                                                                </div>
                                                                                            </li>
                                                                                            <li>
                                                                                                <label for="review_field" class="required"><?php echo Yii::t('default', 'Review');?><em> *</em></label>
                                                                                                <div class="input-box">
                                                                                                    <textarea name="Reviews[review]" id="review_field" cols="10" rows="6" class="required-entry"></textarea>
                                                                                                </div>
                                                                                            </li>
                                                                                        </ul>
                                                                                    </div>

                                                                                </fieldset>
                                                                                <div class="buttons-set">
                                                                                    <button type="submit" title="Submit Review" class="button">
                                                                                        <span><span class="submit-review-text"><?php echo Yii::t('default', 'Submit Review');?></span></span>
                                                                                    </button>
                                                                                </div>
                                                                            </form>
                                                                        </div>
                                                                    <?php
                                                                    }
                                                                    ?>
                                                                </div>
                                                                <div id="yt_tab_additional" class="tab-pane fade">
                                                                    <div class="attribute-specs">
                                                                        <table cellspacing="0" class="data-table" id="product-attribute-specs-table">
                                                                            <tbody><tr class="first odd">
                                                                                    <td><?php echo Yii::t('default', 'Model');?></td>
                                                                                    <td class="data last"><?=$item->model?></td>
                                                                                </tr>
                                                                                <tr class="last odd">
                                                                                    <td><?php echo Yii::t('default', 'Brand');?></td>
                                                                                    <td class="data last"><?=$item->brand->title?></td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                </div>
                                                                <!--<div id="yt_tab_custom" class="tab-pane fade">
                                                                    <p>&nbsp;</p>
                                                                    <table class="data-table" style="width: 100%;" border="1">
                                                                        <tbody>
                                                                            <tr>
                                                                                <td>Brand</td>
                                                                                <td><img title="brand" src="<?= Yii::app()->request->baseUrl; ?>/media/wysiwyg/logo-client.png" alt="brand"></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>History</td>
                                                                                <td>Color sit amet, consectetur adipiscing elit. In gravida pellentesque ligula, vel eleifend turpis blandit vel. Nam quis lorem ut mi mattis ullamcorper ac quis dui. Vestibulum et scelerisque ante, eu sodales mi. Nunc tincidunt tempus varius. Integer ante dolor, suscipit non faucibus a, scelerisque vitae sapien.</td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                    <p>&nbsp;</p>
                                                                    <div id="__if72ru4ruh7ergfewui_once" style="display: none;">&nbsp;</div>                                
                                                                </div>-->
                                                            </div>
                                                        </div>  
                                                        <div style="clear:both;"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--added to fix layout hussam-->
            </div>
        </div>
    </div>   
</div>
<div class="bottom-wrapper">
</div>
</div>
<script>
    function countReviews(){
        $('#pagesform').submit();
    }
    
    function like(review){
        $.ajax({
            type:"post",
            url:"<?=Yii::app()->getBaseUrl(true);?>/home/likeReview",
            data: {id : review},
            success: function (data) {
                        location.reload(); 
                    }
        });
    }
    function dislike(review){
        $.ajax({
            type:"post",
            url:"<?=Yii::app()->getBaseUrl(true);?>/home/dislikeReview",
            data: {id : review},
            success: function (data) {
                        location.reload(); 
                    }
        });
    }
</script>
