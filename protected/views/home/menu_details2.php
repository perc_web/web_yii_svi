 <!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="content-type" content="text/html;charset=UTF-8">
        <meta name="format-detection" content="telephone=no">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
        <title><?php echo Yii::app()->name; ?> </title>
        <link rel="stylesheet" type="text/css" href="<?= Yii::app()->request->baseUrl ?>/css/frontend/default/sm_flipshop/bootstrap/css/bootstrap.min.css" media="all">
        <link rel="stylesheet" type="text/css" href="<?= Yii::app()->request->baseUrl ?>/css/frontend/default/sm_flipshop/css/font-awesome.css" media="all">
        <link rel="stylesheet" type="text/css" href="<?= Yii::app()->request->baseUrl ?>/css/frontend/default/sm_flipshop/css/theme.css" media="all">
        <link rel="stylesheet" type="text/css" href="<?= Yii::app()->request->baseUrl ?>/css/frontend/default/sm_flipshop/aw_blog/css/style.css" media="all">
        <link rel="stylesheet" type="text/css" href="<?= Yii::app()->request->baseUrl ?>/css/style.css">
        <link rel="stylesheet" type="text/css" href="<?= Yii::app()->request->baseUrl; ?>/css/main.css" />

        <link rel="stylesheet" type="text/css" href="<?= Yii::app()->request->baseUrl ?>/css/frontend/default/sm_flipshop/sm/basicproducts/css/style.css" media="all">
        <link rel="stylesheet" type="text/css" href="<?= Yii::app()->request->baseUrl ?>/css/frontend/default/sm_flipshop/sm/dynamicslideshow/css/sm-dynamicslideshow.css" media="all">
        <link rel="stylesheet" type="text/css" href="<?= Yii::app()->request->baseUrl ?>/css/frontend/default/sm_flipshop/sm/dynamicslideshow/css/sm-dynamicslideshow-settings.css" media="all">
        <link rel="stylesheet" type="text/css" href="<?= Yii::app()->request->baseUrl ?>/css/frontend/default/sm_flipshop/sm/imageslider/css/imageslider.css" media="all">
        <link rel="stylesheet" type="text/css" href="<?= Yii::app()->request->baseUrl ?>/css/frontend/default/sm_flipshop/sm/quickview/css/quickview.css" media="all">
        <link rel="stylesheet" type="text/css" href="<?= Yii::app()->request->baseUrl ?>/css/frontend/default/sm_flipshop/sm/searchboxpro/css/searchboxpro.css" media="all">
        <link rel="stylesheet" type="text/css" href="<?= Yii::app()->request->baseUrl ?>/css/frontend/default/sm_flipshop/sm/supercategories/css/animate.css" media="all">     
        <link rel="stylesheet" type="text/css" href="<?= Yii::app()->request->baseUrl ?>/css/frontend/default/sm_flipshop/sm/supercategories/css/supercategories.css" media="all">
        <link rel="stylesheet" href="<?= Yii::app()->request->baseUrl ?>/css/frontend/default/sm_flipshop/css/yt-responsive.css" type="text/css">
        <link rel="stylesheet" href="<?= Yii::app()->request->baseUrl ?>/css/frontend/default/sm_flipshop/css/colors/cyan_red.css" type="text/css">

        <script type="text/javascript" src="<?= Yii::app()->request->baseUrl ?>/css/frontend/default/sm_flipshop/js/jquery-1.11.1.min.js"></script>

        <script type="text/javascript" src="<?= Yii::app()->request->baseUrl ?>/css/frontend/default/sm_flipshop/js/jquery-ui.min.js"></script>

        <script type="text/javascript" src="<?= Yii::app()->request->baseUrl ?>/css/frontend/default/sm_flipshop/bootstrap/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="<?= Yii::app()->request->baseUrl ?>/css/frontend/default/sm_flipshop/js/owl.carousel.js"></script> 
    </head>
    <body id="bd" class="sm_flipshop pattern6   cms-index-index cms-home-left"> 
        <div id="yt_wrapper">
            <!-- BEGIN: Header -->
            <div id="yt_header" class="yt-header wrap">

               

              


                <script type="text/javascript">
                    jQuery(document).ready(function($) {
                        $('.theme-color').click(function() {
                            $($(this).parent().find('.active')).removeClass('active');
                            $(this).addClass('active');
                        }
                        );
                    }
                    );

                </script>

                <a id="yt-totop" href="#" title="<?php echo Yii::t('default', 'Go to Top');?>" style="display: none;">
                </a>
                <script type="text/javascript">
                    // slide to top
                    jQuery(document).ready(function($) {

                        $("#yt-totop").hide();
                        $(function() {
                            var wh = $(window).height();
                            var whtml = $(document).height();
                            $(window).scroll(function() {
                                if ($(this).scrollTop() > whtml / 10) {
                                    $('#yt-totop').fadeIn();
                                }
                                else {
                                    $('#yt-totop').fadeOut();
                                }
                            }
                            );
                            $('#yt-totop').click(function() {
                                $('body,html').animate({
                                    scrollTop: 0
                                }
                                , 800);
                                return false;
                            }
                            );
                        }
                        );
                    }
                    );
                </script>
            </div>
            <!-- END: Header -->


            <script>
                function get_outlets(partner) {
                    $.ajax({
                        type: 'POST', //request type
                        url: "<?= CController::createUrl('home/getoutlets') ?>",
                        data: {partner_id: partner},
                        success: function(data) {
                            $("#outlet").html(data);
                           $('#outlet_drop').val(<?= $_REQUEST['outlet']; ?>);
                        }
                    });
                }
                function get_menus(outlet) {
                    $.ajax({
                        type: 'POST', //request type
                        url: "<?= CController::createUrl('home/getmenus') ?>",
                        data: {outlet_id: outlet},
                        success: function(data) {
                            $("#menu").html(data)
                        }
                    });
                }

                function get_items(menu) {
                    $.ajax({
                        type: 'POST', //request type
                        url: "<?= CController::createUrl('home/getitems') ?>",
                        data: {menu_id: menu},
                        success: function(data) {
                            $("#item").html(data)
                        }
                    });
                }
            </script>
            
               
  
<!-- BEGIN: content -->
<div id="yt_content" class="yt-content wrap">
    <div class="yt-content-inner">
        <div class="main-full">
            <div class="container">
                <div class="row">
                    <?php  // echo  $this->renderPartial('left_side'); ?>

                    <div id="yt_main" class="yt-main-right yt-main col-main col-lg-9 col-md-9">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-9">
                                    <div id="messages_product_view">
                                        <!--<ul class="messages"><li class="notice-msg"><ul><li><span>///</span></li></ul></li></ul></div>-->
                                        <div class="yt-product-detail">
                                            <div class="yt-product-detail-inner">
                                                <div class="row product-essential">
                                                    <div class="box-1">
                                                        <div class="image-box-detail col-lg-5 col-md-5 col-sm-5 col-xs-12">

                                                        <div class="item-zoom">
                                                           
                                                            
                                                      
                                                                <img id="zoom_03" class="slider-zoom" src="<?= Yii::app()->request->baseUrl; ?>/media/menus/<?=$menu->image;?>" data-zoom-image="<?= Yii::app()->request->baseUrl; ?>/media/menus/<?=$menu->image;?>"    style="width:258px  !important;height:258px !important;"   /> 
                                                   
                                                        <div id="gallery_01" class="gallery col-md-12 col-xs-12">
                                                            <?php
                                                            foreach($photos as $photo){ ?>
                                                                <a href="#" data-image="<?= Yii::app()->request->baseUrl; ?>/media/items/<?=$photo->item->image?>" data-zoom-image="<?= Yii::app()->request->baseUrl; ?>/media/items/<?=$photo->item->image?>" class="col-md-3"> 
                                                                <img id="img_01" src="<?= Yii::app()->request->baseUrl; ?>/media/items/<?=$photo->item->image?>" /> 
                                                                </a>
                                                            <?php
                                                            }
                                                            
                                                            ?> 
                                                        </div> 
                                                            </div>

                                                        </div>  

                                                        <script src="<?php echo Yii::app()->baseUrl; ?>/js/jquery.elevatezoom.js"></script>

                                                        <script>
                                                            $("#zoom_03").elevateZoom({gallery: 'gallery_01', zoomType: "inner"});
                                                        </script> 

                                                        <div class="yt-detail-right col-lg-7 col-md-7 col-sm-7 col-xs-12">          
                                                            <form action="<?= Yii::app()->getBaseUrl(true) ?>/home/addCart?flag=menu" method="post" id="product_addtocart_form">              
                                                                <div class="product-shop">
                                                                    <h2 class="product-name">
                                                                        <?= $menu->title ?>
                                                                    </h2>

                                                                    <!--<div class="product-review">
                                                                        <div class="ratings">
                                                                            <div class="rating-box">
                                                                                <div class="rating" style="width:70%"></div>
                                                                            </div>
                                                                            <p class="rating-links">
                                                                                <a href="review/product/list/id/480/index.html">2 Review(s)</a>
                                                                                <span class="separator">|</span>
                                                                                <a href="review/product/list/id/480/index.html#review-form">Add Review</a>
                                                                            </p>
                                                                        </div>
                                                                    </div>--> 
                                                                    
                                                                    <p><?php echo Yii::t('default', 'Price:');?></p>
                                                                   <div class="price-box">
                                                                        <span class="special-price">
                                                                        <span class="special-price" id="product-price-480"><?php echo Yii::app()->params['currency'] ; ?>  <?php echo  $menu->price ?> </span>
                                                                        </span>
                                                                
                                                                    </div>
                                                                    
                                                                     <p><?php echo Yii::t('default', 'Discount:');?></p>
                                                                         <div class="price-box">
                                                                        <?php
                                                                      
                                                                            if ($menu->discount_type == 1) {
                                                                                echo '<span class="special-price">
                                                                        <span class="special-price" id="product-price-480">' . $menu->discount . '%</span>
                                                                        </span>';
                                                                            } else {
                                                                                echo '<span class="special-price">
                                                                        <span class="special-price" id="product-price-480"> ' .Yii::app()->params['currency'].' '. $menu->discount . '  </span>
                                                                        </span>';
                                                                            }
                                                                         
                                                                        ?>
                                                                    </div>
                                                                    
                                                                    
<!--                                                                    <div class="price-box">
                                                                        <span class="special-price">
                                                                        <span class="special-price" id="product-price-480"><?php echo  $menu->discount ?> </span>
                                                                        </span>
                                                                
                                                                    </div>-->
                                                                
                                                                
                                                                    <div class="short-description">
                                                                        <h2 class="title-short-des"><?php echo Yii::t('default', 'QUICK OVERVIEW');?></h2>
                                                                        <p><?= $menu->intro; ?></p>                         
                                                                    </div>
                                                                    <div class="clearer"></div>

                                                                    <input type="hidden" name="Reservations[menu_id]" value="<?=$menu->id?>">
                                                                    <button class="button pull-right" type="submit">
                                                                        <span>
                                                                            <span><?php echo Yii::t('default', 'Add To Calculator');?></span>
                                                                        </span>
                                                                    </button>
                                                                </div>
                                                            </form>
                                                        </div>

                                                        <script>

                                                            $('.form-input').keyup(function () {
                                                                // GET INPUT VALUE
                                                                var day, month, year;
                                                                day = $('.day').val();
                                                                month = $('.month').val();
                                                                year = $('.year').val();
                                                                // ORGANIZE VALUES
                                                                var date = year + '/' + month + '/' + day;
                                                                // GENERATE AGE
                                                                var birthdate, cur, diff, age;
                                                                birthdate = new Date(date);
                                                                cur = new Date();
                                                                diff = cur - birthdate;
                                                                age = Math.floor(diff / 31536000000);
                                                                // DISPLAY AGE
                                                                $('#age').html(age);
                                                            }).keyup();

                                                        </script>


                                                        <div id="yt_tab_products" class="col-md-12 tab-product-detail">
                                                            <ul class="yt-tab-navi">
                                                                <li class="active">
                                                                    <a data-toggle="tab" href="#decription"><?php echo Yii::t('default', 'Description');?></a>
                                                                </li>
<!--                                                                <li >
                                                                    <a data-toggle="tab" href="#tags">Tags</a>
                                                                </li>-->

                                                                <li >
                                                                    <a data-toggle="tab" href="#yt_tab_reviewform"><?php echo Yii::t('default', 'Reviews');?></a>
                                                                </li>
                                                               

                                                                <li >
                                                                    <a data-toggle="tab" href="#items"><?php echo Yii::t('default', 'Items');?></a>
                                                                </li>
                                                            </ul>
                                                            <div class="yt-tab-content tab-content">                        
                                                                <div id="decription" class="tab-pane fade active in">
                                                                    <!-- h2></h2-->
                                                                    <div class="std">
                                                                        <?php
                                                                        echo $menu->description;
                                                                        ?>
                                                                    </div>
                                                                </div>
                                                                <div id="tags" class="tab-pane fade">
                                                                    <div class="collateral-box">
                                                                        <div class="box-collateral box-tags">
                                                                            <h2><?php echo Yii::t('default', 'Product Tags');?></h2>
                                                                            <h3><?php echo Yii::t('default', 'Other people marked this product with these tags:');?></h3>
    <ul id="product-tags_78a2b29331e953127485d2f908b3a07d" class="product-tags">
        <?php

        foreach($item_tags as $tag){ 
           echo  $tag->id;
            $count = ItemTag::model()->count(array('condition' => 'item_id = '.$tag->item_id));
             //  $count = ItemTag::model()->count(array('condition' => 'item_id = '.$tag->item_id.' and tag_id = '.$tag->tag_id));
            ?>
            <li class="first"><?= $tag->tag->title; ?> (<?=$count?>)</li>
        <?php
        }

        ?>
    </ul>

<form id="addTagForm" action="" method="post">
    <div class="form-add">
        <label for="productTagName"><?php echo Yii::t('default', 'Add Your Tags:');?></label>
        <div class="input-box">
            <input type="hidden" class="input-text required-entry" name="productTagName" id="productTagName">
            <input type="hidden" class="input-text required-entry" name="productNewTags" id="productNewTag">
            <?php
            $this->widget('zii.widgets.jui.CJuiAutoComplete', array(
               'name' => 'autocompletesearch',
               'source' => Yii::app()->request->baseUrl . '/home/getTags', // <- path to controller which returns dynamic data
                'options' => array(
                   'minLength' => '1', // min chars to start search
                   'select' => 'js:function(event, ui)
                    { 
                        $("#productTagName").val(ui.item.id);
                        $("#productNewTag").val(ui.item.label);
                    }'
                ),
                'htmlOptions' => array(
                   'class' => 'form-control',
                   'placeholder' => 'Friend,page or event',
                ),
            ));
            ?>
        </div>
        <button type="submit" title="<?php echo Yii::t('default', 'Add Tag');?>" class="button">
            <span>
                <span><?php echo Yii::t('default', 'Add Tag');?></span>
            </span>
        </button>
    </div>
</form>
                                                                        </div>
                                                                    </div>
                                                                </div>

<div id="yt_tab_reviewform" class="tab-pane fade">
    <div class="box-collateral box-reviews" id="customer-reviews">
        <h2><?php echo Yii::t('default', 'Customer Reviews');?></h2>
        <div class="pager">
            <p class="amount">
                <strong><?=count($reviews)?> <?php echo Yii::t('default', 'Item(s)');?></strong>
            </p>
            <div class="limiter">
                <label>Show</label>
                 <form method="post" action="" id="pagesform">
                    <select onchange="countReviews()" class="jqtransformdone" name="page">
                        <option value="5" <?php
                        if ($_REQUEST['page'] == 5) {
                            echo "selected";
                        } else {
                            echo "";
                        }
                        ?>>
                            5            
                        </option>
                        <option value="20" <?php
                        if ($_REQUEST['page'] == 20) {
                            echo "selected";
                        } else {
                            echo "";
                        }
                        ?>>
                            20           
                        </option>
                        <option value="50" <?php
                        if ($_REQUEST['page'] == 50) {
                            echo "selected";
                        } else {
                            echo "";
                        }
                        ?>>
                            50            
                        </option>
                    </select> <?php echo Yii::t('default', 'per page');?> 
                </form>
            </div>
        </div>
        <dl>
            <?php
           
            foreach($reviews as $review){ 
                $price = ($review->price_rate/5)*100;
                $value = ($review->value_rate/5)*100;
                $quality = ($review->quality_rate/5)*100;
            ?>
            <dt>
            <?php echo Yii::t('default', 'first article Review by');?> <span><?= $review->user->username; ?></span>            
            </dt>
            <dd>
                <table class="ratings-table">
                    <colgroup>
                        <col width="1">
                        <col>
                    </colgroup>
                    <tbody>
                        <tr>
                            <th><?php echo Yii::t('default', 'Price');?></th>
                            <td>
                                <div class="rating-box">
                                    <div class="rating" style="width:<?=$price?>%;"></div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th><?php echo Yii::t('default', 'Value');?></th>
                            <td>
                                <div class="rating-box">
                                    <div class="rating" style="width:<?=$value?>%;"></div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th><?php echo Yii::t('default', 'Quality');?></th>
                            <td>
                                <div class="rating-box">
                                    <div class="rating" style="width:<?=$quality?>%;"></div>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <?=$review->review;?>
                <small class="date">(<?php echo Yii::t('default', 'Posted on');?> <?= date('d/m/Y', strtotime($review->date_created)); ?>)</small>
             
            </dd>
            <?php
            }
            
            ?>
        </dl>
    </div>
    <?php
    if(Yii::app()->user->id){
  $review_before=  Reviews::model()->find(array('condition' => 'user_id = '.Yii::app()->user->id.' and item_id = '.$menu->id)) ;
    if( ! $review_before){ ?>
        <div class="form-add">
            <!--h2></h2-->
            <form action="" method="post" id="review-form">
                <input name="form_key" type="hidden" value="0dFh25rijWrMUXuC">
                <fieldset>  
                    <div class="customer-review">
                        <h4><?php echo Yii::t('default', 'How do you rate this product?');?> <!--em class="required">*</em--></h4>
                        <span id="input-message-box"></span>                
                        <table class="data-table" id="product-review-table">                        
                            <thead>
                                <tr class="first last">
                                    <th>&nbsp;</th>
                                    <th><span class="nobr">1 <?php echo Yii::t('default', 'star');?></span></th>
                                    <th><span class="nobr">2 <?php echo Yii::t('default', 'stars');?></span></th>
                                    <th><span class="nobr">3 <?php echo Yii::t('default', 'stars');?></span></th>
                                    <th><span class="nobr">4 <?php echo Yii::t('default', 'stars');?></span></th>
                                    <th><span class="nobr">5 <?php echo Yii::t('default', 'stars');?></span></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr class="first odd">
                                    <th><?php echo Yii::t('default', 'Quality');?></th>
                                    <td class="value"><input type="radio" name="Reviews[quality_rate]" id="Quality_1" value="1" class="radio"></td>
                                    <td class="value"><input type="radio" name="Reviews[quality_rate]" id="Quality_2" value="2" class="radio"></td>
                                    <td class="value"><input type="radio" name="Reviews[quality_rate]" id="Quality_3" value="3" class="radio"></td>
                                    <td class="value"><input type="radio" name="Reviews[quality_rate]" id="Quality_4" value="4" class="radio"></td>
                                    <td class="value last"><input type="radio" name="Reviews[quality_rate]" id="Quality_5" value="5" class="radio"></td>
                                </tr>
                                <tr class="even">
                                    <th><?php echo Yii::t('default', 'Price');?></th>
                                    <td class="value"><input type="radio" name="Reviews[price_rate]" id="Price_1" value="1" class="radio"></td>
                                    <td class="value"><input type="radio" name="Reviews[price_rate]" id="Price_2" value="2" class="radio"></td>
                                    <td class="value"><input type="radio" name="Reviews[price_rate]" id="Price_3" value="3" class="radio"></td>
                                    <td class="value"><input type="radio" name="Reviews[price_rate]" id="Price_4" value="4" class="radio"></td>
                                    <td class="value last"><input type="radio" name="Reviews[price_rate]" id="Price_5" value="5" class="radio"></td>
                                </tr>
                                <tr class="last odd">
                                    <th><?php echo Yii::t('default', 'Value');?></th>
                                    <td class="value"><input type="radio" name="Reviews[value_rate]" id="Value_1" value="1" class="radio"></td>
                                    <td class="value"><input type="radio" name="Reviews[value_rate]" id="Value_2" value="2" class="radio"></td>
                                    <td class="value"><input type="radio" name="Reviews[value_rate]" id="Value_3" value="3" class="radio"></td>
                                    <td class="value"><input type="radio" name="Reviews[value_rate]" id="Value_4" value="4" class="radio"></td>
                                    <td class="value last"><input type="radio" name="Reviews[value_rate]" id="Value_5" value="5" class="radio"></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="write-your-review">         
                        <h4><?php echo Yii::t('default', 'Write your own review');?></h4>
                        <ul class="form-list">
                            <!--<li>
                                <label for="nickname_field" class="required">Nickname<em> *</em></label>
                                <div class="input-box">
                                    <input type="text" name="nickname" id="nickname_field" class="input-text required-entry" value="">
                                </div>
                            </li>-->
                            <li>
                                <label for="summary_field" class="required"><?php echo Yii::t('default', 'Summary of Your Review');?><em> *</em></label>
                                <div class="input-box">
                                    <input type="text" name="Reviews[title]" id="summary_field" class="input-text required-entry" value="">
                                </div>
                            </li>
                            <li>
                                <label for="review_field" class="required"><?php echo Yii::t('default', 'Review');?><em> *</em></label>
                                <div class="input-box">
                                    <textarea name="Reviews[review]" id="review_field" cols="10" rows="6" class="required-entry"></textarea>
                                </div>
                            </li>
                        </ul>
                    </div>

                </fieldset>
                <div class="buttons-set">
                    <button type="submit" title="Submit Review" class="button">
                        <span><span class="submit-review-text"><?php echo Yii::t('default', 'Submit Review');?></span></span>
                    </button>
                </div>
            </form>
        </div>
    <?php
    }
    }
    ?>
</div>
                   
                                                                
    <div id="items" class="tab-pane fade">
   <!-- h2></h2-->
   <div class="std">
          <?php
if ($photos) {
foreach ($photos as $photo) {
   $item= $photo->item ;
      ?>
       
        <div class="ltabs-item item respl-item col-lg-4 col-md-4 col-sm-6" style="-webkit-animation: slideLeft 600ms 300ms; opacity: 1;">
            
                                                        <div class="item-inner">
                                                            <div class="w-image-box">
                                                                <span class="hover-background">
                                                                </span>
                                                                <div class="item-image">
                                                                   
                                                                        <img style="width:200px;height:200px !important;"  alt="<?= $menu->title ?>"   alt="<?= $item->title ?>" title="<?= $item->title ?>" src="<?= Yii::app()->request->baseUrl ?>/media/items/<?= $item->image ?>">
                                                                        
                                                                                                                                                                          </div>
                                                                <div class="label-wrapper">
                                                                    <?php
                                                                    /*
                                                                    if ($item->new_deal == 1) {
                                                                        echo '<div class="new-product">
                                                        New
                                                  </div>';
                                                                    }if ($item->discount_amount) {
                                                                        echo '<div class="sale-product">
                                                        Sale                
                                                  </div>';
                                                                    }
                                                                    */
                                                                    ?>
                                                                </div>
                                                            </div>

                                                            <div class="item-info">
                                                                <div class="item-title">
                                                                   
                                                                        <?= $item->title ?>        
                                                                   
                                                                </div>
                                                                <div class="item-price">
                                                                    <div class="sale-price">

                                                                        <div class="price-box">

                                                                            <span class="special-price">
<!--                                                                                <span class="special-price" id="product-price-443">$ <?php  echo $item->price ; ?></span>-->
                                                                                <?php
                                                                                /*
                                                                                if ($item->discount_amount) {
                                                                                    if ($item->discount_type == 1) {
                                                                                        echo '<span class="special-price" id="product-price-443">' . $item->discount_amount . '%</span>';
                                                                                    } else {
                                                                                        echo '<span class="special-price" id="product-price-443">' . $item->discount_amount . '</span>';
                                                                                    }
                                                                                }
                                                                                */
                                                                                ?>
                                                                            </span>
                                                                        </div>
                                                                    </div>
                                                                </div>
<!--                                                                <div class="item-review">
                                                                    <p class="no-rating">
                                                                       
                                                                            Be the first to review this product
                                                                        
                                                                    </p>
                                                                </div>-->
                                                            </div>
                                                        </div>
                                                    </div>
       
       
       <?php
     }
} else {
                ?>
            <div> <?php echo Yii::t('default', 'No items found.');?></div>
            <?php
              }
            ?>

   </div>
</div>                                                            
                                                               
                                                               
                                                            </div>
                                                        </div>  
                                                        <div style="clear:both;"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--added to fix layout hussam-->
            </div>
        </div>
    </div>   
</div>
<div class="bottom-wrapper">
</div>
</div>
<script>
    function countReviews(){
        $('#pagesform').submit();
    }
    
    function like(review){
        $.ajax({
            type:"post",
            url:"<?=Yii::app()->getBaseUrl(true);?>/home/likeReview",
            data: {id : review},
            success: function (data) {
                        location.reload(); 
                    }
        });
    }
    function dislike(review){
        $.ajax({
            type:"post",
            url:"<?=Yii::app()->getBaseUrl(true);?>/home/dislikeReview",
            data: {id : review},
            success: function (data) {
                        location.reload(); 
                    }
        });
    }
</script>
