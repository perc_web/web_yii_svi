<!-- BEGIN: content -->
<div id="yt_content" class="yt-content wrap">
    <div class="yt-content-inner">
        <div class="main-full">
            <div class="container">
                <div class="row">
                    <?= $this->renderPartial('left_side') ?>
                    <div id="yt_main" class="yt-main-right yt-main col-main col-lg-9 col-md-9">
                        <div class="yt_main_inner"> 
                            <!-- Design Code -->
                            <?php
                            foreach($jobs as $job){ ?>
                                <div class="col-sm-6 col-xs-12 job-content">
                                    <article>
                                        <div class="job-header">
                                            <h3><?=$job->title;?></h3>
                                        </div>
                                        <div class="desc">
                                            <?= $job->description; ?>
                                        </div>
                                        <!--<div class="job-more">
                                            <a href="apply-job.html" class="btn btn-primary pull-right">Details</a>
                                        </div>-->
                                    </article>
                                </div>
                            <?php
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
            <!--added to fix layout hussam-->
        </div>
    </div>
</div>