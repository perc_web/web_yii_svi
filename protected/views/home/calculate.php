<?php if (isset($_REQUEST['category']) && isset($_REQUEST['partner'])) { ?>
                <script>
                    $(document).ready(function() {
                         change_category();
                    });
                    function change_category() {
                         $('#cat_drop').val(<?= $_REQUEST['category']; ?>);
                        change_partners(<?= $_REQUEST['category']; ?>);
                      
                    }
                </script>
                <?php
            }
            ?>
                <script>
                     function change_partners(category) {
                        $.ajax({
                            url: "<?= Yii::app()->getBaseUrl(true); ?>/home/getpartners",
                            data: {cat_id: category},
                            success: function(data) {
                                      $("#partner").html(data);
                                  $('#partner_drop').val(<?= $_REQUEST['partner']; ?>); // after that fill partner by  partner request
                                  var partner ="<?= $_REQUEST['partner']; ?>";
                                  if(partner){
                                       get_outlets(<?= $_REQUEST['partner']; ?>); // then load all outlets by this partner id
                                  }
                                  
                                   var outlet = "<?= $_REQUEST['outlet']; ?>";
                                   
                                  if(outlet){
                                         get_menus(<?= $_REQUEST['outlet']; ?>);  // then load all menus by this outlet request
                                  }
                                  
                                  
                            }
                        });
                    }
                </script>
<!-- BEGIN: content -->
<div id="yt_content" class="yt-content wrap">
    <div class="yt-content-inner">
        <div class="main-full">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <h1 class="cart-address">Calculator</h1>
                        <?php
                                    $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
                                        'action' => Yii::app()->getBaseUrl(true).'/home/addCart',
                                        'id' => 'calculateForm',
                                        'enableAjaxValidation' => false,
                                        'type' => 'horizontal',
                                        'htmlOptions' => array('class' => 'reserve-form col-lg-12 col-md-12  col-sm-12 col-xs-12', 'enctype' => 'multipart/form-data'),
                                    ));
                                    ?>
                                    <div class="row">
                                        <div class="col-md-12 top-buffer">
                                            <div class="form-group col-md-6">
                                                <label class="col-md-4">Category</label>
                                                <div class="col-md-8">
                                                    <?php
                                                    echo $form->dropDownList($model, 'category_id',
                                                    Chtml::listData(Category::model()->findAll(), 'id', 'title'),
                                                    array('prompt' => Yii::t('default', 'Choose') ,
                                                            'class' => 'form-control col-md-8 jqtransformdone',
                                                            'id' => 'cat_drop',
                                                        'onchange' => 'change_partners(this.value)',
                                                            
                                                            )
                                                    );
                                                    
                                                    ?>              
                                                </div>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label class="col-md-4">Partner</label>
                                                <div class="col-md-8" id="partner">
                                                    <select readonly class="form-control col-md-8 jqtransformdone">
                                                    <option><?php echo Yii::t('default', 'Select Category first');?></option>
                                                    </select>
                                                    <?php
                                                    //echo $form->dropdownList($model, 'partner_id', Chtml::listData(Partner::model()->findAll(), 'id', 'title'), array('class' => 'form-control col-md-8 jqtransformdone', 'prompt' => 'Choose'));
                                                    ?>              
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12 top-buffer">
                                            <div class="form-group col-md-6">
                                                <label class="col-md-4"><?php echo Yii::t('default', 'Outlet');?></label>
                                                <div class="col-md-8" id="outlet">
                                                    <select readonly class="form-control col-md-8 jqtransformdone">
                                                    <option><?php echo Yii::t('default', 'Select Partner first');?></option>
                                                    </select>
                                                    <?php
                                                    //echo $form->dropdownList($model, 'outlet_id', Chtml::listData(Outlet::model()->findAll(), 'id', 'title'), array('class' => 'form-control col-md-8 jqtransformdone', 'prompt' => 'Choose'));
                                                    ?>              
                                                </div>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label class="col-md-4"><?php echo Yii::t('default', 'Menu');?></label>
                                                <div class="col-md-8" id="menu">
                                                    <select readonly class="form-control col-md-8 jqtransformdone">
                                                    <option><?php echo Yii::t('default', 'Select Outlet first');?></option>
                                                    </select>     
                                                </div>
                                            </div>
                                            
<!--                                            <div class="form-group col-md-6">
                                                <label class="col-md-4">Item</label>
                                                <div class="col-md-8" id="item">
                                                    <select readonly class="form-control col-md-8 jqtransformdone">
                                                    <option>Select Menu first</option>
                                                    </select>                                                                  
                                                </div>
                                            </div>-->
                                            
                                            
                                        </div>
                                        <!--
                                            <div class="col-md-11"> 
                                                <div class="buttons-set col-md-3 clear_element pull-right ">                                                    
                                                    <button type="submit" title="Calculate" class="button col-md-12 grapefruit "><span><span style="grapefruit">Calculate</span></span></button>
                                                </div>
                                            </div>
                                        -->

                                        </div>
                                    <?php $this->endWidget(); ?>
                        <div class="table-responsive">
                            <table class="order-table">
                                <?php
                                if($cart){
                                ?>
                                <thead>
                                    <tr>
                                        <td><?php echo Yii::t('default', 'Menu Name');?></td>
                                        <td><?php echo Yii::t('default', 'Outlet Name');?></td>
                                        <td><?php echo Yii::t('default', 'Quantity');?></td>
                                        <td><?php echo Yii::t('default', 'Original Price');?></td>
                                        <td><?php echo Yii::t('default', 'Discount amount');?></td>
                                        <td><?php echo Yii::t('default', 'Final Price');?></td>
                                        <td><?php echo Yii::t('default', 'Total Price');?></td>
                                        <td><?php echo Yii::t('default', 'Saved Price');?></td>
                                        <td><?php echo Yii::t('default', 'Remove from Cart');?></td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $saved = 0;
                                    $total = 0;
                                    foreach($cart as $item){
                                        $final_price = $item->price;
                                        if($item->discount_type == 1){
                                            $final_price = $item->price - ($item->price*($item->discount/100));                                             
                                        }else{ 
                                            $final_price = $item->price-$item->discount;
                                        }
                                        $saved += ($item->price*$item->getQuantity()) - ($final_price*$item->getQuantity());
                                        $total += $final_price*$item->getQuantity();
                                        ?>
                                        <tr>
                                        <td><?=$item->title;?></td>
                                        <td><?=$item->outlet->title;?></td>
                                        <td><?=$item->getQuantity();?></td>
                                        <td><?=$item->price;?></td>
                                        <td><?php if($item->discount_type == 1){ echo $item->discount.'%'; }else{ echo $item->discount; }?></td>
                                        <td><?php echo $final_price; ?></td>
                                        <td><?= $final_price*$item->getQuantity(); ?></td>
                                        <td><?= ($item->price*$item->getQuantity()) - ($final_price*$item->getQuantity()); ?></td>
                                        <td>
                                            <a href="<?=Yii::app()->request->baseUrl?>/home/addCart?product=<?=$item->id?>&action=remove"><img src="<?=Yii::app()->request->baseUrl?>/img/esc.png" alt="" /></a>
                                        </td>
                                        </tr>
                                    <?php
                                    }
                                    ?>
                                        
                                        <tr style="border-top: 1px solid #ccc">
                                            <td style="width:142px;"><?php echo Yii::t('default', 'Total Saved :');?></td>
                                            <td><?=$saved?></td>
                                            <td></td>
                                            <td><?php echo Yii::t('default', 'Total Cost :');?></td>
                                            <td><?=$total?></td>
                                        </tr>
                                <?php
                                }else {
                                    echo '<tbody><tr><td>' .Yii::t('default', 'No items found'). ' </td></tr>';
                                }
                                ?>
                                </tbody>
                                
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!--added to fix layout hussam-->
        </div>
    </div>
</div>
<script>

    $('.form-input').keyup(function () {
        // GET INPUT VALUE
        var day, month, year;
        day = $('.day').val();
        month = $('.month').val();
        year = $('.year').val();
        // ORGANIZE VALUES
        var date = year + '/' + month + '/' + day;
        // GENERATE AGE
        var birthdate, cur, diff, age;
        birthdate = new Date(date);
        cur = new Date();
        diff = cur - birthdate;
        age = Math.floor(diff / 31536000000);
        // DISPLAY AGE
        $('#age').html(age);
    }).keyup();
    
    function submit_form(){
        $('#calculateForm').submit();
    }

</script>