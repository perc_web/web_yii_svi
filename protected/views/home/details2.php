      <!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="content-type" content="text/html;charset=UTF-8">
        <meta name="format-detection" content="telephone=no">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
        <title><?php echo Yii::app()->name; ?> </title>
        <link rel="stylesheet" type="text/css" href="<?= Yii::app()->request->baseUrl ?>/css/frontend/default/sm_flipshop/bootstrap/css/bootstrap.min.css" media="all">
        <link rel="stylesheet" type="text/css" href="<?= Yii::app()->request->baseUrl ?>/css/frontend/default/sm_flipshop/css/font-awesome.css" media="all">
        <link rel="stylesheet" type="text/css" href="<?= Yii::app()->request->baseUrl ?>/css/frontend/default/sm_flipshop/css/theme.css" media="all">
        <link rel="stylesheet" type="text/css" href="<?= Yii::app()->request->baseUrl ?>/css/frontend/default/sm_flipshop/aw_blog/css/style.css" media="all">
        <link rel="stylesheet" type="text/css" href="<?= Yii::app()->request->baseUrl ?>/css/style.css">
        <link rel="stylesheet" type="text/css" href="<?= Yii::app()->request->baseUrl; ?>/css/main.css" />

        <link rel="stylesheet" type="text/css" href="<?= Yii::app()->request->baseUrl ?>/css/frontend/default/sm_flipshop/sm/basicproducts/css/style.css" media="all">
        <link rel="stylesheet" type="text/css" href="<?= Yii::app()->request->baseUrl ?>/css/frontend/default/sm_flipshop/sm/dynamicslideshow/css/sm-dynamicslideshow.css" media="all">
        <link rel="stylesheet" type="text/css" href="<?= Yii::app()->request->baseUrl ?>/css/frontend/default/sm_flipshop/sm/dynamicslideshow/css/sm-dynamicslideshow-settings.css" media="all">
        <link rel="stylesheet" type="text/css" href="<?= Yii::app()->request->baseUrl ?>/css/frontend/default/sm_flipshop/sm/imageslider/css/imageslider.css" media="all">
        <link rel="stylesheet" type="text/css" href="<?= Yii::app()->request->baseUrl ?>/css/frontend/default/sm_flipshop/sm/quickview/css/quickview.css" media="all">
        <link rel="stylesheet" type="text/css" href="<?= Yii::app()->request->baseUrl ?>/css/frontend/default/sm_flipshop/sm/searchboxpro/css/searchboxpro.css" media="all">
        <link rel="stylesheet" type="text/css" href="<?= Yii::app()->request->baseUrl ?>/css/frontend/default/sm_flipshop/sm/supercategories/css/animate.css" media="all">     
        <link rel="stylesheet" type="text/css" href="<?= Yii::app()->request->baseUrl ?>/css/frontend/default/sm_flipshop/sm/supercategories/css/supercategories.css" media="all">
        <link rel="stylesheet" href="<?= Yii::app()->request->baseUrl ?>/css/frontend/default/sm_flipshop/css/yt-responsive.css" type="text/css">
        <link rel="stylesheet" href="<?= Yii::app()->request->baseUrl ?>/css/frontend/default/sm_flipshop/css/colors/cyan_red.css" type="text/css">

        <script type="text/javascript" src="<?= Yii::app()->request->baseUrl ?>/css/frontend/default/sm_flipshop/js/jquery-1.11.1.min.js"></script>

        <script type="text/javascript" src="<?= Yii::app()->request->baseUrl ?>/css/frontend/default/sm_flipshop/js/jquery-ui.min.js"></script>

        <script type="text/javascript" src="<?= Yii::app()->request->baseUrl ?>/css/frontend/default/sm_flipshop/bootstrap/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="<?= Yii::app()->request->baseUrl ?>/css/frontend/default/sm_flipshop/js/owl.carousel.js"></script> 
    </head>
    <body id="bd" class="sm_flipshop pattern6   cms-index-index cms-home-left"> 
        <div id="yt_wrapper">
            <!-- BEGIN: Header -->
            <div id="yt_header" class="yt-header wrap">

               

              


                <script type="text/javascript">
                    jQuery(document).ready(function($) {
                        $('.theme-color').click(function() {
                            $($(this).parent().find('.active')).removeClass('active');
                            $(this).addClass('active');
                        }
                        );
                    }
                    );

                </script>

                <a id="yt-totop" href="#" title="Go to Top" style="display: none;">
                </a>
                <script type="text/javascript">
                    // slide to top
                    jQuery(document).ready(function($) {

                        $("#yt-totop").hide();
                        $(function() {
                            var wh = $(window).height();
                            var whtml = $(document).height();
                            $(window).scroll(function() {
                                if ($(this).scrollTop() > whtml / 10) {
                                    $('#yt-totop').fadeIn();
                                }
                                else {
                                    $('#yt-totop').fadeOut();
                                }
                            }
                            );
                            $('#yt-totop').click(function() {
                                $('body,html').animate({
                                    scrollTop: 0
                                }
                                , 800);
                                return false;
                            }
                            );
                        }
                        );
                    }
                    );
                </script>
            </div>
            <!-- END: Header -->


            <script>
                function get_outlets(partner) {
                    $.ajax({
                        type: 'POST', //request type
                        url: "<?= CController::createUrl('home/getoutlets') ?>",
                        data: {partner_id: partner},
                        success: function(data) {
                            $("#outlet").html(data);
                           $('#outlet_drop').val(<?= $_REQUEST['outlet']; ?>);
                        }
                    });
                }
                function get_menus(outlet) {
                    $.ajax({
                        type: 'POST', //request type
                        url: "<?= CController::createUrl('home/getmenus') ?>",
                        data: {outlet_id: outlet},
                        success: function(data) {
                            $("#menu").html(data)
                        }
                    });
                }

                function get_items(menu) {
                    $.ajax({
                        type: 'POST', //request type
                        url: "<?= CController::createUrl('home/getitems') ?>",
                        data: {menu_id: menu},
                        success: function(data) {
                            $("#item").html(data)
                        }
                    });
                }
            </script>
            
         

<!-- BEGIN: content -->
    <div id="yt_content" class="yt-content wrap">
        <div class="yt-content-inner">
            <div class="main-full">
                <div class="container">
                    <div class="row">
                        <?php  // $this->renderPartial('left_side'); ?>
                        <div id="" class="yt-main-right yt-main col-main col-lg-9 col-md-9">
                            <div class="container">
                                <div class="row">
                                    <div class="col-md-9">
                                        <div id="messages_product_view">
                                            <!--<ul class="messages"><li class="notice-msg"><ul><li><span>///</span></li></ul></li></ul></div>-->
                                            <div class="yt-product-detail">
                                                <div class="yt-product-detail-inner">
                                                    <div class="row product-essential">
                                                        <div class="">
<!--                                                            <div class="image-box-detail col-lg-5 col-md-5 col-sm-5 col-xs-12">
                                                                <img src="<?= Yii::app()->request->baseUrl ?>/media/partner/<?= $item->image ?>">
                                                            </div>  -->
                                                            
                                                            
                                                                                 <div class="image-box-detail col-lg-5 col-md-5 col-sm-5 col-xs-12">

                                                        <div class="item-zoom">
                                                            
                                                              <img  id="zoom_03" class="slider-zoom"  src="<?= Yii::app()->request->baseUrl ?>/media/items/<?= $item->image ?>"    style="width:258px  !important;height:258px !important;"   data-zoom-image="<?= Yii::app()->request->baseUrl ?>/media/items/<?= $item->image ?>">
                                                        
                                                        <div id="gallery_01" class="gallery col-md-12 col-xs-12">
                                                            <?php
                                                            foreach($photos as $photo){ ?>
                                                                <a href="#" data-image="<?= Yii::app()->request->baseUrl; ?>/gallery/<?=$photo->rank?>medium.jpg" data-zoom-image="<?= Yii::app()->request->baseUrl; ?>/gallery/<?=$photo->rank?>large.jpg" class="col-md-3"> 
                                                                <img id="img_01" src="<?= Yii::app()->request->baseUrl; ?>/gallery/<?=$photo->rank?>small.jpg" /> 
                                                                </a>
                                                            <?php
                                                            }
                                                            ?> 
                                                        </div> 
                                                            </div>

                                                        </div>  

                                                        <script src="<?php echo Yii::app()->baseUrl; ?>/js/jquery.elevatezoom.js"></script>

                                                        <script>
                                                            $("#zoom_03").elevateZoom({gallery: 'gallery_01', zoomType: "inner"});
                                                        </script>
                                                            
                                                            <div class="yt-detail-right col-lg-7 col-md-7 col-sm-7 col-xs-12">          
                                                                <div class="product-shop">
                                                                    <h2 class="product-name">
                                                                        <?= $item->title ?>
                                                                    </h2>
                                                                    <div class="clearer"></div>
                                                                   <?php
                                                                   if($item->price > 0){
                                                                       ?>
                                                                     <b>  price: </b>  <?php echo Yii::app()->params['currency'] ; ?>   <?= $item->price ?>  
                                                                    
                                                                    <?php
                                                                   }
                                                                   ?>
                                                                   
                                                                    <div class="clearer"></div>
                                                                     <?= $item->intro ?> 
                                                                </div>
                                                            </div>

                                                            <script>

                                                                $('.form-input').keyup(function() {
                                                                    // GET INPUT VALUE
                                                                    var day, month, year;
                                                                    day = $('.day').val();
                                                                    month = $('.month').val();
                                                                    year = $('.year').val();
                                                                    // ORGANIZE VALUES
                                                                    var date = year + '/' + month + '/' + day;
                                                                    // GENERATE AGE
                                                                    var birthdate, cur, diff, age;
                                                                    birthdate = new Date(date);
                                                                    cur = new Date();
                                                                    diff = cur - birthdate;
                                                                    age = Math.floor(diff / 31536000000);
                                                                    // DISPLAY AGE
                                                                    $('#age').html(age);
                                                                }).keyup();

                                                            </script>


                                                            <div id="yt_tab_products" class="col-md-12 tab-product-detail">
                                                                <ul class="yt-tab-navi">
                                                                    <li class="active">
                                                                        <a data-toggle="tab" href="#decription">Description</a>
                                                                    </li>
                                                                    
                                                                      <li >
                                                                        <a data-toggle="tab" href="#recommended">Recommended</a>
                                                                    </li>
                                                                 
                                                                    
<!--                                                                      <li>
                                                                        <a data-toggle="tab" href="#items">Items</a>
                                                                    </li>-->

                                                                </ul>
                                                                <div class="yt-tab-content tab-content">                        
                                                                    <div id="decription" class="tab-pane fade active in">
                                                                        <!-- h2></h2-->
                                                                        <div class="std">
                                                                            <?php
                                                                            echo $item->description;
                                                                            ?>
                                                                        </div>
                                                                    </div>


   
<div id="recommended" class="tab-pane fade">
        <!-- h2></h2-->
        <div class="std">
            <?php
            if ($similar_items) {
                foreach ($similar_items as $similar_item) {
                    ?>
              <div class="ltabs-item item respl-item col-lg-4 col-md-4 col-sm-6" style="-webkit-animation: slideLeft 600ms 300ms; opacity: 1;">
                                                        <div class="item-inner">
                                                            <div class="w-image-box">
                                                                <span class="hover-background">
                                                                </span>
                                                                <div class="item-image">
                                                                   
                                                                        <img style="width:200px;height:200px !important;"   alt="<?= $similar_item->title ?>" title="<?= $similar_item->title ?>" src="<?= Yii::app()->request->baseUrl ?>/media/items/<?= $similar_item->image ?>">
                                                                  </div>
                                                                <div class="label-wrapper">
                                                                    <?php
                                                                    /*
                                                                    if ($item->new_deal == 1) {
                                                                        echo '<div class="new-product">
                                                        New
                                                  </div>';
                                                                    }if ($item->discount_amount) {
                                                                        echo '<div class="sale-product">
                                                        Sale                
                                                  </div>';
                                                                    }
                                                                    */
                                                                    ?>
                                                                </div>
                                                            </div>

                                                            <div class="item-info">
                                                                <div class="item-title">
                                                                    
                                                                        <?= $similar_item->title ?>        
                                                                   
                                                                </div>
                                                                <div class="item-price">
                                                                    <div class="sale-price">

                                                                        <div class="price-box">

                                                                            <span class="special-price">
<!--                                                                                <span class="special-price" id="product-price-443">$ <?php  echo $similar_item->price ; ?></span>-->
                                                                                <?php
                                                                                /*
                                                                                if ($item->discount_amount) {
                                                                                    if ($item->discount_type == 1) {
                                                                                        echo '<span class="special-price" id="product-price-443">' . $item->discount_amount . '%</span>';
                                                                                    } else {
                                                                                        echo '<span class="special-price" id="product-price-443">' . $item->discount_amount . '</span>';
                                                                                    }
                                                                                }
                                                                                */
                                                                                ?>
                                                                            </span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="item-review">
                                                                    <p class="no-rating">
                                                                       
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

              <?php
                }
            }  else {
                ?>
            <div> No items found.</div>
            <?php
              }
            ?>
        </div>
    </div>

                                                                   
                                                                    


                                                                </div>
                                                            </div>  
                                                            <div style="clear:both;"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--added to fix layout hussam-->
                </div>
            </div>
        </div>   
    </div>
    <div class="bottom-wrapper">
    </div>
    </div>
    <script>
        function countReviews() {
            $('#pagesform').submit();
        }

        function like(review) {
            $.ajax({
                type: "post",
                url: "<?= Yii::app()->getBaseUrl(true); ?>/home/likeReview",
                data: {id: review},
                success: function(data) {
                   // location.reload();
                }
            });
        }
        function dislike(review) {
            $.ajax({
                type: "post",
                url: "<?= Yii::app()->getBaseUrl(true); ?>/home/dislikeReview",
                data: {id: review},
                success: function(data) {
                   // location.reload();
                }
            });
        }
    </script>

    
    
    
   
</div>

</body>
</html>