
<div class="clearfix"></div>
<div class="container">
    <div class="row">
        <?= $this->renderPartial('user_menu') ?>
        <!--End col-3--> 
        <!--start col-9-->
        <div class="col-lg-9 col-md-9 ">
            <h2 class="font-h"><?php echo Yii::t('default', 'Users Special Requests');?></h2>
            <?php
            if(Yii::app()->user->hasFlash('request')){ ?>
            <br />
            <div class="alert alert-success"><?=Yii::app()->user->getFlash('request')?></div>
            <?php
            }
            ?>
            <form class="form-horizontal" method="post">
                <div class="form-group text-align">
                    <label class="control-label col-lg-2 col-sm-5 col-xs-12" for="firstname"><?php echo Yii::t('default', 'Place Name');?> </label>
                    <div class="col-lg-4 col-md-6 col-sm-7 col-xs-12 profile-data">
                        <input type="text" required="" placeholder="" name="UserRequests[title]" class="form-control">
                    </div>
                </div>
                
                <div class="form-group text-align">
                    <label class="control-label col-lg-2 col-sm-5 col-xs-12" for="firstname"><?php echo Yii::t('default', 'Place Address');?> </label>
                    <div class="col-md-6 col-sm-7 col-xs-12 profile-data">
                        <textarea required="" placeholder="" class="form-control" name="UserRequests[address]" rows="5"></textarea>
                    </div>
                </div>
            
                <div class="form-group text-align">
                    <label class="control-label col-lg-2 col-sm-5 col-xs-12" for="firstname"> </label>
                    <div class="col-md-6 col-sm-7 col-xs-12 profile-data pull-right">
                        <button type="submit" class="btn edit-btn"><?php echo Yii::t('default', 'Send');?></button>
                    </div>
                </div>
            </form>
        </div>

        <!--end col-9--> 

    </div>
</div>
</div>
