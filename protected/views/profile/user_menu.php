<div class="col-lg-3 col-md-3">
    <div class="left-submenu"> <?php echo Yii::t('default', 'My Account');?> </div>
    <ul class="sub-li">
        <li> <a href="<?=Yii::app()->getBaseUrl(true);?>/profile/recommendedPlaces"><span><?php echo Yii::t('default', 'Recommended places');?></span></a> </li>
        <li> <a href="<?=Yii::app()->getBaseUrl(true);?>/profile/usageInfo"><span ><?php echo Yii::t('default', 'Usage info');?></span></a></li>
        <li> <a href="<?=Yii::app()->getBaseUrl(true);?>/profile/specialRequests"><span><?php echo Yii::t('default', 'Users special requests');?></span></a> </li>
        <li> <a href="<?=Yii::app()->getBaseUrl(true);?>/profile/recommendFriend"><span><?php echo Yii::t('default', 'Recommend a friend');?></span></a> </li>
       
        
        <li id="send-gifts">
          
            <a aria-controls="gifts" aria-expanded="false" href="#gifts" data-parent="#accordion" data-toggle="collapse" class="collapsed">
              <span><?php echo Yii::t('default', 'Send gifts');?></span>
            </a>
          
        </li>
        
        <ul aria-labelledby="send-gifts" role="tabpanel" class="panel-collapse collapse" id="gifts" aria-expanded="false" style="height: 0px;">
         <li><a href="<?=Yii::app()->getBaseUrl(true);?>/profile/notYet"><span><?php echo Yii::t('default', 'SVI voucher');?></span></a></li>
        <li><a href="<?=Yii::app()->getBaseUrl(true);?>/profile/notYet"><span><?php echo Yii::t('default', 'SVI membership');?></span></a></li> 
        </ul>
        
        
    </ul>
</div>