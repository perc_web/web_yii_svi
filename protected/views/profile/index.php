
<div class="clearfix"></div>
<div class="container">
  <div class="row">
    <?=$this->renderPartial('user_menu')?>
    <!--End col-3--> 
    <!--start col-9-->
    <div class="col-lg-9 col-md-9 ">
    <h2 class="font-h"> <?php echo Yii::t('default', 'My Account');?></h2>
      <form class="text-align form-horizontal" >
        <div class="form-group">
        <label class="control-label col-lg-3 col-sm-5 col-xs-5"><?php echo Yii::t('default', 'Full Name');?></label>
        <span class="control-label col-lg-3 col-sm-5 col-xs-5"><?=$user->full_name;?></span>
        </div>
        <div class="form-group">
        <label class="control-label col-lg-3 col-sm-5 col-xs-5"><?php echo Yii::t('default', 'Username');?></label>
        <span class="control-label col-lg-3 col-sm-5 col-xs-5"><?=$user->username;?></span>
        </div>
        <div class="form-group">
        <label class="control-label col-lg-3 col-sm-5 col-xs-5"> <?php echo Yii::t('default', 'Birth Date');?> </label>
        <span class="control-label col-lg-3 col-sm-5 col-xs-5"><?=$user->age?></span>
        </div>
        <div class="form-group">
        <label class="control-label col-lg-3 col-sm-5 col-xs-5"><?php echo Yii::t('default', 'Email');?></label>
        <span class="control-label col-lg-3 col-sm-5 col-xs-5"><?=$user->email?></span>
        </div>
        <div class="form-group">
        <label class="control-label col-lg-3 col-sm-5 col-xs-5"><?php echo Yii::t('default', 'Phone number');?></label>
        <span class="control-label col-lg-3 col-sm-5 col-xs-5"><?=$user->phone?></span>
        </div>
          
           <div class="form-group">
        <label class="control-label col-lg-3 col-sm-5 col-xs-5"><?php echo Yii::t('default', 'Address');?></label>
        <span class="control-label col-lg-3 col-sm-5 col-xs-5"><?=$user->address?></span>
        </div>
        <div class="col-lg-6 col-sm-6 col-xs-6">
        <div class="form-group">
            <a class="btn edit-btn pull-right" href="<?=Yii::app()->getBaseUrl(true);?>/profile/editProfile"><?php echo Yii::t('default', 'Edit');?></a>
        </div>
        </div>
      </form>
    </div>
    <!--end col-9--> 
  </div>
</div>
</div>
