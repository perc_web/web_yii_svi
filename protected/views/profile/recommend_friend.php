
<div class="clearfix"></div>
<div class="container">
  <div class="row">
    <?=$this->renderPartial('user_menu')?>
    
    <!--End col-3--> 
    
    <!--start col-9-->
    <div class="col-lg-9 col-md-9 ">
      
                 <?php
            if (Yii::app()->user->hasFlash('invite')) {
                ?>
                     <div style="margin-top:20px;" class="alert alert-success"> <!-- flash for sucess -->
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <strong><?php echo Yii::t('default', 'Notification !');?></strong> <?php echo Yii::app()->user->getFlash('invite'); ?>
                </div>
              


            <?php }
                ?>
    <h2 class="font-h"><?php echo Yii::t('default', 'Recommend a friend');?></h2>
    
    <?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'user-invite-form',
	'enableAjaxValidation'=>false,
	'type'=>'horizontal',
         'htmlOptions' => array('class' => 'form-horizontal') 
)); ?>
<!--      <form class="form-horizontal">-->
        <div class="form-group text-align">
          <label class="control-label col-lg-2 col-sm-5 col-xs-12" for="firstname"><?php echo Yii::t('default', 'Name');?></label>
          <div class="col-lg-4 col-md-6 col-sm-7 col-xs-12 profile-data">
            	<?php echo $form->textField($model,'name',array('class'=>'form-control','placeholder'=>'')); ?> 
              <?php echo $form->error($model,'name'); ?>

          </div>
        </div>
        
        
        
       
        <div class="form-group text-align">
          <label class="control-label col-lg-2 col-sm-5 col-xs-12" for="firstname"><?php echo Yii::t('default', 'Email');?> </label>
          <div class="col-lg-4 col-md-6 col-sm-7 col-xs-12 profile-data">
            <?php echo $form->textField($model,'email',array('class'=>'form-control','placeholder'=>'')); ?>
               <?php echo $form->error($model,'email'); ?>
          </div>
        </div>
        <div class="col-md-6 col-sm-6 col-xs-6">
        <div class="form-group pull-right">
        <button type="submit" class="btn edit-btn"><?php echo Yii::t('default', 'Invite');?></button>
        </div>
        </div>
      <?php $this->endWidget(); ?>
    </div>
    
    <!--end col-9--> 
    
  </div>
</div>
</div>
