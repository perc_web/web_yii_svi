
<div class="clearfix"></div>
<div class="container">
  <div class="row">
    <?=$this->renderPartial('user_menu')?>
    
    <!--End col-3--> 
    
    <!--start col-9-->
    <div class="col-lg-9 col-md-9 ">
    <h2 class="font-h"><?php echo Yii::t('default', 'Change Password');?></h2>
    
        <?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'user-form',
	'enableAjaxValidation'=>false,
	//'type'=>'horizontal',
        'htmlOptions' => array(	'class' => 'form-horizontal'), 

)); ?>
    
                 <?php
            if (Yii::app()->user->hasFlash('update-success')) {
                ?>
                     <div class="alert alert-success"> <!-- flash for sucess -->
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <strong><?php echo Yii::t('default', 'Notification !');?></strong> <?php echo Yii::app()->user->getFlash('update-success'); ?>
                </div>
              


            <?php }elseif (Yii::app()->user->hasFlash('update-error')) {
                ?>
              
     <div class="alert alert-success"> <!-- flash for sucess -->
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <strong><?php echo Yii::t('default', 'Notification !');?></strong> <?php echo Yii::app()->user->getFlash('update-error'); ?>
                </div>
                <?php
                    
                } ?>
        <div class="form-group text-align">
          <label class="control-label col-lg-2 col-sm-5 col-xs-12" for="firstname"><?php echo Yii::t('default', 'Old password');?> </label>
          <div class="col-lg-4 col-md-6 col-sm-7 col-xs-12 profile-data">
            <input type="password"  placeholder=""name="User[oldpassword]"  class="form-control">
          </div>
        </div>
        
        <div class="form-group text-align">
          <label class="control-label col-lg-2 col-sm-5 col-xs-12" for="firstname"><?php echo Yii::t('default', 'New password');?> </label>
          <div class="col-lg-4 col-md-6 col-sm-7 col-xs-12 profile-data">
            <input type="password"  placeholder=""  name="User[newpassword]"  class="form-control">
          </div>
        </div>
        <div class="form-group text-align">
          <label class="control-label col-lg-2 col-sm-5 col-xs-12" for="firstname"><?php echo Yii::t('default', 'Confirm password');?></label>
          <div class="col-lg-4 col-md-6 col-sm-7 col-xs-12 profile-data">
            <input type="password"  placeholder=""  name="User[newpassword_repeat]"   class="form-control">
          </div>
        </div>
       
        <div class="col-md-6 col-sm-6 col-xs-6">
        <div class="form-group pull-right">
        <button type="submit" class="btn edit-btn"><?php echo Yii::t('default', 'Save change');?> </button>
        </div>
        </div>
        <?php $this->endWidget(); ?>
    </div>
    
    <!--end col-9--> 
    
  </div>
</div>
</div>
