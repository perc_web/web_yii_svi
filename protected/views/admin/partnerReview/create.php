<?php
$this->breadcrumbs=array(
	'Partner Reviews'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List PartnerReview','url'=>array('index','partner_id' => $model->partner_id)),
);
?>

<?php $this->pageTitlecrumbs = 'Create PartnerReview';?>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>