<?php
$this->breadcrumbs=array(
	'Role Users'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List RoleUser','url'=>array('index')),
);
?>

<?php $this->pageTitlecrumbs = 'Create RoleUser';?>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>