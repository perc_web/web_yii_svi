<?php
$this->breadcrumbs=array(
	'Role Users'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List RoleUser','url'=>array('index')),
	array('label'=>'Create RoleUser','url'=>array('create')),
	array('label'=>'Update RoleUser','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete RoleUser','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
);
?>

<?php $this->pageTitlecrumbs = 'View RoleUser #'. $model->id; ?>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'role_id',
		'user_id',
	),
)); ?>
