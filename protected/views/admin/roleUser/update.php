<?php
$this->breadcrumbs=array(
	'Role Users'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List RoleUser','url'=>array('index')),
	array('label'=>'Create RoleUser','url'=>array('create')),
	array('label'=>'View RoleUser','url'=>array('view','id'=>$model->id)),
);
?>

<?php $this->pageTitlecrumbs = 'Update RoleUser #'. $model->id; ?>
<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>