<?php
$this->breadcrumbs=array(
	'Outlets'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Outlet','url'=>array('index','partner_id' => $model->partner_id)),
	array('label'=>'Create Outlet','url'=>array('create','partner_id' => $model->partner_id)),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('outlet-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<!--<h3>Manage </h3>-->
<?php
if($model->partner->id){
     $partner_link=Yii::app()->baseUrl."/admin/partner/view/id/".$model->partner->id  ;
     $this->pageTitlecrumbs = 'Manage Outlets for partner :  <a  href="'.$partner_link.'">'.$model->partner->title .'</a>';
}  else {
     $this->pageTitlecrumbs = 'Manage Outlets';
}
?>


<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'outlet-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
            'title',
            /*
		'partner_id' => array(
                    'name' => 'partner_id',
                    'value' => '$data->partner->title',
                    'filter' => CHtml::listData(Partner::model()->findAll(), 'id', 'title'),
                ),
            */
		
		 // 'address',
		'active' => array(
                    'name' => 'active',
                    'value' => '($data->active == 1)? "Yes" : "No"',
                    'filter' => array(1 => "Yes", 0 => "No"),
                ),
            
              'featured' => array(
                    'name' => 'featured',
                    'value' => '($data->featured == 1)? "Yes" : "No"',
                    'filter' => array(1 => "Yes", 0 => "No"),
                ),
            
		'hot_deal' => array(
                    'name' => 'hot_deal',
                    'value' => '($data->hot_deal == 1)? "Yes" : "No"',
                    'filter' => array(1 => "Yes", 0 => "No"),
                ),
		'new_deal' => array(
                    'name' => 'new_deal',
                    'value' => '($data->new_deal == 1)? "Yes" : "No"',
                    'filter' => array(1 => "Yes", 0 => "No"),
                ),
            
            'new_deal' => array(
                    'name' => 'recomended',
                    'value' => '($data->recomended == 1)? "Yes" : "No"',
                    'filter' => array(1 => "Yes", 0 => "No"),
                ),
            
            /*
                 array(
            'header' => 'Menus',
            'type' => 'raw',
            'value' => 'CHtml::link("Menus", Yii::app()->baseUrl."/admin/menu/index/outlet_id/".$data->id)',
        ),
             array(
            'header' => 'Reviews',
            'type' => 'raw',
            'value' => 'CHtml::link("Reviews", Yii::app()->baseUrl."/admin/outletReview/index/outlet_id/".$data->id)',
        ),
           */ 
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>
