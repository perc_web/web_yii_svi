<?php
$this->breadcrumbs=array(
	'Partner Categories'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List PartnerCategory','url'=>array('index')),
	array('label'=>'Create PartnerCategory','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('partner-category-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<!--<h3>Manage </h3>-->

<?php $this->pageTitlecrumbs = 'Manage Partner Categories';?>

<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'partner-category-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	
    	//'type'=>'striped  condensed',
	'columns'=>array(
		'title',
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>
