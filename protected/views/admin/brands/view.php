<?php
$this->breadcrumbs=array(
	'Brands'=>array('index'),
	$model->title,
);

$this->menu=array(
	array('label'=>'List Brands','url'=>array('index')),
	array('label'=>'Create Brands','url'=>array('create')),
	array('label'=>'Update Brands','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete Brands','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
);
?>

<?php $this->pageTitlecrumbs = 'View Brands "'. $model->title.'"'; ?>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'title',
		'link',
		'active' => array(
                    'name' => 'active',
                    'value' => ($model->active == 1)? "Yes" : "No",
                ),
		'featured' => array(
                    'name' => 'featured',
                    'value' => ($model->featured == 1)? "Yes" : "No",
                ),
                array(
                    'name' => 'image',
                    'type' => 'raw',
                    'value' => CHtml::image(Yii::app()->request->baseUrl.'/media/brands/'.$model->image,'',array('width'=>250)),
                ),
	),
)); ?>
