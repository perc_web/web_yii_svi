<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'brands-form',
	'enableAjaxValidation'=>false,
	'type'=>'horizontal',
        'htmlOptions'=>array('enctype'=>'multipart/form-data'),
)); ?>

	<p class="help-block">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<?php echo $form->label($model, 'title', array('class' => 'form-label', 'for' => 'normal')) ;
      echo EMHelper::megaOgogo($model, 'title', array('class' => 'form-control', 'maxlength' => 255)) ; ?>
<br /><br />

	<?php echo $form->textFieldRow($model,'link',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->checkBoxRow($model,'active'); ?>

	<?php echo $form->checkBoxRow($model,'featured'); ?>
        
        <?php echo $form->fileFieldRow($model,'image');
            echo " <div class=\"control-group \"> <div class=\"controls\">";
            echo CHtml::image(Yii::app()->request->baseUrl.'/media/brands/'.$model->image,'',array('width'=>200));
            echo "</div></div>";
        ?>
        
	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Create' : 'Save',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
