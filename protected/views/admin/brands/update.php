<?php
$this->breadcrumbs=array(
	'Brands'=>array('index'),
	$model->title=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Brands','url'=>array('index')),
	array('label'=>'Create Brands','url'=>array('create')),
	array('label'=>'View Brands','url'=>array('view','id'=>$model->id)),
);
?>

<?php $this->pageTitlecrumbs = 'Update Brands #'. $model->id; ?>
<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>