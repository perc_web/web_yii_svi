<?php
$this->breadcrumbs=array(
	'Brands'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Brands','url'=>array('index')),
);
?>

<?php $this->pageTitlecrumbs = 'Create Brands';?>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>