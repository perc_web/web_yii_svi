<?php
$this->breadcrumbs=array(
	'Brands'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Brands','url'=>array('index')),
	array('label'=>'Create Brands','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('brands-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<!--<h3>Manage </h3>-->

<?php $this->pageTitlecrumbs = 'Manage Brands';?>

<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'brands-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'title',
		'link',
		'active' => array(
                    'name' => 'active',
                    'value' => '($data->active == 1)? "Yes" : "No"',
                    'filter' => array(1 => "Yes", 0 => "No"),
                ),
		'featured' => array(
                    'name' => 'featured',
                    'value' => '($data->featured == 1)? "Yes" : "No"',
                    'filter' => array(1 => "Yes", 0 => "No"),
                ),
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>
