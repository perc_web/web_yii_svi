<?php
$this->breadcrumbs=array(
	'Reservations'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Reservations','url'=>array('index')),
);
?>

<?php $this->pageTitlecrumbs = 'Create Reservations';?>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>