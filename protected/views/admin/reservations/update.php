<?php
$this->breadcrumbs=array(
	'Reservations'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Reservations','url'=>array('index')),
	array('label'=>'Create Reservations','url'=>array('create')),
	array('label'=>'View Reservations','url'=>array('view','id'=>$model->id)),
);
?>

<?php $this->pageTitlecrumbs = 'Update Reservations #'. $model->id; ?>
<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>