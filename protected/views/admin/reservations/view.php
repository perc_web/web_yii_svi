<?php
$this->breadcrumbs=array(
	'Reservations'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Reservations','url'=>array('index')),
	array('label'=>'Create Reservations','url'=>array('create')),
	array('label'=>'Update Reservations','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete Reservations','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
);
?>

<?php $this->pageTitlecrumbs = 'View Reservations #'. $model->id; ?>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'user_id' => array(
                    'name' => 'user_id',
                    'value' => $model->user->username,
                ),
		'date',
                'from',
		'to',
                'category_id' => array(
                    'name' => 'category',
                    'value' => $model->category->title,
                ),
                'partner_id' => array(
                    'name' => 'partner_id',
                    'value' => $model->partner->title,
                ),
		'outlet_id' => array(
                    'name' => 'outlet_id',
                    'value' => $model->outlet->title,
                ),
                'menu_id' => array(
                    'name' => 'menu_id',
                    'value' => $model->menu->title,
                ),
		
		
		'num_customers',
	),
)); ?>
