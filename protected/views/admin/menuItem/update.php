<?php
$this->breadcrumbs=array(
	'Menu Items'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List MenuItem','url'=>array('index','menu_id' => $model->menu_id)),
	array('label'=>'Create MenuItem','url'=>array('create','menu_id' => $model->menu_id)),
	array('label'=>'View MenuItem','url'=>array('view','id'=>$model->id)),
);
?>

<?php $this->pageTitlecrumbs = 'Update MenuItem #'. $model->id; ?>
<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>