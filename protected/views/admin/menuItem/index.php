<?php
$this->breadcrumbs=array(
	'Menu Items'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List MenuItem','url'=>array('index','menu_id' => $model->menu_id)),
	array('label'=>'Create MenuItem','url'=>array('create','menu_id' => $model->menu_id)),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('menu-item-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<!--<h3>Manage </h3>-->

<?php
if($model->menu->id ){
    $menu_link=Yii::app()->baseUrl."/admin/menu/view/id/".$model->menu->id  ;
     $this->pageTitlecrumbs = 'Manage items for menu :  <a  href="'.$menu_link.'">'.$model->menu->title.'</a> ';
}  else {
     $this->pageTitlecrumbs = 'Manage Menu Items';
}
?>


<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'menu-item-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	
    	//'type'=>'striped  condensed',
	'columns'=>array(
		
            /*
             'menu_id' => array(
                    'name' => 'menu_id',
                    'value' => '$data->menu->title',
                    'filter' => CHtml::listData(Menu::model()->findAll(), 'id', 'title'),
                ),
            */
		
             'item_id' => array(
                    'name' => 'item_id',
                    'value' => '$data->item->title',
                    'filter' => CHtml::listData(Item::model()->findAll(), 'id', 'title'),
                ),
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>
