<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'information-form',
	'enableAjaxValidation'=>false,
	'type'=>'horizontal',
     'htmlOptions' => array(	'enctype' => 'multipart/form-data'),
)); ?>

	<p class="help-block">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<?php echo $form->label($model, 'title', array('class' => 'form-label', 'for' => 'normal')) ;
      echo EMHelper::megaOgogo($model, 'title', array('class' => 'form-control', 'maxlength' => 255)) ; ?>
<br /><br />

	<?php echo $form->label($model, 'subtitle', array('class' => 'form-label', 'for' => 'normal')) ;
      echo EMHelper::megaOgogo($model, 'subtitle', array('class' => 'form-control', 'maxlength' => 255)) ; ?>
<br /><br />

	<?php echo $form->textFieldRow($model,'link',array('class'=>'span5','maxlength'=>255)); ?>

        
                <?php
echo $form->dropDownListRow($model, 'image', array('i-con i-con-free-bottom' => "shipping", 'i-con i-con-special'=> "card", 'i-con i-con-promotion' => "promotion", 'i-con i-con-support' => "support"), 
        array('class' => 'span5', 'empty' => 'Select icon'));
?>

        <?php echo $form->checkboxRow($model, 'publish'); ?>

		

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Create' : 'Save',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
