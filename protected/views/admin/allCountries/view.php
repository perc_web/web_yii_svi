<?php
$this->breadcrumbs=array(
	'All Countries'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List AllCountries','url'=>array('index')),
	array('label'=>'Create AllCountries','url'=>array('create')),
	array('label'=>'Update AllCountries','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete AllCountries','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
);
?>

<?php $this->pageTitlecrumbs = 'View AllCountries #'. $model->id; ?>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		
		'country_code',
		'country_name',
		
	),
)); ?>
