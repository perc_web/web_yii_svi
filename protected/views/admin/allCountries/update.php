<?php
$this->breadcrumbs=array(
	'All Countries'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List AllCountries','url'=>array('index')),
	array('label'=>'Create AllCountries','url'=>array('create')),
	array('label'=>'View AllCountries','url'=>array('view','id'=>$model->id)),
);
?>

<?php $this->pageTitlecrumbs = 'Update AllCountries #'. $model->id; ?>
<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>