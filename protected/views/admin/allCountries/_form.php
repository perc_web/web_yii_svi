<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'all-countries-form',
	'enableAjaxValidation'=>false,
	'type'=>'horizontal',
)); ?>

	<p class="help-block">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldRow($model,'country_code',array('class'=>'span5','maxlength'=>2)); ?>

	<?php // echo $form->textFieldRow($model,'country_name',array('class'=>'span5','maxlength'=>100)); ?>
        <?php echo $form->label($model, 'country_name', array('class' => 'form-label', 'for' => 'normal')) ;
              echo EMHelper::megaOgogo($model, 'country_name', array('class' => 'form-control', 'maxlength' => 255)) ; ?>
          <br /><br />



	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Create' : 'Save',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
