<?php
$this->breadcrumbs=array(
	'Item Tags'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List ItemTag','url'=>array('index','item_id' => $model->item_id)),
);
?>

<?php $this->pageTitlecrumbs = 'Create ItemTag';?>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>