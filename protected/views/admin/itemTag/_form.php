<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'item-tag-form',
	'enableAjaxValidation'=>false,
	'type'=>'horizontal',
)); ?>

	<p class="help-block">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

        <div class="control-group ">
            <?php
            echo $form->labelEx($model, 'item_id', array('class' => 'control-label')) ?>
            <?php
            $this->widget('Select2', array(
                'model' => $model,
                'attribute' => 'item_id',
                'data' => CHtml::listData(Menu::model()->findAll(), 'id', 'title'),
                'htmlOptions' => array('class' => "span4", "empty" => "Select menu"),
            ));
            ?>
        </div>

        <div class="control-group ">
            <?php echo $form->labelEx($model, 'tag_id', array('class' => 'control-label')) ?>
            <?php
            $this->widget('Select2', array(
                'model' => $model,
                'attribute' => 'tag_id',
                'data' => CHtml::listData(Tags::model()->findAll(), 'id', 'title'),
                'htmlOptions' => array('class' => "span4", "empty" => "Select Tag"),
            ));
            ?>
        </div>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Create' : 'Save',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
