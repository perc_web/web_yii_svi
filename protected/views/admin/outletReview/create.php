<?php
$this->breadcrumbs=array(
	'Outlet Reviews'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List OutletReview','url'=>array('index','outlet_id' => $model->outlet_id)),
);
?>

<?php $this->pageTitlecrumbs = 'Create OutletReview';?>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>