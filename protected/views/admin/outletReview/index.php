<?php
$this->breadcrumbs=array(
	'Outlet Reviews'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List OutletReview','url'=>array('index','outlet_id' => $model->outlet_id)),
	array('label'=>'Create OutletReview','url'=>array('create','outlet_id' => $model->outlet_id)),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('outlet-review-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<!--<h3>Manage </h3>-->
<?php
if($model->outlet->id){
     $outlet_link=Yii::app()->baseUrl."/admin/outlet/view/id/".$model->outlet->id  ;
     $this->pageTitlecrumbs = 'Manage Reviews for outlet :  <a  href="'.$outlet_link.'">'.$model->outlet->title .'</a>';
}  else {
     $this->pageTitlecrumbs = 'Manage Outlet Reviews';
}
?>



<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'outlet-review-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
    	//'type'=>'striped  condensed',
	'columns'=>array(
		'user_id' => array(
                    'name' => 'user_id',
                    'value' => '$data->user->username',
                    'filter' => CHtml::listData(User::model()->findAll(), 'id', 'username'),
                ),
            /*
		'outlet_id' => array(
                    'name' => 'outlet_id',
                    'value' => '$data->outlet->title',
                    'filter' => CHtml::listData(Outlet::model()->findAll(), 'id', 'title'),
                ),
            */
		'price_rate',
		'value_rate',
		'quality_rate',
            
             'approved' => array(
                    'name' => 'approved',
                    'value' => '($data->approved == 1)? "Yes" : "No"',
                    'filter' => array(1 => 'Yes', 0 => 'No'),
                ),
		
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>
