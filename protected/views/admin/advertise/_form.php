<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'advertise-form',
	'enableAjaxValidation'=>false,
	'type'=>'horizontal',
    'htmlOptions' => array(	'enctype' => 'multipart/form-data'),
)); ?>

	<p class="help-block">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

        <?php
echo $form->dropDownListRow($model, 'page', array(1 => "home", 2=> "why svi card", 3 => "partner&reserve&catalog", 4 => "contact us",5 => "faq",6 => "blog"), 
        array('class' => 'span5', 'empty' => 'Select page'));
?>

             <?php
echo $form->dropDownListRow($model, 'size', array(0 => "very small",1 => "small", 2=> "medium", 3 => "large"), 
        array('class' => 'span5', 'empty' => 'Select size'));
?>

	<?php echo $form->textFieldRow($model,'link',array('class'=>'span5','maxlength'=>255)); ?>

        <?php echo $form->checkboxRow($model, 'active'); ?>

        <?php echo $form->fileFieldRow($model,'image',array('class'=>'span5','maxlength'=>255));
        if($model->isNewRecord != '1')
        {
          echo "<p>";
          echo CHtml::image(Yii::app()->request->baseUrl.'/media/advertise/'.$model->image,'',array('width'=>200));
          echo "</p>";
        }
        ?>


	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Create' : 'Save',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
