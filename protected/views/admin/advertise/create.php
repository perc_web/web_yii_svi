<?php
$this->breadcrumbs=array(
	'Advertises'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Advertise','url'=>array('index')),
);
?>

<?php $this->pageTitlecrumbs = 'Create Advertise';?>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>