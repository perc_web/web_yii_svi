<?php
$this->breadcrumbs=array(
	'Tags'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Tags','url'=>array('index')),
	array('label'=>'Create Tags','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('tags-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<!--<h3>Manage </h3>-->

<?php $this->pageTitlecrumbs = 'Manage Tags';?>

<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'tags-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'title',
		'active' => array(
                    'name' => 'active',
                    'value' => '($data->active == 1)? "Yes" : "No"',
                ),
		array(
                    'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>
