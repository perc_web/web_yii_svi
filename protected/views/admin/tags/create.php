<?php
$this->breadcrumbs=array(
	'Tags'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Tags','url'=>array('index')),
);
?>

<?php $this->pageTitlecrumbs = 'Create Tags';?>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>