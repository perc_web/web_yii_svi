<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'tags-form',
	'enableAjaxValidation'=>false,
	'type'=>'horizontal',
)); ?>

	<p class="help-block">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<?php echo $form->label($model, 'title', array('class' => 'form-label', 'for' => 'normal')) ;
              echo EMHelper::megaOgogo($model, 'title', array('class' => 'form-control', 'maxlength' => 255)) ; ?>
      <br /><br />

     <?php echo $form->textFieldRow($model,'usage',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->checkBoxRow($model,'active'); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Create' : 'Save',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
