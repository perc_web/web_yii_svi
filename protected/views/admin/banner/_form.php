<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'banner-form',
	'enableAjaxValidation'=>false,
	'htmlOptions' => array(	'enctype' => 'multipart/form-data'),
'type'=>'horizontal',
)); ?>

	<p class="help-block">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<?php echo $form->label($model, 'title', array('class' => 'form-label', 'for' => 'normal')) ;
                echo EMHelper::megaOgogo($model, 'title', array('class' => 'form-control', 'maxlength' => 255)) ; ?>
          <br /><br />

	<?php // echo $form->textFieldRow($model,'subtitle',array('class'=>'span5','maxlength'=>255)); ?>
          <?php echo $form->label($model, 'subtitle', array('class' => 'form-label', 'for' => 'normal')) ;
      echo EMHelper::megaOgogo($model, 'subtitle', array('class' => 'form-control', 'maxlength' => 255)) ; ?>
<br /><br />

	<?php // echo $form->textAreaRow($model,'details',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>
<?php echo $form->label($model, 'details', array('class' => 'form-label', 'for' => 'normal')); ?>
<?php echo EMHelper::megaOgogo($model, 'details', array('rows' => 4, 'cols' => 20,'class' => 'span6', 'maxlength' => 1000), 'textArea'); ?>
        
     <?php echo $form->textFieldRow($model,'link',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->fileFieldRow($model,'image',array('class'=>'span5','maxlength'=>255));
?>
        <div class="control-group">
            <div class="controls">
        <?php
	  if($model->isNewRecord != '1')
	  {
	  	echo "<p>";
		echo 	CHtml::image(Yii::app()->request->baseUrl.'/media/banner/'.$model->image,'',array('width'=>200));

		echo "</p>";
	  }
			?>
            </div>
        </div>


	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Create' : 'Save',
		)); ?>
	</div>

<?php $this->endWidget(); ?>