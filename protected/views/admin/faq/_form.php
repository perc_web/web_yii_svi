<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'faq-form',
	'enableAjaxValidation'=>false,
	'type'=>'horizontal',
)); ?>

	<p class="help-block">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<?php
	/*echo " <div class=\"control-group \">
	<label for=\"cat_id\" class=\"control-label\">Category</label>
			 <div class=\"controls\">";
	echo   $form->dropDownList($model,'cat_id',FaqCat::model()->getCategory());
	echo "</div> </div>";*/
	?>

	<?php // echo $form->textFieldRow($model,'quest',array('class'=>'span5','maxlength'=>255)); ?>
        <?php echo $form->label($model, 'quest', array('class' => 'form-label', 'for' => 'normal')) ;
      echo EMHelper::megaOgogo($model, 'quest', array('class' => 'form-control', 'maxlength' => 255)) ; ?>
<br /><br />
<?php echo $form->label($model, 'answer', array('class' => 'form-label', 'for' => 'normal')) ;?>
        <?php echo EMHelper::megaOgogo($model, 'answer', array(), 'floara'); ?>
<!--        <div class="control-group ">
            <div class="control-label required">
                <label> Answer</label>
            </div>
            <div class="controls">
                <?php
//                    $this->widget('application.extensions.eckeditor.ECKEditor', array(
//                        'model'=>$model,
//                        'attribute'=>'answer',
//                    ));
                ?>
            </div>
        </div>-->

	<?php echo $form->checkBoxRow($model,'active'); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Create' : 'Save',
		)); ?>
	</div>

<?php $this->endWidget(); ?>