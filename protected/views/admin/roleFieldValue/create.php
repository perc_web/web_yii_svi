<?php
$this->breadcrumbs=array(
	'Role Field Values'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List RoleFieldValue','url'=>array('index')),
);
?>

<?php $this->pageTitlecrumbs = 'Create RoleFieldValue';?>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>