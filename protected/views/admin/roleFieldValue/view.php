<?php
$this->breadcrumbs=array(
	'Role Field Values'=>array('index'),
	$model->title,
);

$this->menu=array(
	array('label'=>'List RoleFieldValue','url'=>array('index')),
	array('label'=>'Create RoleFieldValue','url'=>array('create')),
	array('label'=>'Update RoleFieldValue','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete RoleFieldValue','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
);
?>

<?php $this->pageTitlecrumbs = 'View RoleFieldValue #'. $model->id; ?>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'title',
		'field_id',
	),
)); ?>
