<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'partner-tag-form',
	'enableAjaxValidation'=>false,
	'type'=>'horizontal',
)); ?>

	<p class="help-block">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

         <?php
        if(! $model->partner_id){
            ?>
        <div class="control-group ">
            <?php
            echo $form->labelEx($model, 'partner_id', array('class' => 'control-label')) ?>
            <?php
            $this->widget('Select2', array(
                'model' => $model,
                'attribute' => 'partner_id',
                'data' => CHtml::listData(Partner::model()->findAll(), 'id', 'title'),
                'htmlOptions' => array('class' => "span4", "empty" => "Select partner"),
            ));
            ?>
        </div>
        <?php
        }
        
        ?>
        
           

	 <div class="control-group ">
            <?php echo $form->labelEx($model, 'tag_id', array('class' => 'control-label')) ?>
            <?php
            $this->widget('Select2', array(
                'model' => $model,
                'attribute' => 'tag_id',
                'data' => CHtml::listData(Tags::model()->findAll(), 'id', 'title'),
                'htmlOptions' => array('class' => "span4", "empty" => "Select Tag"),
            ));
            ?>
        </div>
        
        <?php  // echo $form->textFieldRow($model, 'usage', array('class' => 'span5', 'maxlength' => 255)); ?>

        

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Create' : 'Save',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
