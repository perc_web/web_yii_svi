<?php
$this->breadcrumbs=array(
	'Partner Tags'=>array('index'),
	$model->id,
);


$this->menu=array(
	array('label'=>'List PartnerTag','url'=>array('index','partner_id' => $model->partner_id)),
	array('label'=>'Create PartnerTag','url'=>array('create','partner_id' => $model->partner_id)),
	array('label'=>'Update PartnerTag','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete PartnerTag','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
);
?>


<?php
if($model->partner->id){
     $partner_link=Yii::app()->baseUrl."/admin/partner/view/id/".$model->partner->id  ;
     $this->pageTitlecrumbs = 'View PartnerTag: '. $model->tag->title;
     $this->pageTitlecrumbs = ' for partner :  <a  href="'.$partner_link.'">'.$model->partner->title .'</a>';
}  else {
     $this->pageTitlecrumbs = 'View PartnerTag: '. $model->tag->title;
}
?>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
            'partner_id' => array(
                    'name' => 'partner_id',
                    'value' => $model->partner->title,
                ),
		'tag_id' => array(
                    'name' => 'tag_id',
                    'value' => $model->tag->title,
                ),
           //  'usage'
	),
)); ?>
