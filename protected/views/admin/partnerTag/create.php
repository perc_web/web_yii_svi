<?php
$this->breadcrumbs=array(
	'Partner Tags'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List PartnerTag','url'=>array('index','partner_id' => $model->partner_id)),
);
?>

<?php $this->pageTitlecrumbs = 'Create PartnerTag';?>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>