<?php
$this->breadcrumbs=array(
	'Items'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Item','url'=>array('index','partner_id' => $model->partner_id)),
	array('label'=>'Create Item','url'=>array('create','partner_id' => $model->partner_id)),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('item-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<!--<h3>Manage </h3>-->
<?php
if($model->partner->title){
  $partner_link=Yii::app()->baseUrl."/admin/partner/view/id/".$model->partner->id  ;
     $this->pageTitlecrumbs = 'Manage Items for partner : <a  href="'.$partner_link.'">'.$model->partner->title.'</a>';
}  else {
     $this->pageTitlecrumbs = 'Manage Items';
}
?>

<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'item-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
                'title',
            /*
                'partner_id' => array(
                    'name' => 'partner_id',
                    'value' => '$data->partner->title',
                    'filter'=> CHtml::listData(Partner::model()->findAll(), 'id', 'title'),
                ),
            */
		'cat_id' => array(
                    'name' => 'cat_id',
                    'value' => '$data->cat->title',
                    'filter' => CHtml::listData(Category::model()->findAll(), 'id', 'title'),
                ),
                'price',
                'discount_type' => array(
                    'name' => 'discount_type',
                    'value' => '($data->discount_type == 1)? "Percentage" : "Fixed Amount"',
                ),
		'discount_amount',
                'quantity',
            
            /*
                'active' =>array(
                    'name' => 'active',
                    'value' => '($data->active == 1)? "Yes" : "No"',
                    'filter' => array(1 => "Yes", 0 => "No"),
                ),
		'featured' => array(
                    'name' => 'featured',
                    'value' => '($data->featured == 1)? "Yes" : "No"',
                    'filter' => array(1 => "Yes", 0 => "No"),
                ),
		'hot_deal' => array(
                    'name' => 'hot_deal',
                    'value' => '($data->hot_deal == 1)? "Yes" : "No"',
                    'filter' => array(1 => "Yes", 0 => "No"),
                ),
		'new_deal' => array(
                    'name' => 'new_deal',
                    'value' => '($data->new_deal == 1)? "Yes" : "No"',
                    'filter' => array(1 => "Yes", 0 => "No"),
                ),
               
                array(
                    'class'=>'CLinkColumn',
                    'label'=>'Item Tags',
                    'urlExpression'=>'Yii::app()->request->baseUrl."/admin/itemTag/index?item=".$data->id',
                    'header'=>'Item Tags',
                ),
		array(
                    'class'=>'CLinkColumn',
                    'label'=>'Item Reviews',
                    'urlExpression'=>'Yii::app()->request->baseUrl."/admin/reviews/index?item_id=".$data->id',
                    'header'=>'Item Reviews',
                ),
            */
                array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>
