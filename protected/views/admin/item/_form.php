<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'item-form',
	'enableAjaxValidation'=>false,
	'type'=>'horizontal',
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>

	<p class="help-block">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>
        
        <?php echo $form->label($model, 'title', array('class' => 'form-label', 'for' => 'normal')) ;
              echo EMHelper::megaOgogo($model, 'title', array('class' => 'form-control', 'maxlength' => 255)) ; ?>
        <br /><br />
        
<!--        <div class="control-group ">
            <?php  // echo $form->labelEx($model, 'cat_id', array('class' => 'control-label')) ?>
            <?php
            /*
            $this->widget('Select2', array(
                'model' => $model,
                'attribute' => 'cat_id',
                'data' => CHtml::listData(Category::model()->findAll(), 'id', 'title'),
                'htmlOptions' => array('class' => "span4", "empty" => "Select Category"),
            ));
            */
            ?>
        </div>-->

 <?php
        if(! $model->partner_id){
           ?>
       <div class="control-group ">
            <?php echo $form->labelEx($model, 'partner_id', array('class' => 'control-label')) ?>
            <?php
            $this->widget('Select2', array(
                'model' => $model,
                'attribute' => 'partner_id',
                'data' => CHtml::listData(Partner::model()->findAll(), 'id', 'title'),
                'htmlOptions' => array('class' => "span4", "empty" => "Select Partner"),
            ));
            ?>
        </div>
        <?php
        }
        
        ?>

 
	

	<?php echo $form->textFieldRow($model,'price',array('class'=>'span5','maxlength'=>10)); ?>
        
        <?php  // echo $form->radioButtonListRow($model,'discount_type',array(1 => 'Percentage', 2 => 'Fixed Amount')); ?>

	<?php  // echo $form->textFieldRow($model,'discount_amount',array('class'=>'span5','maxlength'=>10)); ?>
        
        <?php echo $form->textFieldRow($model,'quantity',array('class'=>'span5')); ?>
        
        <?php echo $form->textFieldRow($model,'model',array('class'=>'span5','maxlength'=>255)); ?>
<!--        
        <div class="control-group ">
            <?php  // echo $form->labelEx($model, 'brand_id', array('class' => 'control-label')) ?>
            <?php
            /*
            $this->widget('Select2', array(
                'model' => $model,
                'attribute' => 'brand_id',
                'data' => CHtml::listData(Brands::model()->findAll(), 'id', 'title'),
                'htmlOptions' => array('class' => "span4", "empty" => "Select Brand"),
            ));
            */
            ?>
        </div>-->
        
        <?php echo $form->label($model, 'intro', array('class' => 'form-label', 'for' => 'normal')); ?>
        <?php echo EMHelper::megaOgogo($model, 'intro', array('rows' => 4, 'cols' => 20,'class' => 'span6', 'maxlength' => 1000), 'textArea'); ?>

	<?php echo $form->label($model, 'description', array('class' => 'form-label', 'for' => 'normal')) ;?>
        <?php echo EMHelper::megaOgogo($model, 'description', array(), 'floara'); ?>
        <br />
        
	<?php echo $form->fileFieldRow($model, 'image', array('class' => 'span5', 'maxlength' => 255));
        if(!$model->isNewRecord and $model->image)
        { ?>
            <div class="control-group ">
                <div class="controls">
                    <p id='image-cont'> <?php echo Chtml::image(Yii::app()->baseUrl . '/media/items/' . $model->image, '', array('width' => 200)); ?></p>
                </div>
            </div>
        <?php
        }
        ?>

	<?php  // echo $form->checkBoxRow($model,'active'); ?>

	<?php  // echo $form->checkBoxRow($model,'featured'); ?>

	<?php  // echo $form->checkBoxRow($model,'hot_deal'); ?>

	<?php  // echo $form->checkBoxRow($model,'new_deal'); ?>
        
        <?php  // echo $form->checkBoxRow($model,'recomended'); ?>
        
        <?php echo $form->checkBoxRow($model,'availability'); ?>

<div  class="control-group"  id="temp_image_product"   >
    <img class="loading" style="margin-left:166px;display: none;width:60px;height:60px;" src="<?= Yii::app()->getBaseUrl() ?>/images/loading3.gif"    />
        </div>
        
        <div class="control-group">
            <label class="control-label">Images</label>
            <div class="controls">
                <div class="span<?php echo(isset($_GET['w']) ? $_GET['w'] : '12') ?>" style="width:900px;">
                    <?php
                    $this->widget('GalleryManager', array(
                        'gallery' => $gallery,
                    ));
                    ?>
                    <?php
                        echo $form->hiddenField($model,'gallery_id');
                    ?>
                </div>
            </div>
        </div>
        
	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Create' : 'Save',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
