<?php
$this->breadcrumbs=array(
	'Roles'=>array('index'),
	$model->title=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Role','url'=>array('index')),
	// array('label'=>'Create Role','url'=>array('create')),
	array('label'=>'View Role','url'=>array('view','id'=>$model->id)),
);
?>

<?php $this->pageTitlecrumbs = 'Update Role #'. $model->id; ?>
<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>