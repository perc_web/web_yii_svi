<?php
$this->breadcrumbs=array(
	'Roles'=>array('index'),
	$model->title,
);

$this->menu=array(
	array('label'=>'List Role','url'=>array('index')),
	// array('label'=>'Create Role','url'=>array('create')),
	array('label'=>'Update Role','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete Role','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
);
?>

<?php $this->pageTitlecrumbs = 'View Role #'. $model->id; ?>

<?php    
//$role= Role::model()->findByPk(1)  ;
  //echo serialize($role->permissions)  ;
  
/*
echo "<pre>" ;
  print_r(unserialize($role->permissions))  ;
  echo "</pre>" ;
  
  $arr=unserialize($role->permissions) ;
  
  echo $arr['pet']['access']  ;
*/
/*
$privs = unserialize(Yii::app()->user->getState('privs_arr'));

echo "<pre>" ;
  print_r($privs)  ;
  echo "</pre>" ;
*/


     

?>


<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'title',
		'description',
		// 'permissions',
	),
)); ?>


<?php if (Yii::app()->user->hasFlash("roles_saved")) { ?>
    <div class="alert warning" style="color: green;"><?php echo Yii::app()->user->getFlash("roles_saved") ?></div>
<?php } ?>


    
   <?php 
   
  if($model->id==1){ // super admin
      ?>
    <div> the super admin have acces  to all modules  </div>
    <?php
      
  }  else {
      ?>
    
    <form method="post">

    <div class="control-group " style="position:relative;margin-top: 20px;">
        <label class="control-label"><?php echo Yii::t('translate', 'Employees'); ?> </label>
        <div>
            <?php
            $criteria = new CDbCriteria;
           // $criteria->condition = 'groups_id=2'; // admin not superadmin or normal user 
     $criteria->condition = 'id != 1'; // admin not superadmin or normal user 
            //$criteria->addCondition('enable_login = 1');
           // $criteria->addCondition('id != 1');
            $this->widget('Select2', array(
                'model' => $role_users,
                'attribute' => 'user_id',
                'data' => CHtml::listData(User::model()->findAll($criteria), 'id', 'username'),
                'htmlOptions' => array('class' => "span3", 'multiple' => 'multiple'),
            ));
            ?>
        </div>
    </div>

  <div  class="seprate"></div>

    <?php
    $privs = array();
    if ($model->permissions) {
        $privs = unserialize($model->permissions);
    }
    ?>
    <div id="collapse4" class="body">
        <table id="dataTable" class="table table-bordered table-condensed table-hover table-striped">
            <thead>
                <tr>
                    <th><?php echo Yii::t('translate', 'Modules'); ?></th>
                    <?php foreach ($fields as $field) { ?>
                        <?php $count++; ?>
                        <th><?php echo $field->title; ?></th>
                    <?php } ?>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($modules as $module) { ?>
                    <tr>
                        <td><?php echo $module->title; ?></td>
                        <?php for ($i = 0; $i < count($fields); $i++) { ?>
                            <?php
                            $value = "";
                            if (isset($privs[$module->url][$fields[$i]->real_action])) {
                                $value = $privs[$module->url][$fields[$i]->real_action];
                            }

                            $h_opt = array(
                                'id' => $module->url . "_change",
                                'class' => 'span11 mods_acc'
                            );
                            if ($i > 0) {
                                $h_opt = array(
                                    'class' => 'span11 ' . $module->url . "_change"
                                );
                            }
                            ?>
                            <td>
                                <?php echo CHtml::dropDownList("role_" . $fields[$i]->id . '_' . $module->id, $value, CHtml::listData(RoleFieldValue::model()->findAll(array('condition' => 'field_id=' . $fields[$i]->id)), 'id', 'title'), $h_opt); ?>
                            </td>
                        <?php } ?>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
        <div>
            <input type="submit" value="<?php echo Yii::t('translate', 'Save'); ?>" class="btn btn-primary" name="save" style="float: right;" />
            <div style="clear: both;"></div>
        </div>
    </div>
</form>
    
    
    <?php
  }
   
   
   ?> 
    

<script>
    $("document").ready(function() {
        $(".mods_acc").change(function() {
            var ths = $(this);
            if (ths.val() == 2) {
                $("." + ths.attr("id")).children("option:first-child").prop("selected", true);
            } else {
                $("." + ths.attr("id")).children("option:last-child").prop("selected", true);
            }
        });
    });
</script>
