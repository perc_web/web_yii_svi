<?php
$this->breadcrumbs=array(
	'Roles'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Role','url'=>array('index')),
);
?>

<?php $this->pageTitlecrumbs = 'Create Role';?>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>