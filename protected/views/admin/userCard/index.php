<?php
$this->breadcrumbs=array(
	'User Cards'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List UserCard','url'=>array('index','user_id' => $model->user_id)),
	array('label'=>'Create UserCard','url'=>array('create','user_id' => $model->user_id)),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('user-card-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<!--<h3>Manage </h3>-->

<?php $this->pageTitlecrumbs = 'Manage User Cards';?>

<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'user-card-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
    	//'type'=>'striped  condensed',
	'columns'=>array(
            
            'user_id' => array(
                    'name' => 'user_id',
                    'value' => '$data->user->username',
                    'filter'=>CHtml::listData(User::model()->findAll(), 'id', 'username'),
                   
                ),
		'card_name',
		'valid_for',
		'usage',
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>
