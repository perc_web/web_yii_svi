<?php
$this->breadcrumbs=array(
	'User Cards'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List UserCard','url'=>array('index','user_id' => $model->user_id)),
	array('label'=>'Create UserCard','url'=>array('create','user_id' => $model->user_id)),
	array('label'=>'Update UserCard','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete UserCard','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
);
?>

<?php $this->pageTitlecrumbs = 'View UserCard #'. $model->id; ?>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'user_id' => array(
                    'name' => 'user_id',
                    'value' => $model->user->username,
                ),
		'card_name',
		'valid_for',
		'usage',
	),
)); ?>
