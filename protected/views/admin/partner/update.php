<?php
$this->breadcrumbs=array(
	'Partners'=>array('index'),
	$model->title=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Partner','url'=>array('index')),
	array('label'=>'Create Partner','url'=>array('create')),
	array('label'=>'View Partner','url'=>array('view','id'=>$model->id)),
);
?>

<?php $this->pageTitlecrumbs = 'Update Partner " '. $model->title.' "'; ?>
<?php echo $this->renderPartial('_form',array('model'=>$model, 'gallery' => $gallery)); ?>