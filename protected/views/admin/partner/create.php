<?php
$this->breadcrumbs=array(
	'Partners'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Partner','url'=>array('index')),
);
?>

<?php $this->pageTitlecrumbs = 'Create Partner';?>

<?php echo $this->renderPartial('_form', array('model'=>$model, 'gallery' => $gallery)); ?>