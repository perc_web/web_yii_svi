<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
        'type'=>'horizontal',
	'method'=>'get',
)); ?>

	<?php echo $form->textFieldRow($model,'id',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'title',array('class'=>'span5','maxlength'=>100)); ?>

	<?php echo $form->textFieldRow($model,'parent_id',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'url',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'show',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'employee_id',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'company_id',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'permission',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'sort_num',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'table_name',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'field',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'appear_access',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'show_appear',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'is_report',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'icon',array('class'=>'span5','maxlength'=>255)); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>'Search',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
