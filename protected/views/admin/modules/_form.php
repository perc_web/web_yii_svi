<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'modules-form',
	'enableAjaxValidation'=>false,
	'type'=>'horizontal',
)); ?>

	<p class="help-block">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldRow($model,'title',array('class'=>'span5','maxlength'=>100)); ?>

	<?php  // echo $form->textFieldRow($model,'parent_id',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'url',array('class'=>'span5','maxlength'=>255)); ?>

	<?php  // echo $form->textFieldRow($model,'show',array('class'=>'span5')); ?>

	<?php // echo $form->textFieldRow($model,'employee_id',array('class'=>'span5')); ?>

	<?php // echo $form->textFieldRow($model,'company_id',array('class'=>'span5')); ?>

	<?php //  echo $form->textFieldRow($model,'permission',array('class'=>'span5')); ?>

	<?php // echo $form->textFieldRow($model,'sort_num',array('class'=>'span5')); ?>

	<?php // echo $form->textFieldRow($model,'table_name',array('class'=>'span5','maxlength'=>255)); ?>

	<?php  //  echo $form->textFieldRow($model,'field',array('class'=>'span5','maxlength'=>255)); ?>

	<?php  // echo $form->textFieldRow($model,'appear_access',array('class'=>'span5','maxlength'=>255)); ?>

	<?php  // echo $form->textFieldRow($model,'show_appear',array('class'=>'span5')); ?>

	<?php //  echo $form->textFieldRow($model,'is_report',array('class'=>'span5')); ?>

	<?php  // echo $form->textFieldRow($model,'icon',array('class'=>'span5','maxlength'=>255)); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Create' : 'Save',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
