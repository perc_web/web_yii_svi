<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'jobs-form',
	'enableAjaxValidation'=>false,
	'type'=>'horizontal',
)); ?>

	<p class="help-block">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<?php // echo $form->textFieldRow($model, 'title', array('class' => 'span5', 'maxlength' => 255)); ?>
<?php echo $form->label($model, 'title', array('class' => 'form-label', 'for' => 'normal')) ;
      echo EMHelper::megaOgogo($model, 'title', array('class' => 'form-control', 'maxlength' => 255)) ; ?>
<br /><br />
<!--<div class="control-group ">-->
    <!--<label class="control-label" for="Pages_intro">Description</label>-->
    <!--<div class="controls">-->
        <?php echo $form->label($model, 'description', array('class' => 'form-label', 'for' => 'normal')) ;?>
        <?php echo EMHelper::megaOgogo($model, 'description', array(), 'floara'); ?>
        
	<?php echo $form->checkBoxRow($model,'approved'); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Create' : 'Save',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
