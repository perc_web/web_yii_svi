<?php
$this->breadcrumbs=array(
	'Jobs'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Jobs','url'=>array('index')),
	array('label'=>'Create Jobs','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('jobs-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<!--<h3>Manage </h3>-->

<?php $this->pageTitlecrumbs = 'Manage Jobs';?>

<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'jobs-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'title',
		'created_date',
		'approved' => array(
                    'name' => 'approved',
                    'value' => '($data->approved == 1)? "Approved" : "Not Approved"',
                    'filter' => array(1 => 'Approved', 0 => 'Not Approved'),
                ),
                
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>
