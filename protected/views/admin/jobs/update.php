<?php
$this->breadcrumbs=array(
	'Jobs'=>array('index'),
	$model->title=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Jobs','url'=>array('index')),
	array('label'=>'Create Jobs','url'=>array('create')),
	array('label'=>'View Jobs','url'=>array('view','id'=>$model->id)),
);
?>

<?php $this->pageTitlecrumbs = 'Update Jobs " '. $model->title.' "'; ?>
<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>