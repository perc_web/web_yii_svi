<?php
$this->breadcrumbs=array(
	'Menus'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Menu','url'=>array('index','outlet_id' => $model->outlet_id)),
	array('label'=>'Create Menu','url'=>array('create','outlet_id' => $model->outlet_id)),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('menu-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<!--<h3>Manage </h3>-->
<?php
if($model->outlet->id &&   $model->outlet->partner->id){
    $outlet_link=Yii::app()->baseUrl."/admin/outlet/view/id/".$model->outlet->id  ;
     $partner_link=Yii::app()->baseUrl."/admin/partner/view/id/".$model->outlet->partner->id  ;
     $this->pageTitlecrumbs = 'Manage Menus for outlet :  <a  href="'.$outlet_link.'">'.$model->outlet->title.'</a>  for partner:   <a  href="'.$partner_link.'">' . $model->outlet->partner->title.' </a>';
}  else {
     $this->pageTitlecrumbs = 'Manage Menus';
}
?>


<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'menu-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	
    	//'type'=>'striped  condensed',
	'columns'=>array(
		'id',
            /*
            'outlet_id' => array(
                    'name' => 'outlet_id',
                    'value' => '$data->outlet->title',
                    'filter' => CHtml::listData(Outlet::model()->findAll(), 'id', 'title'),
                ),
		*/
		// 'description',
		
		/*
            'active' => array(
                    'name' => 'active',
                    'value' => '($data->active == 1)? "Yes" : "No"',
                    'filter' => array(1 => "Yes", 0 => "No"),
                ),
            
            'featured' => array(
                    'name' => 'featured',
                    'value' => '($data->featured == 1)? "Yes" : "No"',
                    'filter' => array(1 => "Yes", 0 => "No"),
                ),
            
		'hot_deal' => array(
                    'name' => 'hot_deal',
                    'value' => '($data->hot_deal == 1)? "Yes" : "No"',
                    'filter' => array(1 => "Yes", 0 => "No"),
                ),
		'new_deal' => array(
                    'name' => 'new_deal',
                    'value' => '($data->new_deal == 1)? "Yes" : "No"',
                    'filter' => array(1 => "Yes", 0 => "No"),
                ),
            
            'new_deal' => array(
                    'name' => 'recomended',
                    'value' => '($data->recomended == 1)? "Yes" : "No"',
                    'filter' => array(1 => "Yes", 0 => "No"),
                ),
            */
            
            /*
               array(
            'header' => 'Items',
            'type' => 'raw',
            'value' => 'CHtml::link("Items", Yii::app()->baseUrl."/admin/menuItem/index/menu_id/".$data->id)',
        ),
            
     array(
            'header' => 'Tags',
            'type' => 'raw',
            'value' => 'CHtml::link("Tags", Yii::app()->baseUrl."/admin/itemTag/index/item_id/".$data->id)',
        ),
            
            
             array(
            'header' => 'Reviews',
            'type' => 'raw',
            'value' => 'CHtml::link("Reviews", Yii::app()->baseUrl."/admin/reviews/index/item_id/".$data->id)',
        ),
            */
	
            array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>
