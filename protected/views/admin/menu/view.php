<?php
$this->breadcrumbs=array(
	'Menus'=>array('index'),
	$model->title,
);

$this->menu=array(
	array('label'=>'List Menu','url'=>array('index','outlet_id' => $model->outlet_id)),
	array('label'=>'Create Menu','url'=>array('create','outlet_id' => $model->outlet_id)),
	array('label'=>'Update Menu','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete Menu','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
);
?>

<?php
if($model->outlet->id &&   $model->outlet->partner->id){
    $outlet_link=Yii::app()->baseUrl."/admin/outlet/view/id/".$model->outlet->id  ;
     $partner_link=Yii::app()->baseUrl."/admin/partner/view/id/".$model->outlet->partner->id  ;
     $this->pageTitlecrumbs = 'View Menu: '. $model->id;
     $this->pageTitlecrumbs .= ' for outlet :  <a  href="'.$outlet_link.'">'.$model->outlet->title.'</a>  for partner:   <a  href="'.$partner_link.'">' . $model->outlet->partner->title.' </a>';
}  else {
    $this->pageTitlecrumbs = 'View Menu: '. $model->id;
}
?>


<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
            /*
             array(
			'name' => 'outlet_id',
			'value' => $model->outlet->title  
		),
             * */
             

            /*
		'title',
              'intro',
		'description',
		'price',
             'discount_type' => array(
                    'name' => 'discount_type',
                    'value' => ($model->discount_type == 1)? "Percentage" : "Fixed Amount",
                ),
                'discount',
            
              array(
            'name' => 'active',
            'value' => $model->active == 1 ? "active" : "Not active",
        ),
            
              array(
            'name' => 'featured',
            'value' => $model->featured == 1 ? "Yes" : "No",
        ),
            
            	'hot_deal' => array(
                    'name' => 'hot_deal',
                    'value' => ($model->hot_deal == 1)? "Yes" : "No",
                ),
		'new_deal' => array(
                    'name' => 'new_deal',
                    'value' => ($model->new_deal == 1)? "Yes" : "No",
                ),
                'recomended' => array(
                    'name' => 'recomended',
                    'value' => ($model->recomended == 1)? "Yes" : "No",
                ),
            
              array(
            'name'=>'image',
            'type'=>'raw',
            'value'=>CHtml::image(Yii::app()->request->baseUrl.'/media/menus/'.$model->image,$model->image,array('width'=>250)),
        ),
          
            */
	),
)); ?>


<!--Items-->

<div >
    <header class="acc_col_head">
        <div class="icons"><i class="icon-eye-open"></i></div>
            <a href="<?php echo Yii::app()->baseUrl  ?>/admin/menuItem/index/menu_id/<?php echo $model->id ; ?>" >
        <h5 style="color: rgb(255, 255, 255);"><?= Yii::t('translate', 'Items') ?></h5>
        </a>
        <div class="toolbar">
             <a  href="<?php echo Yii::app()->baseUrl  ?>/admin/menu/update/id/<?php echo $model->id ; ?>">
                                    <i class="icon-plus"></i>
                                    </a>
            
<!--             <a  href="<?php echo Yii::app()->baseUrl  ?>/admin/menuItem/index/menu_id/<?php echo $model->id ; ?>">
                                    <i class="icon-th-list"></i>
               </a>-->
            
             <a class="accordion-toggle minimize-box collapsed" data-toggle="collapse" href="#partner_items">
                        <i class="icon-chevron-down"></i>
                    </a>
            
        </div>
    </header>

    <div id="partner_items" style="height: 10px;" class="collapse">

        <?php
        $this->widget('bootstrap.widgets.TbGridView', array(
            'id' => 'user-email-grid',
            'dataProvider' => Helper::getAll($model->id, 'MenuItem', 'menu_id'),
  
            'columns'=>array(
		
            
             'item_id' => array(
                    'name' => 'item_id',
                    'value' => '$data->item->title',
                    'filter' => CHtml::listData(Item::model()->findAll(), 'id', 'title'),
                ),
                 array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
                        'template' => '{view}',
                        'buttons'=>array(       
                                'view' => array(
                                  'url'=>'Yii::app()->controller->createUrl("/admin/menuItem/view", array("id"=>$data->id))',
                                ),
                        ),
		),
               
		
	),
        ));
        ?>



    </div>
</div>

<div  style="width:600px;min-height:10px;margin-left: 220px;clear: both"></div>

<!--reviews-->

<div >
    <header class="acc_col_head">
        <div class="icons"><i class="icon-eye-open"></i></div>
            <a href="<?php echo Yii::app()->baseUrl  ?>/admin/reviews/index/item_id/<?php echo $model->id ; ?>" >
        <h5 style="color: rgb(255, 255, 255);"><?= Yii::t('translate', 'Reviews') ?></h5>
        </a>
        <div class="toolbar">
             <a  href="<?php echo Yii::app()->baseUrl  ?>/admin/reviews/create/item_id/<?php echo $model->id ; ?>">
                                    <i class="icon-plus"></i>
                                    </a>
            
              <a  href="<?php echo Yii::app()->baseUrl  ?>/admin/reviews/index/item_id/<?php echo $model->id ; ?>">
                                    <i class="icon-th-list"></i>
               </a>
            
            <a class="accordion-toggle minimize-box collapsed" data-toggle="collapse" href="#menu_reviews">
                        <i class="icon-chevron-down"></i>
                    </a>
            
            
        </div>
    </header>

    <div id="menu_reviews" style="height: 10px;" class="collapse">

        <?php
        $this->widget('bootstrap.widgets.TbGridView', array(
            'id' => 'user-email-grid',
            'dataProvider' => Helper::getAll($model->id, 'Reviews', 'item_id'),
  
         'columns'=>array(
		'user_id' => array(
                    'name' => 'user_id',
                    'value' => '$data->user->username',
                    'filter'=>CHtml::listData(User::model()->findAll(), 'id', 'username'),
                   
                ),
		
         
		'price_rate',
		'value_rate',
		'quality_rate',
            
             'approved' => array(
                    'name' => 'approved',
                    'value' => '($data->approved == 1)? "Yes" : "No"',
                    'filter' => array(1 => 'Yes', 0 => 'No'),
                ),
		
             array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
                        'template' => '{view}',
                        'buttons'=>array(       
                                'view' => array(
                                  'url'=>'Yii::app()->controller->createUrl("/admin/reviews/view", array("id"=>$data->id))',
                                ),
                        ),
		),
               
	),
        ));
        ?>



    </div>
</div>