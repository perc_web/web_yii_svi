<?php
$this->breadcrumbs=array(
	'Menus'=>array('index'),
	$model->title=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Menu','url'=>array('index','outlet_id' => $model->outlet_id)),
	array('label'=>'Create Menu','url'=>array('create','outlet_id' => $model->outlet_id)),
	array('label'=>'View Menu','url'=>array('view','id'=>$model->id)),
);
?>

<?php $this->pageTitlecrumbs = 'Update Menu #'. $model->id; ?>
<?php echo $this->renderPartial('_form',array('model'=>$model, 'items'=>$items)); ?>