<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'user-form',
	'enableAjaxValidation'=>true,
	'type'=>'horizontal',
        'htmlOptions' => array(	'enctype' => 'multipart/form-data'),

)); ?>
<script>

$( document ).ready(function() {
	$('.row .btn').on('click', function(e) {
    e.preventDefault();
    var $this = $(this);
    var $collapse = $this.closest('.collapse-group').find('.collapse');
    $collapse.collapse('toggle');
	});

});
</script>


	<p class="help-block">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); 
        
        if(Yii::app()->user->hasFlash('Passchange')){
          
           echo Yii::app()->user->getFlash('Passchange');   
        }
        ?>
        <?php
        /*
            echo " <div class=\"control-group \">
               <label for=\"UserDetails_city\" class=\"control-label\">User Group</label>
                                <div class=\"controls\">";
           echo   $form->dropDownList($model,'groups_id',Groups::model()->getGroups());
           echo "</div> </div>";
           */
        ?>
        
        <?php
                $roleuser = RoleUser::model()->findByAttributes(array('user_id' => $model->id));
                 

        ?>
      
        <?php  
           echo " <div class=\"control-group \">
             <label for=\"UserDetails_city\" class=\"control-label\">User role <span class=\"required\">*</span></label>
                              <div class=\"controls\">"; 
          $data= Chtml::listData(Role::model()->findAll(), 'id', 'title');
        echo CHtml::dropDownList('user_role', $roleuser->role_id, $data , array('required' => 'required', 'prompt' => 'select role' ));
        echo "</div> </div>";

            ?>
        
	<?php echo $form->textFieldRow($model,'username',array('class'=>'span5','maxlength'=>50)); ?>

  	<?php echo $form->textFieldRow($model,'email',array('class'=>'span5','maxlength'=>255)); ?>
        
	<?php echo $form->textFieldRow($model,'fname',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'lname',array('class'=>'span5','maxlength'=>255)); ?>
        
        <?php echo $form->textFieldRow($model,'full_name',array('class'=>'span5','maxlength'=>255)); ?>
        
        <?php echo $form->textFieldRow($model,'phone',array('class'=>'span5','maxlength'=>255)); ?>
        

        <?php   
        $start_year = date('Y-m-d', strtotime('-16 year'));
        ?>
        <div class="control-group">
    <?php echo $form->labelEx($model, 'age', array('class' => 'control-label')); ?>
    <div class='controls'>
        <?php
        $this->widget('zii.widgets.jui.CJuiDatePicker', array(
            'name' => 'age',
            'attribute' => 'age',
            'model' => $model,
            'options' => array(
                'dateFormat' => 'yy-m-d',
               // 'altFormat' => 'dd-mm-yy',
                'changeMonth' => true,
                'changeYear' => true,
                 'maxDate'=>$start_year,
                'yearRange'=>'-100:+0', // range of years (1914-2014) and its dynamic
               // 'appendText' => 'yyyy-mm-dd',
            ),
                )
        );
        ?>
    </div>
</div>
        
        <div class="control-group ">
            <?php echo $form->labelEx($model, 'country', array('class' => 'control-label')) ?>
            <?php
            $this->widget('Select2', array(
                'model' => $model,
                'attribute' => 'country',
                'data' => CHtml::listData(AllCountries::model()->findAll(), 'id', 'country_name'),
                'htmlOptions' => array('class' => "span4", "empty" => "No country selected"),
            ));
            ?>
        </div>
        
        <div class="control-group ">
            <?php echo $form->labelEx($model, 'payment_method', array('class' => 'control-label')) ?>
            <?php
            $this->widget('Select2', array(
                'model' => $model,
                'attribute' => 'payment_method',
                'data' => array(1 => 'Online', 2 => 'Cash On Deliver'),
                'htmlOptions' => array('class' => "span4"),
            ));
            ?>
        </div>
        
        <?php echo $form->textFieldRow($model,'address',array('class'=>'span5','maxlength'=>255)); ?>
         <?php echo $form->textFieldRow($model,'city',array('class'=>'span5','maxlength'=>255)); ?>
         <?php echo $form->textFieldRow($model,'state',array('class'=>'span5','maxlength'=>255)); ?>
         <?php echo $form->textFieldRow($model,'post_code',array('class'=>'span5','maxlength'=>255)); ?>

        
        <?php
        echo $form->fileFieldRow($model, 'image', array('class' => 'span5', 'maxlength' => 255));
        if ($model->isNewRecord != '1'){
        ?>
            <div class="control-group ">
                <div class="controls">
                    <p id='image-cont'> <?php echo Chtml::image(Yii::app()->baseUrl . '/media/members/' . $model->image, '', array('width' => 200)); ?></p>
                </div>
            </div>
        <?php
            }
        ?>

	<?php echo $form->textAreaRow($model,'details',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>

	<?php echo $form->checkboxRow($model,'active'); ?>
        
        
	<?php 
        if($model->id >0){
        ?>
        <div class="row control-group">
            <div class="collapse-group controls">
                <p><a class="btn control-label" href="#">Change password</a></p>
                <p class="collapse">
                    <label class="control-label">Old Password :</label>
                    <span class="controls"> 
                        <?php echo  CHtml::passwordField('User[oldpassword]'); ?>
                    </span>
                    <br><br>
                    <label class="control-label"> New Password:</label>
                    <span class="controls">
                        <?php echo  CHtml::passwordField('User[newpassword]'); ?>
                    </span>
                    <br><br>
                    <label class="control-label">New Password repeat: </label>
                    <span class="controls">
                        <?php echo  CHtml::passwordField('User[newpassword_repeat]'); ?>
                    </span>	
                </p>
            </div>
        </div>
        <?php 
        }else{
          echo $form->passwordFieldRow($model,'password',array('class'=>'span5','maxlength'=>90)); 
          
	  echo $form->passwordFieldRow($model,'password_repeat',array('class'=>'span5','maxlength'=>90));
        }
        ?>
        
	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Create' : 'Save',
		)); ?>
	</div>

<?php $this->endWidget(); ?>

