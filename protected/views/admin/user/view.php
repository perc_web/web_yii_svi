<?php
$this->breadcrumbs=array(
	'Users'=>array('index'),
	$model->username.' Profile',
);

$this->menu=array(
	array('label'=>'List User','url'=>array('index')),
	array('label'=>'Create User','url'=>array('create')),
	array('label'=>'Update User','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete User','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
);
?>

<?php $this->pageTitlecrumbs = 'View User "'. $model->username.' "'; ?>
<?php     //  $roleuser = RoleUser::model()->findByAttributes(array('user_id' => $model->id)); ?>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
            
            array(
                'label'=>'role',
                'value'=>User::RoleName($model->id)
            ),
            
            
            'username',
            'email',
            'fname',
            'lname',
            
            
            'address',
            'post_code',
            'city',
            'state',
            'details',
            'age',
            
             array(
			'name' => 'counrty',
			'value' => $model->country0->country_name 
		),
            'last_login_ip',
            'last_login_country',
            
            array(
                'name'=>'image',
                'type'=>'raw',
                'value'=>CHtml::image(Yii::app()->request->baseUrl.'/media/members/'.$model->image,$model->username,array('width'=>250)),
            ),
            
	),
)); ?>
