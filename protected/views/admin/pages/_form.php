<?php
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id' => 'pages-form',
        'type' => 'horizontal',
        'enableAjaxValidation' => false,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
    ));
?>

<p class="help-block">Fields with <span class="required">*</span> are required.</p>

<?php echo $form->errorSummary($model); ?>

<?php echo $form->label($model, 'title', array('class' => 'form-label', 'for' => 'normal')) ;
      echo EMHelper::megaOgogo($model, 'title', array('class' => 'form-control', 'maxlength' => 255)) ; ?>
<br /><br />
<?php // echo $form->textAreaRow($model, 'intro', array('rows' => 4, 'cols' => 20, 'class' => 'span6')); ?>
<?php echo $form->label($model, 'intro', array('class' => 'form-label', 'for' => 'normal')); ?>
<?php echo EMHelper::megaOgogo($model, 'intro', array('rows' => 4, 'cols' => 20,'class' => 'span6', 'maxlength' => 1000), 'textArea'); ?>

        <?php echo $form->label($model, 'details', array('class' => 'form-label', 'for' => 'normal')) ;?>
        <?php echo EMHelper::megaOgogo($model, 'details', array(), 'floara'); ?>

<?php echo $form->fileFieldRow($model, 'image', array('class' => 'span5', 'maxlength' => 255));
?>

<?php
if(!$model->isNewRecord and $model->image)
{
?>
<div class="control-group ">
    <label class="control-label" for="Pages_intro">Image</label>
    <div class="controls">
        <p id='image-cont'> <?php echo Chtml::image(Yii::app()->baseUrl . '/media/pages/' . $model->image, 'image', array('width' => 200)); ?></p>
        <?php
        echo CHtml::ajaxLink(
                'Delete Image', array('/admin/pages/deleteimage/id/' . $model->id), array(
            'success' => 'function(data){
                        //var obj = jQuery.parseJSON(data);
                        if(data =="done"){
                           document.getElementById("image-cont").innerHTML=" Image Deleted";
                       }
                    }'
                ), array('class' => 'left0px')
        );
        ?>
    </div>
</div>
<?php
}
?>

<?php echo $form->textFieldRow($model, 'video', array('class' => 'span15', 'maxlength' => 255, 'prepend' => 'http')); ?>

<?php echo $form->checkboxRow($model, 'publish'); ?>

<div class="form-actions">
    <?php
    $this->widget('bootstrap.widgets.TbButton', array(
        'buttonType' => 'submit',
        'type' => 'primary',
        'label' => $model->isNewRecord ? 'Create' : 'Save',
    ));
    ?>
</div>

<?php $this->endWidget(); ?>
