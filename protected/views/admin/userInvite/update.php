<?php
$this->breadcrumbs=array(
	'User Invites'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List UserInvite','url'=>array('index')),
	array('label'=>'Create UserInvite','url'=>array('create')),
	array('label'=>'View UserInvite','url'=>array('view','id'=>$model->id)),
);
?>

<?php $this->pageTitlecrumbs = 'Update UserInvite #'. $model->id; ?>
<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>