<?php
$this->breadcrumbs=array(
	'User Invites'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List UserInvite','url'=>array('index')),
);
?>

<?php $this->pageTitlecrumbs = 'Create UserInvite';?>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>