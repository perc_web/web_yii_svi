<?php
$this->breadcrumbs=array(
	'User Invites'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List UserInvite','url'=>array('index')),
	array('label'=>'Create UserInvite','url'=>array('create')),
	array('label'=>'Update UserInvite','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete UserInvite','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
);
?>

<?php $this->pageTitlecrumbs = 'View UserInvite #'. $model->id; ?>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
            'user_id' => array(
                    'name' => 'user_id',
                    'value' => $model->user->username,
                ),
		'name',
		'email',
	),
)); ?>
