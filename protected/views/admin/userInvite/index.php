<?php
$this->breadcrumbs=array(
	'User Invites'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List UserInvite','url'=>array('index')),
	array('label'=>'Create UserInvite','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('user-invite-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<!--<h3>Manage </h3>-->

<?php $this->pageTitlecrumbs = 'Manage User Invites';?>

<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'user-invite-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
    	//'type'=>'striped  condensed',
	'columns'=>array(
            
             'user_id' => array(
                    'name' => 'user_id',
                    'value' => '$data->user->username',
                    'filter'=>CHtml::listData(User::model()->findAll(), 'id', 'username'),
                   
                ),
		'name',
		'email',
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>
