<?php
$this->breadcrumbs=array(
	'Subscribtions'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Subscribtion','url'=>array('index')),
	array('label'=>'Create Subscribtion','url'=>array('create')),
	array('label'=>'View Subscribtion','url'=>array('view','id'=>$model->id)),
);
?>

<?php $this->pageTitlecrumbs = 'Update Subscribtion #'. $model->id; ?>
<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>