<?php
$this->breadcrumbs=array(
	'Subscribtions'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Subscribtion','url'=>array('index')),
	array('label'=>'Create Subscribtion','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('subscribtion-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<!--<h3>Manage </h3>-->

<?php $this->pageTitlecrumbs = 'Manage Subscribtions';?>

<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'subscribtion-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'email',
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>
