<?php
$this->breadcrumbs=array(
	'User Requests'=>array('index'),
	$model->title=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List UserRequests','url'=>array('index')),
	array('label'=>'Create UserRequests','url'=>array('create')),
	array('label'=>'View UserRequests','url'=>array('view','id'=>$model->id)),
);
?>

<?php $this->pageTitlecrumbs = 'Update UserRequests #'. $model->id; ?>
<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>