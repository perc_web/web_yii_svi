<?php
$this->breadcrumbs=array(
	'User Requests'=>array('index'),
	$model->title,
);

$this->menu=array(
	array('label'=>'List UserRequests','url'=>array('index')),
	array('label'=>'Create UserRequests','url'=>array('create')),
	array('label'=>'Update UserRequests','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete UserRequests','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
);
?>

<?php $this->pageTitlecrumbs = 'View UserRequests #'. $model->id; ?>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'title',
		'address',
                'user_id' => array(
                    'name' => 'user_id',
                    'value' => $model->user->username,
                )
	),
)); ?>
