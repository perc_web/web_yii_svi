<?php 
ob_start();

// change the following paths if necessary
$yii=dirname(__FILE__).'/../yii115/framework/yii.php';

$config=dirname(__FILE__).'/protected/config/main.php';

// remove the following lines when in production mode
defined('YII_DEBUG') or define('YII_DEBUG',true);
// specify how many levels of call stack should be shown in each log message
defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL',3);

require_once($yii);
Yii::createWebApplication($config)->run();
  
  $users=User::model()->findAll();
  foreach($users as $u){
    if((time()-strtotime($u->last_login)) >= (5*60)){
      $u->login=0;
      $u->save(false);
    }
  }
  
  if(Yii::app()->user->id){
    $us=User::model()->findByPk(Yii::app()->user->id);
    $us->login=1;
    $us->last_login=date('Y-m-d H:i:s');
    $us->save(false);
  }
  
?>
