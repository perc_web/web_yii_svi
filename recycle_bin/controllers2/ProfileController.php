<?php

class ProfileController extends FrontController {

    /**
     * Declares class-based actions.
     */
    public function actions() {
        return array(
            // captcha action renders the CAPTCHA image displayed on the contact page
            'captcha' => array(
                'class' => 'CCaptchaAction',
                'backColor' => 0xFFFFFF,
            ),
        );
    }

    /**
     * This is the default 'index' action that is invoked
     * when an action is not explicitly requested by users.
     */
    public function actionIndex() {
        $user = User::model()->findByPk(Yii::app()->user->id);
        $this->render('index', array('user' => $user));
    }
    public function actionusageInfo(){
         $user = User::model()->findByPk(Yii::app()->user->id);
         if($user){
             
                    $criteira = new CDbCriteria;
                    $cond= 'user_id="'.$user->id.'"';
                    $criteira->condition = $cond;
                    $criteira->order = 'id DESC';
                    $cards = UserCard::model()->findAll($criteira);
                   
                $this->render('usage_info',array('cards'=>$cards));
             
         }else {
            throw new CHttpException(404, 'The requested page does not exist.');
        }
    }
    public function actionEditProfile(){
        $user = User::model()->findByPk(Yii::app()->user->id);
        if(isset($_POST['User'])){
            $user->attributes = $_POST['User'];
            $user->save();
        }
        $this->render('edit_profile', array('user' => $user));
    }
   
    
      public function actionChangepassword() {
        $model = User::model()->findByPk(Yii::app()->user->id);
        if ($model) {

            // change password
            $savedpass = $validatepass = 0;
            if ($_POST['User']['oldpassword'] != '') {
                $savedpass = $model->password;

                $validatepass = $model->hash($_POST['User']['oldpassword']);
                $newpassword = $model->hash($_POST['User']['newpassword']);
                $newpassword_repeat = $model->hash($_POST['User']['newpassword_repeat']);
                //   echo   $validatepass ; die ;
                if ($savedpass == $validatepass AND $newpassword == $newpassword_repeat) {
                    $model->password = User::simple_encrypt($_POST['User']['newpassword']);


                    if( $model->save()){
                         Yii::app()->user->setFlash('update-success', 'Your information has been Updated Successfully.');
                          $this->redirect(array('profile/index')); 
                         
                    }  else {
                         Yii::app()->user->setFlash('update-error', 'Please complete your information correctly.');
                    }
                   
                   
                } else {
                    Yii::app()->user->setFlash('update-error', 'Please complete your information correctly.');
                }
            }
            
            
            

            $this->render('change_password', array('model' => $model));
        } else {
            throw new CHttpException(404, 'The requested page does not exist.');
        }
    }
    
    
    public function actionRecommendedPlaces() {
         $user = User::model()->findByPk(Yii::app()->user->id);
        if ($user) {
            if( $user->interested_category){
                     $cat_arr = explode(',', $user->interested_category);
                     $criteira = new CDbCriteria;
                   //  $cond= 'cat_id in (' . implode(",", $cat_arr) . ')  ';
                    $cond= 'subcat_id LIKE "%'.$user->interested_category.'%"';
                    $criteira->condition = $cond;
                     $criteira->order = 'id DESC';
                    $partners = Partner::model()->findAll($criteira); 
            }
                  
       
                    $this->render('recommended_places',array('partners'=>$partners));
        }else {
            throw new CHttpException(404, 'The requested page does not exist.');
        }
        
        
       
    }
   public function actionRecommendFriend() {
          $user = User::model()->findByPk(Yii::app()->user->id);
           if ($user) {
               
                $model=new UserInvite;
                $model->user_id=$user->id ;
		if(isset($_POST['UserInvite']))
		{
			$model->attributes=$_POST['UserInvite'];
                        if($model->save()){
                           Yii::app()->user->setFlash('invite', 'You Recommend a friend successsfully');
                        }
				
		}
                
                $this->render('recommend_friend',array('model'=>$model));
           }
       
        
       
    }
    
    
    public function actionspecialRequests() {
        $model = new UserRequests;
        $user = User::model()->findByPk(Yii::app()->user->id);
        if(isset($_POST['UserRequests'])){
            $model->attributes = $_POST['UserRequests'];
            $model->user_id = $user->id;
            if($model->save()){
                Yii::app()->user->setFlash('request', 'Your Rquest Has Been Sent');
                    $notification = new Notifications;
                    $notification->url = Yii::app()->getBaseUrl(true) . '/admin/userRequests/view/id/' . $model->id;
                    $notification->notification = 'New User Request From "'.$user->username.'"';
                    $notification->notifier_id = $user->id;
                    $notification->save();
            }
        }
        $this->render('special_requests', array('model' => $model));
    }
    
    public function actionNotYet(){
        $this->render('not_yet');
    }   
}
?>
