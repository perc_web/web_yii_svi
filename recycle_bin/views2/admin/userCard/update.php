<?php
$this->breadcrumbs=array(
	'User Cards'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List UserCard','url'=>array('index','user_id' => $model->user_id)),
	array('label'=>'Create UserCard','url'=>array('create','user_id' => $model->user_id)),
	array('label'=>'View UserCard','url'=>array('view','id'=>$model->id)),
);
?>

<?php $this->pageTitlecrumbs = 'Update UserCard #'. $model->id; ?>
<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>