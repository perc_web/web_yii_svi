<?php
$this->breadcrumbs=array(
	'User Cards'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List UserCard','url'=>array('index','user_id' => $model->user_id)),
);
?>

<?php $this->pageTitlecrumbs = 'Create UserCard';?>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>