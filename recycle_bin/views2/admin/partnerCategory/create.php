<?php
$this->breadcrumbs=array(
	'Partner Categories'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List PartnerCategory','url'=>array('index')),
);
?>

<?php $this->pageTitlecrumbs = 'Create PartnerCategory';?>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>