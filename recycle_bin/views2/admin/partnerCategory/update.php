<?php
$this->breadcrumbs=array(
	'Partner Categories'=>array('index'),
	$model->title=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List PartnerCategory','url'=>array('index')),
	array('label'=>'Create PartnerCategory','url'=>array('create')),
	array('label'=>'View PartnerCategory','url'=>array('view','id'=>$model->id)),
);
?>

<?php $this->pageTitlecrumbs = 'Update PartnerCategory #'. $model->id; ?>
<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>