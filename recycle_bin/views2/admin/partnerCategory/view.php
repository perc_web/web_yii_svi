<?php
$this->breadcrumbs=array(
	'Partner Categories'=>array('index'),
	$model->title,
);

$this->menu=array(
	array('label'=>'List PartnerCategory','url'=>array('index')),
	array('label'=>'Create PartnerCategory','url'=>array('create')),
	array('label'=>'Update PartnerCategory','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete PartnerCategory','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
);
?>

<?php $this->pageTitlecrumbs = 'View PartnerCategory #'. $model->id; ?>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'title',
	),
)); ?>
