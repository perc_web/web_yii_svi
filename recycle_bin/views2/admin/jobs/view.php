<?php
$this->breadcrumbs=array(
	'Jobs'=>array('index'),
	$model->title,
);

$this->menu=array(
	array('label'=>'List Jobs','url'=>array('index')),
	array('label'=>'Create Jobs','url'=>array('create')),
	array('label'=>'Update Jobs','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete Jobs','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
);
?>

<?php $this->pageTitlecrumbs = 'View Jobs " '. $model->title.' "'; ?>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'title',
		'description',
		'created_date',
		'approved' => array(
                    'name' => 'approved',
                    'value' => ($model->approved == 1)? "Approved" : "Not Approved",
                ),
	),
)); ?>
