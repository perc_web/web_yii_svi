<?php
$this->breadcrumbs=array(
	'Jobs'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Jobs','url'=>array('index')),
);
?>

<?php $this->pageTitlecrumbs = 'Create Jobs';?>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>