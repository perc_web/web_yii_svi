

<?php
/* @var $this SiteController */
/* @var $error array */

 $this->pageTitle=Yii::app()->name . ' - Error';
$this->breadcrumbs=array(
	'Error',
);
?>

<h2>Error <?php echo $code;   // [code] => 404 ?></h2>  

<div class="error">
<?php echo CHtml::encode($message);  //[message] => Unable to resolve the request "admin/pet23"  ?>
</div>

