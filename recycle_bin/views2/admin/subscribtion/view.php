<?php
$this->breadcrumbs=array(
	'Subscribtions'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Subscribtion','url'=>array('index')),
	array('label'=>'Create Subscribtion','url'=>array('create')),
	array('label'=>'Update Subscribtion','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete Subscribtion','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
);
?>

<?php $this->pageTitlecrumbs = 'View Subscribtion #'. $model->id; ?>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'email',
	),
)); ?>
