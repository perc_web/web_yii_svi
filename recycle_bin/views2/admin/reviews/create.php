<?php
$this->breadcrumbs=array(
	'Reviews'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Reviews','url'=>array('index' ,'item_id' => $model->item_id)),
);
?>

<?php $this->pageTitlecrumbs = 'Create Reviews';?>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>