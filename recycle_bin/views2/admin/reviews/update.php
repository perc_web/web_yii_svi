<?php
$this->breadcrumbs=array(
	'Reviews'=>array('index'),
	$model->title=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Reviews','url'=>array('index' ,'item_id' => $model->item_id)),
	array('label'=>'Create Reviews','url'=>array('create' ,'item_id' => $model->item_id)),
	array('label'=>'View Reviews','url'=>array('view','id'=>$model->id)),
);
?>

<?php $this->pageTitlecrumbs = 'Update Reviews #'. $model->id; ?>
<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>