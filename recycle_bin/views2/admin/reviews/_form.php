<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'reviews-form',
	'enableAjaxValidation'=>false,
	'type'=>'horizontal',
)); ?>

	<p class="help-block">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	    
        <div class="control-group ">
            <?php
            echo $form->labelEx($model, 'user_id', array('class' => 'control-label')) ?>
            <?php
            $this->widget('Select2', array(
                'model' => $model,
                'attribute' => 'user_id',
                'data' => CHtml::listData(User::model()->findAll(), 'id', 'username'),
                'htmlOptions' => array('class' => "span4", "empty" => "Select User"),
            ));
            ?>
        </div>
        
         <?php
        if(! $model->item_id){
           ?>
       <div class="control-group ">
            <?php
            echo $form->labelEx($model, 'item_id', array('class' => 'control-label')) ?>
            <?php
            $this->widget('Select2', array(
                'model' => $model,
                'attribute' => 'item_id',
                'data' => CHtml::listData(Menu::model()->findAll(), 'id', 'title'),
                'htmlOptions' => array('class' => "span4", "empty" => "Select menu"),
            ));
            ?>
        </div>
        <?php
        }
        
        ?>
      
        
	

	<?php echo $form->textFieldRow($model,'price_rate',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'value_rate',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'quality_rate',array('class'=>'span5')); ?>

	<?php echo $form->textAreaRow($model,'review',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>
        
      <?php echo $form->checkboxRow($model, 'approved', array('value' => 1, 'uncheckValue' => 0)); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Create' : 'Save',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
