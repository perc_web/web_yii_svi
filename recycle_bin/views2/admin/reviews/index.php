<?php
$this->breadcrumbs=array(
	'Reviews'=>array('index'),
	'Manage',
);

$this->menu=array( 
	array('label'=>'List Reviews','url'=>array('index' ,'item_id' => $model->item_id)),
	array('label'=>'Create Reviews','url'=>array('create' ,'item_id' => $model->item_id)),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('reviews-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<!--<h3>Manage </h3>-->
<?php
if($model->item->id ){
    $menu_link=Yii::app()->baseUrl."/admin/menu/view/id/".$model->item->id  ;
     $this->pageTitlecrumbs = 'Manage Reviews for menu :  <a  href="'.$menu_link.'">'.$model->item->title.'</a> ';
}  else {
     $this->pageTitlecrumbs = 'Manage Reviews';
}
?>


<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'reviews-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	
    	//'type'=>'striped  condensed',
	'columns'=>array(
		'user_id' => array(
                    'name' => 'user_id',
                    'value' => '$data->user->username',
                    'filter'=>CHtml::listData(User::model()->findAll(), 'id', 'username'),
                   
                ),
            
		/*
            'item_id' => array(
                    'name' => 'item_id',
                    'value' => '$data->item->title',
                    'filter'=>CHtml::listData(Menu::model()->findAll(), 'id', 'title'),
                   
                ),
            */
		'price_rate',
		'value_rate',
		'quality_rate',
            
             'approved' => array(
                    'name' => 'approved',
                    'value' => '($data->approved == 1)? "Yes" : "No"',
                    'filter' => array(1 => 'Yes', 0 => 'No'),
                ),
		/*
		'review',
		'date_created',
		'title',
		'approved',
		*/
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>
