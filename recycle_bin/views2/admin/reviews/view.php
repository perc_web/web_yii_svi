<?php
$this->breadcrumbs=array(
	'Reviews'=>array('index'),
	$model->title,
);

$this->menu=array(
	array('label'=>'List Reviews','url'=>array('index' ,'item_id' => $model->item_id)),
	array('label'=>'Create Reviews','url'=>array('create' ,'item_id' => $model->item_id)),
	array('label'=>'Update Reviews','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete Reviews','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
);
?>

<?php
if($model->item->id ){
    $menu_link=Yii::app()->baseUrl."/admin/menu/view/id/".$model->item->id  ;
     $this->pageTitlecrumbs = 'Manage Reviews for menu :  <a  href="'.$menu_link.'">'.$model->item->title.'</a> ';
}  else {
     $this->pageTitlecrumbs = 'View Reviews #'. $model->id;
}
?>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'user_id' => array(
                    'name' => 'user_id',
                    'value' => $model->user->username,
                ),
		'item_id' => array(
                    'name' => 'item_id',
                    'value' => $model->item->title,
                ),
		'price_rate',
		'value_rate',
		'quality_rate',
		'review',
            	
            'approved' => array(
                    'name' => 'approved',
                    'value' => ($model->approved == 1)? "Yes" : "No",
                ),
	),
)); ?>
