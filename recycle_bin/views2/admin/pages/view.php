<?php
$this->breadcrumbs=array(
	'Pages'=>array('index'),
	$model->title,
);

$this->menu=array(
	array('label'=>'List Pages','url'=>array('index')),
	array('label'=>'Update Pages','url'=>array('update','id'=>$model->id)),
);
?>

 <?php
$this->breadcrumbs=array(
    'Pages'=>array('index'),
    $model->title,
);

$this->menu=array(
    array('label'=>'List Pages','url'=>array('index')),
    array('label'=>'Update Pages','url'=>array('update','id'=>$model->id)),
);
?>

<?php $this->pageTitlecrumbs = 'View Pages "'. $model->title.' "'; ?>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'title',
		'intro',
		array(
                    'name'=>'details',
                    'type'=>'raw',
                ),
		array(
		'name'=>'image',
		'type'=>'raw',
		'value'=>CHtml::image(Yii::app()->request->baseUrl.'/media/pages/'.$model->image,$model->title,array('width'=>150)),
		),
		array(
                    'name'=>'publish',
                    'type'=>'raw',
                    'value'=>$model->getStatus($model->publish),
		),

	),
)); ?>



