<?php
$this->breadcrumbs=array(
	'User Requests'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List UserRequests','url'=>array('index')),
);
?>

<?php $this->pageTitlecrumbs = 'Create UserRequests';?>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>