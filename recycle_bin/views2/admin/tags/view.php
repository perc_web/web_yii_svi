<?php
$this->breadcrumbs=array(
	'Tags'=>array('index'),
	$model->title,
);

$this->menu=array(
	array('label'=>'List Tags','url'=>array('index')),
	array('label'=>'Create Tags','url'=>array('create')),
	array('label'=>'Update Tags','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete Tags','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
);
?>

<?php $this->pageTitlecrumbs = 'View Tags #'. $model->id; ?>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'title',
		'active' => array(
                    'name' => 'active',
                    'value' => ($model->active == 1)? "Yes" : "No",
                ),
	),
)); ?>
