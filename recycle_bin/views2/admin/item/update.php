<?php
$this->breadcrumbs=array(
	'Items'=>array('index'),
	$model->title=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Item','url'=>array('index','partner_id' => $model->partner_id)),
	array('label'=>'Create Item','url'=>array('create','partner_id' => $model->partner_id)),
	array('label'=>'View Item','url'=>array('view','id'=>$model->id)),
);
?>

<?php $this->pageTitlecrumbs = 'Update Item " '. $model->title.' "'; ?>
<?php echo $this->renderPartial('_form',array('model'=>$model,'gallery' => $gallery)); ?>