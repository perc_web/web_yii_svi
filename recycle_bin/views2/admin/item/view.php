<?php
$this->breadcrumbs=array(
	'Items'=>array('index'),
	$model->title,
);

$this->menu=array(
	array('label'=>'List Item','url'=>array('index','partner_id' => $model->partner_id)),
	array('label'=>'Create Item','url'=>array('create','partner_id' => $model->partner_id)),
	array('label'=>'Update Item','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete Item','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
);
?>

<?php
if($model->partner->id){
     $partner_link=Yii::app()->baseUrl."/admin/partner/view/id/".$model->partner->id  ;
      $this->pageTitlecrumbs = 'View Item: '. $model->title;
     $this->pageTitlecrumbs .= ' for  partner:  <a  href="'.$partner_link.'">'.$model->partner->title .'</a>';
}  else {
     $this->pageTitlecrumbs = 'View Item " '. $model->title.' "';
}
?>


<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
                'title',
		'partner_id' => array(
                    'name' => 'partner_id',
                    'value' => $model->partner->title,
                ),
		'cat_id' => array(
                    'name' > 'cat_id',
                    'value' => $model->cat->title,
                ),
                'price',
            /*
                'discount_type' => array(
                    'name' => 'discount_type',
                    'value' => ($model->discount_type == 1)? "Percentage" : "Fixed Amount",
                ),
		'discount_amount',
            */
                'quantity',
		'intro',
            'description' => array(
                    'name'=>'description',
                    'type'=>'raw',
                   
		),
		'image' => array(
                    'name'=>'image',
                    'type'=>'raw',
                    'value'=>CHtml::image(Yii::app()->request->baseUrl.'/media/items/'.$model->image,"",array('width'=>150)),
		),
            
            /*
		'active' => array(
                    'name' => 'active',
                    'value' => ($model->active == 1)? "Yes" : "No",
                ),
		'featured' => array(
                    'name' => 'featured',
                    'value' => ($model->featured == 1)? "Yes" : "No",
                ),
		'hot_deal' => array(
                    'name' => 'hot_deal',
                    'value' => ($model->hot_deal == 1)? "Yes" : "No",
                ),
		'new_deal' => array(
                    'name' => 'new_deal',
                    'value' => ($model->new_deal == 1)? "Yes" : "No",
                ),
                'recomended' => array(
                    'name' => 'recomended',
                    'value' => ($model->recomended == 1)? "Yes" : "No",
                ),
            */
            
	),
)); ?>


