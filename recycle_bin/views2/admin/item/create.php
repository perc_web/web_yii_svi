<?php
$this->breadcrumbs=array(
	'Items'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Item','url'=>array('index','partner_id' => $model->partner_id)),
);
?>

<?php $this->pageTitlecrumbs = 'Create Item';?>

<?php echo $this->renderPartial('_form', array('model'=>$model,'gallery' => $gallery)); ?>