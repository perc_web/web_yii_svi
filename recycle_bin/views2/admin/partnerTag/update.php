<?php
$this->breadcrumbs=array(
	'Partner Tags'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List PartnerTag','url'=>array('index','partner_id' => $model->partner_id)),
	array('label'=>'Create PartnerTag','url'=>array('create','partner_id' => $model->partner_id)),
	array('label'=>'View PartnerTag','url'=>array('view','id'=>$model->id)),
);
?>

<?php $this->pageTitlecrumbs = 'Update PartnerTag #'. $model->id; ?>
<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>