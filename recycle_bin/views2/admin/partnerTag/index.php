<?php
$this->breadcrumbs=array(
	'Partner Tags'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List PartnerTag','url'=>array('index','partner_id' => $model->partner_id)),
	array('label'=>'Create PartnerTag','url'=>array('create','partner_id' => $model->partner_id)),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('partner-tag-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<!--<h3>Manage </h3>-->
<?php
if($model->partner->title){
     $partner_link=Yii::app()->baseUrl."/admin/partner/view/id/".$model->partner->id  ;
     $this->pageTitlecrumbs = 'Manage Tags for partner :  <a  href="'.$partner_link.'">'.$model->partner->title .'</a>';
}  else {
     $this->pageTitlecrumbs = 'Manage Tags';
}
?>


<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'partner-tag-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
    	//'type'=>'striped  condensed',
	'columns'=>array(
		/*
            'partner_id' => array(
                    'name' => 'partner_id',
                    'value' => '$data->partner->title',
                    'filter' => CHtml::listData(Partner::model()->findAll(), 'id', 'title'),
                ),
            */
		'tag_id' => array(
                    'name' => 'tag_id',
                    'value' => '$data->tag->title',
                    'filter' => CHtml::listData(Tags::model()->findAll(), 'id', 'title'),
                ),
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>
