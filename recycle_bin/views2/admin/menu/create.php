<?php
$this->breadcrumbs=array(
	'Menus'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Menu','url'=>array('index','outlet_id' => $model->outlet_id)),
);
?>

<?php $this->pageTitlecrumbs = 'Create Menu';?>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>