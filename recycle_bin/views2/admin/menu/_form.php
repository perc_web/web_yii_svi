
<style>
    

.well {
    background-color: #DCDCDC;
    border: 1px solid #E3E3E3;
    border-radius: 4px;
    box-shadow: 0 1px 1px rgba(0, 0, 0, 0.05) inset;
    margin-bottom: 20px;
    min-height: 20px;
    padding: 19px;
}
    
</style>



<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'menu-form',
	'enableAjaxValidation'=>false,
	'type'=>'horizontal',
     'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>

	<p class="help-block">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

            <?php
        if(! $model->outlet_id){
           ?>
          <div class="control-group ">
            <?php echo $form->labelEx($model, 'outlet_id', array('class' => 'control-label')) ?>
            <?php
            $this->widget('Select2', array(
                'model' => $model,
                'attribute' => 'outlet_id',
                'data' => CHtml::listData(Outlet::model()->findAll(), 'id', 'title'),
                'htmlOptions' => array('class' => "span4", "empty" => "Select outlet"),
            ));
            ?>
        </div>
        
        <?php
        }
        
        ?>
        
     
        
        

	<?php  echo $form->hiddenField($model,'title',array('class'=>'span5','maxlength'=>255)); ?>

                <?php  // echo $form->textAreaRow($model,'intro',array('rows'=>4, 'cols'=>50, 'class'=>'span6')); ?>

	<?php  // echo $form->textAreaRow($model,'description',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>

        <?php   // echo $form->textFieldRow($model,'price',array('class'=>'span5','maxlength'=>10)); ?>
          <?php  // echo $form->radioButtonListRow($model,'discount_type',array(1 => 'Percentage', 2 => 'Fixed Amount')); ?>

        <?php  // echo $form->textFieldRow($model,'discount',array('class'=>'span5','maxlength'=>10)); ?>
        
        <?php 
        
       // echo $form->hiddenField($model, 'image', array('class' => 'span5', 'maxlength' => 255));
      
        
        ?>
        

     
	<?php  // echo $form->checkBoxRow($model,'active'); ?>

	<?php  // echo $form->checkBoxRow($model,'featured'); ?>

	<?php  // echo $form->checkBoxRow($model,'hot_deal'); ?>

	<?php  // echo $form->checkBoxRow($model,'new_deal'); ?>
        
        <?php  // echo $form->checkBoxRow($model,'recomended'); ?>
        
        
  <h5> menu items: </h5>
  <div  class="well" id="items">
      
<?php

 $menu_items=  $model->outlet->partner->items ;
  if($menu_items){
      $data=$menu_items ;
  }else{
      $data= array() ;
  }
  
 if($items){
  
     $i = 1;
      foreach ($items as $item) {
         echo "<div class='copy$i ' style='border: 1px solid buttonface; margin-bottom: 4px'>";
        echo " <div class=\"control-group \">
	<label for=\"UserDetails_city\" class=\"control-label\">Item</label>
			 <div class=\"controls\">";
        echo CHtml::dropDownList('item[]', $item->item->id, CHtml::listData($data, 'id', 'title'), array('id' => 'country' . $i, 'class' => 'span5 select_country', 'prompt' => "select item"));
        echo "</div> </div>";
        
    $i++;
   } 
 }
 ?>


 <div class="clone" style="border: 1px solid buttonface;">

         
        <?php
        echo " <div class=\"control-group \">
	<label for=\"UserDetails_city\" class=\"control-label\">Item</label>
			 <div class=\"controls\">";
        echo CHtml::dropDownList('item[]', $value->item->id, CHtml::listData($data, 'id', 'title'), array('class' => 'span5 select_country', 'prompt' => "select item"));
        echo "</div> </div>";
        ?>

       
    </div> 
      
      
<?php

$this->widget('ext.reCopy.ReCopyWidget', array(
    'targetClass' => 'clone',
));
?>

  </div>




        

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Create' : 'Save',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
