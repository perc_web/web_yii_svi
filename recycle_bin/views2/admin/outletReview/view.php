<?php
$this->breadcrumbs=array(
	'Outlet Reviews'=>array('index'),
	$model->title,
);

$this->menu=array(
	array('label'=>'List OutletReview','url'=>array('index','outlet_id' => $model->outlet_id)),
	array('label'=>'Create OutletReview','url'=>array('create','outlet_id' => $model->outlet_id)),
	array('label'=>'Update OutletReview','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete OutletReview','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
);
?>

<?php
if($model->outlet->id){
     $outlet_link=Yii::app()->baseUrl."/admin/outlet/view/id/".$model->outlet->id  ;
     $this->pageTitlecrumbs = 'View Reviews for outlet :  <a  href="'.$outlet_link.'">'.$model->outlet->title .'</a>';
}  else {
    $this->pageTitlecrumbs = 'View OutletReview #'. $model->id;
}
?>


<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		
            'user_id' => array(
                    'name' => 'user_id',
                    'value' => $model->user->username,
                ),
		'outlet_id' => array(
                    'name' => 'outlet_id',
                    'value' => $model->outlet->title,
                ),
		'price_rate',
		'value_rate',
		'quality_rate',
		'review',
            	
            'approved' => array(
                    'name' => 'approved',
                    'value' => ($model->approved == 1)? "Yes" : "No",
                ),
	),
)); ?>
