<?php
$this->breadcrumbs=array(
	'Outlet Reviews'=>array('index'),
	$model->title=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List OutletReview','url'=>array('index','outlet_id' => $model->outlet_id)),
	array('label'=>'Create OutletReview','url'=>array('create','outlet_id' => $model->outlet_id)),
	array('label'=>'View OutletReview','url'=>array('view','id'=>$model->id)),
);
?>

<?php $this->pageTitlecrumbs = 'Update OutletReview #'. $model->id; ?>
<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>