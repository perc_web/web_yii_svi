<?php
$this->breadcrumbs=array(
	'Role Fields'=>array('index'),
	$model->title,
);

$this->menu=array(
	array('label'=>'List RoleField','url'=>array('index')),
	array('label'=>'Create RoleField','url'=>array('create')),
	array('label'=>'Update RoleField','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete RoleField','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
);
?>

<?php $this->pageTitlecrumbs = 'View RoleField #'. $model->id; ?>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'title',
		'real_action',
	),
)); ?>
