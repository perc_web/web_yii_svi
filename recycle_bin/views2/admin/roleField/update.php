<?php
$this->breadcrumbs=array(
	'Role Fields'=>array('index'),
	$model->title=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List RoleField','url'=>array('index')),
	array('label'=>'Create RoleField','url'=>array('create')),
	array('label'=>'View RoleField','url'=>array('view','id'=>$model->id)),
);
?>

<?php $this->pageTitlecrumbs = 'Update RoleField #'. $model->id; ?>
<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>