<?php
$this->breadcrumbs=array(
	'Role Fields'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List RoleField','url'=>array('index')),
);
?>

<?php $this->pageTitlecrumbs = 'Create RoleField';?>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>