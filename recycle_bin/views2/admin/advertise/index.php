<?php
$this->breadcrumbs=array(
	'Advertises'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Advertise','url'=>array('index')),
	array('label'=>'Create Advertise','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('advertise-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<!--<h3>Manage </h3>-->

<?php $this->pageTitlecrumbs = 'Manage Advertises';?>


<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'advertise-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	//'orderField' => 'sort',
    	//'idField' => 'id',
    	//'orderUrl' => 'order',
    	//'type'=>'striped  condensed',
	'columns'=>array(
		
            
            
             array(
                        'name'=>'page',
                       'value'=>' Advertise::getPage($data->page) ',
                    'filter' => array(1 => "home", 2=> "why svi card", 3 => "partner&reserve&catalog", 4 => "contact us",5 => "faq",6 => "blog"),
                  
                    ),
		
            
             array(
                        'name'=>'size',
                       'value'=>' Advertise::getSize($data->size) ',
                    'filter' =>  array(0 => "very small",1 => "small", 2=> "medium", 3 => "large"),
                  
                    ),
            
            
		
              array(
                    'name' => 'active',
                     'value' => '($data->active == 1) ? "active" : "Unactive" ' ,
                       'filter' => array('1' => 'active', '0' => 'Unactive'),
                     ),
		'link',
            
             array(
                    'name'=>'image',
                    'type'=>'Raw',
                    'value'=>'(!empty($data->image))?CHtml::image(Yii::app()->request->baseUrl."/media/advertise/".$data->image,"",array("style"=>"width:50px;height:50px;")):"no image"',
                ),
            
		/*
		'sort',
		*/
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>
