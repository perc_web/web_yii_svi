<?php
$this->breadcrumbs=array(
	'Advertises'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Advertise','url'=>array('index')),
	array('label'=>'Create Advertise','url'=>array('create')),
	array('label'=>'Update Advertise','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete Advertise','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
);
?>

<?php $this->pageTitlecrumbs = 'View Advertise #'. $model->id; ?>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		
             array(
            'name' => 'page',
            'value' =>  Advertise::getPage($model->page)  ,
        ),
		
            
            array(
            'name' => 'size',
            'value' =>  Advertise::getSize($model->size)  ,
        ),
		
             'active' => array(
            'name' => 'active',
            'value' => ($model->active == 1)? "active" : "Unactive",
        ),
		'link',
		
              array(
            'name'=>'image',
            'type'=>'raw',
            'value'=>CHtml::image(Yii::app()->request->baseUrl.'/media/advertise/'.$model->image,$model->image,array('width'=>250)),
        ),
	),
)); ?>
