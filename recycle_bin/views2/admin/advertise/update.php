<?php
$this->breadcrumbs=array(
	'Advertises'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Advertise','url'=>array('index')),
	array('label'=>'Create Advertise','url'=>array('create')),
	array('label'=>'View Advertise','url'=>array('view','id'=>$model->id)),
);
?>

<?php $this->pageTitlecrumbs = 'Update Advertise #'. $model->id; ?>
<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>