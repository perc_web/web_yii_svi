<?php
$this->breadcrumbs=array(
	'Modules'=>array('index'),
	$model->title,
);

$this->menu=array(
	array('label'=>'List Modules','url'=>array('index')),
	array('label'=>'Create Modules','url'=>array('create')),
	array('label'=>'Update Modules','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete Modules','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
);
?>

<?php $this->pageTitlecrumbs = 'View Modules #'. $model->id; ?>



<div class="table-responsive"> 
<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'title',
		//'parent_id',
		'url',
		//'show',
		//'employee_id',
		//'company_id',
		//'permission',
		//'sort_num',
		//'table_name',
		//'field',
		//'appear_access',
		//'show_appear',
		//'is_report',
		//'icon',
	),
)); ?>

</div>