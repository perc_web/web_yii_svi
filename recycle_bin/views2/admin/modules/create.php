<?php
$this->breadcrumbs=array(
	'Modules'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Modules','url'=>array('index')),
);
?>

<?php $this->pageTitlecrumbs = 'Create Modules';?>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>