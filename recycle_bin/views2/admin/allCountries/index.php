<?php
$this->breadcrumbs=array(
	'All Countries'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List AllCountries','url'=>array('index')),
	array('label'=>'Create AllCountries','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('all-countries-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<!--<h3>Manage </h3>-->

<?php $this->pageTitlecrumbs = 'Manage All Countries';?>


<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'all-countries-grid',
	'dataProvider'=>$model->search(),
	 'filter'=>$model,
	//'orderField' => 'sort',
    	//'idField' => 'id',
    	//'orderUrl' => 'order',
    	//'type'=>'striped  condensed',
	'columns'=>array(
		'country_code',
		'country_name',
		
		
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>
