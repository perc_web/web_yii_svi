<?php
$this->breadcrumbs=array(
	'Informations'=>array('index'),
	$model->title,
);

$this->menu=array(
	array('label'=>'List Information','url'=>array('index')),
	array('label'=>'Create Information','url'=>array('create')),
	array('label'=>'Update Information','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete Information','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
);
?>

<?php $this->pageTitlecrumbs = 'View Information #'. $model->id; ?>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'title',
		'subtitle',
		'link',
            
               'publish' => array(
            'name' => 'publish',
            'value' => ($model->publish == 1)? "Publish" : "UnPublish",
        ),
       
            
             array(
            'name' => 'image',
            'value' =>  Information::getIcon($model->image)  ,
        ),
		
	),
)); ?>
