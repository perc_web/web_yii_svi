<?php
$this->breadcrumbs=array(
	'Informations'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Information','url'=>array('index')),
	
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('information-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<!--<h3>Manage </h3>-->

<?php $this->pageTitlecrumbs = 'Manage Informations';?>

<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'information-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'title',
		'subtitle',
		'link',
		
            
               'publish' => array(
                    'name' => 'publish',
                    'value' => '($data->publish == 1)? "publish" : "UnPublish"',
                ),
            
         
            
              array(
                        'name'=>'image',
                       'value'=>'Information::getIcon($data->image) ',
                    'filter' =>  array('i-con i-con-free-bottom' => "shipping", 'i-con i-con-special'=> "card", 'i-con i-con-promotion' => "promotion", 'i-con i-con-support' => "support"),
                  
                    ),
            
		
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
                          'template' => '{view} {update}'
		),
	),
)); ?>
