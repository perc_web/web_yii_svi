<?php
$this->breadcrumbs=array(
	'Informations'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Information','url'=>array('index')),
);
?>

<?php $this->pageTitlecrumbs = 'Create Information';?>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>