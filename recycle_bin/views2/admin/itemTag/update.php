<?php
$this->breadcrumbs=array(
	'Item Tags'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List ItemTag','url'=>array('index','item_id' => $model->item_id)),
	array('label'=>'Create ItemTag','url'=>array('create','item_id' => $model->item_id)),
	array('label'=>'View ItemTag','url'=>array('view','id'=>$model->id)),
);
?>

<?php $this->pageTitlecrumbs = 'Update ItemTag #'. $model->id; ?>
<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>