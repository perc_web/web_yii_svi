<?php
$this->breadcrumbs=array(
	'Item Tags'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List ItemTag','url'=>array('index','item_id' => $model->item_id)),
	array('label'=>'Create ItemTag','url'=>array('create','item_id' => $model->item_id)),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('item-tag-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<!--<h3>Manage </h3>-->

<?php $this->pageTitlecrumbs = 'Manage Item Tags';?>

<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'item-tag-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
                'item_id' => array(
                    'name' => 'item_id',
                    'value' => '$data->item->title',
                    'filter' => CHtml::listData(Menu::model()->findAll(), 'id', 'title'),
                ),
		'tag_id' => array(
                    'name' => 'tag_id',
                    'value' => '$data->tag->title',
                    'filter' => CHtml::listData(Tags::model()->findAll(), 'id', 'title'),
                ),
		array(
                    'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>
