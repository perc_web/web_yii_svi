<?php
$this->breadcrumbs=array(
	'Item Tags'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List ItemTag','url'=>array('index','item_id' => $model->item_id)),
	array('label'=>'Create ItemTag','url'=>array('create','item_id' => $model->item_id)),
	array('label'=>'Update ItemTag','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete ItemTag','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
);
?>

<?php $this->pageTitlecrumbs = 'View ItemTag #'. $model->id; ?>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'item_id' => array(
                    'name' => 'item_id',
                    'value' => $model->item->title,
                ),
		'tag_id' => array(
                    'name' => 'tag_id',
                    'value' => $model->tag->title,
                ),
		//'user_id',
	),
)); ?>
