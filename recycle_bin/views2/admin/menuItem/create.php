<?php
$this->breadcrumbs=array(
	'Menu Items'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List MenuItem','url'=>array('index','menu_id' => $model->menu_id)),
);
?>

<?php $this->pageTitlecrumbs = 'Create MenuItem';?>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>