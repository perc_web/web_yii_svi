<?php
$this->breadcrumbs=array(
	'Menu Items'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List MenuItem','url'=>array('index','menu_id' => $model->menu_id)),
	array('label'=>'Create MenuItem','url'=>array('create','menu_id' => $model->menu_id)),
	array('label'=>'Update MenuItem','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete MenuItem','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
);
?>

<?php
if($model->menu->id ){
    $menu_link=Yii::app()->baseUrl."/admin/menu/view/id/".$model->menu->id  ;
     $this->pageTitlecrumbs = 'View items for menu :  <a  href="'.$menu_link.'">'.$model->menu->title.'</a> ';
}  else {
     $this->pageTitlecrumbs = 'View Menu Item';
}
?>



<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		
//              array(
//			'name' => 'menu_id',
//			'value' => $model->menu->title  
//		),
		
             array(
			'name' => 'item_id',
			'value' => $model->item->title  
		),
	),
)); ?>
