<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'menu-item-form',
	'enableAjaxValidation'=>false,
	'type'=>'horizontal',
)); ?>

   <?php 
   $menu=  Menu::model()->findByPk($model->menu_id) ;
  $menu_items=  $menu->outlet->partner->items ;
  if($menu_items){
      $data=$menu_items ;
  }else{
     $data= Item::model()->findAll() ;
  }
      
     
   ?>
	<p class="help-block">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

        <?php
        if(! $model->menu_id){
           ?>
         <div class="control-group ">
            <?php echo $form->labelEx($model, 'menu_id', array('class' => 'control-label')) ?>
            <?php
            $this->widget('Select2', array(
                'model' => $model,
                'attribute' => 'menu_id',
                'data' => CHtml::listData(Menu::model()->findAll(), 'id', 'title'),
                'htmlOptions' => array('class' => "span4", "empty" => "Select menu"),
            ));
            ?>
        </div>
        <?php
        }
        
        ?>
        
          

               <div class="control-group ">
            <?php echo $form->labelEx($model, 'item_id', array('class' => 'control-label')) ?>
            <?php
            $this->widget('Select2', array(
                'model' => $model,
                'attribute' => 'item_id',
                'data' => CHtml::listData($data, 'id', 'title'),
                'htmlOptions' => array('class' => "span4", "empty" => "Select item"),
            ));
            ?>
        </div>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Create' : 'Save',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
