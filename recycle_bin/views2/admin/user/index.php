<?php
$this->breadcrumbs=array(
	'Users'=>array('index'),
	'Manage',
);
$this->menu=array(
	array('label'=>'List User','url'=>array('index')),
	array('label'=>'Create User','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('user-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<?php $this->pageTitlecrumbs = 'Manage Users';?> 
<?php      $roleuser = RoleUser::model()->findByAttributes(array('user_id' => $model->id)); ?>


<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'user-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
            'username',
            'email',
            'username',
            /*
            'groups_id'=>array(// display 'author.username' using an expression
                'name'=>'groups_id',
                'value'=>'$data->usergroup->group_title',
                'filter'=> Groups::model()->getgroups(),
            ),
            */
            
            
             
            array(
                'header'=>'role',
                'value'=>'User::RoleName($data->id)',
                'filter'=>Chtml::listData(Role::model()->findAll(), 'id', 'title'),
            ),
           
            
          
            
            array(
                'class'=>'CLinkColumn',
                'label'=>'Reservations',
                'urlExpression'=>'Yii::app()->request->baseUrl."/admin/reservations/index?user=".$data->id',
                'header'=>'Reservations',
            ),
            
            
             array(
                'class'=>'CLinkColumn',
                'label'=>'Cards',
                'urlExpression'=>'Yii::app()->request->baseUrl."/admin/userCard/index?user_id=".$data->id',
                'header'=>'Cards',
            ),
            array(
                'class'=>'bootstrap.widgets.TbButtonColumn',
            ),
	),
)); ?>
