<?php
$this->breadcrumbs=array(
	'Categories'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Category','url'=>array('index')),
	array('label'=>'Create Category','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('category-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<?php $this->pageTitlecrumbs = 'Manage Categories';?>

<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'category-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
            'title',
            array(
                'name'=>'image',
                'type'=>'html',
                'value'=>'(!empty($data->image))?CHtml::image(Yii::app()->request->baseUrl."/media/category/".$data->image,"",array("style"=>"width:50px;height:50px;")):"no image"',
                'filter' => ' ',
            ),
            array(
                    'class'=>'bootstrap.widgets.TbButtonColumn',
            ),
	),
)); ?>
