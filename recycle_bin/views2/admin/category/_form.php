<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'category-form',
	'enableAjaxValidation'=>false,
	'type'=>'horizontal',
          'htmlOptions'=>array('enctype'=>'multipart/form-data'),
)); ?>

	<p class="help-block">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldRow($model,'title',array('class'=>'span5','maxlength'=>255)); ?>
        
         <div class="control-group ">
            <?php echo $form->labelEx($model, 'parent', array('class' => 'control-label')) ?>
            <?php
            $this->widget('Select2', array(
                'model' => $model,
                'attribute' => 'parent',
                'data' => CHtml::listData(Category::model()->findAll(), 'id', 'title'),
                'htmlOptions' => array('class' => "span4", "empty" => "Select Parent Category"),
            ));
            ?>
        </div>

        <?php echo $form->fileFieldRow($model,'image');
            echo " <div class=\"control-group \"> <div class=\"controls\">";
            echo CHtml::image(Yii::app()->request->baseUrl.'/media/category/'.$model->image,'',array('width'=>200));
            echo "</div></div>";
        ?>

	<div class="form-actions">
            <?php $this->widget('bootstrap.widgets.TbButton', array(
                    'buttonType'=>'submit',
                    'type'=>'primary',
                    'label'=>$model->isNewRecord ? 'Create' : 'Save',
            )); ?>
	</div>

<?php $this->endWidget(); ?>
