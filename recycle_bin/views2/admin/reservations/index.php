<?php
$this->breadcrumbs=array(
	'Reservations'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Reservations','url'=>array('index')),
	array('label'=>'Create Reservations','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('reservations-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<!--<h3>Manage </h3>-->

<?php $this->pageTitlecrumbs = 'Manage Reservations';?>

<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'reservations-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'user_id' => array(
                    'name' => 'user_id',
                    'value' => '$data->user->username',
                    'filter' => CHtml::listData(User::model()->findAll(), 'id', 'title'),
                ),
		'date',
		'item_id' => array(
                    'name' => 'item_id',
                    'value' => '$data->item->title',
                    'filter' => CHtml::listData(Item::model()->findAll(), 'id', 'title'),
                ),
		'from',
		'to',
		/*'category_id',
		'num_customers',
		*/
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>
