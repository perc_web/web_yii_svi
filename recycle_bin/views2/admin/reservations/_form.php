<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'reservations-form',
	'enableAjaxValidation'=>false,
	'type'=>'horizontal',
)); ?>

	<p class="help-block">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

        <div class="control-group ">
            <?php
            $model->user_id = $_SESSION['user'];
            echo $form->labelEx($model, 'user_id', array('class' => 'control-label')) ?>
            <?php
            $this->widget('Select2', array(
                'model' => $model,
                'attribute' => 'user_id',
                'data' => CHtml::listData(User::model()->findAll(), 'id', 'username'),
                'htmlOptions' => array('class' => "span4", "empty" => "Select User"),
            ));
            ?>
        </div>

	<div class="control-group ">
            <?php echo $form->labelEx($model, 'partner_id', array('class' => 'control-label')) ?>
            <?php
            $this->widget('Select2', array(
                'model' => $model,
                'attribute' => 'partner_id',
                'data' => CHtml::listData(Partner::model()->findAll(), 'id', 'title'),
                'htmlOptions' => array('class' => "span4", "empty" => "Select Partner"),
            ));
            ?>
        </div>

        <div class="control-group ">
            <?php echo $form->labelEx($model, 'outlet_id', array('class' => 'control-label')) ?>
            <?php
            $this->widget('Select2', array(
                'model' => $model,
                'attribute' => 'outlet_id',
                'data' => CHtml::listData(Outlet::model()->findAll(), 'id', 'title'),
                'htmlOptions' => array('class' => "span4", "empty" => "Select Outlet"),
            ));
            ?>
        </div>
        
        <div class="control-group ">
            <?php echo $form->labelEx($model, 'category_id', array('class' => 'control-label')) ?>
            <?php
            $this->widget('Select2', array(
                'model' => $model,
                'attribute' => 'category_id',
                'data' => CHtml::listData(Category::model()->findAll(), 'id', 'title'),
                'htmlOptions' => array('class' => "span4", "empty" => "Select Category"),
            ));
            ?>
        </div>
        
        <div class="control-group ">
            <?php echo $form->labelEx($model, 'menu_id', array('class' => 'control-label')) ?>
            <?php
            $this->widget('Select2', array(
                'model' => $model,
                'attribute' => 'menu_id',
                'data' => CHtml::listData(Menu::model()->findAll(), 'id', 'title'),
                'htmlOptions' => array('class' => "span4", "empty" => "Select Menu"),
            ));
            ?>
        </div>
        
<!--        <div class="control-group ">
            <?php  // echo $form->labelEx($model, 'item_id', array('class' => 'control-label')) ?>
            <?php
            /*
            $this->widget('Select2', array(
                'model' => $model,
                'attribute' => 'item_id',
                'data' => CHtml::listData(Item::model()->findAll(), 'id', 'title'),
                'htmlOptions' => array('class' => "span4", "empty" => "Select Item"),
            ));
            */
            ?>
        </div>-->
        
        <div class="control-group ">
            <?php echo $form->labelEx($model, 'date', array('class' => 'control-label')) ?>
            <div class="controls">
                <?php
                $this->widget('application.extensions.CJuiDateTimePicker.CJuiDateTimePicker', array(
                    'model' => $model, //Model object
                    'attribute' => 'date', //attribute name
                    'language' => '',
                    'mode' => 'date', //use "time","date" or "datetime" (default)
                    'options' => array(
                        "dateFormat" => Yii::app()->params['dateFormat'],
                        'changeMonth' => 'true',
                        'changeYear' => 'true',
                        'showOtherMonths' => true, // Show Other month in jquery
                    ), // jquery plugin options
                    'htmlOptions' => array(
                        'style' => 'height:20px;',
                        'class' => 'span5',
                    ),
                ));
                ?>
            </div>
        </div>

        <div class="control-group">
            <?php echo $form->labelEx($model, 'from', array('class' => 'control-label')) ?>
            <div class="controls">
                <?php Yii::import('application.extensions.CJuiDateTimePicker.CJuiDateTimePicker');
                    $this->widget('CJuiDateTimePicker',array(
                        'model'=>$model, //Model object
                        'attribute'=>'from', //attribute name
                        'language' => 'en',
                        'htmlOptions'=>array(
                            'class'=>'span5'
                        ),
                        'mode'=>'time', //use "time","date" or "datetime" (default)
                        //'options'=>array("dateFormat"=>'yy-mm-dd') ,// jquery plugin options
                    )); ?>
            </div>
        </div>
        
        <div class="control-group">
            <?php echo $form->labelEx($model, 'to', array('class' => 'control-label')) ?>
            <div class="controls">
                 <?php Yii::import('application.extensions.CJuiDateTimePicker.CJuiDateTimePicker');
                    $this->widget('CJuiDateTimePicker',array(
                        'model'=>$model, //Model object
                        'attribute'=>'to', //attribute name
                        'language' => 'en',
                        'htmlOptions'=>array(
                            'class'=>'span5'
                        ),
                        'mode'=>'time', //use "time","date" or "datetime" (default)
                        //'options'=>array("dateFormat"=>'yy-mm-dd') ,// jquery plugin options
                    )); ?>
            </div>
        </div>
        

	<?php echo $form->textFieldRow($model,'num_customers',array('class'=>'span5')); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Create' : 'Save',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
