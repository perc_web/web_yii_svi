<?php
$this->breadcrumbs=array(
	'Role Field Values'=>array('index'),
	$model->title=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List RoleFieldValue','url'=>array('index')),
	array('label'=>'Create RoleFieldValue','url'=>array('create')),
	array('label'=>'View RoleFieldValue','url'=>array('view','id'=>$model->id)),
);
?>

<?php $this->pageTitlecrumbs = 'Update RoleFieldValue #'. $model->id; ?>
<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>