<?php
$this->breadcrumbs=array(
	'Outlets'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Outlet','url'=>array('index','partner_id' => $model->partner_id)),
);
?>

<?php $this->pageTitlecrumbs = 'Create Outlet';?>

<?php echo $this->renderPartial('_form', array('model'=>$model, 'gallery' => $gallery)); ?>