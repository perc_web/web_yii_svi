<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'outlet-form',
	'enableAjaxValidation'=>false,
	'type'=>'horizontal',
     'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>

	<p class="help-block">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>
        <?php
        if(! $model->partner_id){
           ?>
        <div class="control-group ">
            <?php echo $form->labelEx($model, 'partner_id', array('class' => 'control-label')) ?>
            <?php
            $this->widget('Select2', array(
                'model' => $model,
                'attribute' => 'partner_id',
                'data' => CHtml::listData(Partner::model()->findAll(), 'id', 'title'),
                'htmlOptions' => array('class' => "span4", "empty" => "Select Partner"),
            ));
            ?>
        </div>
        <?php
        }
        
        ?>
      

	<?php echo $form->textFieldRow($model,'title',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textAreaRow($model,'intro',array('rows'=>4, 'cols'=>50, 'class'=>'span6')); ?>

        <div class="control-group ">
            <label class="control-label" for="Pages_intro">Description</label>
            <div class="controls">
                <?php
                $this->widget('application.extensions.floara.Floara', array(
                    'model' => $model,
                    'attribute' => 'description',
                ));
                ?> 
            </div>
        </div>
        
	<?php  // echo $form->textFieldRow($model,'address',array('class'=>'span5','maxlength'=>255)); ?>
        
        <?= $form->textFieldRow($model, 'discount'); ?>
        
        <?php echo $form->fileFieldRow($model, 'image', array('class' => 'span5', 'maxlength' => 255));
        if(!$model->isNewRecord and $model->image)
        { ?>
            <div class="control-group ">
                <div class="controls">
                    <p id='image-cont'> <?php echo Chtml::image(Yii::app()->baseUrl . '/media/outlet/' . $model->image, '', array('width' => 200)); ?></p>
                </div>
            </div>
        <?php
        }
        ?>
	
        
        <div  class="control-group"  id="temp_image_product"   >
    <img class="loading" style="margin-left:166px;display: none;width:60px;height:60px;" src="<?= Yii::app()->getBaseUrl() ?>/images/loading3.gif"    />
        </div>
        
        <div class="control-group">
            <label class="control-label">Partner Gallery</label>
            <div class="controls">
                <div class="span<?php echo(isset($_GET['w']) ? $_GET['w'] : '12') ?>" style="width:900px;">
                    <?php
                    $this->widget('GalleryManager', array(
                        'gallery' => $gallery,
                    ));
                    ?>
                    <?php
                        echo $form->hiddenField($model,'gallery_id', array('value' => $gallery->id));
                    ?>
                </div>
            </div>
        </div>
        
        <?php echo $form->checkBoxRow($model,'active'); ?>
        
        <?php echo $form->checkBoxRow($model,'featured'); ?>

	<?php echo $form->checkBoxRow($model,'hot_deal'); ?>

	<?php echo $form->checkBoxRow($model,'new_deal'); ?>
        
        <?php echo $form->checkBoxRow($model,'recomended'); ?>
        
        
	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Create' : 'Save',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
