<?php
$this->breadcrumbs=array(
	'Outlets'=>array('index'),
	$model->title=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Outlet','url'=>array('index','partner_id' => $model->partner_id)),
	array('label'=>'Create Outlet','url'=>array('create','partner_id' => $model->partner_id)),
	array('label'=>'View Outlet','url'=>array('view','id'=>$model->id)),
);
?>

<?php $this->pageTitlecrumbs = 'Update Outlet " '. $model->title.' "'; ?>
<?php echo $this->renderPartial('_form',array('model'=>$model, 'gallery' => $gallery)); ?>