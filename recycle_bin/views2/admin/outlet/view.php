<?php
$this->breadcrumbs=array(
    'Outlets'=>array('index'),
    $model->title,
);

$this->menu=array(
    array('label'=>'List Outlet','url'=>array('index' ,'partner_id' => $model->partner_id)),
    array('label'=>'Create Outlet','url'=>array('create','partner_id' => $model->partner_id)),
    array('label'=>'Update Outlet','url'=>array('update','id'=>$model->id)),
    array('label'=>'Delete Outlet','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
);
?>

<?php
if($model->partner->id){
     $partner_link=Yii::app()->baseUrl."/admin/partner/view/id/".$model->partner->id  ;
      $this->pageTitlecrumbs = 'View Outlet " '. $model->title.' "';
     $this->pageTitlecrumbs .= ' for  partner:  <a  href="'.$partner_link.'">'.$model->partner->title .'</a>';
}  else {
     $this->pageTitlecrumbs = 'View Outlet " '. $model->title.' "';
}
?>




<?php $this->widget('bootstrap.widgets.TbDetailView',array(
  'data'=>$model,
  'attributes'=>array(
    'partner_id' => array(
                    'name' => 'partner_id',
                    'value' => $model->partner->title
                ),
    'title',
    'intro',
            'description' => array(
                    'name'=>'description',
                    'type'=>'raw',
                   
    ),
            
     // 'address',
    'active' => array(
                    'name' => 'active',
                    'value' => ($model->active == 1)? "Yes" : "No",
                ),
            
              array(
            'name' => 'featured',
            'value' => $model->featured == 1 ? "Yes" : "No",
        ),
            
              'hot_deal' => array(
                    'name' => 'hot_deal',
                    'value' => ($model->hot_deal == 1)? "Yes" : "No",
                ),
    'new_deal' => array(
                    'name' => 'new_deal',
                    'value' => ($model->new_deal == 1)? "Yes" : "No",
                ),
                'recomended' => array(
                    'name' => 'recomended',
                    'value' => ($model->recomended == 1)? "Yes" : "No",
                ),
            
            
                'discount',
                'image' => array(
                    'name'=>'image',
                    'type'=>'raw',
                    'value'=>CHtml::image(Yii::app()->request->baseUrl.'/media/outlet/'.$model->image,"",array('width'=>150)),
    ),
  ),
)); 
?>


<!--Menus-->
<?php 
$outlet_menu = Menu::model()->findByAttributes(array('outlet_id' => $model->id));
?>
<div >
    <header class="acc_col_head">
        <div class="icons"><i class="icon-eye-open"></i></div>
        <a href="<?php echo Yii::app()->baseUrl  ?>/admin/menu/view/id/<?php echo $outlet_menu->id ; ?>" >
        <h5  style="color: rgb(255, 255, 255);" ><?= Yii::t('translate', 'Menu') ?></h5>
        </a>
        <div class="toolbar">
            <?php
if($model->partner->items){
   ?>
     
<!--             <a  href="<?php echo Yii::app()->baseUrl  ?>/admin/menu/create/outlet_id/<?php echo $model->id ; ?>"  >
                                    <i class="icon-plus"></i>
                                    </a> -->
            
 <?php           
}

?>
            
             
           
            
            
        </div>
    </header>

   
</div>
<div  style="width:600px;min-height:10px;margin-left: 220px;clear: both"></div>








<!--Reviews-->

<div >
    <header class="acc_col_head">
        <div class="icons"><i class="icon-eye-open"></i></div>
        <a href="<?php echo Yii::app()->baseUrl  ?>/admin/outletReview/index/outlet_id/<?php echo $model->id ; ?>" >
        <h5  style="color: rgb(255, 255, 255);"><?= Yii::t('translate', 'Reviews') ?></h5>
        </a>
        <div class="toolbar">
              <a  href="<?php echo Yii::app()->baseUrl  ?>/admin/outletReview/create/outlet_id/<?php echo $model->id ; ?>"  >
                                    <i class="icon-plus"></i>
                                    </a> 
            
              <a  href="<?php echo Yii::app()->baseUrl  ?>/admin/outletReview/index/outlet_id/<?php echo $model->id ; ?>">
                                    <i class="icon-th-list"></i>
               </a>
            
             <a class="accordion-toggle minimize-box collapsed" data-toggle="collapse" href="#outlet_reviews">
                        <i class="icon-chevron-down"></i>
                    </a>
            
           
        </div>
    </header>

    <div id="outlet_reviews" style="height: 10px;" class="collapse">

        <?php
        $this->widget('bootstrap.widgets.TbGridView', array(
            'id' => 'user-email-grid',
            'dataProvider' => Helper::getAll($model->id, 'OutletReview', 'outlet_id'),
     'columns'=>array(
    'user_id' => array(
                    'name' => 'user_id',
                    'value' => '$data->user->username',
                    'filter' => CHtml::listData(User::model()->findAll(), 'id', 'username'),
                ),
    
    'price_rate',
    'value_rate',
    'quality_rate',
            
             'approved' => array(
                    'name' => 'approved',
                    'value' => '($data->approved == 1)? "Yes" : "No"',
                    'filter' => array(1 => 'Yes', 0 => 'No'),
                ),
    
         array(
      'class'=>'bootstrap.widgets.TbButtonColumn',
                        'template' => '{view}',
                        'buttons'=>array(       
                                'view' => array(
                                  'url'=>'Yii::app()->controller->createUrl("/admin/OutletReview/view", array("id"=>$data->id))',
                                ),
                        ),
    ),
    
  ),
        ));
        ?>



    </div>
</div>
