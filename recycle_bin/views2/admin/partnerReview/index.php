<?php
$this->breadcrumbs=array(
	'Partner Reviews'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List PartnerReview','url'=>array('index','partner_id' => $model->partner_id)),
	array('label'=>'Create PartnerReview','url'=>array('create','partner_id' => $model->partner_id)),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('partner-review-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<!--<h3>Manage </h3>-->
<?php
if($model->partner->id){
  $partner_link=Yii::app()->baseUrl."/admin/partner/view/id/".$model->partner->id  ;
     $this->pageTitlecrumbs = 'Manage Reviews for partner :  <a  href="'.$partner_link.'">'.$model->partner->title .'</a>';
}  else {
     $this->pageTitlecrumbs = 'Manage Reviews';
}
?>

<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'partner-review-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
    	//'type'=>'striped  condensed',
	'columns'=>array(
		'user_id' => array(
                    'name' => 'user_id',
                    'value' => '$data->user->username',
                    'filter'=>CHtml::listData(User::model()->findAll(), 'id', 'username'),
                   
                ),
            /*
		'partner_id' => array(
                    'name' => 'partner_id',
                    'value' => '$data->partner->title',
                     'filter'=>CHtml::listData(Partner::model()->findAll(), 'id', 'title'),
                ),
            */
		'price_rate',
		'value_rate',
		'quality_rate',
            
             'approved' => array(
                    'name' => 'approved',
                    'value' => '($data->approved == 1)? "Yes" : "No"',
                    'filter' => array(1 => 'Yes', 0 => 'No'),
                ),
		
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>
