<?php
$this->breadcrumbs=array(
	'Partner Reviews'=>array('index'),
	$model->title=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List PartnerReview','url'=>array('index','partner_id' => $model->partner_id)),
	array('label'=>'Create PartnerReview','url'=>array('create','partner_id' => $model->partner_id)),
	array('label'=>'View PartnerReview','url'=>array('view','id'=>$model->id)),
);
?>

<?php $this->pageTitlecrumbs = 'Update PartnerReview #'. $model->id; ?>
<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>