<?php
$this->breadcrumbs=array(
	'Partner Reviews'=>array('index'),
	$model->title,
);

$this->menu=array(
	array('label'=>'List PartnerReview','url'=>array('index','partner_id' => $model->partner_id)),
	array('label'=>'Create PartnerReview','url'=>array('create','partner_id' => $model->partner_id)),
	array('label'=>'Update PartnerReview','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete PartnerReview','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
);
?>

<?php
if($model->partner->id){
  $partner_link=Yii::app()->baseUrl."/admin/partner/view/id/".$model->partner->id  ;
     $this->pageTitlecrumbs = 'Manage Reviews for partner :  <a  href="'.$partner_link.'">'.$model->partner->title .'</a>';
}  else {
     $this->pageTitlecrumbs = 'View PartnerReview #'. $model->id;
}
?>


<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'user_id' => array(
                    'name' => 'user_id',
                    'value' => $model->user->username,
                ),
		'partner_id' => array(
                    'name' => 'partner_id',
                    'value' => $model->partner->title,
                ),
		'price_rate',
		'value_rate',
		'quality_rate',
		'review',
            	
            'approved' => array(
                    'name' => 'approved',
                    'value' => ($model->approved == 1)? "Yes" : "No",
                ),
	),
)); ?>
