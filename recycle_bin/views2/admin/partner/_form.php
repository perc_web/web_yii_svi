<?php
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id' => 'partner-form',
    'enableAjaxValidation' => false,
    'type' => 'horizontal',
    'htmlOptions' => array('enctype' => 'multipart/form-data'),
        ));
?>
<?php
if (!$model->isNewRecord && $model->cat_id) {

    $cond='parent is null && parent ="'.$model->cat_id.'"';
    $data = CHtml::listData(Category::model()->findAll(array('condition' => $cond)), 'id', 'title');
} else {
      $cond='parent is null ';
    $data = CHtml::listData(Category::model()->findAll(array('condition' => $cond)), 'id', 'title');

   
}
?>

<p class="help-block">Fields with <span class="required">*</span> are required.</p>

<?php echo $form->errorSummary($model); ?>

<?php echo $form->textFieldRow($model, 'title', array('class' => 'span5', 'maxlength' => 255)); ?>

<?php echo $form->textAreaRow($model, 'intro', array('rows' => 4, 'cols' => 20, 'class' => 'span6')); ?>

<div class="control-group ">
    <label class="control-label" for="Pages_intro">Description</label>
    <div class="controls">
        <?php
        $this->widget('application.extensions.floara.Floara', array(
            'model' => $model,
            'attribute' => 'description',
        ));
        ?> 
    </div>
</div>


<?php echo $form->textFieldRow($model, 'discount', array('class' => 'span5', 'maxlength' => 255)); ?>

<div class="control-group ">
    <?php echo $form->labelEx($model, 'cat_id', array('class' => 'control-label')) ?>
    <?php
    $this->widget('Select2', array(
        'model' => $model,
        'attribute' => 'cat_id',
        'data' => CHtml::listData(Category::model()->findAll(array('condition' => 'parent is null')), 'id', 'title'),
        'htmlOptions' => array('class' => "span4", "empty" => "Select category", 'onchange' => 'update_sub(this.value)'),
    ));
    ?>
</div>



<div class="control-group ">
    <?php echo $form->labelEx($model, 'subcat_id', array('class' => 'control-label')) ?>

    <?php
    $this->widget('Select2', array(
        'model' => $model,
        'attribute' => 'subcat_id',
        'data' => $data,
        'htmlOptions' => array('class' => "span4", 'multiple' => 'multiple'),
    ));
    ?>

</div>

  
        <div class="control-group ">
            <?php echo $form->labelEx($model, 'brand_id', array('class' => 'control-label')) ?>
            <?php
            $this->widget('Select2', array(
                'model' => $model,
                'attribute' => 'brand_id',
                'data' => CHtml::listData(Brands::model()->findAll(), 'id', 'title'),
                'htmlOptions' => array('class' => "span4", "empty" => "Select Brand"),
            ));
            ?>
        </div>


<?php
echo $form->fileFieldRow($model, 'image', array('class' => 'span5', 'maxlength' => 255));
if (!$model->isNewRecord and $model->image) {
    ?>
    <div class="control-group ">
        <div class="controls">
            <p id='image-cont'> <?php echo Chtml::image(Yii::app()->baseUrl . '/media/partner/' . $model->image, '', array('width' => 200)); ?></p>
        </div>
    </div>
    <?php
}
?>

<?php //  echo $form->checkBoxRow($model, 'new'); ?>

<?php echo $form->checkBoxRow($model, 'featured'); ?>

<?php echo $form->checkBoxRow($model, 'active'); ?>

<div  class="control-group"  id="temp_image_product"   >
    <img class="loading" style="margin-left:166px;display: none;width:60px;height:60px;" src="<?= Yii::app()->getBaseUrl() ?>/images/loading3.gif"    />
        </div>

<div class="control-group">
    <label class="control-label">Partner Gallery</label>
    <div class="controls">
        <div class="span<?php echo(isset($_GET['w']) ? $_GET['w'] : '12') ?>" style="width:900px;">
            <?php
            $this->widget('GalleryManager', array(
                'gallery' => $gallery,
            ));
            ?>
            <?php
            echo $form->hiddenField($model, 'gallery_id', array('value' => $gallery->id));
            ?>
        </div>
    </div>
</div>

<div class="form-actions">
    <?php
    $this->widget('bootstrap.widgets.TbButton', array(
        'buttonType' => 'submit',
        'type' => 'primary',
        'label' => $model->isNewRecord ? 'Create' : 'Save',
    ));
    ?>
</div>

<?php $this->endWidget(); ?>
<script>

    function update_sub(parent_cat) {
        $.ajax({
            type: 'POST', //request type
            url: "<?= Yii::app()->getBaseUrl(true) ?>/admin/partner/updateSubCategory",
            data: {parent_id: parent_cat},
            success: function(data) {
                $('#Partner_subcat_id').html(data);

            }
        });
    }
</script>