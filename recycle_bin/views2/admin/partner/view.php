<?php
$this->breadcrumbs=array(
	'Partner'=>array('index'),
	$model->title,
);

$this->menu=array(
	array('label'=>'List Partner','url'=>array('index')),
	array('label'=>'Create Partner','url'=>array('create')),
	array('label'=>'Update Partner','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete Partner','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
);
?>

<?php $this->pageTitlecrumbs = 'View Partner " '. $model->title.' "'; ?>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'title',
		'intro',
		
            'description' => array(
		'name'=>'description',
		'type'=>'raw',
		
		),
            'discount',
                array(
                    'name' => 'cat_id',
                    'value' => $model->cat->title  
		),
            
            'brand_id' => array(
                    'name' => 'brand_id',
                    'value' => $model->brand->title,
                ),
		'image' => array(
		'name'=>'image',
		'type'=>'raw',
		'value'=>CHtml::image(Yii::app()->request->baseUrl.'/media/partner/'.$model->image,"",array('width'=>150)),
		),
		'new' => array(
                    'name' => 'new',
                    'value' => ($model->new == 1)? "Yes" : "No",
                ),
		'featured'  => array(
                    'name' => 'featured',
                    'value' => ($model->featured == 1)? "Yes" : "No",
                ),
		'active'  => array(
                    'name' => 'active',
                    'value' => ($model->active == 1)? "Yes" : "No",
                ),
	),
)); ?>


<!--Outlets-->

<div >
    <header class="acc_col_head">
        <div class="icons"><i class="icon-eye-open"></i></div>
        <a href="<?php echo Yii::app()->baseUrl  ?>/admin/outlet/index/partner_id/<?php echo $model->id ; ?>" >
        <h5  style="color: rgb(255, 255, 255);"><?= Yii::t('translate', 'Outlets') ?></h5>
        </a>
        
        
        <div class="toolbar">
             <a  href="<?php echo Yii::app()->baseUrl  ?>/admin/outlet/create/partner_id/<?php echo $model->id ; ?>"  >
                                    <i class="icon-plus"></i>
                                    </a>
            
            
             <a  href="<?php echo Yii::app()->baseUrl  ?>/admin/outlet/index/partner_id/<?php echo $model->id ; ?>">
                                    <i class="icon-th-list"></i>
               </a>
              <a class="accordion-toggle minimize-box collapsed" data-toggle="collapse" href="#partner_outlet">
                        <i class="icon-chevron-down"></i>
                    </a>
          
           
        </div>
    </header>

    <div id="partner_outlet" style="height: 10px;" class="collapse">

        <?php
        $this->widget('bootstrap.widgets.TbGridView', array(
            'id' => 'user-email-grid',
            'dataProvider' => Helper::getAll($model->id, 'Outlet', 'partner_id'),
      'columns'=>array(
            'title',
           
		'address',
		'active' => array(
                    'name' => 'active',
                    'value' => '($data->active == 1)? "Yes" : "No"',
                    'filter' => array(1 => "Yes", 0 => "No"),
                ),
            
              'featured' => array(
                    'name' => 'featured',
                    'value' => '($data->featured == 1)? "Yes" : "No"',
                    'filter' => array(1 => "Yes", 0 => "No"),
                ),
            
		'hot_deal' => array(
                    'name' => 'hot_deal',
                    'value' => '($data->hot_deal == 1)? "Yes" : "No"',
                    'filter' => array(1 => "Yes", 0 => "No"),
                ),
		'new_deal' => array(
                    'name' => 'new_deal',
                    'value' => '($data->new_deal == 1)? "Yes" : "No"',
                    'filter' => array(1 => "Yes", 0 => "No"),
                ),
            
            'new_deal' => array(
                    'name' => 'recomended',
                    'value' => '($data->recomended == 1)? "Yes" : "No"',
                    'filter' => array(1 => "Yes", 0 => "No"),
                ),
            
           array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
                        'template' => '{view}',
                        'buttons'=>array(       
                                'view' => array(
                                  'url'=>'Yii::app()->controller->createUrl("/admin/Outlet/view", array("id"=>$data->id))',
                                ),
                        ),
		),
               
            
		
	),
        ));
        ?>



    </div>
</div>
<div  style="width:600px;min-height:10px;margin-left: 220px;clear: both"></div>
  

<!--Items-->

<div >
    <header class="acc_col_head">
        <div class="icons"><i class="icon-eye-open"></i></div>
            <a href="<?php echo Yii::app()->baseUrl  ?>/admin/item/index/partner_id/<?php echo $model->id ; ?>" >
        <h5 style="color: rgb(255, 255, 255);"><?= Yii::t('translate', 'Items') ?></h5>
        </a>
        <div class="toolbar">
             <a  href="<?php echo Yii::app()->baseUrl  ?>/admin/item/create/partner_id/<?php echo $model->id ; ?>">
                                    <i class="icon-plus"></i>
               </a>
            
             <a  href="<?php echo Yii::app()->baseUrl  ?>/admin/item/index/partner_id/<?php echo $model->id ; ?>">
                                    <i class="icon-th-list"></i>
               </a>
            
             <a class="accordion-toggle minimize-box collapsed" data-toggle="collapse" href="#partner_items">
                        <i class="icon-chevron-down"></i>
                    </a>
            
           
        </div>
    </header>

    <div id="partner_items" style="height: 10px;" class="collapse">

        <?php
        $this->widget('bootstrap.widgets.TbGridView', array(
            'id' => 'user-email-grid',
            'dataProvider' => Helper::getAll($model->id, 'Item', 'partner_id'),
    	'columns'=>array(
                'title',
              
		'cat_id' => array(
                    'name' => 'cat_id',
                    'value' => '$data->cat->title',
                    'filter' => CHtml::listData(Category::model()->findAll(), 'id', 'title'),
                ),
                'price',
                'discount_type' => array(
                    'name' => 'discount_type',
                    'value' => '($data->discount_type == 1)? "Percentage" : "Fixed Amount"',
                ),
		'discount_amount',
                'quantity',
            
             array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
                        'template' => '{view}',
                        'buttons'=>array(       
                                'view' => array(
                                  'url'=>'Yii::app()->controller->createUrl("/admin/Item/view", array("id"=>$data->id))',
                                ),
                        ),
		),
            
	),
        ));
        ?>



    </div>
</div>

<div  style="width:600px;min-height:10px;margin-left: 220px;clear: both"></div>


<!--Reviews-->

<div >
    <header class="acc_col_head">
        <div class="icons"><i class="icon-eye-open"></i></div>
            <a href="<?php echo Yii::app()->baseUrl  ?>/admin/partnerReview/index/partner_id/<?php echo $model->id ; ?>" >
        <h5  style="color: rgb(255, 255, 255);"><?= Yii::t('translate', 'Reviews') ?></h5>
        </a>
        <div class="toolbar">
             <a  href="<?php echo Yii::app()->baseUrl  ?>/admin/partnerReview/create/partner_id/<?php echo $model->id ; ?>"  >
                                    <i class="icon-plus"></i>
                                    </a>
            
             <a  href="<?php echo Yii::app()->baseUrl  ?>/admin/partnerReview/index/partner_id/<?php echo $model->id ; ?>">
                                    <i class="icon-th-list"></i>
               </a>
            
              <a class="accordion-toggle minimize-box collapsed" data-toggle="collapse" href="#partner_reviews">
                        <i class="icon-chevron-down"></i>
                    </a>
            
        </div>
    </header>

    <div id="partner_reviews" style="height: 10px;" class="collapse">

        <?php
        $this->widget('bootstrap.widgets.TbGridView', array(
            'id' => 'user-email-grid',
            'dataProvider' => Helper::getAll($model->id, 'PartnerReview', 'partner_id'),
    'columns'=>array(
		'user_id' => array(
                    'name' => 'user_id',
                    'value' => '$data->user->username',
                    'filter'=>CHtml::listData(User::model()->findAll(), 'id', 'username'),
                   
                ),
		
		'price_rate',
		'value_rate',
		'quality_rate',
            
             'approved' => array(
                    'name' => 'approved',
                    'value' => '($data->approved == 1)? "Yes" : "No"',
                    'filter' => array(1 => 'Yes', 0 => 'No'),
                ),
		
        array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
                        'template' => '{view}',
                        'buttons'=>array(       
                                'view' => array(
                                  'url'=>'Yii::app()->controller->createUrl("/admin/PartnerReview/view", array("id"=>$data->id))',
                                ),
                        ),
		),
		
	),
        ));
        ?>



    </div>
</div>

<div  style="width:600px;min-height:10px;margin-left: 220px;clear: both"></div>

<!--Tags-->

<div >
    <header class="acc_col_head">
        <div class="icons"><i class="icon-eye-open"></i></div>
        <a href="<?php echo Yii::app()->baseUrl  ?>/admin/partnerTag/index/partner_id/<?php echo $model->id ; ?>" >
        <h5  style="color: rgb(255, 255, 255);"><?= Yii::t('translate', 'Tags') ?></h5>
        </a>
        
        
        <div class="toolbar">
             <a  href="<?php echo Yii::app()->baseUrl  ?>/admin/partnerTag/create/partner_id/<?php echo $model->id ; ?>"  >
                                    <i class="icon-plus"></i>
                                    </a>
            
            
             <a  href="<?php echo Yii::app()->baseUrl  ?>/admin/partnerTag/index/partner_id/<?php echo $model->id ; ?>">
                                    <i class="icon-th-list"></i>
               </a>
            
             <a class="accordion-toggle minimize-box collapsed" data-toggle="collapse" href="#partner_tags">
                        <i class="icon-chevron-down"></i>
                    </a>
            
           
        </div>
    </header>

    <div id="partner_tags" style="height: 10px;" class="collapse">

        <?php
        $this->widget('bootstrap.widgets.TbGridView', array(
            'id' => 'user-email-grid',
            'dataProvider' => Helper::getAll($model->id, 'PartnerTag', 'partner_id'),
    'columns'=>array(
	
		'tag_id' => array(
                    'name' => 'tag_id',
                    'value' => '$data->tag->title',
                    'filter' => CHtml::listData(Tags::model()->findAll(), 'id', 'title'),
                ),
        
           array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
                        'template' => '{view}',
                        'buttons'=>array(       
                                'view' => array(
                                  'url'=>'Yii::app()->controller->createUrl("/admin/PartnerTag/view", array("id"=>$data->id))',
                                ),
                        ),
		),
		
	),
        ));
        ?>



    </div>
</div>
<div  style="width:600px;min-height:10px;margin-left: 220px;clear: both"></div>