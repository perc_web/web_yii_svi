<?php
$this->breadcrumbs=array(
	'Partners'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Partner','url'=>array('index')),
	array('label'=>'Create Partner','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('partner-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<!--<h3>Manage </h3>-->

<?php $this->pageTitlecrumbs = 'Manage Partners';?>

<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'partner-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'title',
            'discount',
            	
            'cat_id' => array(
                    'name' => 'cat_id',
                    'value' => '$data->cat->title',
                    'filter' => CHtml::listData(Category::model()->findAll(), 'id', 'title'),
                ),
            
             'brand_id' => array(
                    'name' => 'brand_id',
                    'value' => '$data->brand->title',
                    'filter' => CHtml::listData(Brands::model()->findAll(), 'id', 'title'),
                ),
            /*
            
		'new' => array(
                    'name' => 'new',
                    'value' => '($data->new == 1)? "Yes" : "No"',
                    'filter' => array(1 => "Yes", 0 => "No"),
                ),
            */
		'featured' => array(
                    'name' => 'featured',
                    'value' => '($data->featured == 1)? "Yes" : "No"',
                    'filter' => array(1 => "Yes", 0 => "No"),
                ),
		'active' => array(
                    'name' => 'active',
                    'value' => '($data->active == 1)? "Yes" : "No"',
                    'filter' => array(1 => "Yes", 0 => "No"),
                ),
               
            /*
               array(
            'header' => 'Outlets',
            'type' => 'raw',
            'value' => 'CHtml::link("Outlets", Yii::app()->baseUrl."/admin/outlet/index/partner_id/".$data->id)',
        ),
            
             array(
            'header' => 'Items',
            'type' => 'raw',
            'value' => 'CHtml::link("Items", Yii::app()->baseUrl."/admin/item/index/partner_id/".$data->id)',
        ),
            
             array(
                    'class'=>'CLinkColumn',
                    'label'=>'Reviews',
                    'urlExpression'=>'Yii::app()->request->baseUrl."/admin/partnerReview/index?partner_id=".$data->id',
                    'header'=>'Reviews',
                ),
            
         
     array(
            'header' => 'Tags',
            'type' => 'raw',
            'value' => 'CHtml::link("Tags", Yii::app()->baseUrl."/admin/partnerTag/index/partner_id/".$data->id)',
        ),
             
         */ 
            
        
            
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>
