<div class="col-lg-3 col-md-3">
    <div class="left-submenu"> My Account </div>
    <ul class="sub-li">
        <li> <a href="<?=Yii::app()->getBaseUrl(true);?>/profile/recommendedPlaces"><span>Recommended places</span></a> </li>
        <li> <a href="<?=Yii::app()->getBaseUrl(true);?>/profile/usageInfo"><span >Usage info</span></a></li>
        <li> <a href="<?=Yii::app()->getBaseUrl(true);?>/profile/specialRequests"><span>Users special requests</span></a> </li>
        <li> <a href="<?=Yii::app()->getBaseUrl(true);?>/profile/recommendFriend"><span>Recommend a friend</span></a> </li>
       
        
        <li id="send-gifts">
          
            <a aria-controls="gifts" aria-expanded="false" href="#gifts" data-parent="#accordion" data-toggle="collapse" class="collapsed">
              <span>Send gifts</span>
            </a>
          
        </li>
        
        <ul aria-labelledby="send-gifts" role="tabpanel" class="panel-collapse collapse" id="gifts" aria-expanded="false" style="height: 0px;">
         <li><a href="<?=Yii::app()->getBaseUrl(true);?>/profile/notYet"><span>SVI voucher</span></a></li>
        <li><a href="<?=Yii::app()->getBaseUrl(true);?>/profile/notYet"><span>SVI membership</span></a></li> 
        </ul>
        
        
    </ul>
</div>