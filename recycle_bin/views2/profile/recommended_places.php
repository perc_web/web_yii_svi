      <?php
  $this->widget('application.extensions.fancybox.EFancyBox', array(
    'target'=>'.fancyElm',
    'config'=>array(
        'maxWidth'    => 1000,
        'maxHeight'   => 1000,
        'fitToView'   => false,
        'width'       => '90%',
        'height'      => '90%',
        'autoSize'    => false,
        'closeClick'  => false,
        'openEffect'  => 'none',
        'closeEffect' => 'none',
	'type'        =>'iframe',
    ),
));
  
?>        
  
<div class="clearfix"></div>
<div class="container">
  <div class="row">
    <?=$this->renderPartial('user_menu')?>
    
    <!--End col-3--> 
 
    
    <!--start col-9-->
    <div class="col-lg-9 col-md-9">
      <h2 class="font-h">Recommended places</h2>
      
      
      
         
<?php
    if ($partners) {
        foreach ($partners as $partner) {
          ?>
      
     <div class="item col-lg-4 col-md-6 col-sm-6 respl-item itemsscroll">
                                            <div class="item-inner">
                                                <div class="w-image-box">
                                                    <span class="hover-background"></span>
                                                    <div class="item-image">
                                                        <a href="<?= Yii::app()->request->baseUrl ?>/partner-<?php echo $partner->slug; ?>" title="<?=$partner->title?>" class="product-image"><img src="<?= Yii::app()->request->baseUrl ?>/media/partner/<?php echo $partner->image; ?>" alt="<?=$partner->title?>"></a>
                                                        <a class="sm_quickview_handler  fancyElm" title="Quick View" href="<?= Yii::app()->request->baseUrl ?>/partnerfancy-<?php echo $partner->slug; ?>">Quick View</a>
                                                        <div class="quick-btns">
                                                            <?php
                                                            if(Yii::app()->user->id){ ?>
                                                                <a class="sm_quickview_handler quick-reserve" title="Quick Reserve" href="<?= Yii::app()->request->baseUrl ?>/home/reserve?category=<?=$partner->cat_id?>&partner=<?=$partner->id?>"><img src="<?= Yii::app()->request->baseUrl ?>/images/free-ship-icon.png" alt="" /></a>
                                                            <?php
                                                            }
                                                            ?>
                                                            <a class=" sm_quickview_handler quick-calculate" title="Quick Calculate" href="<?= Yii::app()->request->baseUrl ?>/home/calculate?category=<?=$partner->cat_id?>&partner=<?=$partner->id?>"><img src="<?= Yii::app()->request->baseUrl ?>/images/icon-title.png" alt="" /></a>
                                                        </div>
                                                         <?php
                                                                   if($partner->discount > 0){
                                                                       ?>
                                                                    
                                                                      <span class="disc">Discount: <?=$partner->discount?></span>
                                                                    
                                                                    <?php
                                                                   }
                                                                   ?>
                                                                      
                                                                      
                                                              <a class="fancyElm"  href="<?= Yii::app()->request->baseUrl ?>/partnerfancy-<?php echo $partner->slug; ?>">
                                                                  <p >
                                                                <?= substr($partner->intro, 0, 50)?>
                                                            </p>
                                                                 
                                                              </a>
                                                        
                                                       
                                                    </div>
                                                </div>
                                                <div class="item-info">
                                                    <div class="item-title">
                                                        <a href="<?= Yii::app()->request->baseUrl ?>/partner-<?php echo $partner->slug; ?>" title="<?=$partner->title;?>"><?php echo $partner->title; ?></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
            
            <?php
        }
    }  else {
        ?>
          <div> No items found</div>
      <?php
}
    ?>

      
    </div>
    
    <!--end col-9--> 
    
  </div>
</div>

