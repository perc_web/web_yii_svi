
<div class="clearfix"></div>
<div class="container">
  <div class="row">
    <?=$this->renderPartial('user_menu')?>
    <!--End col-3--> 
    <!--start col-9-->
    <div class="col-lg-9 col-md-9 ">
    <h2 class="font-h">Edit Profile</h2>
    <?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'user-form',
	//'enableAjaxValidation'=>true,
	//'type'=>'horizontal',
        'htmlOptions' => array(	'class' => 'form-horizontal'),

)); ?>
        <div class="form-group text-align">
          <label class="control-label col-lg-2 col-sm-5 col-xs-12" for="firstname">Full name </label>
          <div class="col-lg-4 col-md-6 col-sm-7 col-xs-12 profile-data">
              <input type="text" required="" placeholder="" id="" name="User[full_name]" value="<?=$user->full_name;?>" class="form-control">
          </div>
        </div>
        <div class="form-group text-align">
          <label class="control-label col-lg-2 col-sm-5 col-xs-12" for="firstname">username </label>
          <div class="col-lg-4 col-md-6 col-sm-7 col-xs-12 profile-data">
              <input type="text" required="" placeholder="" name="User[username]" value="<?=$user->username?>" id="Username" class="form-control">
          </div>
        </div>
        <div class="form-group text-align">
          <label class="control-label col-lg-2 col-sm-5 col-xs-12" for="firstname">Email</label>
          <div class="col-lg-4 col-md-6 col-sm-7 col-xs-12 profile-data">
              <input type="email" required="" name="User[email]" value="<?=$user->email?>" placeholder="" id="" class="form-control">
          </div>
        </div>
        <div class="form-group text-align">
          <label class="control-label col-lg-2 col-sm-5 col-xs-12" for="firstname">Brith Date </label>
          <div class="col-lg-4 col-md-6 col-sm-7 col-xs-12 profile-data">
 <?php   
        $start_year = date('Y-m-d', strtotime('-16 year'));
        ?>
                <?php
        $this->widget('zii.widgets.jui.CJuiDatePicker', array(
            'name' => 'age',
            'attribute' => 'age',
            'model' => $user,
            'options' => array(
                'dateFormat' => 'yy-m-d',
               // 'altFormat' => 'dd-mm-yy',
                'changeMonth' => true,
                'changeYear' => true,
                 'maxDate'=>$start_year,
                'yearRange'=>'-100:+0', // range of years (1914-2014) and its dynamic
               // 'appendText' => 'yyyy-mm-dd',
            ),
            'htmlOptions' => array(
                        'class' => 'form-control',
                    ),
                )
        );
        ?>
          </div>
        </div>
        <div class="form-group text-align">
          <label class="control-label col-lg-2 col-sm-5 col-xs-12" for="firstname">phone number </label>
          <div class="col-lg-4 col-md-6 col-sm-7 col-xs-12 profile-data">
              <input type="text" required="" placeholder="" id="" name="User[phone]" value="<?=$user->phone?>" class="form-control">
          </div>
        </div>
        <div class="form-group text-align">
          <label class="control-label col-lg-2 col-sm-5 col-xs-12" for="firstname">Address </label>
          <div class="col-lg-4 col-md-6 col-sm-7 col-xs-12 profile-data">
              <input type="text" required="" placeholder="" id="" name="User[address]" value="<?=$user->address?>" class="form-control">
          </div>
        </div>
        <div class="form-group text-align">
          <label class="control-label col-lg-2 col-sm-5 col-xs-12" for="firstname">Payment Method </label>
          <div class="col-lg-4 col-md-6 col-sm-7 col-xs-12 profile-data">
              <select name="User[payment_method]" class="form-control">
                  <option value="1" <?php if($user->payment_method == 1){echo 'selected'; }else{ echo ''; } ?>>Online</option>
                  <option value="2" <?php if($user->payment_method == 2){echo 'selected'; }else{ echo ''; } ?>>Cash On Deliver</option>
              </select>
          </div>
        </div>
        <div class="col-md-6 col-sm-6 col-xs-6">
        <div class="form-group pull-right">
        <button type="submit" class="btn edit-btn">Save change </button>
        
        <a  href="<?php echo Yii::app()->request->baseUrl; ?>/profile/changepassword"  class="btn edit-btn">Change password </a>
        </div>
        </div>
      <?php $this->endWidget(); ?>
    </div>
    
    <!--end col-9--> 
    
  </div>
</div>
</div>
