
<div class="clearfix"></div>
<div class="container">
    <div class="row">
        <?= $this->renderPartial('user_menu') ?>


        <!--End col-3-->

        <!--start col-9-->
        <div class="col-lg-9 col-md-9 table-card">
            <h2 class="font-h">Usage info</h2>
            <div class="table-responsive">
                <colgroup>
                    <col class="col-xs-1">
                    <col class="col-xs-3">
                    <col class="col-xs-2">
                    <col class="col-xs-2">
                </colgroup>
                <?php
                if ($cards) {
                    ?>
                    <table class="table">
                        <thead>
                            <tr>
                                
                                <th>Card name</th>
                                <th>Valid for</th>
                                <th>No# of Usage</th>


                            </tr>

                        </thead>
                        <tbody>
                            <?php
                            foreach ($cards as $card) {
                                ?>

                                 <tr>
                                
                                <td><?php echo  $card->card_name;  ?></td>
                                <td><?php echo  $card->valid_for;  ?></td>
                                <td><?php echo  $card->usage;  ?></td>

                                     </tr>
                                    <?php
                                }
                                ?>


                        </tbody>
                    </table>


                    <?php
                } else {
                    ?>
                    <div> No items found</div>
                    <?php
                }
                ?>

            </div>

        </div>


        <!--end col-9--> 



    </div> 
</div>
</div>

