<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" lang="en-US" xml:lang="en" xmlns:v="urn:schemas-microsoft-com:vml">
<head>
 <title>CropZoom </title>
    
    <link href="<?php echo Yii::app()->getBaseUrl(true); ?>/js/Crop/css/jquery-ui.css" rel="Stylesheet" type="text/css" /> 
    <link href="<?php echo Yii::app()->getBaseUrl(true); ?>/js/Crop/css/jquery.cropzoom.css" rel="Stylesheet" type="text/css" />  
    <script type="text/javascript" src="<?php echo Yii::app()->getBaseUrl(true); ?>/js/Crop/js/jquery.js"></script>
    <script type="text/javascript" src="<?php echo Yii::app()->getBaseUrl(true); ?>/js/Crop/js/jquery-ui.js"></script>
    <script type="text/javascript" src="<?php echo Yii::app()->getBaseUrl(true); ?>/js/Crop/js/jquery.cropzoom.js"></script>
    
    
  
    
    <?php
    
//        /echo $model['image'].'ddddddddddd';
        $appfullpath=Yii::app()->getBaseUrl(true);
    ?>
    
    <script type="text/javascript">
    $(document).ready(function(){
       
       var cropzoom2 = $('#crop_container2').cropzoom({
            width:800,
            height:500,
            bgColor: '#CCC',
            enableRotation:true,
            enableZoom:true,
            zoomSteps:10,
            rotationSteps:10,
           
            selector:{        
              centered:true,
              borderColor:'blue',
              borderColorHover:'yellow',
              startWithOverlay: true,
			  w:<?= $model['width']?>,
			  h:<?= $model['height']?>,
              hideOverlayOnDragAndResize: true              
            },
            image:{
                source:'<?= $appfullpath."/".$model['image'];?>',
                width:1024,
                height:768,
                minZoom:50,
                maxZoom:200,
               
                startZoom:80,
                useStartZoomAsMinZoom:true,
                snapToContainer:true
            }
        });
        $('#restore2').click(function(){
            cropzoom2.restore();
        });
        $('#crop2').click(function(){ 
            cropzoom2.send('<?= $appfullpath?>/cropimage/cropzoom','POST',{},function(rta){
                $('#result_image').find('img').remove();
                var img = $('<img />').attr('src',rta);
                $('#result_image').append(img);
            });
        });
        
    });
</script>

</head>
<body>

    <div id="page-background-glare">
        <div id="page-background-glare-image"></div>
    </div>
    <div id="crop_container2"></div>
    <span class="button-wrapper" id="restore2">
          <a class="btn" href="javascript:void(0)">Restore</a>
    </span>
    <span class="button-wrapper" id="crop2">
          <a class="btn" href="javascript:void(0)">Crop</a>
    </span>
    <div id="movement"></div>
    <div id="zoom"></div>
    <div id="rot"></div>
    <div id="result_image"></div>
</body>
</html>
