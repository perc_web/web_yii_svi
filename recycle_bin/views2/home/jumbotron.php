<div class="yt-slideshow sl-home-2cl">
    <script type="text/javascript" src="<?= Yii::app()->request->baseUrl ?>/css/frontend/default/sm_flipshop/sm/dynamicslideshow/js/jquery-noconflict.js"></script>
    <script type="text/javascript" src="<?= Yii::app()->request->baseUrl ?>/css/frontend/default/sm_flipshop/sm/dynamicslideshow/js/jquery.themepunch.plugins.min.js"></script>
    <script type="text/javascript" src="<?= Yii::app()->request->baseUrl ?>/css/frontend/default/sm_flipshop/sm/dynamicslideshow/js/jquery.themepunch.revolution.js"></script>
    
     <?php
            if (Yii::app()->user->hasFlash('subscribe')) {
                ?>
                     <div class="alert alert-success"> <!-- flash for sucess -->
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <strong>Notification !</strong> <?php echo Yii::app()->user->getFlash('subscribe'); ?>
                </div>
              


            <?php } ?>
        
    
    
    <div class="dynamicslideshow-container  alway" id="dynamicslideshow_291421413405081"> 
        <div class="dynamicslideshow dynamicslideshow-load">
            <ul>
                <?php
                $banners = Banner::model()->findAll();
                foreach ($banners as $banner) {
                    ?>
                    <li data-transition="slotzoom-horizontal" data-slotamount="1" data-masterspeed="1000">
                        <img src="<?= Yii::app()->request->baseUrl ?>/media/banner/<?= $banner->image ?>">
                        <div class="caption lft title_big"  data-x="69" data-y="65" data-speed="900" data-start="1800" data-easing="easeOutBack">
                            <?= $banner->title ?><br>
                            <?= $banner->subtitle; ?>
                        </div>
                        <div class="hiddenphone-caption caption lfb stl text_big1 title_medium" data-x="69" data-y="165" data-speed="1100" data-start="2000" data-easing="easeOutBack">
                            <p><?= $banner->details; ?></p>
                        </div>
                        <div class="hiddenphone-caption caption lfb stb btn_slide"  data-x="69" data-y="216" data-speed="1100" data-start="2600" data-easing="easeOutExpo"><a title="shop now" href="<?= $banner->link ?>">shop now</a></div>
                    </li>
                    <?php
                }
                ?>	
            </ul>
        </div>
    </div>

    <script type="text/javascript">
        //jQuery(document).ready(function($) {	
        jQuery(window).load(function() {
            jQuery('.dynamicslideshow').removeClass('dynamicslideshow-load');
            if (jQuery.fn.cssOriginal != undefined)
                jQuery.fn.css = jQuery.fn.cssOriginal;
            jQuery('#dynamicslideshow_291421413405081 > .dynamicslideshow').revolution(
                    {
                        delay: 7000,
                        startheight: 450,
                        startwidth: 870,
                        hideThumbs: 200,
                        thumbWidth: 100, // Thumb With and Height and Amount (only if navigation Tyope set to thumb !)
                        thumbHeight: 50,
                        thumbAmount: 5,
                        navigationType: "none", // bullet, thumb, none
                        navigationArrows: "solo", // nexttobullets, solo (old name verticalcentered), none
                        navigationStyle: "round", // round,square,navbar,round-old,square-old,navbar-old, or any from the list in the docu (choose between 50+ different item),
                        navigationHAlign: "right", // Vertical Align top,center,bottom
                        navigationVAlign: "center", // Horizontal Align left,center,right
                        navigationHOffset: 0,
                        navigationVOffset: 20,
                        soloArrowLeftHalign: "left",
                        soloArrowLeftValign: "center",
                        soloArrowLeftHOffset: 20,
                        soloArrowLeftVOffset: 0,
                        soloArrowRightHalign: "right",
                        soloArrowRightValign: "center",
                        soloArrowRightHOffset: 20,
                        soloArrowRightVOffset: 0,
                        touchenabled: "on", // Enable Swipe Function : on/off
                        onHoverStop: "on", // Stop Banner Timet at Hover on Slide on/off
                        stopAtSlide: -1, // Stop Timer if Slide "x" has been Reached. If stopAfterLoops set to 0, then it stops already in the first Loop at slide X which defined. -1 means do not stop at any slide. stopAfterLoops has no sinn in this case.
                        stopAfterLoops: 0, // Stop Timer if All slides has been played "x" times. IT will stop at THe slide which is defined via stopAtSlide:x, if set to -1 slide never stop automatic
                        hideCaptionAtLimit: 0, // It Defines if a caption should be shown under a Screen Resolution ( Basod on The Width of Browser)
                        hideAllCaptionAtLilmit: 0, // Hide all The Captions if Width of Browser is less then this value
                        hideSliderAtLimit: 0, // Hide the whole slider, and stop also functions if Width of Browser is less than this value
                        shadow: 0, //0 = no Shadow, 1,2,3 = 3 Different Art of Shadows  (No Shadow in Fullwidth Version !)
                        fullWidth: "on"							// Turns On or Off the Fullwidth Image Centering in FullWidth Modus			
                    });
        });
    </script>

</div>