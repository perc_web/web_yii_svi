<!-- BEGIN: content -->
<div id="yt_content" class="yt-content wrap">
    <div class="yt-content-inner">
        <div class="slidershow-megamenu"  style="background-color: #ccc;padding-bottom: 0px;">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3 col-md-3">
                        <div class="title-left-menu">
                            Category									
                        </div>
                        <link media="all" href="<?= Yii::app()->request->baseUrl ?>/css/frontend/default/sm_flipshop/sm/megamenu/css/vertical/black/megamenu.css" type="text/css" rel="stylesheet">


                        <div class="css_effect_ver sm_megamenu_wrapper_vertical_menu sambar" id="sm_megamenu_menu543ed99895f48" data-sam="4971413405080">
                            <?php $this->renderPartial("sideMenu"); ?>

                        </div>
                        <!--End Module-->


                        <script type="text/javascript">
                            // <![CDATA[ 
                            jQuery(document).ready(function($) {
                                jQuery('.sm_megamenu_wrapper_vertical_menu .sm_megamenu_menu > li').has('div.sm-megamenu-child').addClass('parent-item');
                            }
                            );
                            // ]]> 
                        </script>
                        <script type="text/javascript">
                            jQuery('document').ready(function($) {
                                $i = 0;
                                $('#yt_left .sm_megamenu_wrapper_vertical_menu ul.sm_megamenu_menu').append('<div class="more-w"><span class="more-view">More Categories</span></div>');
                                $('#yt_left .sm_megamenu_wrapper_vertical_menu ul.sm_megamenu_menu > li').each(function() {
                                    $i++;
                                    if ($i > 8) {
                                        $(this).css('display', 'none');
                                    }
                                    //$('div.sm_megamenu_wrapper_vertical_menu').css('height', $('ul.sm_megamenu_menu').outerHeight());
                                }
                                );
                                $('#yt_left .sm_megamenu_wrapper_vertical_menu ul.sm_megamenu_menu .more-w > .more-view').click(function() {
                                    if ($(this).hasClass('open')) {
                                        $i = 0;
                                        $('#yt_left .sm_megamenu_wrapper_vertical_menu ul.sm_megamenu_menu > li').each(function() {
                                            $i++;
                                            if ($i > 8) {
                                                $(this).slideUp(200);
                                            }
                                        }
                                        );
                                        $(this).removeClass('open');
                                        $('.more-w').removeClass('active-i');
                                        $(this).text('More Categories');
                                    }
                                    else {
                                        $i = 0;
                                        $('#yt_left .sm_megamenu_wrapper_vertical_menu ul.sm_megamenu_menu > li').each(function() {
                                            $i++;
                                            if ($i > 8) {
                                                $(this).slideDown(200);
                                            }
                                        }
                                        );
                                        $(this).addClass('open');
                                        $('.more-w').addClass('active-i');
                                        $(this).text('Close Menu');
                                    }
                                }
                                );
                            }
                            )
                        </script>

                    </div>
                    <div class="col-lg-9 col-md-9">
                        <div class="yt-slideshow sl-home-2cl">
                            <?php $this->renderPartial("jumbotron"); ?>
                        </div>
                    </div>
                </div>
                <!--close row-->
            </div>
            <!--close container-->
        </div>
        <!--close slidershow-->

        <div class="main-full">
            <?php $this->renderPartial("HomeContent", array('latest_items' => $latest_items, 'featured_brands' => $featured_brands, 'hot_deals' => $hot_deals, 'recomended' => $recomended, 'blogs' => $blogs, 'informations' => $informations, 'featured_deals' => $featured_deals,'outlets' => $outlets,)); ?>
            <!--added to fix layout hussam-->
        </div>
    </div>
</div>

</div>
<div class="bottom-wrapper">
</div>
</div>

