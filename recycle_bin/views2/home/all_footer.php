<div id="yt_footer" class="yt-footer wrap">
	<div class="yt-footer-wrap">
      <div class="yt-footer-top">
        <div class="container">
          <div class="row">
            <div class="yt-footer-top-wrap">
              <div class="block block-subscribe-footer">
                
                <div class="block-title">
                  <h2>
                    Subscrible to the Best of FlipShop 
                  </h2>
                </div>
                
                <div class="block-content">
                  <form action="http://localhost:8000/code_v1.0.1/sm_flipshop_quickstart_pl_m1.9.0.1_v1.0.1/index.php/newsletter/subscriber/new/" method="post" id="newsletter-validate-detail">
                    
                    <div class="input-box">
                      <input type="text" name="email" id="newsletter" title="Sign up for our newsletter" class="input-text required-entry validate-email" value="Enter your email..." onblur="if(this.value=='') this.value='Enter your email...';" onfocus="if(this.value=='Enter your email...') this.value='';">
                    </div>
                    <div class="actions">
                      <button type="submit" title="Subscrible" class="button">
                        <span>
                          <span>
                            Subscrible 
                          </span>
                          
                        </span>
                      </button>
                    </div>
                  </form>
                  <script type="text/javascript">
                    //<![CDATA[
                    var newsletterSubscriberFormDetail = new VarienForm('newsletter-validate-detail');
                    //]]>
                  </script>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="yt-footer-middle">
        <?php $this->renderPartial("footer");?>
      </div>
      <div class="yt-footer-bottom">
        <div class="container">
          <div class="row">
			<div class="col-lg-12 col-md-12 yt-copyright">
              SM Flipshop © 2014 
              <b>
                <a title="Magento Themes" target="_blank" href="http://www.magentech.com/themes/premium-themes.html">
                  Magento Themes
                </a>
              </b>
              Demo Store. All Rights Reserved. Designed by 
              <b>
                <a href="http://www.magentech.com/index.html" target="_blank" title="MagenTech.Com">
                  MagenTech.Com
                </a>
              </b>
              
			</div>
          </div>
        </div>
      </div>
    </div>
    
    
  </div>