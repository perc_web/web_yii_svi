      
    <?php
  $this->widget('application.extensions.fancybox.EFancyBox', array(
    'target'=>'.fancyElm',
    'config'=>array(
        'maxWidth'    => 1000,
        'maxHeight'   => 1000,
        'fitToView'   => false,
        'width'       => '90%',
        'height'      => '90%',
        'autoSize'    => false,
        'closeClick'  => false,
        'openEffect'  => 'none',
        'closeEffect' => 'none',
	'type'        =>'iframe',
    ),
));
  
?> 

<!-- BEGIN: content -->
<div id="yt_content" class="yt-content wrap">
    <div class="yt-content-inner">
        <div class="main-full">
            <div class="container">
                <div class="row">
                    <?= $this->renderPartial('left_side'); ?>

                    <div id="yt_main" class="yt-main-right yt-main col-main col-lg-9 col-md-9">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-9">
                                    <div id="messages_product_view">
                                        <!--<ul class="messages"><li class="notice-msg"><ul><li><span>///</span></li></ul></li></ul></div>-->
                                        <div class="yt-product-detail">
                                            <div class="yt-product-detail-inner">
                                                <div class="row product-essential">
                                                    <div class="box-1">
                                                        <div class="image-box-detail col-lg-5 col-md-5 col-sm-5 col-xs-12">

                                                        <div class="item-zoom">
                                                           
                                                            
                                                      
                                                                <img id="zoom_03" class="slider-zoom" src="<?= Yii::app()->request->baseUrl; ?>/media/menus/<?=$menu->image;?>" data-zoom-image="<?= Yii::app()->request->baseUrl; ?>/media/menus/<?=$menu->image;?>"/> 
                                                   
                                                        <div id="gallery_01" class="gallery col-md-12 col-xs-12">
                                                            <?php
                                                            foreach($photos as $photo){ ?>
                                                                <a href="#" data-image="<?= Yii::app()->request->baseUrl; ?>/media/items/<?=$photo->item->image?>" data-zoom-image="<?= Yii::app()->request->baseUrl; ?>/media/items/<?=$photo->item->image?>" class="col-md-3"> 
                                                                <img id="img_01" src="<?= Yii::app()->request->baseUrl; ?>/media/items/<?=$photo->item->image?>" /> 
                                                                </a>
                                                            <?php
                                                            }
                                                            
                                                            ?> 
                                                        </div> 
                                                            </div>

                                                        </div>  

                                                        <script src="<?php echo Yii::app()->baseUrl; ?>/js/jquery.elevatezoom.js"></script>

                                                        <script>
                                                            $("#zoom_03").elevateZoom({gallery: 'gallery_01', zoomType: "inner"});
                                                        </script> 

                                                        <div class="yt-detail-right col-lg-7 col-md-7 col-sm-7 col-xs-12">          
                                                            <form action="<?= Yii::app()->getBaseUrl(true) ?>/home/addCart?flag=menu" method="post" id="product_addtocart_form">              
                                                                <div class="product-shop">
                                                                    <h2 class="product-name">
                                                                        <?= $menu->title ?>
                                                                    </h2>

                                                                    <!--<div class="product-review">
                                                                        <div class="ratings">
                                                                            <div class="rating-box">
                                                                                <div class="rating" style="width:70%"></div>
                                                                            </div>
                                                                            <p class="rating-links">
                                                                                <a href="review/product/list/id/480/index.html">2 Review(s)</a>
                                                                                <span class="separator">|</span>
                                                                                <a href="review/product/list/id/480/index.html#review-form">Add Review</a>
                                                                            </p>
                                                                        </div>
                                                                    </div>--> 
                                                                    
                                                                    <p>Price:</p>
                                                                   <div class="price-box">
                                                                        <span class="special-price">
                                                                        <span class="special-price" id="product-price-480"><?php echo Yii::app()->params['currency'] ; ?>  <?php echo  $menu->price ?> </span>
                                                                        </span>
                                                                
                                                                    </div>
                                                                    
                                                                     <p>Discount:</p>
                                                                         <div class="price-box">
                                                                        <?php
                                                                      
                                                                            if ($menu->discount_type == 1) {
                                                                                echo '<span class="special-price">
                                                                        <span class="special-price" id="product-price-480">' . $menu->discount . '%</span>
                                                                        </span>';
                                                                            } else {
                                                                                echo '<span class="special-price">
                                                                        <span class="special-price" id="product-price-480"> ' .Yii::app()->params['currency'] .' '. $menu->discount . '  </span>
                                                                        </span>';
                                                                            }
                                                                         
                                                                        ?>
                                                                    </div>
                                                                    
                                                                    
<!--                                                                    <div class="price-box">
                                                                        <span class="special-price">
                                                                        <span class="special-price" id="product-price-480"><?php echo  $menu->discount ?> </span>
                                                                        </span>
                                                                
                                                                    </div>-->
                                                                
                                                                
                                                                    <div class="short-description">
                                                                        <h2 class="title-short-des">QUICK OVERVIEW</h2>
                                                                        <p><?= $menu->intro; ?></p>                         
                                                                    </div>
                                                                    <div class="clearer"></div>

                                                                    <input type="hidden" name="Reservations[menu_id]" value="<?=$menu->id?>">
                                                                    <button class="button pull-right" type="submit">
                                                                        <span>
                                                                            <span>Add To Calculator</span>
                                                                        </span>
                                                                    </button>
                                                                </div>
                                                            </form>
                                                        </div>

                                                        <script>

                                                            $('.form-input').keyup(function () {
                                                                // GET INPUT VALUE
                                                                var day, month, year;
                                                                day = $('.day').val();
                                                                month = $('.month').val();
                                                                year = $('.year').val();
                                                                // ORGANIZE VALUES
                                                                var date = year + '/' + month + '/' + day;
                                                                // GENERATE AGE
                                                                var birthdate, cur, diff, age;
                                                                birthdate = new Date(date);
                                                                cur = new Date();
                                                                diff = cur - birthdate;
                                                                age = Math.floor(diff / 31536000000);
                                                                // DISPLAY AGE
                                                                $('#age').html(age);
                                                            }).keyup();

                                                        </script>


                                                        <div id="yt_tab_products" class="col-md-12 tab-product-detail">
                                                            <ul class="yt-tab-navi">
                                                                <li class="active">
                                                                    <a data-toggle="tab" href="#decription">Description</a>
                                                                </li>
<!--                                                                <li >
                                                                    <a data-toggle="tab" href="#tags">Tags</a>
                                                                </li>-->

                                                                <li >
                                                                    <a data-toggle="tab" href="#yt_tab_reviewform">Reviews</a>
                                                                </li>
                                                               

                                                                <li >
                                                                    <a data-toggle="tab" href="#items">Items</a>
                                                                </li>
                                                            </ul>
                                                            <div class="yt-tab-content tab-content">                        
                                                                <div id="decription" class="tab-pane fade active in">
                                                                    <!-- h2></h2-->
                                                                    <div class="std">
                                                                        <?php
                                                                        echo $menu->description;
                                                                        ?>
                                                                    </div>
                                                                </div>
                                                                <div id="tags" class="tab-pane fade">
                                                                    <div class="collateral-box">
                                                                        <div class="box-collateral box-tags">
                                                                            <h2>Product Tags</h2>
                                                                            <h3>Other people marked this product with these tags:</h3>
    <ul id="product-tags_78a2b29331e953127485d2f908b3a07d" class="product-tags">
        <?php

        foreach($item_tags as $tag){ 
           echo  $tag->id;
            $count = ItemTag::model()->count(array('condition' => 'item_id = '.$tag->item_id));
             //  $count = ItemTag::model()->count(array('condition' => 'item_id = '.$tag->item_id.' and tag_id = '.$tag->tag_id));
            ?>
            <li class="first"><?= $tag->tag->title; ?> (<?=$count?>)</li>
        <?php
        }

        ?>
    </ul>

<form id="addTagForm" action="" method="post">
    <div class="form-add">
        <label for="productTagName">Add Your Tags:</label>
        <div class="input-box">
            <input type="hidden" class="input-text required-entry" name="productTagName" id="productTagName">
            <input type="hidden" class="input-text required-entry" name="productNewTags" id="productNewTag">
            <?php
            $this->widget('zii.widgets.jui.CJuiAutoComplete', array(
               'name' => 'autocompletesearch',
               'source' => Yii::app()->request->baseUrl . '/home/getTags', // <- path to controller which returns dynamic data
                'options' => array(
                   'minLength' => '1', // min chars to start search
                   'select' => 'js:function(event, ui)
                    { 
                        $("#productTagName").val(ui.item.id);
                        $("#productNewTag").val(ui.item.label);
                    }'
                ),
                'htmlOptions' => array(
                   'class' => 'form-control',
                   'placeholder' => 'Friend,page or event',
                ),
            ));
            ?>
        </div>
        <button type="submit" title="Add Tag" class="button">
            <span>
                <span>Add Tag</span>
            </span>
        </button>
    </div>
</form>
                                                                        </div>
                                                                    </div>
                                                                </div>

<div id="yt_tab_reviewform" class="tab-pane fade">
    <div class="box-collateral box-reviews" id="customer-reviews">
        <h2>Customer Reviews</h2>
        <div class="pager">
            <p class="amount">
                <strong><?=count($reviews)?> Item(s)</strong>
            </p>
            <div class="limiter">
                <label>Show</label>
                 <form method="post" action="" id="pagesform">
                    <select onchange="countReviews()" class="jqtransformdone" name="page">
                        <option value="5" <?php
                        if ($_REQUEST['page'] == 5) {
                            echo "selected";
                        } else {
                            echo "";
                        }
                        ?>>
                            5            
                        </option>
                        <option value="20" <?php
                        if ($_REQUEST['page'] == 20) {
                            echo "selected";
                        } else {
                            echo "";
                        }
                        ?>>
                            20           
                        </option>
                        <option value="50" <?php
                        if ($_REQUEST['page'] == 50) {
                            echo "selected";
                        } else {
                            echo "";
                        }
                        ?>>
                            50            
                        </option>
                    </select> per page 
                </form>
            </div>
        </div>
        <dl>
            <?php
           
            foreach($reviews as $review){ 
                $price = ($review->price_rate/5)*100;
                $value = ($review->value_rate/5)*100;
                $quality = ($review->quality_rate/5)*100;
            ?>
            <dt>
            first article Review by <span><?= $review->user->username; ?></span>            
            </dt>
            <dd>
                <table class="ratings-table">
                    <colgroup>
                        <col width="1">
                        <col>
                    </colgroup>
                    <tbody>
                        <tr>
                            <th>Price</th>
                            <td>
                                <div class="rating-box">
                                    <div class="rating" style="width:<?=$price?>%;"></div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th>Value</th>
                            <td>
                                <div class="rating-box">
                                    <div class="rating" style="width:<?=$value?>%;"></div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th>Quality</th>
                            <td>
                                <div class="rating-box">
                                    <div class="rating" style="width:<?=$quality?>%;"></div>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <?=$review->review;?>
                <small class="date">(Posted on <?= date('d/m/Y', strtotime($review->date_created)); ?>)</small>
             
            </dd>
            <?php
            }
            
            ?>
        </dl>
    </div>
    <?php
    if(Yii::app()->user->id){
  $review_before=  Reviews::model()->find(array('condition' => 'user_id = '.Yii::app()->user->id.' and item_id = '.$menu->id)) ;
    if( ! $review_before){ ?>
        <div class="form-add">
            <!--h2></h2-->
            <form action="" method="post" id="review-form">
                <input name="form_key" type="hidden" value="0dFh25rijWrMUXuC">
                <fieldset>  
                    <div class="customer-review">
                        <h4>How do you rate this product? <!--em class="required">*</em--></h4>
                        <span id="input-message-box"></span>                
                        <table class="data-table" id="product-review-table">                        
                            <thead>
                                <tr class="first last">
                                    <th>&nbsp;</th>
                                    <th><span class="nobr">1 star</span></th>
                                    <th><span class="nobr">2 stars</span></th>
                                    <th><span class="nobr">3 stars</span></th>
                                    <th><span class="nobr">4 stars</span></th>
                                    <th><span class="nobr">5 stars</span></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr class="first odd">
                                    <th>Quality</th>
                                    <td class="value"><input type="radio" name="Reviews[quality_rate]" id="Quality_1" value="1" class="radio"></td>
                                    <td class="value"><input type="radio" name="Reviews[quality_rate]" id="Quality_2" value="2" class="radio"></td>
                                    <td class="value"><input type="radio" name="Reviews[quality_rate]" id="Quality_3" value="3" class="radio"></td>
                                    <td class="value"><input type="radio" name="Reviews[quality_rate]" id="Quality_4" value="4" class="radio"></td>
                                    <td class="value last"><input type="radio" name="Reviews[quality_rate]" id="Quality_5" value="5" class="radio"></td>
                                </tr>
                                <tr class="even">
                                    <th>Price</th>
                                    <td class="value"><input type="radio" name="Reviews[price_rate]" id="Price_1" value="1" class="radio"></td>
                                    <td class="value"><input type="radio" name="Reviews[price_rate]" id="Price_2" value="2" class="radio"></td>
                                    <td class="value"><input type="radio" name="Reviews[price_rate]" id="Price_3" value="3" class="radio"></td>
                                    <td class="value"><input type="radio" name="Reviews[price_rate]" id="Price_4" value="4" class="radio"></td>
                                    <td class="value last"><input type="radio" name="Reviews[price_rate]" id="Price_5" value="5" class="radio"></td>
                                </tr>
                                <tr class="last odd">
                                    <th>Value</th>
                                    <td class="value"><input type="radio" name="Reviews[value_rate]" id="Value_1" value="1" class="radio"></td>
                                    <td class="value"><input type="radio" name="Reviews[value_rate]" id="Value_2" value="2" class="radio"></td>
                                    <td class="value"><input type="radio" name="Reviews[value_rate]" id="Value_3" value="3" class="radio"></td>
                                    <td class="value"><input type="radio" name="Reviews[value_rate]" id="Value_4" value="4" class="radio"></td>
                                    <td class="value last"><input type="radio" name="Reviews[value_rate]" id="Value_5" value="5" class="radio"></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="write-your-review">         
                        <h4>Write your own review</h4>
                        <ul class="form-list">
                            <!--<li>
                                <label for="nickname_field" class="required">Nickname<em> *</em></label>
                                <div class="input-box">
                                    <input type="text" name="nickname" id="nickname_field" class="input-text required-entry" value="">
                                </div>
                            </li>-->
                            <li>
                                <label for="summary_field" class="required">Summary of Your Review<em> *</em></label>
                                <div class="input-box">
                                    <input type="text" name="Reviews[title]" id="summary_field" class="input-text required-entry" value="">
                                </div>
                            </li>
                            <li>
                                <label for="review_field" class="required">Review<em> *</em></label>
                                <div class="input-box">
                                    <textarea name="Reviews[review]" id="review_field" cols="10" rows="6" class="required-entry"></textarea>
                                </div>
                            </li>
                        </ul>
                    </div>

                </fieldset>
                <div class="buttons-set">
                    <button type="submit" title="Submit Review" class="button">
                        <span><span class="submit-review-text">Submit Review</span></span>
                    </button>
                </div>
            </form>
        </div>
    <?php
    }
    }
    ?>
</div>
                   
                                                                
    <div id="items" class="tab-pane fade">
   <!-- h2></h2-->
   <div class="std">
          <?php
if ($photos) {
foreach ($photos as $photo) {
   $item= $photo->item ;
      ?>
       
      <div class="item col-lg-4 col-md-6 col-sm-6 respl-item itemsscroll">
                                                <div class="item-inner">
                                                    <div class="w-image-box">
                                                        <span class="hover-background"></span>
                                                        <div class="item-image">
                                                            <a href="<?= Yii::app()->request->baseUrl ?>/Product-<?php echo $item->slug; ?>" title="<?=$item->title?>"  class="product-image"><img src="<?= Yii::app()->request->baseUrl ?>/media/items/<?php echo $item->image; ?>" alt="<?=$item->title?>"     style="width:258px  !important;height:258px !important;"  ></a>
                                                            <a class="sm_quickview_handler fancyElm" title="Quick View" href="<?= Yii::app()->request->baseUrl ?>/itemfancy-<?= $item->slug ?>">Quick View</a>
         
                                                                      
      <?php
      echo ' <span class="disc">price: '.Yii::app()->params['currency'].' '. $item->price .' </span>';
    
?>  

                                                          
                                                           
                                                              <a class="fancyElm"  href="<?= Yii::app()->request->baseUrl ?>/itemfancy-<?php echo $item->slug; ?>">
                                                                  <p >
                                                                <?= substr($item->intro, 0, 50)?>
                                                            </p>
                                                                 
                                                              </a>
                                                            
                                                        </div>
                                                        
                                                        
                                                        
                                                    </div>
                                                    <div class="item-info">
                                                        <div class="item-title">
                                                            <a href="<?= Yii::app()->request->baseUrl ?>/Product-<?= $item->slug ?>" title="<?=$item->title;?>"><?php echo $item->title; ?></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
       
       
       <?php
     }
} else {
                ?>
            <div> No items found.</div>
            <?php
              }
            ?>

   </div>
</div>                                                            
                                                               
                                                               
                                                            </div>
                                                        </div>  
                                                        <div style="clear:both;"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--added to fix layout hussam-->
            </div>
        </div>
    </div>   
</div>
<div class="bottom-wrapper">
</div>
</div>
<script>
    function countReviews(){
        $('#pagesform').submit();
    }
    
    function like(review){
        $.ajax({
            type:"post",
            url:"<?=Yii::app()->getBaseUrl(true);?>/home/likeReview",
            data: {id : review},
            success: function (data) {
                        location.reload(); 
                    }
        });
    }
    function dislike(review){
        $.ajax({
            type:"post",
            url:"<?=Yii::app()->getBaseUrl(true);?>/home/dislikeReview",
            data: {id : review},
            success: function (data) {
                        location.reload(); 
                    }
        });
    }
</script>
