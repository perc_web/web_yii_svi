
<!-- BEGIN: content -->
<div id="yt_content" class="yt-content wrap">
    <div class="yt-content-inner">
        <div class="main-full">
            <?php if (isset($_REQUEST['category']) && isset($_REQUEST['partner'])) { ?>
                <script>
                    $(document).ready(function() {
                        setTimeout(change_category(), 1000);
                    });
                    
                    function change_category() {
                        $('#cat_drop').val(<?= $_REQUEST['category']; ?>); // get category from url
                         
                        change_partners(<?= $_REQUEST['category']; ?>); // then load all partnersby this cat_id
                        setTimeout(function(){
                            $('#partner_drop').val(<?= $_REQUEST['partner']; ?>); // after that fill partner by  partner request
                            get_outlets(<?= $_REQUEST['partner']; ?>); // then load all outlets by this partner id
                            
                             $('#outlet_drop').val(<?= $_REQUEST['outlet']; ?>); // after that fill outlet by  outlet request
                            get_menus(<?= $_REQUEST['outlet']; ?>);  // then load all menus by this outlet request
                            
                            
                        },1000);
                    }
                </script>
                <?php
            }
            ?>
                <script>
                    function change_partners(category) {
                        $.ajax({
                            url: "<?= Yii::app()->getBaseUrl(true); ?>/home/getpartners",
                            data: {cat_id: category},
                            success: function(data) {
                                $("#partner").html(data);
                            }
                        });
                    }
                </script>
            <div class="container">
                <div class="row">
                    <?= $this->renderPartial('left_side') ?>
                    <!--<div class="col-lg-3 col-md-3">
                      <div class="title-left-menu">
                        Category                  
                      </div>
                      <link media="all" href="http://demo.magentech.com/themes/sm_flipshop/skin/frontend/default/sm_flipshop/sm/megamenu/css/vertical/black/megamenu.css" type="text/css" rel="stylesheet">
                      
                      <div class="css_effect_ver sm_megamenu_wrapper_vertical_menu sambar" id="sm_megamenu_menu543e5eacd17a2" data-sam="1689053291413373612">
                    <?php //  include("sideMenu.php"); ?>
          </div>
          
  
                  <div class="yt-left-wrap">
                      <div class="block-st-img"><a title="Static Image Left" href="#"><img src="http://demo.magentech.com/themes/sm_flipshop/media/wysiwyg/static-img-left.png" alt="Static Image Left"></a></div>
  
                  </div>
  </div>-->
                    <div class="col-lg-9 col-md-9">

                        <?php
                        if (Yii::app()->user->hasFlash('reserve')) {
                            ?>
                            <div class="alert alert-success"> <!-- flash for sucess -->
                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                                <strong>Notification !</strong> <?php echo Yii::app()->user->getFlash('reserve'); ?>
                            </div>
                        <?php } ?>
                        <div class="row box-1-reserve">
                            <div class="col-md-12 page-title title-reserve">
                                <h1>RESERVE</h1>
                            </div>
                            <div class="contact-map-wrap col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="block-map1">
                                    <div class="col-md-12 sub-header">
                                        <h3>Reservation Details</h3>
                                    </div>
                                    <?php
                                    $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
                                        'id' => 'contactForm',
                                        'enableAjaxValidation' => false,
                                        'type' => 'horizontal',
                                        'htmlOptions' => array('class' => 'reserve-form col-lg-12 col-md-12  col-sm-12 col-xs-12', 'enctype' => 'multipart/form-data',
                                   'onsubmit'=>"return check();",
                                            ),
                                    ));
                                    ?>
                                    <div class="row">
                                        <div class="col-md-12 top-buffer">
                                            <div class="form-group col-md-6">
                                                <label class="col-md-4">Category</label>
                                                <div class="col-md-8">
                                                    <?php
                                                    echo $form->dropDownList($model, 'category_id', Chtml::listData(Category::model()->findAll(), 'id', 'title'), array('prompt' => 'Choose',
                                                        'class' => 'form-control col-md-8 jqtransformdone',
                                                        'id' => 'cat_drop',
                                                        'onchange' => 'change_partners(this.value)'
                                                            )
                                                    );
                                                    ?>              
                                                </div>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label class="col-md-4">Partner</label>
                                                <div class="col-md-8" id="partner">
                                                    <select readonly class="form-control col-md-8 jqtransformdone">
                                                        <option>Select Category first</option>
                                                    </select>
<?php
// echo $form->dropdownList($model, 'partner_id', Chtml::listData(Partner::model()->findAll(), 'id', 'title'), array('class' => 'form-control col-md-8 jqtransformdone', 'prompt' => 'Choose'));
?>              
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12 top-buffer">
                                            <div class="form-group col-md-6">
                                                <label class="col-md-4">Outlet</label>
                                                <div class="col-md-8" id="outlet">
                                                    <select readonly class="form-control col-md-8 jqtransformdone">
                                                        <option>Select Partner first</option>
                                                    </select>
<?php
//echo $form->dropdownList($model, 'outlet_id', Chtml::listData(Outlet::model()->findAll(), 'id', 'title'), array('class' => 'form-control col-md-8 jqtransformdone', 'prompt' => 'Choose'));
?>              
                                                </div>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label class="col-md-4">Menu</label>
                                                <div class="col-md-8" id="menu">
                                                    <select readonly class="form-control col-md-8 jqtransformdone">
                                                        <option>Select Outlet first</option>
                                                    </select>
<?php
//echo $form->dropdownList($model, 'item_id', Chtml::listData(Item::model()->findAll(), 'id', 'title'), array('class' => 'form-control col-md-8 jqtransformdone', 'prompt' => 'Choose'));
?>              
                                                </div>
                                            </div>
                                            
<!--                                            <div class="form-group col-md-6">
                                                <label class="col-md-4">Item</label>
                                                <div class="col-md-8" id="item">
                                                    <select readonly class="form-control col-md-8 jqtransformdone">
                                                        <option>Select Menu first</option>
                                                    </select>
<?php
//echo $form->dropdownList($model, 'item_id', Chtml::listData(Item::model()->findAll(), 'id', 'title'), array('class' => 'form-control col-md-8 jqtransformdone', 'prompt' => 'Choose'));
?>              
                                                </div>
                                            </div>-->
                                            
                                            
                                            
                                        </div>


                                        <div class="col-md-12">
                                            <div class="form-group col-md-6">
                                                <label class="col-md-4">Date</label>
                                                <div class="col-md-8">

<?php
Yii::import('application.extensions.CJuiDateTimePicker.CJuiDateTimePicker');
$this->widget('CJuiDateTimePicker', array(
    'model' => $model, //Model object
    'attribute' => 'date', //attribute name
    'language' => '',
    //  You can force English language by setting the language attribute as '' (empty string)
    'options' => array(
        "dateFormat" => 'yy-mm-dd',
        // 'dateFormat'=>'mm/dd/yy',//Date format 'mm/dd/yy','yy-mm-dd','d M, y','d MM, y','DD, d MM, yy'
        //   'changeMonth' => 'true',
        //  'changeYear' => 'true',
        'showOtherMonths' => true, // Show Other month in jquery
    ),
    'htmlOptions' => array(
    //'class'=>'date_booking_details'
    ),
    'mode' => 'date', //use "time","date" or "datetime" (default)
));
?>        

                                                </div>
                                            </div>

                                        </div>
                                        
                                        
                                        <div class="col-md-12">
                                            <div class="form-group col-md-6">
                                                <label class="col-md-4">From</label>
                                                <div class="col-md-3 pull-me-right"> 

<?php
Yii::import('application.extensions.CJuiDateTimePicker.CJuiDateTimePicker');
$this->widget('CJuiDateTimePicker', array(
    'model' => $model, //Model object
    'attribute' => 'from', //attribute name
    'language' => 'en',
    'htmlOptions' => array(
        'class' => 'span2',
        'style' => 'width:100%'
    ),
    'mode' => 'time', //use "time","date" or "datetime" (default)
      'options'=>array(
           'onSelect' => 'js:function() {
                        $("#Reservations_to").val(this.value);
                    }
                    ',
          ) ,
));
?>


                                                </div>
                                            
                                                <label class="col-md-1">To</label>
                                                <div class="col-md-3 pull-me-right">

<?php // echo $form->dropdownList($model, 'to', Helper::allHours(), array( 'class' => 'form-control col-md-8 jqtransformdone'));   ?>
<?php
Yii::import('application.extensions.CJuiDateTimePicker.CJuiDateTimePicker');
$this->widget('CJuiDateTimePicker', array(
    'model' => $model, //Model object
    'attribute' => 'to', //attribute name
    'language' => 'en',
    'htmlOptions' => array(
        'class' => 'span2',
        'style' => 'width:100%'
    ),
    'mode' => 'time', //use "time","date" or "datetime" (default)
        //'options'=>array("dateFormat"=>'yy-mm-dd') ,// jquery plugin options
     'options'=>array(
           'onSelect' => 'js:function() {
                        var from=$("#Reservations_from").val() ;
                          if(this.value <=  from){
                          alert("To must be greater than From");
                          }                        
                    }
                    ',
          ) ,
));
?>

                                                </div>

                                            </div>
                                            <div class="form-group col-md-6">
                                                <label class="col-md-4"># of Customers</label>
                                                <div class="col-md-8">
                                                    <input type="number" class="max-width"   value="1"   name="Reservations[num_customers]" >
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-11"> 
                                            <div class="buttons-set col-md-3 clear_element pull-right ">
                                                <input type="text" name="hideit" id="hideit" value="" style="display:none !important;">
                                                <button type="submit" title="Reserve" class="button col-md-12 grapefruit "><span><span style="grapefruit">Reserve</span></span></button>
                                            </div>
                                        </div>


                                    </div>
<?php $this->endWidget(); ?>
                                </div>
                                <div style="clear:both; padding:15px 0px;">&nbsp;</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>




<script>
 
function check()
 {
   var from =$("#Reservations_from").val() ;
   var to =$("#Reservations_to").val() ;
  if(to <=  from){
                          alert("To must be greater than From");
                          return false;
                    }else{
                         return true;
                    }  
 
}
 
</script>
   

