
          
         <?php
  $this->widget('application.extensions.fancybox.EFancyBox', array(
    'target'=>'.fancyElm',
    'config'=>array(
        'maxWidth'    => 1000,
        'maxHeight'   => 1000,
        'fitToView'   => false,
        'width'       => '90%',
        'height'      => '90%',
        'autoSize'    => false,
        'closeClick'  => false,
        'openEffect'  => 'none',
        'closeEffect' => 'none',
	'type'        =>'iframe',
    ),
));
  
?>        
  

<script>var d = document.getElementById("partnersNavBtn");
    d.className = d.className + " layout-flipshop-parent ";</script>
<div class="container">
    <div class="row">
        <?= $this->renderPartial('left_side') ?>
        <div id="yt_main" class="yt-main-right yt-main col-main col-lg-9 col-md-9">
            <div class="yt_main_inner">  
                <?php
                $advertise = Advertise::model()->find(array('condition' => 'active=1 and page=3 and size=3', 'order' => 'rand()'));
                ?>
                <div class="page-title category-title">
                    <div class="category-description std">
                        <a title="<?php echo $advertise->link; ?>"  target="_blank" href="<?php echo $advertise->link; ?>">
                            <img src="<?= Yii::app()->request->baseUrl ?>/media/advertise/<?php echo $advertise->image; ?>" alt="<?php echo $advertise->link; ?>">
                        </a>
                    </div>
                </div>
<!--                <div class="block block-tags">
                    <div class="block-content">
                        <ul class="tags-list">
                            <?php
                            foreach($categories as $cat){ ?>
                                <li>
                                    <a style="font-size:145%;" href="<?=Yii::app()->getBaseUrl(true);?>/home/partners?category=<?=$cat->slug?>"><?=$cat->title?></a>
                                </li>
                            <?php
                            }
                            ?>
                        </ul>
                    </div>
                </div>-->
                <div id="catalog-listing">
                    <div class="category-products">
                        <div class="toolbar">
                            <div class="toolbar-inner">
                                <div class="sorter">
                                    <div class="view-mode-wrap">
                                        <p class="view-mode">
                                            <a title="Grid" href="#grid2" data-toggle="tab" class="grid">Grid</a>&nbsp;
                                            <a href="#list2" data-toggle="tab" title="List" class="list">List</a>&nbsp;
                                        </p>
                                    </div>
                                    <div class="sort-by-wrap">
                                        <div class="sort-by">
                                            <label>Sort By </label>
                                            <div class="select-new">
                                                <div class="select-inner dropdown">
                                                    <div class="overwrite-sortby">Name</div>
                                                    <span class="btn-toolbar" data-toggle="dropdown"></span>
                                                    <ul id="sort_by" class="dropdown-menu" role="menu" aria-labelledby="dLabel">
                                                        <li>
                                                            <a href="javascript:void(0)" onclick="insertParam('sort', 'title desc')">DESC</a>
                                                            <a href="javascript:void(0)" onclick="insertParam('sort', 'title asc')">ASC</a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <a class="btn-sortby set-desc arrow-down" href="" title="Set Descending Direction">Desc</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="myTabContent2" class="tab-content content-tab-block">
                            <div class="tab-pane fade active in" id="grid2">
                                <div class="products-grid row" id="filter1">  
                                    <?php
                                    if ($partners) {
                                        foreach ($partners as $partner) {
                                    ?>
                                        <div class="item col-lg-4 col-md-6 col-sm-6 respl-item itemsscroll">
                                            <div class="item-inner">
                                                <div class="w-image-box">
                                                    <span class="hover-background"></span>
                                                    <div class="item-image">
                                                        <a href="<?= Yii::app()->request->baseUrl ?>/partner-<?php echo $partner->slug; ?>" title="<?=$partner->title?>" class="product-image">
                                                            <img style="width:270px;height: 270px !important ;" src="<?= Yii::app()->request->baseUrl ?>/media/partner/<?php echo $partner->image; ?>" alt="<?=$partner->title?>"></a>
                                                        <a class="sm_quickview_handler fancyElm" title="Quick View" href="<?= Yii::app()->request->baseUrl ?>/partnerfancy-<?php echo $partner->slug; ?>">Quick View</a>
                                                        <div class="quick-btns">
                                                            <?php
                                                            if(Yii::app()->user->id){ ?>
                                                                <a class="sm_quickview_handler quick-reserve" title="Quick Reserve" href="<?= Yii::app()->request->baseUrl ?>/home/reserve?category=<?=$partner->cat_id?>&partner=<?=$partner->id?>"><img src="<?= Yii::app()->request->baseUrl ?>/images/free-ship-icon.png" alt="" /></a>
                                                            <?php
                                                            }else{
                                                                ?>
                                                                                                           <a class="sm_quickview_handler quick-reserve" title="Quick Reserve"  data-toggle="modal" data-target="#modal-login" title="Login" ><img src="<?= Yii::app()->request->baseUrl ?>/images/free-ship-icon.png" alt="" /></a>    
                                                                    
                                                           <?php
                                                                
                                                            }
                                                            ?>
                                                            <a class=" sm_quickview_handler quick-calculate" title="Quick Calculate" href="<?= Yii::app()->request->baseUrl ?>/home/calculate?category=<?=$partner->cat_id?>&partner=<?=$partner->id?>"><img src="<?= Yii::app()->request->baseUrl ?>/images/icon-title.png" alt="" /></a>
                                                        </div>
                                                         
                                                               <?php
                                                                   if($partner->discount > 0){
                                                                       ?>
                                                                    
                                                                       <span class="disc">Discount:  <?php echo Yii::app()->params['currency'] ; ?>  <?=$partner->discount?></span>
                                                                    
                                                                    <?php
                                                                   }
                                                                   ?>
                                                      
                                                        
                                                         <a class="fancyElm"  href="<?= Yii::app()->request->baseUrl ?>/partnerfancy-<?php echo $partner->slug; ?>">
                                                                  <p>
                                                                <?= substr($partner->intro, 0, 50)?>
                                                            </p>
                                                                 
                                                              </a>
                                                            
                                                    </div>
                                                </div>
                                                <div class="item-info">
                                                    <div class="item-title">
                                                        <a href="<?= Yii::app()->request->baseUrl ?>/partner-<?php echo $partner->slug; ?>" title="<?=$partner->title;?>"><?php echo $partner->title; ?></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php
                                        }
                                      } else {
                                    ?>
                                <div> No items found.</div>
                                <?php
                                  }

                                  ?>  
                                </div>
                                <?php
                                $this->widget('ext.yiinfinite-scroll.YiinfiniteScroller', array(
                                    'contentSelector' => '#filter1',
                                    'itemSelector' => 'div.itemsscroll',
                                    'loadingText' => '',
                                    'donetext' => '',
                                    'pages' => $scroll_partners,
                                    'navigationLinkText' => '',
                                ));
                                ?>
                            </div>
                            <div class="tab-pane fade" id="list2">
                                <div class="products-list row" id="filter2">
                                    <?php
                                    if ($partners) {
                                        foreach ($partners as $partner) {
                                    ?>
                                        <div class="item col-xs-12 respl-item itemsscroll2 clearfix">
                                            <div class="item-inner">
                                                <div class="w-image-box">
                                                    <span class="hover-background"></span>
                                                    <div class="item-image">
                                                        <a href="<?= Yii::app()->request->baseUrl ?>/partner-<?php echo $partner->slug; ?>" title="<?=$partner->title?>" class="product-image"><img src="<?= Yii::app()->request->baseUrl ?>/media/partner/<?php echo $partner->image; ?>" alt="<?=$partner->title?>"></a>
                                                        <a class="sm_quickview_handler  fancyElm" title="Quick View" href="<?= Yii::app()->request->baseUrl ?>/partner-<?php echo $partner->slug; ?>">Quick View</a>
                                                        <div class="quick-btns">
                                                            <?php
                                                            if(Yii::app()->user->id){ ?>
                                                                <a class="sm_quickview_handler quick-reserve" title="Quick Reserve" href="<?= Yii::app()->request->baseUrl ?>/home/reserve?category=<?=$partner->cat_id?>&partner=<?=$partner->id?>"><img src="<?= Yii::app()->request->baseUrl ?>/images/free-ship-icon.png" alt="" /></a>
                                                            <?php
                                                            }else{
                                                                ?>
                                                                                                           <a class="sm_quickview_handler quick-reserve" title="Quick Reserve"  data-toggle="modal" data-target="#modal-login" title="Login" ><img src="<?= Yii::app()->request->baseUrl ?>/images/free-ship-icon.png" alt="" /></a>    
                                                                    
                                                           <?php
                                                                
                                                            }
                                                           
                                                            ?>
                                                            <a class=" sm_quickview_handler quick-calculate" title="Quick Calculate" href="<?= Yii::app()->request->baseUrl ?>/home/calculate?category=<?=$partner->cat_id?>&partner=<?=$partner->id?>"><img src="<?= Yii::app()->request->baseUrl ?>/images/icon-title.png" alt="" /></a>
                                                        </div>
                                                        
                                                          <?php
                                                                   if($partner->discount > 0){
                                                                       ?>
                                                                    
                                                                   <span class="disc">Discount: <?php echo Yii::app()->params['currency'] ; ?>   <?=$partner->discount?></span>
                                                                    
                                                                    <?php
                                                                   }
                                                                   ?>
                                                        <a class="fancyElm"  href="<?= Yii::app()->request->baseUrl ?>/partner-<?php echo $partner->slug; ?>">
                                                                  <p>
                                                                <?= substr($partner->intro, 0, 50)?>
                                                            </p>
                                                                 
                                                              </a>
                                                    </div>
                                                </div>
                                                <div class="item-info">
                                                    <div class="item-title">
                                                        <a href="<?= Yii::app()->request->baseUrl ?>/partner-<?php echo $partner->slug; ?>" title="<?php echo $partner->title; ?>"><?php echo $partner->title; ?></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php
                                        }
                                    }else {
                                    ?>
                                <div> No items found.</div>
                                <?php
                                  }

                                  ?>  
                                      
                                </div>
                                <?php
                                $this->widget('ext.yiinfinite-scroll.YiinfiniteScroller', array(
                                    'contentSelector' => '#filter2',
                                    'itemSelector' => 'div.itemsscroll2',
                                    'loadingText' => '',
                                    'donetext' => '',
                                    'pages' => $scroll_partners,
                                    'navigationLinkText' => '',
                                ));
                                ?>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>                                                 
    </div>
</div>
</div>
</div>