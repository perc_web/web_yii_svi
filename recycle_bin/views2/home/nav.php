

<div class="container">
    <div class="row yt-header-under-wrap">
        <div class="yt-main-menu col-md-12">
            <div id="yt-responsivemenu" class="yt-responsivemenu">
                <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                </button>
                <div id="yt_resmenu_collapse" style="height: auto;" class="nav-collapse collapse">
                    <ul class="nav-menu clearfix">
                        <li class="nav-home">
                            <a class="nav-home" href="<?= Yii::app()->getBaseUrl(true); ?>/home" title="Home">
                                <span>
                                    Home 
                                </span>
                            </a>
                        </li>
                        <li class="level0 nav-1 parent" onmouseover="toggleMenu(this, 1)" onmouseout="toggleMenu(this, 0)">
                            <!-- WhySvi.php -->
                            <a href="#" class="">
                                <span>
                                    Why Svi Card
                                </span>
                            </a>
                            <div class="res-wrapnav" style="height: 0px;">
                                <ul class="level1">
                                    <li class="level1 nav-1-1">
                                        <a href="#">
                                            <span>
                                                Why Svi Card?
                                            </span>
                                        </a>
                                    </li>
                                    <li class="level1 nav-1-2">
                                        <a href="#">
                                            <span>
                                                How it Works
                                            </span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <span class="menuress-toggle" style="height: 0px;">
                            </span>
                            <span class="menuress-toggle" style="height: 0px;">
                            </span>
                        </li>
                        <li class="level0 nav-2 " onmouseover="toggleMenu(this, 1)" onmouseout="toggleMenu(this, 0)">
                            <a href="<?= Yii::app()->getBaseUrl(true); ?>/home/partner" class="">
                                <span>
                                    Our Partners
                                </span>
                            </a>
                            <span class="menuress-toggle" style="height: 0px;">
                            </span>
                            <span class="menuress-toggle" style="height: 0px;">
                            </span>
                        </li>
                        <li class="level0 nav-3">
                            <!-- Reserve.php -->
                            <a href="#">
                                <span>
                                    Reserve 
                                </span>
                            </a>
                        </li>
                        <li class="level0 nav-4" onmouseover="toggleMenu(this, 1)" onmouseout="toggleMenu(this, 0)">
                            <a href="#" class="">
                                <span>
                                    Calculate
                                </span>
                            </a>
                            <span class="menuress-toggle" style="height: 0px;">
                            </span>
                            <span class="menuress-toggle" style="height: 0px;">
                            </span>
                        </li>
                        <li class="level0 nav-5 " onmouseover="toggleMenu(this, 1)" onmouseout="toggleMenu(this, 0)">
                            <a href="#" class=""><!-- grid.php -->
                                <span>
                                    Catalog	
                                </span>
                            </a>
                            <span class="menuress-toggle" style="height: 0px;">
                            </span>
                            <span class="menuress-toggle" style="height: 0px;">
                            </span>
                        </li>
                        <li class="level0 nav-6 " onmouseover="toggleMenu(this, 1)" onmouseout="toggleMenu(this, 0)">
                            <a href="#" class=""><!-- Contact.php -->
                                <span>
                                    Contact Us
                                </span>
                            </a>
                            <span class="menuress-toggle" style="height: 0px;">
                            </span>
                            <span class="menuress-toggle" style="height: 0px;">
                            </span>
                        </li>
                        <li class="level0 nav-7 " onmouseover="toggleMenu(this, 1)" onmouseout="toggleMenu(this, 0)">
                            <a href="<?= Yii::app()->getBaseUrl(true); ?>/FAQ" class=""><!-- FAQ.php -->
                                <span>
                                    FAQ
                                </span>
                            </a>
                            <span class="menuress-toggle" style="height: 0px;">
                            </span>
                            <span class="menuress-toggle" style="height: 0px;">
                            </span>
                        </li>
                    </ul>
                </div>
                <div class="link-mobile">
                    <span>
                    </span>
                    <a id="btn-account-links" href="javascript:void(0)" class="btn-trigger">
                    </a>
                    <div class="content-myaccount-m" style="display: none;">
                        <ul>
                            <li>
                                <a href="customer/account/login/index.html" title="Login">
                                    Login
                                </a>
                            </li>
                            <li>
                                <a href="customer/account/create/index.html" title="Register">
                                    Register
                                </a>
                            </li>

                            <li>
                                <a href="customer/account/index.html" title="My Account">
                                    My Account
                                </a>
                            </li>
                        </ul>
                        <ul class="links">
                            <li class="first">
                                <a href="wishlist/index.html" title="My Wishlist">
                                    My Wishlist
                                </a>
                            </li>
                            <li class=" last">
                                <a href="checkout/index.html" title="Checkout" class="top-link-checkout">
                                    Checkout
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="yt-menu">

                <link media="all" href="<?= Yii::app()->request->baseUrl ?>/css/frontend/default/sm_flipshop/sm/megamenu/css/horizontal/black/megamenu.css" type="text/css" rel="stylesheet">

                <div class="css_effect sm_megamenu_wrapper_horizontal_menu sambar" id="sm_megamenu_menu543ed997e74bd" data-sam="96241413405079">
                    <div class="sambar-inner">
                        <a class="btn-sambar" data-sapi="collapse" href="#sm_megamenu_menu543ed997e74bd">
                            <span class="icon-bar">
                            </span>
                            <span class="icon-bar">
                            </span>
                            <span class="icon-bar">
                            </span>
                        </a>

                        <ul class="sm-megamenu-hover sm_megamenu_menu sm_megamenu_menu_black" data-jsapi="on">
                            <?php 
                            if(Yii::app()->controller->id=="home" && Yii::app()->controller->action->id=="index" ){
                             $class="layout-flipshop-parent"  ;  
                            }  else {
                                 $class="";
                            }
                            ?>
                            
                            <li id="homeNavBtn" class="<?php echo $class ;   ?> other-toggle   
                                sm_megamenu_lv1 sm_megamenu_nodrop ">
                                <a class="sm_megamenu_head sm_megamenu_nodrop " href="<?= Yii::app()->getBaseUrl(true); ?>/home" id="sm_megamenu_463">
                                    <span class="sm_megamenu_icon sm_megamenu_nodesc">

                                        <span class="sm_megamenu_title">
                                            Home 
                                        </span>
                                    </span>
                                </a>

                            </li>
                            <li id="whySviNavBtn" class="electronic-item-parent  other-toggle   
                                sm_megamenu_lv1 sm_megamenu_drop parent">
                                <a class="sm_megamenu_head sm_megamenu_drop " href="<?php echo HtmlHelper::HtmlPageLink(1) ?>" id="sm_megamenu_283">
                                    <span class="sm_megamenu_icon sm_megamenu_nodesc">

                                        <span class="sm_megamenu_title">
                                            Why Svi Card
                                        </span>


                                    </span>
                                </a>
                                <div class="sm_megamenu_dropdown_2columns " style="padding:10px">
                                    <div class="sm_megamenu_content">
                                        <div class="content-home">
                                            <div class="layout-color layout-theme">
                                                <h2>
                                                    
                                                      <a href="<?php echo HtmlHelper::HtmlPageLink(1) ?>" title="Why SVI Card">
                                                               Why SVI Card
                                                            </a>
                                                    
                                                </h2>
                                                <ul class="list-theme">
                                                    <li>
                                                        <h2>
                                                            <a href="<?php echo HtmlHelper::HtmlPageLink(2) ?>" title="What is Svi Card?">
                                                                What is Svi Card?
                                                            </a>
                                                        </h2>
                                                    </li>                                                    <li>
                                                        <h2>												
                                                            <a href="<?php echo HtmlHelper::HtmlPageLink(3) ?>" title="How does it works?">
                                                                How does it works?
                                                            </a>
                                                        </h2>
                                                    </li>
                                                </ul>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li id="partnersNavBtn" class="men-item-parent  other-toggle   
                                sm_megamenu_lv1 sm_megamenu_nodrop    ">
                                <a class="sm_megamenu_head sm_megamenu_drop " href="<?= Yii::app()->getBaseUrl(true); ?>/home/partners" id="sm_megamenu_284">
                                    <span class="sm_megamenu_icon sm_megamenu_nodesc">

                                        <span class="sm_megamenu_title">
                                            Our Partners
                                        </span>


                                    </span>
                                </a>

                            </li>
                            <?php if (Yii::app()->user->id) { ?>
                                <li id="reserveNavBtn" class="women-item-parent  other-toggle   
                                    sm_megamenu_lv1 sm_megamenu_nodrop   ">
                                    <a class="sm_megamenu_head sm_megamenu_drop " href="<?= Yii::app()->getBaseUrl(true); ?>/home/reserve" id="sm_megamenu_285">
                                        <span class="sm_megamenu_icon sm_megamenu_nodesc">
                                            <span class="sm_megamenu_title">
                                                Reserve
                                            </span>
                                        </span>
                                    </a>
                                </li>
                                <?php
                            }else{
                                ?>
        
                                <li  style="cursor: pointer;" id="reserveNavBtn" class="women-item-parent  other-toggle   
                                    sm_megamenu_lv1 sm_megamenu_nodrop">
                                    <a   data-toggle="modal" data-target="#modal-login" title="Login"   class="sm_megamenu_head sm_megamenu_drop "  id="sm_megamenu_285">
                                        <span class="sm_megamenu_icon sm_megamenu_nodesc">
                                            <span class="sm_megamenu_title">
                                                Reserve
                                            </span>
                                        </span>
                                    </a>
                                </li>
                                
                            <?php
                                
                            }
                            ?>
                            <li id="calculateNavBtn" class="babykids-item-parent  other-toggle   
                                sm_megamenu_lv1 sm_megamenu_nodrop   ">
                                <a class="sm_megamenu_head sm_megamenu_nodrop " href="<?= Yii::app()->getBaseUrl(true); ?>/home/calculate" id="sm_megamenu_286">
                                    <span class="sm_megamenu_icon sm_megamenu_nodesc">

                                        <span class="sm_megamenu_title">
                                            Calculate
                                        </span>
                                    </span>
                                </a>
                            </li>
                            <li id="catalogNavBtn" class="bookmedia-item-parent  other-toggle   
                                sm_megamenu_lv1 sm_megamenu_nodrop   ">
                                <a class="sm_megamenu_head sm_megamenu_nodrop " href="<?= Yii::app()->getBaseUrl(true); ?>/Catalog" id="sm_megamenu_287">
                                    <span class="sm_megamenu_icon sm_megamenu_nodesc">

                                        <span class="sm_megamenu_title">
                                            Catalog  
                                        </span>


                                    </span>
                                </a>
                            </li>
                            <li id="contactNavBtn" class="homekitchen-item-parent  other-toggle   
                                sm_megamenu_lv1 sm_megamenu_nodrop   ">
                                <a class="sm_megamenu_head sm_megamenu_nodrop " href="<?= Yii::app()->getBaseUrl(true); ?>/home/contact?type=3" id="sm_megamenu_288">
                                    <span class="sm_megamenu_icon sm_megamenu_nodesc">
                                        <span class="sm_megamenu_title">
                                            Contact Us
                                        </span>
                                    </span>
                                </a>
                            </li>
                            <li id="faqNavBtn" class=" other-toggle   
                                sm_megamenu_lv1 sm_megamenu_nodrop   ">
                                <a class="sm_megamenu_head sm_megamenu_nodrop " href="<?= Yii::app()->getBaseUrl(true); ?>/home/faq" id="sm_megamenu_289">
                                    <span class="sm_megamenu_icon sm_megamenu_nodesc">

                                        <span class="sm_megamenu_title">
                                            FAQ
                                        </span>


                                    </span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <!--End Module-->


            </div>
        </div>

    </div>
</div>