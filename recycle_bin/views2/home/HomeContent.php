<!--<script>var d = document.getElementById("homeNavBtn");
    d.className = d.className + " layout-flipshop-parent ";</script>-->

<div class="container">
    <div class="row">
        <div id="yt_left" class="col-lg-3 col-md-3">
            <div class="yt-left-wrap">



                <?php
                $this->widget('application.extensions.fancybox.EFancyBox', array(
                    'target' => '.fancyElm',
                    'config' => array(
                        'maxWidth' => 1000,
                        'maxHeight' => 1000,
                        'fitToView' => false,
                        'width' => '90%',
                        'height' => '90%',
                        'autoSize' => false,
                        'closeClick' => false,
                        'openEffect' => 'none',
                        'closeEffect' => 'none',
                        'type' => 'iframe',
                    ),
                ));
                ?>        



                <?php
                $advertise = Advertise::model()->find(array('condition' => ' active=1 and page=1 and size=2', 'order' => 'rand()'));
                ?>
                <div class="block-st-img">
                    <a title="<?php echo $advertise->link; ?>"  target="_blank" href="<?php echo $advertise->link; ?>">
                        <img src="<?= Yii::app()->request->baseUrl ?>/media/advertise/<?php echo $advertise->image; ?>" alt="<?php echo $advertise->link; ?>">
                    </a>
                </div>


                <!--[if lt IE 9]>
      <div id="sm_basic_products_141340508123639" class="sm-basic-products  msie lt-ie9">
      <![endif]-->
                <!--[if IE 9]>
<div id="sm_basic_products_141340508123639" class="sm-basic-products  msie">
<![endif]-->
                <!--[if gt IE 9]>
                <!-->
                <div id="sm_basic_products_141340508123639" class="block sm-basic-products block-product">
                    <!--
<![endif]-->
                    <!-- Begin bs-items     -->
                    <div class="block-title">
                        <strong>
                            <span>
                                Latest  Deals
                            </span>
                        </strong>
                    </div>
                    <div class="bs-items bs-resp01-1 bs-resp02-1 bs-resp03-1 bs-resp04-1 ">
                        <?php
                        $i = 1;
                        foreach ($latest_items as $item) {
                            ?>
                            <!-- Begin bs-item-cf -->
                            <div class="bs-item cf">
                                <!-- Begin bs-item-inner -->
                                <div class="bs-item-inner">
                                    <div class="bs-image">
                                        <a href="<?= Yii::app()->getBaseUrl(true); ?>/outlet-<?= $item->slug ?>" title="<?= $item->title; ?>">
                                            <img src="<?= Yii::app()->request->baseUrl ?>/media/outlet/<?= $item->image ?>" title="<?= $item->title ?>" alt="<?= $item->title; ?>">
                                        </a>
                                    </div>
                                    <!-- Begin bs-content -->
                                    <div class="bs-content">
                                        <div class="bs-title">
                                            <a href="<?= Yii::app()->getBaseUrl(true); ?>/outlet-<?= $item->slug ?>" title="<?= $item->title; ?>">
                                                <?= $item->title; ?>              
                                            </a>
                                        </div>
                                        <div class="bs-price">
                                            <div class="sale-price">
                                                <div class="price-box">                 
                                                    <span class="special-price">


                                                        <span class="special-price" id="product-price-443">                                                         
                                                            <?php
                                                            if ($item->discount > 0) {
                                                                ?>

                                                                <span class="disc">Discount: <?= $item->discount ?></span>

                                                                <?php
                                                            }
                                                            ?>
                                                        </span>



                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- End bs-content -->
                                </div>
                                <!-- End bs-item-inner -->
                            </div>
                            <!-- End bs-item-cf -->
                            <div class="clr<?= $i ?>">
                            </div>
                            <?php
                            $i++;
                        }
                        ?>
                        <div class="clr1 clr2 clr4">
                        </div>
                    </div>
                    <!-- End bs-items -->
                </div>
                <!-- End Sm-basic-products -->


                <?php
                $advertise = Advertise::model()->find(array('condition' => ' active=1 and page=1 and size=0', 'order' => 'rand()'));
                ?>
                <div class="block-st-img">
                    <a title="<?php echo $advertise->link; ?>"  target="_blank" href="<?php echo $advertise->link; ?>">
                        <img src="<?= Yii::app()->request->baseUrl ?>/media/advertise/<?php echo $advertise->image; ?>" alt="<?php echo $advertise->link; ?>"   style="
                             width: 100%;
                             height: 135px;
                             ">
                    </a>
                </div>


                <!--[if lt IE 9]>
<div id="sm_basic_products_1413405081836" class="sm-basic-products  msie lt-ie9">
<![endif]-->
                <!--[if IE 9]>
<div id="sm_basic_products_1413405081836" class="sm-basic-products  msie">
<![endif]-->
                <!--[if gt IE 9]>
                <!-->

                <!-- End Sm-basic-products -->


                <div class="block block-tags" style="margin-bottom:0px">
                    <div class="block-title">
                        <strong>
                            <span>
                                Popular Tags
                            </span>
                        </strong>
                    </div>
                    <div class="block-content">
                        <ul class="tags-list">
                            <?php
                            $sql = "SELECT  * FROM tags  WHERE active=1   ORDER BY `usage` DESC";
                            $tags = Tags::model()->findAllBySql($sql);

                            if ($tags) {
                                foreach ($tags as $tag) {
                                    ?>
                                    <li><a href="<?= Yii::app()->getBaseUrl(true); ?>/Tag-<?= $tag->id ?>" style="font-size:75%;"><?= $tag->title; ?></a></li>
                                    <?php
                                }
                            }
                            ?>
                        </ul>
                    </div>
                </div>


                <?php
                $advertise = Advertise::model()->find(array('condition' => ' active=1 and page=1 and size=2', 'order' => 'rand()'));
                ?>
                <div class="block-st-img">
                    <a title="<?php echo $advertise->link; ?>"  target="_blank" href="<?php echo $advertise->link; ?>">
                        <img src="<?= Yii::app()->request->baseUrl ?>/media/advertise/<?php echo $advertise->image; ?>" alt="<?php echo $advertise->link; ?>">
                    </a>
                </div>
            </div>
        </div>

        <div id="yt_main" class="yt-main-right yt-main col-main col-lg-9 col-md-9">
            <div class="yt_main_inner">


                <?php
                $advertise = Advertise::model()->find(array('condition' => 'active=1 and page=1 and size=1', 'order' => 'rand()'));
                ?>
                <div class="save-up" style="margin-bottom: 20px;">
                    <a title="<?php echo $advertise->link; ?>"  target="_blank" href="<?php echo $advertise->link; ?>">
                        <img src="<?= Yii::app()->request->baseUrl ?>/media/advertise/<?php echo $advertise->image; ?>" alt="<?php echo $advertise->link; ?>">
                    </a>
                </div>


                <div class="super-categories-wrapper">
                    <div class="row">
                        <div class="col-md-12 sp-block">
                            <!--[if lt IE 9]>
        <div id="sm_listing_tabs_263551413405083" class="sj-listing-tabs msie lt-ie9 first-load">
        <![endif]-->
                            <!--[if IE 9]>
        <div id="sm_listing_tabs_263551413405083" class="sj-listing-tabs msie first-load">
        <![endif]-->
                            <!--[if gt IE 9]>
                            <!-->
                            <div id="sm_listing_tabs_263551413405083" class="sj-listing-tabs first-load">
                                <!--
          <![endif]-->




                                <div class="ltabs-wrap tab-product-ex">
                                    <div class="ltabs-tabs-container" data-delay="300" data-duration="600" data-effect="slideLeft" data-ajaxurl="#" data-modid="sm_listing_tabs_263551413405083">
                                        <!--Begin Tabs-->

                                        <div class="ltabs-tabs-wrap">
                                            <ul class="ltabs-tabs yt-tab-navi cf">

                                                <li class="ltabs-tab  tab-loaded tab-sel" data-category-id="top_rating" data-active-content=".items-category-top_rating" data-cmspage-id="home-left" data-category-select="87">
                                                    <a class="ltabs-tab-label">Featured Deals</a>
                                                </li>
                                                <li class="ltabs-tab" data-category-id="created_at" data-active-content=".items-category-created_at" data-cmspage-id="home-left" data-category-select="87">
                                                    <a class="ltabs-tab-label">Hot Deals</a>
                                                </li>

                                            </ul>
                                        </div>

                                        <script type="text/javascript">
                                            //<![CDATA[
                                            jQuery(document).ready(function ($) {
                                                ;
                                                (function (element) {
                                                    var $element = $(element),
                                                            $tab = $('.ltabs-tab', $element),
                                                            $tab_label = $('.ltabs-tab-label', $tab),
                                                            $tabs = $('.ltabs-tabs', $element),
                                                            ajax_url = $tabs.parents('.ltabs-tabs-container').attr('data-ajaxurl'),
                                                            effect = $tabs.parents('.ltabs-tabs-container').attr('data-effect'),
                                                            delay = $tabs.parents('.ltabs-tabs-container').attr('data-delay'),
                                                            duration = $tabs.parents('.ltabs-tabs-container').attr('data-duration'),
                                                            rl_moduleid = $tabs.parents('.ltabs-tabs-container').attr('data-modid'),
                                                            $items_content = $('.ltabs-items', $element),
                                                            $items_inner = $('.ltabs-items-inner', $items_content),
                                                            $items_first_active = $('.ltabs-items-selected', $element),
                                                            $load_more = $('.ltabs-loadmore', $element),
                                                            $btn_loadmore = $('.ltabs-loadmore-btn', $load_more),
                                                            $select_box = $('.ltabs-selectbox', $element),
                                                            $tab_label_select = $('.ltabs-tab-selected', $element);

                                                    enableSelectBoxes();
                                                    function enableSelectBoxes() {
                                                        $tab_wrap = $('.ltabs-tabs-wrap', $element),
                                                                $tab_label_select.html($('.ltabs-tab', $element).filter('.tab-sel').children('.ltabs-tab-label').html());
                                                        if ($(window).innerWidth() <= 479) {
                                                            $tab_wrap.addClass('ltabs-selectbox');
                                                        }
                                                        else {
                                                            $tab_wrap.removeClass('ltabs-selectbox');
                                                        }
                                                    }

                                                    $('span.ltabs-tab-selected, span.ltabs-tab-arrow', $element).click(function () {
                                                        if ($('.ltabs-tabs', $element).hasClass('ltabs-open')) {
                                                            $('.ltabs-tabs', $element).removeClass('ltabs-open');
                                                        }
                                                        else {
                                                            $('.ltabs-tabs', $element).addClass('ltabs-open');
                                                        }
                                                    }
                                                    );

                                                    $(window).resize(function () {
                                                        if ($(window).innerWidth() <= 479) {
                                                            $('.ltabs-tabs-wrap', $element).addClass('ltabs-selectbox');
                                                        }
                                                        else {
                                                            $('.ltabs-tabs-wrap', $element).removeClass('ltabs-selectbox');
                                                        }
                                                    }
                                                    );

                                                    function showAnimateItems(el) {
                                                        var $_items = $('.new-ltabs-item', el), nub = 0;
                                                        $('.ltabs-loadmore-btn', el).fadeOut('fast');
                                                        $_items.each(function (i) {
                                                            nub++;
                                                            switch (effect) {
                                                                case 'none' :
                                                                    $(this).css({'opacity': '1', 'filter': 'alpha(opacity = 100)'}
                                                                    );
                                                                    break;
                                                                default:
                                                                    animatesItems($(this), nub * delay, i, el);
                                                            }
                                                            if (i == $_items.length - 1) {
                                                                $('.ltabs-loadmore-btn', el).fadeIn(delay);
                                                            }
                                                            $(this).removeClass('new-ltabs-item');
                                                        }
                                                        );
                                                    }

                                                    function animatesItems($this, fdelay, i, el) {
                                                        var $_items = $('.ltabs-item', el);
                                                        $this.attr("style",
                                                                "-webkit-animation:" + effect + " " + duration + "ms;"
                                                                + "-moz-animation:" + effect + " " + duration + "ms;"
                                                                + "-o-animation:" + effect + " " + duration + "ms;"
                                                                + "-moz-animation-delay:" + fdelay + "ms;"
                                                                + "-webkit-animation-delay:" + fdelay + "ms;"
                                                                + "-o-animation-delay:" + fdelay + "ms;"
                                                                + "animation-delay:" + fdelay + "ms;").delay(fdelay).animate({
                                                            opacity: 1,
                                                            filter: 'alpha(opacity = 100)'
                                                        }
                                                        , {
                                                            delay: 100
                                                        }
                                                        );
                                                        if (i == ($_items.length - 1)) {
                                                            $(".ltabs-items-inner").addClass("play");
                                                        }
                                                    }

                                                    showAnimateItems($items_first_active);
                                                    $tab.on('click.tab', function () {
                                                        var $this = $(this);
                                                        if ($this.hasClass('tab-sel'))
                                                            return false;
                                                        if ($this.parents('.ltabs-tabs').hasClass('ltabs-open')) {
                                                            $this.parents('.ltabs-tabs').removeClass('ltabs-open');
                                                        }
                                                        $tab.removeClass('tab-sel');
                                                        $this.addClass('tab-sel');
                                                        var items_active = $this.attr('data-active-content');
                                                        var _items_active = $(items_active, $element);
                                                        $items_content.removeClass('ltabs-items-selected');
                                                        _items_active.addClass('ltabs-items-selected');
                                                        $tab_label_select.html($tab.filter('.tab-sel').children('.ltabs-tab-label').html());
                                                        var $loading = $('.ltabs-loading', _items_active);
                                                        var cms_page = $this.attr('data-cmspage-id');
                                                        var loaded = _items_active.hasClass('ltabs-items-loaded');
                                                        if (!loaded && !_items_active.hasClass('ltabs-process')) {
                                                            _items_active.addClass('ltabs-process');
                                                            var category_id = $this.attr('data-category-id');
                                                            var category_select = $this.attr('data-category-select');
                                                            $loading.show();
                                                            $.ajax({
                                                                type: 'POST',
                                                                url: ajax_url,
                                                                data: {
                                                                    listing_tabs_moduleid: rl_moduleid,
                                                                    is_ajax_listing_tabs: 1,
                                                                    ajax_reslisting_start: 0,
                                                                    categoryid: category_id,
                                                                    category_select: category_select,
                                                                    time_temp: '141340508332297',
                                                                    cms_page: cms_page
                                                                }
                                                                ,
                                                                success: function (data) {
                                                                    if (data.items_markup != '') {
                                                                        $('.ltabs-items-inner', _items_active).html(data.items_markup);
                                                                        _items_active.addClass('ltabs-items-loaded').removeClass('ltabs-process');
                                                                        $loading.remove();
                                                                        showAnimateItems(_items_active);
                                                                        updateStatus(_items_active);
                                                                    }
                                                                }
                                                                ,
                                                                dataType: 'json'
                                                            }
                                                            );

                                                        }
                                                        else {
                                                            $('.ltabs-item', $items_content).removeAttr('style').addClass('new-ltabs-item').css('opacity', 0);
                                                            showAnimateItems(_items_active);
                                                        }
                                                    }
                                                    );

                                                    function updateStatus($el) {
                                                        $('.ltabs-loadmore-btn', $el).removeClass('loading');
                                                        var countitem = $('.ltabs-item', $el).length;
                                                        $('.ltabs-image-loading', $el).css({
                                                            display: 'none'}
                                                        );
                                                        $('.ltabs-loadmore-btn', $el).parent().attr('data-rl_start', countitem);
                                                        var rl_total = $('.ltabs-loadmore-btn', $el).parent().attr('data-rl_total');
                                                        var rl_load = $('.ltabs-loadmore-btn', $el).parent().attr('data-rl_load');
                                                        var rl_allready = $('.ltabs-loadmore-btn', $el).parent().attr('data-rl_allready');

                                                        if (countitem >= rl_total) {
                                                            $('.ltabs-loadmore-btn', $el).addClass('loaded');
                                                            $('.ltabs-image-loading', $el).css({
                                                                display: 'none'}
                                                            );
                                                            $('.ltabs-loadmore-btn', $el).attr('data-label', rl_allready);
                                                            $('.ltabs-loadmore-btn', $el).removeClass('loading');
                                                        }
                                                    }

                                                    $btn_loadmore.on('click.loadmore', function () {
                                                        var $this = $(this);
                                                        var cms_page = $this.attr('data-cmspage-id');
                                                        if ($this.hasClass('loaded') || $this.hasClass('loading')) {
                                                            return false;
                                                        }
                                                        else {
                                                            $this.addClass('loading');
                                                            $('.ltabs-image-loading', $this).css({
                                                                display: 'inline-block'}
                                                            );
                                                            var rl_start = $this.parent().attr('data-rl_start'),
                                                                    rl_moduleid = $this.parent().attr('data-modid'),
                                                                    rl_ajaxurl = $this.parent().attr('data-ajaxurl'),
                                                                    effect = $this.parent().attr('data-effect'),
                                                                    category_id = $this.parent().attr('data-categoryid'),
                                                                    category_select = $this.parent().attr('data-category-select'),
                                                                    items_active = $this.parent().attr('data-active-content');
                                                            var _items_active = $(items_active, $element);

                                                            $.ajax({
                                                                type: 'POST',
                                                                url: rl_ajaxurl,
                                                                data: {
                                                                    listing_tabs_moduleid: rl_moduleid,
                                                                    is_ajax_listing_tabs: 1,
                                                                    ajax_reslisting_start: rl_start,
                                                                    categoryid: category_id,
                                                                    category_select: category_select,
                                                                    time_temp: '141340508326030',
                                                                    cms_page: cms_page
                                                                }
                                                                ,
                                                                success: function (data) {
                                                                    if (data.items_markup != '') {
                                                                        $(data.items_markup).insertAfter($('.ltabs-item', _items_active).nextAll().last());
                                                                        $('.ltabs-image-loading', $this).css({
                                                                            display: 'none'}
                                                                        );
                                                                        showAnimateItems(_items_active);
                                                                        updateStatus(_items_active);
                                                                    }
                                                                }
                                                                , dataType: 'json'
                                                            }
                                                            );
                                                        }
                                                        return false;
                                                    }
                                                    );
                                                }
                                                )('#sm_listing_tabs_263551413405083');
                                            }
                                            );
                                            //]]>
                                        </script>

                                    </div>
                                </div>
                                <!-- End Tabs-->



                                <div class="ltabs-items-container">
                                    <!--Begin Items-->
                                    <div class="ltabs-items items-category-best_sales">
                                        <div class="ltabs-items-inner row ltabs00-4 ltabs01-4 ltabs02-3 ltabs03-2 ltabs04-1 slideLeft play">
                                            <div class="ltabs-loading">
                                            </div>
                                        </div>
                                        <div class="ltabs-loadmore" data-active-content=".items-category-best_sales" data-category-select="87" data-categoryid="best_sales" data-rl_start="3" data-rl_total="8" data-rl_allready="
                                             All ready" data-ajaxurl="#" data-modid="
                                             " data-rl_load="
                                             2">

                                            <div data-cmspage-id="home-left" title="Load more" class="ltabs-loadmore-btn " data-label="
                                                 Load more">
                                                <span class="ltabs-image-loading">
                                                </span>

                                            </div>
                                        </div>
                                    </div>




                                    <div class="ltabs-items ltabs-items-loaded items-category-top_rating ltabs-items-selected  ">
                                        <div class="ltabs-items-inner row ltabs00-4 ltabs01-4 ltabs02-3 ltabs03-2 ltabs04-1 slideLeft play  ">




                                            <?php
                                            if ($featured_deals) {
                                                foreach ($featured_deals as $outlet) {
                                                    $partner = $outlet->partner
                                                    ?>
                                                    <div class="item col-lg-4 col-md-6 col-sm-6 respl-item itemsscroll">
                                                        <div class="item-inner">
                                                            <div class="w-image-box">
                                                                <span class="hover-background"></span>
                                                                <div class="item-image">
                                                                    <a href="<?= Yii::app()->request->baseUrl ?>/outlet-<?php echo $outlet->slug; ?>" title="<?= $outlet->title ?>" class="product-image"><img src="<?= Yii::app()->request->baseUrl ?>/media/outlet/<?php echo $outlet->image; ?>" alt="<?= $partner->title ?>"></a>
                                                                    <a class="sm_quickview_handler  fancyElm" title="Quick View" href="<?= Yii::app()->request->baseUrl ?>/outletfancy-<?php echo $outlet->slug; ?>">Quick View</a>

                                                                    <div class="quick-btns">
                                                                        <?php if (Yii::app()->user->id) { ?>
                                                                            <a class="sm_quickview_handler quick-reserve" title="Quick Reserve" href="<?= Yii::app()->request->baseUrl ?>/home/reserve?category=<?= $partner->cat_id ?>&partner=<?= $partner->id ?>&outlet=<?= $outlet->id ?>"><img src="<?= Yii::app()->request->baseUrl ?>/images/free-ship-icon.png" alt="" /></a>
                                                                            <?php
                                                                        }else{
                                                                ?>
                                                                                                           <a class="sm_quickview_handler quick-reserve" title="Quick Reserve"  data-toggle="modal" data-target="#modal-login" title="Login" ><img src="<?= Yii::app()->request->baseUrl ?>/images/free-ship-icon.png" alt="" /></a>    
                                                                    
                                                           <?php
                                                                
                                                            }
                                                            ?>
                                                                       
                                                                        <a class=" sm_quickview_handler quick-calculate" title="Quick Calculate" href="<?= Yii::app()->request->baseUrl ?>/home/calculate?category=<?= $partner->cat_id ?>&partner=<?= $partner->id ?>&outlet=<?= $outlet->id ?>"><img src="<?= Yii::app()->request->baseUrl ?>/images/icon-title.png" alt="" /></a>
                                                                    </div>

                                                                    <?php
                                                                    if ($outlet->discount > 0) {
                                                                        ?>

                                                                        <span class="disc">Discount: <?php echo Yii::app()->params['currency'] ; ?>  <?= $outlet->discount ?></span>

                                                                        <?php
                                                                    }
                                                                    ?>

                                                                    <a class="fancyElm"  href="<?= Yii::app()->request->baseUrl ?>/outletfancy-<?php echo $outlet->slug; ?>">
                                                                        <p>
                                                                            <?= substr($outlet->intro, 0, 50) ?>
                                                                        </p>

                                                                    </a>


                                                                </div>
                                                            </div>
                                                            <div class="item-info">
                                                                <div class="item-title">
                                                                    <a href="<?= Yii::app()->request->baseUrl ?>/outlet-<?php echo $outlet->slug; ?>" title="<?= $outlet->title; ?>"><?php echo $outlet->title; ?></a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <?php
                                                }
                                            } else {
                                                ?>
                                                <div> No items found.</div>
                                                <?php
                                            }
                                            ?>

                                        </div>

                                    </div>
                                    <div   class="ltabs-items  ltabs-items-loaded items-category-created_at">
                                        <div class="ltabs-items-inner row ltabs00-4 ltabs01-4 ltabs02-3 ltabs03-2 ltabs04-1 slideLeft play">

                                            <?php
                                            if ($hot_deals) {
                                                foreach ($hot_deals as $outlet) {
                                                    $partner = $outlet->partner
                                                    ?>
                                                    <div class="item col-lg-4 col-md-6 col-sm-6 respl-item itemsscroll">
                                                        <div class="item-inner">
                                                            <div class="w-image-box">
                                                                <span class="hover-background"></span>
                                                                <div class="item-image">
                                                                    <a href="<?= Yii::app()->request->baseUrl ?>/outlet-<?php echo $outlet->slug; ?>" title="<?= $outlet->title ?>" class="product-image"><img src="<?= Yii::app()->request->baseUrl ?>/media/outlet/<?php echo $outlet->image; ?>" alt="<?= $partner->title ?>"></a>
                                                                    <a class="sm_quickview_handler  fancyElm" title="Quick View" href="<?= Yii::app()->request->baseUrl ?>/outletfancy-<?php echo $outlet->slug; ?>">Quick View</a>

                                                                    <div class="quick-btns">
                                                                        <?php if (Yii::app()->user->id) { ?>
                                                                            <a class="sm_quickview_handler quick-reserve" title="Quick Reserve" href="<?= Yii::app()->request->baseUrl ?>/home/reserve?category=<?= $partner->cat_id ?>&partner=<?= $partner->id ?>&outlet=<?= $outlet->id ?>"><img src="<?= Yii::app()->request->baseUrl ?>/images/free-ship-icon.png" alt="" /></a>
                                                                            <?php
                                                                        }else{
                                                                ?>
                                                                                                           <a class="sm_quickview_handler quick-reserve" title="Quick Reserve"  data-toggle="modal" data-target="#modal-login" title="Login" ><img src="<?= Yii::app()->request->baseUrl ?>/images/free-ship-icon.png" alt="" /></a>    
                                                                    
                                                           <?php
                                                                
                                                            }
                                                            ?>
                                                                        
                                                                        <a class=" sm_quickview_handler quick-calculate" title="Quick Calculate" href="<?= Yii::app()->request->baseUrl ?>/home/calculate?category=<?= $partner->cat_id ?>&partner=<?= $partner->id ?>&outlet=<?= $outlet->id ?>"><img src="<?= Yii::app()->request->baseUrl ?>/images/icon-title.png" alt="" /></a>
                                                                    </div>

                                                                    <?php
                                                                    if ($outlet->discount > 0) {
                                                                        ?>

                                                                        <span class="disc">Discount:  <?php echo Yii::app()->params['currency'] ; ?>   <?= $outlet->discount ?></span>

                                                                        <?php
                                                                    }
                                                                    ?>

                                                                    <a class="fancyElm"  href="<?= Yii::app()->request->baseUrl ?>/outletfancy-<?php echo $outlet->slug; ?>">
                                                                        <p>
                                                                            <?= substr($outlet->intro, 0, 50) ?>
                                                                        </p>

                                                                    </a>


                                                                </div>
                                                            </div>
                                                            <div class="item-info">
                                                                <div class="item-title">
                                                                    <a href="<?= Yii::app()->request->baseUrl ?>/outlet-<?php echo $outlet->slug; ?>" title="<?= $outlet->title; ?>"><?php echo $outlet->title; ?></a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <?php
                                                }
                                            } else {
                                                ?>
                                                <div> No items found.</div>
                                                <?php
                                            }
                                            ?>


                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--End Items-->
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <?php
        $advertise = Advertise::model()->find(array('condition' => 'active=1 and page=1 and size=1', 'order' => 'rand()'));
        ?>
        <div style="margin-top: 40px;">
            <a title="<?php echo $advertise->link; ?>"  target="_blank" href="<?php echo $advertise->link; ?>" >
                <img src="<?= Yii::app()->request->baseUrl ?>/media/advertise/<?php echo $advertise->image; ?>" alt="<?php echo $advertise->link; ?>">
            </a>
        </div>
    </div>
</div>
<div class="ft-brand-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-md-12 feature-brand-block">
                <div id="sm_imageslider_94161413405085" class="sm_imageslider_wrap">
                    <div class="title-imageslider title-home">
                        Featured Brands     
                    </div>
                    <div class="sm_imageslider slider-feature-brand">
                        <?php foreach ($featured_brands as $brand) { ?>
                            <div class="item">
                                <a href="<?= $brand->link ?>" title="<?= $brand->title; ?>"  >
                                    <img src="<?= Yii::app()->request->baseUrl ?>/media/brands/<?= $brand->image ?>" alt="<?= $brand->title ?>" />
                                </a>
                            </div>
                            <?php
                        }
                        ?>
                    </div>
                </div>
                <script type="text/javascript">
                    //<![CDATA[
                    jQuery(document).ready(function ($) {
                        $('.slider-feature-brand').owlCarousel({
                            pagination: false,
                            center: false,
                            nav: true,
                            loop: true,
                            margin: 30,
                            navText: ['prev', 'next'],
                            slideBy: 1,
                            autoplay: true,
                            autoplayTimeout: 2500,
                            autoplayHoverPause: true,
                            autoplaySpeed: 800,
                            startPosition: 0,
                            responsive: {
                                0: {
                                    items: 2}
                                ,
                                480: {
                                    items: 3}
                                ,
                                768: {
                                    items: 4}
                                ,
                                1200: {
                                    items: 6}
                            }
                        }
                        );

                    }
                    );

                    //]]>
                </script>


            </div>
        </div>
    </div>

</div>

<div class="three-block">
    <div class="container">
        <div class="row">

<!--            <form action="<?= Yii::app()->getBaseUrl(true) ?>/home/addCart" method="post" id="product_addtocart_form"> -->
            <div class="col-md-4 block-product">

                <div class="three-title-home">
                    <h2>
                        Recomended      
                    </h2>
                </div>

                <!--[if lt IE 9]>
<div id="sm_basic_products_141340508518785" class="sm-basic-products  msie lt-ie9">
<![endif]-->
                <!--[if IE 9]>
<div id="sm_basic_products_141340508518785" class="sm-basic-products  msie">
<![endif]-->
                <!--[if gt IE 9]>
                <!-->
                <div id="sm_basic_products_141340508518785" class="sm-basic-products">
                    <!--
<![endif]-->
                    <!-- Begin bs-items     -->
                    <div class="bs-items bs-resp01-1 bs-resp02-1 bs-resp03-1 bs-resp04-1 ">
                        <?php
                        $j = 1;
                        foreach ($recomended as $recom) {
                            $partner = $recom->partner;
                            ?>
                            <!-- Begin bs-item-cf -->
                            <div class="bs-item cf">
                                <!-- Begin bs-item-inner -->
                                <div class="bs-item-inner">
                                    <div class="bs-image">
                                        <a href="<?= Yii::app()->getBaseUrl(true); ?>/outlet-<?= $recom->slug ?>" title="<?= $recom->title ?>">
                                            <img src="<?= Yii::app()->request->baseUrl ?>/media/outlet/<?= $recom->image ?>" title="<?= $recom->title ?>" alt="<?= $recom->title ?>">
                                        </a>
                                    </div>
                                    <!-- Begin bs-content -->
                                    <div class="bs-content">
                                        <div class="bs-title">
                                            <a href="<?= Yii::app()->getBaseUrl(true); ?>/outlet-<?= $recom->slug ?>" title="<?= $recom->title ?>">
                                                <?= $recom->title ?>                  
                                            </a>
                                        </div>
                                        <div class="bs-price">
                                            <div class="sale-price">
                                                <div class="price-box">
                                                    <span class="regular-price" id="product-price-404"> 
                                                        <?php
                                                        if ($recom->discount > 0) {
                                                            ?>

                      <?php echo Yii::app()->params['currency'] ; ?>    <?= $recom->discount ?>

                                                            <?php
                                                        }
                                                        ?>

                                                    </span>
                                                </div>
                                            </div>
                                        </div> 
                                        <input type="hidden" name="product" value="<?= $recom->id ?>">

                                        <a   href="<?= Yii::app()->request->baseUrl ?>/home/calculate?category=<?= $partner->cat_id ?>&partner=<?= $partner->id ?>&outlet=<?= $recom->id ?>">
                                            <div class="bs-btn-addtocart">
                                                <button  title="Add to Cart" class="button btn-cart">
                                                    <span>
                                                        <span>
                                                            Add to Cart
                                                        </span>
                                                    </span>
                                                </button>
                                            </div>
                                        </a>


                                    </div>
                                    <!-- End bs-content -->
                                </div>
                                <!-- End bs-item-inner -->
                            </div>
                            <!-- End bs-item-cf -->
                            <div class="clr<?= $j ?>">
                            </div>
                            <?php
                            $j++;
                        }
                        ?>

                        <!-- End bs-item-cf -->
                        <div class="clr1 clr2 clr4">
                        </div>
                    </div>
                    <!-- End bs-items -->
                </div>
                <!-- End Sm-basic-products -->


            </div>

            <!--            </form>-->

            <div class="col-md-4 lastes-blog">

                <div class="block-lt-blogs">
                    <div class="three-title-home">
                        <h2>
                            Latest Blogs
                        </h2>
                    </div>

                    <ul class="grid-blog">

                        <?php foreach ($blogs as $blog) { ?>
                            <li class="item">
                                <div class="item-latest-blog">
                                    <div class="img-wrapper item-lb-image">
                                        <a title="<?= $blog->title ?>" href="<?= Yii::app()->request->baseUrl ?>/blog-<?= $blog->slug ?>">
                                            <img alt="<?= $blog->title ?>" src="<?= Yii::app()->request->baseUrl ?>/media/blog/<?= $blog->image ?>">

                                        </a>
                                    </div>
                                    <div class="lt-blog-info info-first-blog">
                                        <div class="item-lb-title">
                                            <a title="<?= $blog->title ?>" href="<?= Yii::app()->request->baseUrl ?>/blog-<?= $blog->slug ?>">
                                                <span>
                                                    <?= $blog->title ?>
                                                </span>
                                            </a>
                                        </div>
                                        <div class="w-date">
                                            <?= $blog->create_date; ?>        
                                        </div>
                                        <div class="item-lb-description">
                                            <?= substr($blog->post, 0, 100) ?> ...
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <?php
                        }
                        ?>
                    </ul>

                </div>

            </div>




            <?php
            $advertise = Advertise::model()->find(array('condition' => ' active=1 and page=1 and size=2', 'order' => 'rand()'));
            ?>
            <div class="col-md-4 client-say">
                <div class="block-st-img">
                    <a title="<?php echo $advertise->link; ?>"  target="_blank" href="<?php echo $advertise->link; ?>">
                        <img src="<?= Yii::app()->request->baseUrl ?>/media/advertise/<?php echo $advertise->image; ?>" alt="<?php echo $advertise->link; ?>" style="
                             width: 100%;
                             ">
                    </a>
                </div>

            </div>
        </div>
    </div>
</div>




<div class="container">
    <div class="row">
        <div class="service-bottom-home-left">

            <?php
            if ($informations) {
                foreach ($informations as $info) {
                    ?>
                    <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                        <div class="<?php echo $info->image; ?>">
                            <a title="<?php echo $info->title; ?>"  target="_blank" href="<?php echo $info->link; ?>">
                                <?php echo $info->title; ?>
                            </a>
                        </div>
                        <div class="service-info">
                            <h2>
                                <a title="<?php echo $info->title; ?>"  target="_blank" href="<?php echo $info->link; ?>" >
                                    <?php echo $info->title; ?>
                                </a>
                            </h2>
                            <p>
                                <?php echo $info->subtitle; ?>
                            </p>
                        </div>
                    </div>


                    <?php
                }
            }
            ?> 


        </div>
    </div>
</div>
