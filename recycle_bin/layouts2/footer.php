<div id="yt_footer" class="yt-footer wrap">
	<div class="yt-footer-wrap">
      <div class="yt-footer-top">
        <div class="container">
          <div class="row">
            <div class="yt-footer-top-wrap">
              <div class="block block-subscribe-footer">
                
                <div class="block-title">
                  <h2>
                    <?php echo Yii::t('default', 'Subscribe to the Best of SVI Cards')?>
                  </h2>
                </div>
                
                <div class="block-content">
                  <form action="<?=Yii::app()->getBaseUrl(true);?>/home/Subscribe" method="post" id="newsletter-validate-detail">
                    <div class="input-box">
                      <input type="email" name="email" id="newsletter" title="<?php echo Yii::t('default', 'Sign up for our newsletter')?>" class="input-text required-entry validate-email" placeholder="Enter your email...">
                    </div>
                    <div class="actions">
                      <button type="submit" title="<?php echo Yii::t('default', 'Subscribe')?> " class="button">
                        <span>
                          <span>
                            <?php echo Yii::t('default', 'Subscribe')?> 
                          </span>
                        </span>
                      </button>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="yt-footer-middle">
       <div class="container">
                <div class="row">
                  <div class="col-md-8 most-popular">
                    <div class="title-footer">
                      <h2><?php echo Yii::t('default', 'More Information')?> </h2>
                      <div class="content-footer">
                        <div class="row">
                          <div class="col-md-3 col-sm-6">
                            <h3></h3>
                            <ul class="list-links">
                              
                              <li>
                                <a title="<?php echo Yii::t('default', 'Terms & Conditions')?>" href="<?php echo HtmlHelper::HtmlPageLink(4) ?>"><?php echo Yii::t('default', 'Terms & Conditions')?></a>
                              </li>
                            </ul>
                          </div>
                          <div class="col-md-3 col-sm-6">
                            <h3></h3>
                            <ul class="list-links">
                              <li>
                                <a title="<?php echo Yii::t('default', 'Advertise')?>" href="<?=Yii::app()->getBaseUrl(true);?>/home/contact?type=1"><?php echo Yii::t('default', 'Advertise')?></a>
                              </li>
                            </ul>
                          </div>
                          <div class="col-md-3 col-sm-6 clear-left">
                            <h3></h3>
                            <ul class="list-links">
       
                              
                              <li>
                                <a title="<?php echo Yii::t('default', 'Join As Partner')?>" href="<?=Yii::app()->getBaseUrl(true);?>/home/contact?type=2"><?php echo Yii::t('default', 'Join As Partner')?></a>
                              </li>
                            </ul>
                          </div>
                          <div class="col-md-3 col-sm-6">
                            <h3></h3>
                            <ul class="list-links">
 
                              <li>
                                <a title="<?php echo Yii::t('default', 'Careers')?>" href="<?=Yii::app()->getBaseUrl(true);?>/home/careers"><?php echo Yii::t('default', 'Careers')?></a>
                              </li>
                            </ul>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="row">
                      
                     
                      <div class="col-md-12 payment-wrap" style="padding-top:0px">
                        <div class="title-footer">
                          <h2>
                            <?php echo Yii::t('default', 'We Accept')?>
                          </h2>
                        </div>
                        <div class="content-footer">
                          <ul class="list-payment">
                            <li title="<?php echo Yii::t('default', 'Payment 1')?>">
                              <img src="<?=Yii::app()->request->baseUrl?>/media/wysiwyg/payment-1.png" width = "62" height = "64"  alt="<?php echo Yii::t('default', 'Payment 1')?>">
                            </li>
                            <li title="<?php echo Yii::t('default', 'Payment 2')?>">
                              <img src="<?=Yii::app()->request->baseUrl?>/media/wysiwyg/payment-2.png" alt="<?php echo Yii::t('default', 'Payment 4')?>">
                            </li>
                            <li title="<?php echo Yii::t('default', 'Payment 4')?>">
                              <img src="<?=Yii::app()->request->baseUrl?>/media/wysiwyg/payment-4.png" alt="<?php echo Yii::t('default', 'Payment 4')?>">
                            </li>
                          </ul>
                        </div>
                      </div>

                    </div>
                  </div>
                </div>
              </div>
      </div>
  
    </div>
    
    
  </div>
</div>

</body>
</html>