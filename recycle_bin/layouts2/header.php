<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="content-type" content="text/html;charset=UTF-8">
        <meta name="format-detection" content="telephone=no">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
        <title><?php echo Yii::app()->name; ?> </title>
        <link rel="stylesheet" type="text/css" href="<?= Yii::app()->request->baseUrl ?>/css/frontend/default/sm_flipshop/bootstrap/css/bootstrap.min.css" media="all">
        <link rel="stylesheet" type="text/css" href="<?= Yii::app()->request->baseUrl ?>/css/frontend/default/sm_flipshop/css/font-awesome.css" media="all">
        <link rel="stylesheet" type="text/css" href="<?= Yii::app()->request->baseUrl ?>/css/frontend/default/sm_flipshop/css/theme.css" media="all">
        <link rel="stylesheet" type="text/css" href="<?= Yii::app()->request->baseUrl ?>/css/frontend/default/sm_flipshop/aw_blog/css/style.css" media="all">
        
	<link rel="stylesheet" type="text/css" href="<?= Yii::app()->request->baseUrl ?>/css/style.css">
        <link rel="stylesheet" type="text/css" href="<?= Yii::app()->request->baseUrl; ?>/css/main.css" />

	<?php if (Yii::app()->language == 'ar') { ?>
            <link rel="stylesheet" type="text/css" href="<?= Yii::app()->request->baseUrl ?>/css/style_ar.css">
            <link rel="stylesheet" type="text/css" href="<?= Yii::app()->request->baseUrl; ?>/css/main_ar.css" />
        <?php } else { ?>
            <link rel="stylesheet" type="text/css" href="<?= Yii::app()->request->baseUrl ?>/css/style.css">
            <link rel="stylesheet" type="text/css" href="<?= Yii::app()->request->baseUrl; ?>/css/main.css" />
        <?php } ?>


        <link rel="stylesheet" type="text/css" href="<?= Yii::app()->request->baseUrl ?>/css/frontend/default/sm_flipshop/sm/basicproducts/css/style.css" media="all">
        <link rel="stylesheet" type="text/css" href="<?= Yii::app()->request->baseUrl ?>/css/frontend/default/sm_flipshop/sm/dynamicslideshow/css/sm-dynamicslideshow.css" media="all">
        <link rel="stylesheet" type="text/css" href="<?= Yii::app()->request->baseUrl ?>/css/frontend/default/sm_flipshop/sm/dynamicslideshow/css/sm-dynamicslideshow-settings.css" media="all">
        <link rel="stylesheet" type="text/css" href="<?= Yii::app()->request->baseUrl ?>/css/frontend/default/sm_flipshop/sm/imageslider/css/imageslider.css" media="all">
        <link rel="stylesheet" type="text/css" href="<?= Yii::app()->request->baseUrl ?>/css/frontend/default/sm_flipshop/sm/quickview/css/quickview.css" media="all">
        <link rel="stylesheet" type="text/css" href="<?= Yii::app()->request->baseUrl ?>/css/frontend/default/sm_flipshop/sm/searchboxpro/css/searchboxpro.css" media="all">
        <link rel="stylesheet" type="text/css" href="<?= Yii::app()->request->baseUrl ?>/css/frontend/default/sm_flipshop/sm/supercategories/css/animate.css" media="all">     
        <link rel="stylesheet" type="text/css" href="<?= Yii::app()->request->baseUrl ?>/css/frontend/default/sm_flipshop/sm/supercategories/css/supercategories.css" media="all">
        <link rel="stylesheet" href="<?= Yii::app()->request->baseUrl ?>/css/frontend/default/sm_flipshop/css/yt-responsive.css" type="text/css">
        <link rel="stylesheet" href="<?= Yii::app()->request->baseUrl ?>/css/frontend/default/sm_flipshop/css/colors/cyan_red.css" type="text/css">

        <script type="text/javascript" src="<?= Yii::app()->request->baseUrl ?>/css/frontend/default/sm_flipshop/js/jquery-1.11.1.min.js"></script>

        <script type="text/javascript" src="<?= Yii::app()->request->baseUrl ?>/css/frontend/default/sm_flipshop/js/jquery-ui.min.js"></script>

        <script type="text/javascript" src="<?= Yii::app()->request->baseUrl ?>/css/frontend/default/sm_flipshop/bootstrap/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="<?= Yii::app()->request->baseUrl ?>/css/frontend/default/sm_flipshop/js/owl.carousel.js"></script> 
    </head>
    <body id="bd" class="sm_flipshop pattern6   cms-index-index cms-home-left"> 
        <div id="yt_wrapper">
            <!-- BEGIN: Header -->
            <div id="yt_header" class="yt-header wrap">

                <div class="yt-header-top">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-4 welcome-wrap">
                                <p class="welcome-msg">
                                    <?php echo Yii::t('default', 'Welcome to SVI-Card');?>
                                </p>
                            </div>
                            <div class="col-md-8 toplinks-setting">
                                <div class="links-top">
                                    <ul class="links links-second">
                                        <?php if (Yii::app()->user->id) { ?>
                                            <li class="last">
                                                <a title="My Account" href="<?= Yii::app()->getBaseUrl(true); ?>/home/logout" style="font-weight: bold;">
                                                    <?php echo Yii::t('default', 'Logout');?>
                                                </a>
                                            </li>
                                            <li class="last">
                                                <a title="My Account" href="<?= Yii::app()->request->baseUrl ?>/profile" style="font-weight: bold;">
                                                    <?php echo Yii::t('default', 'My Account');?>
                                                </a>
                                            </li>
                                        <?php } else {
                                            ?>
                                            <li>
                                                <a title="Register" data-toggle="modal" data-target="#modal-register" style="font-weight: bold;">
                                                    <?php echo Yii::t('default', 'Register');?>
                                                </a>
                                                <div class="modal fade" id="modal-register" tabindex="-1" role="dialog" aria-hidden="true">
                                                    <div class="modal-dialog block-popup-login">
                                                        <a href="javascript:void(0)" title="Close" class="close close-login" data-dismiss="modal">
                                                            <?php echo Yii::t('default', 'Close');?>
                                                        </a>
                                                        <div id="reg-error-div" class="alert btn-danger" style="display: none;color:#FFF"></div>

                                                        <?php
                                                        $form_reg = $this->beginWidget('CActiveForm', array(
                                                            'id' => 'user-register-form',
                                                            'htmlOptions' => array(
                                                            ),
                                                        ));
                                                        ?>
                                                        <div class="block-content">
                                                            <?php echo $form_reg->emailField($this->user_signUp, 'email', array('class' => 'input-text', 'placeholder' => 'Email')); ?>
                                                            <?php echo $form_reg->error($this->user_signUp, 'email'); ?>
                                                            <?php echo $form_reg->passwordField($this->user_signUp, 'password', array('class' => 'input-text', 'placeholder' => 'Password')); ?>
                                                            <?php echo $form_reg->error($this->user_signUp, 'password'); ?>
                                                            <?php echo $form_reg->passwordField($this->user_signUp, 'password_repeat', array('class' => 'input-text', 'placeholder' => 'Confirm Password')); ?>
                                                            <?php echo $form_reg->error($this->user_signUp, 'password_repeat'); ?>
                                                            <?php echo $form_reg->textField($this->user_signUp, 'full_name', array('class' => 'input-text', 'placeholder' => 'Full name')); ?>
                                                            <?php echo $form_reg->error($this->user_signUp, 'full_name'); ?>
                                                            <?php echo $form_reg->textField($this->user_signUp, 'phone', array('class' => 'input-text', 'placeholder' => 'Phone number')); ?>
                                                            <?php echo $form_reg->error($this->user_signUp, 'phone'); ?>
                                                            <?php // echo $form_reg->textField($this->user_signUp, 'age', array('class' => 'input-text', 'placeholder' => 'Age 16+', 'min' => 16)); ?>

                                                            <?php
                                                            $start_year = date('Y-m-d', strtotime('-16 year'));
                                                            ?>
                                                            <?php
                                                            $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                                                                'name' => 'age',
                                                                'attribute' => 'age',
                                                                'model' => $this->user_signUp,
                                                                'options' => array(
                                                                    'dateFormat' => 'yy-m-d',
                                                                    // 'altFormat' => 'dd-mm-yy',
                                                                    //'changeMonth' => true,
                                                                    //'changeYear' => true,
                                                                    'maxDate' => $start_year,
                                                                    'yearRange' => '-100:+0', // range of years (1914-2014) and its dynamic
                                                                // 'appendText' => 'yyyy-mm-dd',
                                                                ),
                                                                'htmlOptions' => array(
                                                                    'class' => 'input-text',
                                                                    'placeholder' => 'Age 16+',
                                                                ),
                                                                    )
                                                            );
                                                            ?>  

                                                            <?php echo $form_reg->error($this->user_signUp, 'age'); ?>
                                                            <?php echo $form_reg->textField($this->user_signUp, 'address', array('class' => 'input-text', 'placeholder' => 'Address')); ?>
                                                            <?php echo $form_reg->error($this->user_signUp, 'address'); ?>
                                                            <?php echo $form_reg->dropDownList($this->user_signUp, 'payment_method', array(1 => 'Online', 2 => 'Cash On Deliver'), array('class' => 'input-text')); ?>
                                                            <?php echo $form_reg->error($this->user_signUp, 'payment_method'); ?>
                                                            
    <label>Interested in:</label><br />
    <select name="categories[]" class="input-text" multiple="multiple">
        <?php
        foreach (Category::model()->findAll() as $cat) {
         $sub_cats = Category::model()->findAllByAttributes(array('parent' => $cat->id));
         
                                    if ($sub_cats) {
                                        ?>
                          <optgroup label="<?= $cat->title ?>">
<!--                               <option value="<?= $cat->id ?>"> <?= $cat->title ?></option>-->
                               
                                     <?php
                                        foreach ($sub_cats as $sub_cat) {
                                            ?>
                                           
                                            <option value="<?= $sub_cat->id ?>"> <?= $sub_cat->title ?></option>
                                            <?php
                                        }
                                        ?>
                                             </optgroup>
                                      <?php
                                    }
                                    

            ?>
           
            <?php
        }
        ?>
    </select>
                                                            <div class="actions">
                                                                <div class="submit-login">
                                                                    <?php
                                                                    echo CHtml::ajaxSubmitButton(
                                                                            'Register', array('/home/register'), array(
                                                                        'beforeSend' => 'function(){
            $("#reg_btn").attr("disabled",true);
        }',
                                                                        'complete' => 'function(){
            $("#reg_btn").attr("disabled",false);
        }',
                                                                        'success' => 'function(data){
        data=jQuery.trim(data);
            var x=data.split("*-*");
            if(x[0] == "wrong"){
                $("#reg-error-div").show();
                $("#reg-error-div").html("<h5>Register failed!</h5>");$("#reg-error-div").append(x[1]);
            }
            else{
                data=jQuery.trim(data);
                $("#user-register-form").html("<h4 style=\"color:red;text-align:center;\">Our Confirmation mail will send on your email address</h4>");
                parent.location.href = "' . Yii::app()->request->baseUrl . '/"+data;                                                                 
            }
                data=jQuery.trim(data);
        }'
                                                                            ), array("id" => "reg_btn", "class" => "button btn-r submit-repl")
                                                                    );
                                                                    ?>   


                                                                </div>
                                                            </div>
                                                            <?php $this->endWidget(); ?>
                                                            <a class="btn btn-fb btn-default fb-bt" href="<?= Yii::app()->getBaseUrl(true); ?>/home/loginFB"><img src="<?= Yii::app()->request->baseUrl; ?>/img/fb-icon.png" alt="">Register with Facebook</a>                                                      
                                                            <div class="more-actions">
                                                                <div class="ft-link-p">
                                                                    <span>
                                                                        Already member. 
                                                                    </span>
                                                                    <a data-toggle="modal" data-target="#modal-login" title="Login" class="button-reg">
                                                                        Login
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                            <li>
                                                <a data-toggle="modal" data-target="#modal-login" title="Login" style="font-weight: bold;">
                                                    Login
                                                </a>
                                                <div class="modal fade" id="modal-login" tabindex="-1" role="dialog" aria-hidden="true">
                                                    <div class="modal-dialog block-popup-login">
                                                        <a href="javascript:void(0)" title="Close" class="close close-login" data-dismiss="modal">
                                                            Close
                                                        </a>
                                                        <div   id="login-div">
                                                            <div id="login-error-div"  style="padding: 10px;color: #f00;"  class="errorMessage" style="display: none;"></div>
                                                            <?php
                                                            $form = $this->beginWidget('CActiveForm', array(
                                                                'id' => 'login-form',
                                                                'action' => Yii::app()->createUrl('/home/Login'),
                                                                'htmlOptions' => array(
                                                                ),
                                                            ));
                                                            ?>
                                                            <div class="block-content">
                                                                <?php echo $form->textField($this->user_login, 'username', array('placeholder' => 'username', 'class' => 'input-text')); ?>                           
                                                                <?php echo $form->passwordField($this->user_login, 'password', array('class' => 'input-text', 'placeholder' => 'Password')); ?>
                                                                <div class="actions">
                                                                    <div class="chk-remember">
                                                                        <div class="checker" id="uniform-chk_remember">
                                                                            <span>
                                                                                <div class="checker" id="uniform-chk_remember">
                                                                                    <span>
                                                                                        <div class="checker" id="uniform-chk_remember">
                                                                                            <span>
                                                                                                <input id="chk_remember" type="checkbox" name="rememberme" value="remember">
                                                                                            </span>
                                                                                        </div>
                                                                                    </span>
                                                                                </div>
                                                                            </span>
                                                                        </div>

                                                                        <label for="chk_remember">
                                                                            Remember
                                                                        </label>

                                                                    </div>
                                                                    <div class="submit-login">
                                                                        <?php
                                                                        echo CHtml::ajaxSubmitButton(
                                                                                'Login', array('/home/login'), array(
                                                                            'beforeSend' => 'function(){
                                                    $("#login_btn").attr("disabled",true);
                                                }',
                                                                            'complete' => 'function(){
                                                    $("#login-form").each(function(){ this.reset();});
                                                    $("#login_btn").attr("disabled",false);
                                                                            }',
                                                                            'success' => 'function(data){
                                                    data=jQuery.trim(data);
                                                   var x=data.split("*-*");
                                                 //  alert(x[1] ); 
                                                  if(x[0] == "wrong"){
                                                     if(x[1]  == "Incorrect username or password<br><br>"){
                                                 // alert("wrong pass") ;
                                                   $("#login-error-div").show();
                                                        $("#login-error-div").html("<h4>Incorrect username or password.</h4>");$("#login-error-div").append("");
                                                  
                                                  }else if(x[1]  == "This account is login before<br><br>"){
                                                     //alert("login before") ;
                                                      $("#login-error-div").show();
                                                        $("#login-error-div").html("<h4>This account is login before.</h4>");$("#login-error-div").append("");
                                                  }
                                               }else{
                                                        $("#login-div").html("<h4 style=\"padding: 10px;color: #0f0;\"   >Login Successful! Please Wait...</h4>");
                                                        parent.location.href = "' . Yii::app()->request->baseUrl . '/"+data;                                                                 
                                                    }
                                               
                                                }'
                                                                                ), array("id" => "login_btn", "class" => "button submit-repl")
                                                                        );
                                                                        ?>
                                                                    </div>
                                                                    <a class="btn btn-fb btn-default fb-bt" href="<?= Yii::app()->getBaseUrl(true); ?>/home/loginFB"><img src="<?= Yii::app()->request->baseUrl; ?>/img/fb-icon.png" alt="">Login with Facebook</a>                                                      

                                                                </div>
                                                                <div class="more-actions">
                                                                    <div class="ft-link-p">
                                                                        <!--<a data-toggle="modal" data-target="#modal-forgetpass" class="f-left">
                                                                            Forgot your password
                                                                        </a>-->
                                                                    </div>
                                                                    <div class="ft-link-p">
                                                                        <span>
                                                                            Dont have account. 
                                                                        </span>
                                                                        <a data-toggle="modal" data-target="#modal-register" title="Create an Account" class="button-reg">
                                                                            Register here
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <?php $this->endWidget(); ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                            <?php
                                        }
                                        ?>
                                        <li>
                                            <?php $this->widget('ext.EasyMultiLanguage.widgets.LanguageSelectorWidget', array(
                                                    'style'    => 'inline',  // "dropDown" or "inline". Optional. Default is "dropDown"
                                                    'cssClass' => 'lang',  // Optional. Additional css class for selector.
                                            )); 

                                            ?>
                                            
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="yt-header-content">
                    <div class="container">
                        <div class="row">
                            <div class="logo-w col-lg-3 col-md-3">
                                <a title="SVI-Card" href="<?= Yii::app()->getBaseUrl(true) ?>/home">
                                    <img src="<?= Yii::app()->request->baseUrl ?>/images/logo.jpg" alt="SVI - Online Store" style="
                                         height: 50px; margin-top: -15px;
                                         ">
                                </a>
                            </div>
                            <div class="header-content-right col-lg-9 col-md9">
                                <div id="social-wraps-container" class="col-md-2"  >
                                    <div class="socials-wrap" style="margin-top:0px">
                                        <ul>
                                            <li>
                                                <a  target="_blank" href="<?= Yii::app()->params['facebook'] ?>" style="
                                                    background-image: url(&quot;<?= Yii::app()->request->baseUrl; ?>/css/frontend/default/sm_flipshop/images/fb-logo-flat.jpg&quot;);  background-size: cover;  width: 43px;  height: 43px;
                                                    "></a>
                                            </li>
                                            <li>
                                                <a   target="_blank" href="<?= Yii::app()->params['twitter'] ?>" style="
                                                     background-image: url(&quot;<?= Yii::app()->request->baseUrl; ?>/css/frontend/default/sm_flipshop/images/twitter-logo-flat.jpg&quot;);  background-size: cover;  width: 43px;  height: 43px;
                                                     "></a>
                                            </li>
                                            
                                            
                                            <li>
                                                <a   target="_blank" href="<?= Yii::app()->params['google'] ?>" style="
                                                     background-image: url(&quot;<?= Yii::app()->request->baseUrl; ?>/css/frontend/default/sm_flipshop/images/google.png&quot;);  background-size: cover;  width: 43px;  height: 43px;
                                                     "></a>
                                            </li>
                                            
                                             <li>
                                                <a   target="_blank" href="<?= Yii::app()->params['linkedin'] ?>" style="
                                                     background-image: url(&quot;<?= Yii::app()->request->baseUrl; ?>/css/frontend/default/sm_flipshop/images/linkedin.png&quot;);  background-size: cover;  width: 43px;  height: 43px;
                                                     "></a>
                                            </li>
                                            
                                            <li>
                                                <a   target="_blank" href="<?= Yii::app()->params['youtube'] ?>" style="
                                                     background-image: url(&quot;<?= Yii::app()->request->baseUrl; ?>/css/frontend/default/sm_flipshop/images/youtube.png&quot;);  background-size: cover;  width: 43px;  height: 43px;
                                                     "></a>
                                            </li>

                                        </ul>
                                    </div>              

                                </div>
                                <div id="sm_serachbox_pro8911413405079" class="sm-serachbox-pro">
                                    <span>
                                    </span>
                                    <a id="btn-search-box" href="javascript:void(0)" class="btn-trigger">
                                    </a>
                                    <script>
                                        jQuery(document).ready(function($) {
                                            $('#btn-search-box').click(function() {
                                                $('.content-myaccount-m').hide();
                                                $('.sm-searbox-content').slideToggle(200);
                                            }
                                            );
                                            $('.sm-searbox-content,#btn-search-box').click(function(e) {
                                                e.stopPropagation();
                                            }
                                            );
                                            $(document).click(function() {
                                                $('.sm-searbox-content').hide();
                                            }
                                            );
                                        }
                                        );
                                    </script>
                                    <div class="sm-searbox-content" style="display: none;">
                                        <?php $this->renderPartial("//home/searchForm"); ?>
                                    </div>
                                </div>


                            </div>
                        </div>
                    </div>
                </div>

                <div class="yt-header-under">
                    <?php $this->renderPartial("//home/nav"); ?>
                </div>

                <script type="text/javascript">
                    jQuery(document).ready(function($) {
                        $('.theme-color').click(function() {
                            $($(this).parent().find('.active')).removeClass('active');
                            $(this).addClass('active');
                        }
                        );
                    }
                    );

                </script>

                <a id="yt-totop" href="#" title="Go to Top" style="display: none;">
                </a>
                <script type="text/javascript">
                    // slide to top
                    jQuery(document).ready(function($) {

                        $("#yt-totop").hide();
                        $(function() {
                            var wh = $(window).height();
                            var whtml = $(document).height();
                            $(window).scroll(function() {
                                if ($(this).scrollTop() > whtml / 10) {
                                    $('#yt-totop').fadeIn();
                                }
                                else {
                                    $('#yt-totop').fadeOut();
                                }
                            }
                            );
                            $('#yt-totop').click(function() {
                                $('body,html').animate({
                                    scrollTop: 0
                                }
                                , 800);
                                return false;
                            }
                            );
                        }
                        );
                    }
                    );
                </script>
            </div>
            <!-- END: Header -->


            <script>
                function get_outlets(partner) {
                    $.ajax({
                        type: 'POST', //request type
                        url: "<?= CController::createUrl('home/getoutlets') ?>",
                        data: {partner_id: partner},
                        success: function(data) {
                            $("#outlet").html(data);
                           $('#outlet_drop').val(<?= $_REQUEST['outlet']; ?>);
                        }
                    });
                }
                function get_menus(outlet) {
                    $.ajax({
                        type: 'POST', //request type
                        url: "<?= CController::createUrl('home/getmenus') ?>",
                        data: {outlet_id: outlet},
                        success: function(data) {
                            $("#menu").html(data) ;
                            $('#Reservations_menu_id').val(<?= $_REQUEST['menu']; ?>);
                        }
                    });
                }

                function get_items(menu) {
                    $.ajax({
                        type: 'POST', //request type
                        url: "<?= CController::createUrl('home/getitems') ?>",
                        data: {menu_id: menu},
                        success: function(data) {
                            $("#item").html(data)
                        }
                    });
                }
            </script>
            
            
            
