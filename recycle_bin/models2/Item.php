<?php

/**
 * This is the model class for table "item".
 *
 * The followings are the available columns in table 'item':
 * @property integer $id
 * @property integer $partner_id
 * @property string $title
 * @property string $intro
 * @property string $description
 * @property string $price
 * @property string $image
 * @property integer $gallery_id
 * @property integer $cat_id
 * @property integer $quantity
 * @property integer $active
 * @property integer $featured
 * @property string $slug
 * @property integer $discount_type
 * @property string $discount_amount
 * @property integer $hot_deal
 * @property integer $new_deal
 *
 * The followings are the available model relations:
 * @property Category $cat
 * @property Gallery $gallery
 * @property Partner $partner
 */
class Item extends CActiveRecord implements IECartPosition
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'item';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('partner_id, title, price,image,intro', 'required'),
			array('partner_id, gallery_id, cat_id, quantity, active, featured, discount_type, hot_deal, new_deal, recomended, availability', 'numerical', 'integerOnly'=>true),
			array('title, image, slug, model, brand', 'length', 'max'=>255),
			array('discount_amount', 'length', 'max'=>10),
                        array('price', 'match' ,
                        'pattern'=> '/^\s*-?[1-9]\d*(\.\d{1,2})?\s*$/u',    
                        'message'=> '{attribute}: must be number'
                        ),
			array('intro, description,cat_id', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, partner_id, title, intro, description, price, image, gallery_id, cat_id, quantity, active, featured, slug, discount_type, discount_amount, hot_deal, new_deal', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'cat' => array(self::BELONGS_TO, 'Category', 'cat_id'),
			'gallery' => array(self::BELONGS_TO, 'Gallery', 'gallery_id'),
			'partner' => array(self::BELONGS_TO, 'Partner', 'partner_id'),
                        'brand' => array(self::BELONGS_TO, 'Brands', 'brand_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'partner_id' => 'Partner',
			'title' => 'Title',
			'intro' => 'Intro',
			'description' => 'Description',
			'price' => 'Price',
			'image' => 'Image',
			'gallery_id' => 'Gallery',
			'cat_id' => 'Category',
			'quantity' => 'Quantity',
			'active' => 'Active',
			'featured' => 'Featured',
			'slug' => 'Slug',
			'discount_type' => 'Discount Type',
			'discount_amount' => 'Discount Amount',
			'hot_deal' => 'Hot Deal',
			'new_deal' => 'New Deal',
                        'availability' => 'In Stock'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('partner_id',$this->partner_id);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('intro',$this->intro,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('price',$this->price,true);
		$criteria->compare('image',$this->image,true);
		$criteria->compare('gallery_id',$this->gallery_id);
		$criteria->compare('cat_id',$this->cat_id);
		$criteria->compare('quantity',$this->quantity);
		$criteria->compare('active',$this->active);
		$criteria->compare('featured',$this->featured);
		$criteria->compare('slug',$this->slug,true);
		$criteria->compare('discount_type',$this->discount_type);
		$criteria->compare('discount_amount',$this->discount_amount,true);
		$criteria->compare('hot_deal',$this->hot_deal);
		$criteria->compare('new_deal',$this->new_deal);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Item the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        
        protected function beforeSave(){
            $this->slug = Helper::slugify($this->title);
            $this->slug .= $this->id;
            return true;
        }
        
        function getId(){
            return $this->id;
        }

        function getPrice(){
            return $this->price;
        }
}
