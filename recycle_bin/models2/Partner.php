<?php

/**
 * This is the model class for table "partner".
 *
 * The followings are the available columns in table 'partner':
 * @property integer $id
 * @property string $title
 * @property string $intro
 * @property string $description
 * @property string $image
 * @property string $slug
 * @property integer $new
 * @property integer $featured
 * @property integer $active
 *
 * The followings are the available model relations:
 * @property Item[] $items
 * @property Outlet[] $outlets
 * 
 * discount
 * cat_id
 */
class Partner extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'partner';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
		       array('title,image,intro', 'required'),
			array('new, featured, active, gallery_id', 'numerical', 'integerOnly'=>true),
			array('title, image, slug', 'length', 'max'=>255),
			array('intro, description,cat_id,subcat_id,brand_id', 'safe'),
                        array('discount', 'match' ,
                        'pattern'=> '/^\s*-?[1-9]\d*(\.\d{1,2})?\s*$/u',    
                        'message'=> 'Discount must be number'
                        ),

			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, title, intro, description, image, slug, new, featured, active,discount,cat_id,brand_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'items' => array(self::HAS_MANY, 'Item', 'partner_id'),
			'outlets' => array(self::HAS_MANY, 'Outlet', 'partner_id'),
                        'cat' => array(self::BELONGS_TO, 'Category', 'cat_id'),
                       'brand' => array(self::BELONGS_TO, 'Brands', 'brand_id'),

                    
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
            return array(
                'id' => 'ID',
                'title' => 'Title',
                'intro' => 'Intro',
                'description' => 'Description',
                'image' => 'Image',
                'slug' => 'Slug',
                'new' => 'New',
                'featured' => 'Featured',
                'active' => 'Active',
                'cat_id' => 'category',
                'subcat_id' => 'Sub Category',
                'brand_id' => 'Brand',
            );
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('intro',$this->intro,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('image',$this->image,true);
		$criteria->compare('slug',$this->slug,true);
		$criteria->compare('new',$this->new);
		$criteria->compare('featured',$this->featured);
		$criteria->compare('active',$this->active);
                $criteria->compare('discount',$this->discount);
                $criteria->compare('cat_id',$this->cat_id);
                $criteria->compare('brand_id',$this->brand_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Partner the static model class
	 */
	public static function model($className=__CLASS__)
	{
            return parent::model($className);
	}
        
        protected function beforeSave(){
            $this->slug = Helper::slugify($this->title);
            $this->slug .= $this->id;
            return true;
        }
}
