<?php

/**
 * This is the model class for table "modules".
 *
 * The followings are the available columns in table 'modules':
 * @property integer $id
 * @property string $title
 * @property integer $parent_id
 * @property string $url
 * @property integer $show
 * @property integer $employee_id
 * @property integer $company_id
 * @property integer $permission
 * @property integer $sort_num
 * @property string $table_name
 * @property string $field
 * @property string $appear_access
 * @property integer $show_appear
 * @property integer $is_report
 * @property string $icon
 *
 * The followings are the available model relations:
 * @property User $employee
 * @property CrmCompany $company
 */
class Modules extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'modules';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('parent_id, show, employee_id, company_id, permission, sort_num, show_appear, is_report', 'numerical', 'integerOnly'=>true),
			array('title', 'length', 'max'=>100),
			array('url, table_name, field, appear_access, icon', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, title, parent_id, url, show, employee_id, company_id, permission, sort_num, table_name, field, appear_access, show_appear, is_report, icon', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'employee' => array(self::BELONGS_TO, 'User', 'employee_id'),
			'company' => array(self::BELONGS_TO, 'CrmCompany', 'company_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'title' => 'Title',
			'parent_id' => 'Parent',
			'url' => 'Url',
			'show' => 'Show',
			'employee_id' => 'Employee',
			'company_id' => 'Company',
			'permission' => 'if it will get permission or not   hidden just for us',
			'sort_num' => 'Sort Num',
			'table_name' => 'Table Name',
			'field' => 'Field',
			'appear_access' => '2 => crm, 1 => pm, 3 => as',
			'show_appear' => 'Show Appear',
			'is_report' => 'Is Report',
			'icon' => 'Icon',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('parent_id',$this->parent_id);
		$criteria->compare('url',$this->url,true);
		$criteria->compare('show',$this->show);
		$criteria->compare('employee_id',$this->employee_id);
		$criteria->compare('company_id',$this->company_id);
		$criteria->compare('permission',$this->permission);
		$criteria->compare('sort_num',$this->sort_num);
		$criteria->compare('table_name',$this->table_name,true);
		$criteria->compare('field',$this->field,true);
		$criteria->compare('appear_access',$this->appear_access,true);
		$criteria->compare('show_appear',$this->show_appear);
		$criteria->compare('is_report',$this->is_report);
		$criteria->compare('icon',$this->icon,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Modules the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
