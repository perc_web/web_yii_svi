<?php

/**
 * This is the model class for table "reviews".
 *
 * The followings are the available columns in table 'reviews':
 * @property integer $id
 * @property integer $user_id
 * @property integer $item_id
 * @property integer $price_rate
 * @property integer $value_rate
 * @property integer $quality_rate
 * @property string $review
 * @property string $date_created
 * @property string $title
 * @property integer $approved
 *
 * The followings are the available model relations:
 * @property Item $item
 * @property User $user
 */
class Reviews extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'reviews';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('user_id, item_id, price_rate, value_rate, quality_rate, approved', 'numerical', 'integerOnly'=>true),
			array('title', 'length', 'max'=>255),
			array('review, date_created', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, user_id, item_id, price_rate, value_rate, quality_rate, review, date_created, title, approved', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'item' => array(self::BELONGS_TO, 'Menu', 'item_id'),
			'user' => array(self::BELONGS_TO, 'User', 'user_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'user_id' => 'User',
			'item_id' => 'Menu',
			'price_rate' => 'Price Rate',
			'value_rate' => 'Value Rate',
			'quality_rate' => 'Quality Rate',
			'review' => 'Review',
			'date_created' => 'Date Created',
			'title' => 'Title',
			'approved' => 'Approved',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('item_id',$this->item_id);
		$criteria->compare('price_rate',$this->price_rate);
		$criteria->compare('value_rate',$this->value_rate);
		$criteria->compare('quality_rate',$this->quality_rate);
		$criteria->compare('review',$this->review,true);
		$criteria->compare('date_created',$this->date_created,true);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('approved',$this->approved);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Reviews the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
