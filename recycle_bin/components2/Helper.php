<?php

/**
 *
 *
 * @version $Id$
 * @copyright 2013
 */
class Helper {

    public static function PlayVideo($model) {
        $player = Yii::app()->controller->widget('ext.Yiitube', array('v' => $model->video, 'size' => 'small'));


        return '<div class="VideoPlay">' . $player->play() . '</div>';
    }

    public static function yiiparam($name, $default = null) {
        if (isset(Yii::app()->params[$name]))
            return Yii::app()->params[$name];
        else
            return $default;
    }

    public static function DrawPageLink($page_id) {
        $page = Pages::model()->findByPk($page_id);
        if ($page === null) {
            return 'Not-Found';
        }

        return 'home/page/view/' . $page->url;
    }

    public static function GenerateRandomKey($length = 10) {

        $chars = array_merge(range(0, 9), range('a', 'z'), range('A', 'Z'));
        shuffle($chars);
        $password = implode(array_slice($chars, 0, $length));

        return $password;
    }

    public static function getGalleryImages($gallery_id) {

        $criteria = new CDbCriteria();

        $criteria->condition = 'gallery_id=:UID';

        $criteria->params = array(':UID' => $gallery_id);
        $criteria->order = 'rank';

        $gallery = GalleryPhoto::model()->findAll($criteria);

        return $gallery;
    }

    public static function slugify($text) {
        // replace non letter or digits by -
        $text = preg_replace('~[^\\pL\d]+~u', '-', $text);

        // trim
        $text = trim($text, '-');

        // transliterate
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

        // lowercase
        $text = strtolower($text);

        // remove unwanted characters
        $text = preg_replace('~[^-\w]+~', '', $text);
        if (empty($text)) {
            return 'n-a';
        }
        return $text;
    }

    public static function active_admin($controller_id) {
        if (Yii::app()->controller->id == $controller_id) {
            return 'active';
        }
        return '';
    }

    public static function getStatus($status, $yes = 'yes', $no = 'no') {
        if ($status == 1) {
            return $yes;
        } else {
            return $no;
        }
    }
    
    
     public static function allHours() {
                return array(
                    '01' => '01',
                    '02' => '02',
                    '03' => '03',
                    '04' => '04',
                    '05' => '05',
                    '06' => '06',
                    '07' => '07',
                    '08' => '08',
                    '09' => '09',
                    '10' => '10',
                    '11' => '11',
                    '12' => '12',
             
                );
            }
            
            
            
            public static function allCurrency() {
                return array(
                    '£' => 'Egypt Pound (£) ',
                    '$' => 'Dollar ($)',
                    '¥' => 'Japan Yen (¥)',
                   
                );
            }
            
                
     public static function getAll($id = '', $model = '', $field = '') {
        $criteria = new CDbCriteria;
        $criteria->condition = $field . '=' . $id;

        return new CActiveDataProvider($model, array(
            'criteria' => $criteria,
        ));
    }

   
    // ====================access roles=========================================//
       public static function get_modules_access() {
        $criteria = new CDbCriteria();
        //$criteria->condition = 'appear_access="2"';
        $modules = Modules::model()->findAll($criteria);
        return $modules;
    }
    
     public static function store_privs($id) {
        $set = "no";
        $privs = array();
        $allowed = array("Enabled", "All");
        $roles_users = RoleUser::model()->findAllByAttributes(array('user_id' => $id));
        if ($roles_users) {

            foreach ($roles_users as $role_user) {

               // $modules = Modules::model()->findAllByAttributes(array('appear_access' => '2'));
                $modules = Modules::model()->findAll();
                $fields = RoleField::model()->findAll();

                $stored_privs = array();
                $role = Role::model()->findByPk($role_user->role_id);
                if ($role->permissions) {
                    $stored_privs = unserialize($role->permissions);
                }

                if ($modules && $fields) {

                    $set = "yes";

                    foreach ($modules as $module) {

                        //$module_title = $module->url;
                        for ($i = 0; $i < count($fields); $i++) {


                            if (isset($stored_privs[$module->url][$fields[$i]->real_action])) {
                                $value = RoleFieldValue::model()->findByPk($stored_privs[$module->url][$fields[$i]->real_action]);
                                if ($value) {
                                    if (in_array($value->title, $allowed)) {
                                        if (!isset($privs[$module->url][$fields[$i]->real_action])) {
                                            $privs[$module->url][$fields[$i]->real_action] = "opened";
                                        }
                                    } else {
                                        $privs[$module->url][$fields[$i]->real_action] = "closed";
                                    }
                                } else {
                                    $privs[$module->url][$fields[$i]->real_action] = "closed";
                                }
                            }
                        }
                    }
                }
            }
        }
        

        Yii::app()->user->setState("privs_set", $set);
       Yii::app()->user->setState("privs_arr", serialize($privs));
           
    }


}

?>