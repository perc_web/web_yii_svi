<?php

/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class AdminController extends Controller {
    

    /**
     * @var string the default layout for the controller view. Defaults to '//layouts/column1',
     * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
     */
    public $layout = '//layouts/column2';
    public $pageTitlecrumbs;

    public function init() {
         Yii::app()->errorHandler->errorAction='admin/dashboard/error'; // to make error to this layout

        // set the default theme for any other controller that inherit the admin controller
        Yii::app()->theme = 'bootstrap';

        $parameters = Settings::model()->findByPk(1);

        Yii::app()->params['google'] = $parameters['google'];
        Yii::app()->params['twitter'] = $parameters['twitter'];
        Yii::app()->params['pinterest'] = $parameters['pinterest'];
        Yii::app()->params['support_email'] = $parameters['support_email'];
        Yii::app()->params['email'] = $parameters['email'];
        Yii::app()->params['adminEmail'] = $parameters['email'];
        Yii::app()->params['facebook'] = $parameters['facebook'];
     
        Yii::app()->params['phone'] = $parameters['phone'];
        Yii::app()->params['mobile'] = $parameters['mobile'];
        Yii::app()->params['fax'] = $parameters['fax'];
        Yii::app()->params['address'] = $parameters['address'];


        
        
        //load js files
         if (!Yii::app()->request->isAjaxRequest)
            $this->registerMainScripts();
        
    }
    
     protected function registerMainScripts() {
        /**
         * libs
         */
      //  Yii::app()->clientScript->registerCoreScript('jquery'); //jQuery
        /**
         * dev files
         */
        Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl . '/js/customadmin.js', CClientScript::POS_END); //Custom js file for developers
    }

    protected function beforeAction($action) {
        
        ///////////////////////////error//////////////////////////////////////
        $parameters = Errormessage::model()->findByPk(1);
        Yii::app()->params['error_heading'] = $parameters['error_heading'];
        Yii::app()->params['error_subhead'] = $parameters['error_subhead'];
        Yii::app()->params['error_image'] = $parameters['error_image'];
        Yii::app()->params['error_home'] = $parameters['error_home'];
        Yii::app()->params['error_homeactive'] = $parameters['error_homeactive'];
        Yii::app()->params['error_prev'] = $parameters['error_prev'];
        Yii::app()->params['error_prevactive'] = $parameters['error_prevactive'];
        //////////////////////////////////error////////////////////////////////////

        //if the user is not admin redirect to the home page of the normal user
        if (Yii::app()->user->isGuest) {
            $this->redirect(Yii::app()->baseurl . '/dashboard');
        }else {
             
        $controller = Yii::app()->controller->id;
        $controller= str_replace( "admin/", "",$controller)  ;
        $action = $action->id;
        
            if (!(Yii::app()->user->getState("group") == 1)) { // not super admin apply you roles
                $user = User::model()->findByPk(Yii::app()->user->id);
                    Helper::store_privs($user->id);
         
                $flag = false;
                if (Yii::app()->user->getState('privs_set') == "yes") {
                    $privs = unserialize(Yii::app()->user->getState('privs_arr'));
                    
                    if (isset($privs[$controller]['access'])) {
                        if ($privs[$controller]['access'] == "opened") {
                            if (isset($privs[$controller][$action])) {
                                if ($privs[$controller][$action] == "opened") {
                                    $flag = true;
                                } else {
                                    $flag = false;
                                }
                            } else {
                                $flag = true;
                            }
                        } else {
                            $flag = false;
                        }
                    } else {
                        $flag = true;
                    }
                } else {
                    $flag = false;
                }

                if (!$flag) {
                    throw new CHttpException(404, "You don't have permission to access this part");
                }
            }
        }
        return parent::beforeAction($action);
    }

}
