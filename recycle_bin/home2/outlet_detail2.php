   
      <!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="content-type" content="text/html;charset=UTF-8">
        <meta name="format-detection" content="telephone=no">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
        <title><?php echo Yii::app()->name; ?> </title>
        <link rel="stylesheet" type="text/css" href="<?= Yii::app()->request->baseUrl ?>/css/frontend/default/sm_flipshop/bootstrap/css/bootstrap.min.css" media="all">
        <link rel="stylesheet" type="text/css" href="<?= Yii::app()->request->baseUrl ?>/css/frontend/default/sm_flipshop/css/font-awesome.css" media="all">
        <link rel="stylesheet" type="text/css" href="<?= Yii::app()->request->baseUrl ?>/css/frontend/default/sm_flipshop/css/theme.css" media="all">
        <link rel="stylesheet" type="text/css" href="<?= Yii::app()->request->baseUrl ?>/css/frontend/default/sm_flipshop/aw_blog/css/style.css" media="all">
        <link rel="stylesheet" type="text/css" href="<?= Yii::app()->request->baseUrl ?>/css/style.css">
        <link rel="stylesheet" type="text/css" href="<?= Yii::app()->request->baseUrl; ?>/css/main.css" />

        <link rel="stylesheet" type="text/css" href="<?= Yii::app()->request->baseUrl ?>/css/frontend/default/sm_flipshop/sm/basicproducts/css/style.css" media="all">
        <link rel="stylesheet" type="text/css" href="<?= Yii::app()->request->baseUrl ?>/css/frontend/default/sm_flipshop/sm/dynamicslideshow/css/sm-dynamicslideshow.css" media="all">
        <link rel="stylesheet" type="text/css" href="<?= Yii::app()->request->baseUrl ?>/css/frontend/default/sm_flipshop/sm/dynamicslideshow/css/sm-dynamicslideshow-settings.css" media="all">
        <link rel="stylesheet" type="text/css" href="<?= Yii::app()->request->baseUrl ?>/css/frontend/default/sm_flipshop/sm/imageslider/css/imageslider.css" media="all">
        <link rel="stylesheet" type="text/css" href="<?= Yii::app()->request->baseUrl ?>/css/frontend/default/sm_flipshop/sm/quickview/css/quickview.css" media="all">
        <link rel="stylesheet" type="text/css" href="<?= Yii::app()->request->baseUrl ?>/css/frontend/default/sm_flipshop/sm/searchboxpro/css/searchboxpro.css" media="all">
        <link rel="stylesheet" type="text/css" href="<?= Yii::app()->request->baseUrl ?>/css/frontend/default/sm_flipshop/sm/supercategories/css/animate.css" media="all">     
        <link rel="stylesheet" type="text/css" href="<?= Yii::app()->request->baseUrl ?>/css/frontend/default/sm_flipshop/sm/supercategories/css/supercategories.css" media="all">
        <link rel="stylesheet" href="<?= Yii::app()->request->baseUrl ?>/css/frontend/default/sm_flipshop/css/yt-responsive.css" type="text/css">
        <link rel="stylesheet" href="<?= Yii::app()->request->baseUrl ?>/css/frontend/default/sm_flipshop/css/colors/cyan_red.css" type="text/css">

        <script type="text/javascript" src="<?= Yii::app()->request->baseUrl ?>/css/frontend/default/sm_flipshop/js/jquery-1.11.1.min.js"></script>

        <script type="text/javascript" src="<?= Yii::app()->request->baseUrl ?>/css/frontend/default/sm_flipshop/js/jquery-ui.min.js"></script>

        <script type="text/javascript" src="<?= Yii::app()->request->baseUrl ?>/css/frontend/default/sm_flipshop/bootstrap/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="<?= Yii::app()->request->baseUrl ?>/css/frontend/default/sm_flipshop/js/owl.carousel.js"></script> 
    </head>
    <body id="bd" class="sm_flipshop pattern6   cms-index-index cms-home-left"> 
        <div id="yt_wrapper">
            <!-- BEGIN: Header -->
            <div id="yt_header" class="yt-header wrap">

               

              


                <script type="text/javascript">
                    jQuery(document).ready(function($) {
                        $('.theme-color').click(function() {
                            $($(this).parent().find('.active')).removeClass('active');
                            $(this).addClass('active');
                        }
                        );
                    }
                    );

                </script>

                <a id="yt-totop" href="#" title="Go to Top" style="display: none;">
                </a>
                <script type="text/javascript">
                    // slide to top
                    jQuery(document).ready(function($) {

                        $("#yt-totop").hide();
                        $(function() {
                            var wh = $(window).height();
                            var whtml = $(document).height();
                            $(window).scroll(function() {
                                if ($(this).scrollTop() > whtml / 10) {
                                    $('#yt-totop').fadeIn();
                                }
                                else {
                                    $('#yt-totop').fadeOut();
                                }
                            }
                            );
                            $('#yt-totop').click(function() {
                                $('body,html').animate({
                                    scrollTop: 0
                                }
                                , 800);
                                return false;
                            }
                            );
                        }
                        );
                    }
                    );
                </script>
            </div>
            <!-- END: Header -->


            <script>
                function get_outlets(partner) {
                    $.ajax({
                        type: 'POST', //request type
                        url: "<?= CController::createUrl('home/getoutlets') ?>",
                        data: {partner_id: partner},
                        success: function(data) {
                            $("#outlet").html(data);
                           $('#outlet_drop').val(<?= $_REQUEST['outlet']; ?>);
                        }
                    });
                }
                function get_menus(outlet) {
                    $.ajax({
                        type: 'POST', //request type
                        url: "<?= CController::createUrl('home/getmenus') ?>",
                        data: {outlet_id: outlet},
                        success: function(data) {
                            $("#menu").html(data)
                        }
                    });
                }

                function get_items(menu) {
                    $.ajax({
                        type: 'POST', //request type
                        url: "<?= CController::createUrl('home/getitems') ?>",
                        data: {menu_id: menu},
                        success: function(data) {
                            $("#item").html(data)
                        }
                    });
                }
            </script>
            
                
  

<!-- BEGIN: content -->
    <div id="yt_content" class="yt-content wrap">
        <div class="yt-content-inner">
            <div class="main-full">
                <div class="container">
                    <div class="row">
                        <?php // $this->renderPartial('left_side'); ?>
                        <div id="" class="yt-main-right yt-main col-main col-lg-9 col-md-9">
                            <div class="container">
                                <div class="row">
                                    <div class="col-md-9">
                                        <div id="messages_product_view">
                                            <!--<ul class="messages"><li class="notice-msg"><ul><li><span>///</span></li></ul></li></ul></div>-->
                                            <div class="yt-product-detail">
                                                <div class="yt-product-detail-inner">
                                                    <div class="row product-essential">
                                                        <div class="">
<!--                                                            <div class="image-box-detail col-lg-5 col-md-5 col-sm-5 col-xs-12">
                                                                <img src="<?= Yii::app()->request->baseUrl ?>/media/outlet/<?= $outlet->image ?>">
                                                            </div>  -->



                                                                                 <div class="image-box-detail col-lg-5 col-md-5 col-sm-5 col-xs-12">

                                                        <div class="item-zoom">
                                                      
                                                                
                                                                  <img  id="zoom_03" class="slider-zoom"  src="<?= Yii::app()->request->baseUrl ?>/media/outlet/<?= $outlet->image ?>"  data-zoom-image="<?= Yii::app()->request->baseUrl ?>/media/outlet/<?= $outlet->image ?>"    style="width:258px  !important;height:258px !important;" >
                                                       
                                                        <div id="gallery_01" class="gallery col-md-12 col-xs-12">
                                                            <?php
                                                            foreach($photos as $photo){ ?>
                                                                <a href="#" data-image="<?= Yii::app()->request->baseUrl; ?>/gallery/<?=$photo->rank?>medium.jpg" data-zoom-image="<?= Yii::app()->request->baseUrl; ?>/gallery/<?=$photo->rank?>large.jpg" class="col-md-3"> 
                                                                <img id="img_01" src="<?= Yii::app()->request->baseUrl; ?>/gallery/<?=$photo->rank?>small.jpg" /> 
                                                                </a>
                                                            <?php
                                                            }
                                                            ?> 
                                                        </div> 
                                                            </div>

                                                        </div>  

                                                        <script src="<?php echo Yii::app()->baseUrl; ?>/js/jquery.elevatezoom.js"></script>

                                                        <script>
                                                            $("#zoom_03").elevateZoom({gallery: 'gallery_01', zoomType: "inner"});
                                                        </script>

                                                            <div class="yt-detail-right col-lg-7 col-md-7 col-sm-7 col-xs-12">          
                                                                <div class="product-shop">
                                                                    <h2 class="product-name">
                                                                       
                                                                        <?= $outlet->title ?>
                                                                    </h2>
                                                                    <div class="clearer"></div>
                                                                    

  <?php
                                                                   if($outlet->discount > 0){
                                                                       ?>
                                                                     <b>  <?php echo Yii::t('default', '');?> </b>  <?php echo Yii::app()->params['currency'] ; ?>    <?= $outlet->discount ?>  
                                                                    
                                                                    <?php
                                                                   }
                                                                   ?>
                                                                    <div class="clearer"></div>
                                                                     <?= $outlet->intro ?> 
                                                                    
                                                                </div>
                                                            </div>

                                                            <script>

                                                                $('.form-input').keyup(function() {
                                                                    // GET INPUT VALUE
                                                                    var day, month, year;
                                                                    day = $('.day').val();
                                                                    month = $('.month').val();
                                                                    year = $('.year').val();
                                                                    // ORGANIZE VALUES
                                                                    var date = year + '/' + month + '/' + day;
                                                                    // GENERATE AGE
                                                                    var birthdate, cur, diff, age;
                                                                    birthdate = new Date(date);
                                                                    cur = new Date();
                                                                    diff = cur - birthdate;
                                                                    age = Math.floor(diff / 31536000000);
                                                                    // DISPLAY AGE
                                                                    $('#age').html(age);
                                                                }).keyup();

                                                            </script>


                                                            <div id="yt_tab_products" class="col-md-12 tab-product-detail">
                                                                <ul class="yt-tab-navi">
                                                                    <li class="active">
                                                                        <a data-toggle="tab" href="#decription"><?php echo Yii::t('default', 'Description');?></a>
                                                                    </li>
                                                                    <li>
                                                                        <a data-toggle="tab" href="#yt_tab_reviewform"><?php echo Yii::t('default', 'Reviews');?></a>
                                                                    </li>

                                                                    <li >
                                                                        <a data-toggle="tab" href="#recommended"><?php echo Yii::t('default', 'Recommended');?></a>
                                                                    </li>
                                                                 
                                                                       
                                                                    
<!--                                                                      <li>
                                                                        <a data-toggle="tab" href="#menus">Menus</a>
                                                                    </li>-->
                                                                    
                                                                     <li>
                                                                        <a data-toggle="tab" href="#menu_items"><?php echo Yii::t('default', '');?><?php echo Yii::t('default', 'Menu Items');?></a>
                                                                    </li>
                                                                    
                                                                    

                                                                </ul>
                                                                <div class="yt-tab-content tab-content">                        
                                                                    <div id="decription" class="tab-pane fade active in">
                                                                        <!-- h2></h2-->
                                                                        <div class="std">
                                                                            <?php
                                                                            echo $outlet->description;
                                                                            ?>
                                                                        </div>
                                                                    </div>
        <div id="yt_tab_reviewform" class="tab-pane fade">
            <div class="box-collateral box-reviews" id="customer-reviews">
                <h2><?php echo Yii::t('default', 'Customer Reviews');?></h2>
                <div class="pager">
                    <p class="amount">
                        <strong><?= count($reviews) ?> Item(s)</strong>
                    </p>
                    <div class="limiter">
                        <label>Show</label>
                         <form method="post" action="" id="pagesform">
                    <select onchange="countReviews()" class="jqtransformdone" name="page">
                        <option value="5" <?php
                        if ($_REQUEST['page'] == 5) {
                            echo Yii::t('default', 'selected');
                        } else {
                            echo "";
                        }
                        ?>>
                            5            
                        </option>
                        <option value="20" <?php
                        if ($_REQUEST['page'] == 20) {
                            echo Yii::t('default', 'selected');
                        } else {
                            echo "";
                        }
                        ?>>
                            20           
                        </option>
                        <option value="50" <?php
                        if ($_REQUEST['page'] == 50) {
                            echo Yii::t('default', 'selected');
                        } else {
                            echo "";
                        }
                        ?>>
                            50            
                        </option>
                    </select> per page 
                </form>
                    </div>
                </div>
                <dl>
                    <?php
                    foreach ($reviews as $review) {
                        $price = ($review->price_rate / 5) * 100;
                        $value = ($review->value_rate / 5) * 100;
                        $quality = ($review->quality_rate / 5) * 100;
                        ?>
                        <dt>
                        <?php echo Yii::t('default', 'first article Review by');?> <span><?= $review->user->username; ?></span>            
                        </dt>
                        <dd>
                            <table class="ratings-table">
                                <colgroup>
                                    <col width="1">
                                    <col>
                                </colgroup>
                                <tbody>
                                    <tr>
                                        <th><?php echo Yii::t('default', 'Price');?></th>
                                        <td>
                                            <div class="rating-box">
                                                <div class="rating" style="width:<?= $price ?>%;"></div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th><?php echo Yii::t('default', 'Value');?></th>
                                        <td>
                                            <div class="rating-box">
                                                <div class="rating" style="width:<?= $value ?>%;"></div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th><?php echo Yii::t('default', 'Quality');?></th>
                                        <td>
                                            <div class="rating-box">
                                                <div class="rating" style="width:<?= $quality ?>%;"></div>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <?= $review->review; ?>
                            <small class="date">(<?php echo Yii::t('default', 'Posted on');?> <?= date('d/m/Y', strtotime($review->date_created)); ?>)</small>
                            <?php
                            /*
                            if (Yii::app()->user->id && $review->user_id != Yii::app()->user->id) {
                                if (ReviewLikes::model()->find(array('condition' => 'review_id = ' . $review->id . ' and user_id = ' . Yii::app()->user->id))) {
                                    echo '<button class="button pull-right" style="margin-top: -21px;" onclick="dislike(' . $review->id . ')">Dislike</button>';
                                } else {
                                    echo '<button class="button pull-right" style="margin-top: -21px;" onclick="like(' . $review->id . ')">Like</button>';
                                }
                            }
                            */
                            ?>
                        </dd>
                        <?php
                    }
                    ?>
                </dl>
            </div>
            <?php if (Yii::app()->user->id && !OutletReview::model()->find(array('condition' => 'user_id = ' . Yii::app()->user->id . ' and outlet_id = ' . $outlet->id))) { ?>
                <div class="form-add">
                    <!--h2></h2-->
                    <form action="" method="post" id="review-form">
                        <input name="form_key" type="hidden" value="0dFh25rijWrMUXuC">
                        <fieldset>  
                            <div class="customer-review">
                                <h4><?php echo Yii::t('default', 'How do you rate this product?');?> <!--em class="required">*</em--></h4>
                                <span id="input-message-box"></span>                
                                <table class="data-table" id="product-review-table">                        
                                    <thead>
                                        <tr class="first last">
                                            <th>&nbsp;</th>
                                            <th><span class="nobr">1 <?php echo Yii::t('default', 'star');?></span></th>
                                            <th><span class="nobr">2 <?php echo Yii::t('default', 'stars');?></span></th>
                                            <th><span class="nobr">3 <?php echo Yii::t('default', 'stars');?></span></th>
                                            <th><span class="nobr">4 <?php echo Yii::t('default', 'stars');?></span></th>
                                            <th><span class="nobr">5 <?php echo Yii::t('default', 'stars');?></span></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr class="first odd">
                                            <th><?php echo Yii::t('default', 'Quality');?></th>
                                            <td class="value"><input type="radio" name="OutletReview[quality_rate]" id="Quality_1" value="1" class="radio"></td>
                                            <td class="value"><input type="radio" name="OutletReview[quality_rate]" id="Quality_2" value="2" class="radio"></td>
                                            <td class="value"><input type="radio" name="OutletReview[quality_rate]" id="Quality_3" value="3" class="radio"></td>
                                            <td class="value"><input type="radio" name="OutletReview[quality_rate]" id="Quality_4" value="4" class="radio"></td>
                                            <td class="value last"><input type="radio" name="OutletReview[quality_rate]" id="Quality_5" value="5" class="radio"></td>
                                        </tr>
                                        <tr class="even">
                                            <th><?php echo Yii::t('default', 'Price');?></th>
                                            <td class="value"><input type="radio" name="OutletReview[price_rate]" id="Price_1" value="1" class="radio"></td>
                                            <td class="value"><input type="radio" name="OutletReview[price_rate]" id="Price_2" value="2" class="radio"></td>
                                            <td class="value"><input type="radio" name="OutletReview[price_rate]" id="Price_3" value="3" class="radio"></td>
                                            <td class="value"><input type="radio" name="OutletReview[price_rate]" id="Price_4" value="4" class="radio"></td>
                                            <td class="value last"><input type="radio" name="OutletReview[price_rate]" id="Price_5" value="5" class="radio"></td>
                                        </tr>
                                        <tr class="last odd">
                                            <th><?php echo Yii::t('default', 'Value');?></th>
                                            <td class="value"><input type="radio" name="OutletReview[value_rate]" id="Value_1" value="1" class="radio"></td>
                                            <td class="value"><input type="radio" name="OutletReview[value_rate]" id="Value_2" value="2" class="radio"></td>
                                            <td class="value"><input type="radio" name="OutletReview[value_rate]" id="Value_3" value="3" class="radio"></td>
                                            <td class="value"><input type="radio" name="OutletReview[value_rate]" id="Value_4" value="4" class="radio"></td>
                                            <td class="value last"><input type="radio" name="OutletReview[value_rate]" id="Value_5" value="5" class="radio"></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="write-your-review">         
                                <h4><?php echo Yii::t('default', 'Write your own review');?></h4>
                                <ul class="form-list">
                                    <!--<li>
                                        <label for="nickname_field" class="required">Nickname<em> *</em></label>
                                        <div class="input-box">
                                            <input type="text" name="nickname" id="nickname_field" class="input-text required-entry" value="">
                                        </div>
                                    </li>-->
                                    <li>
                                        <label for="summary_field" class="required"><?php echo Yii::t('default', 'Summary of Your Review');?><em> *</em></label>
                                        <div class="input-box">
                                            <input type="text" name="OutletReview[title]" id="summary_field" class="input-text required-entry" value="">
                                        </div>
                                    </li>
                                    <li>
                                        <label for="review_field" class="required"><?php echo Yii::t('default', 'Review');?><em> *</em></label>
                                        <div class="input-box">
                                            <textarea name="OutletReview[review]" id="review_field" cols="10" rows="6" class="required-entry"></textarea>
                                        </div>
                                    </li>
                                </ul>
                            </div>

                        </fieldset>
                        <div class="buttons-set">
                            <button type="submit" title="Submit Review" class="button">
                                <span><span class="submit-review-text"><?php echo Yii::t('default', 'Submit Review');?></span></span>
                            </button>
                        </div>
                    </form>
                </div>
                <?php
            }
            ?>
        </div>

    <div id="recommended" class="tab-pane fade">
        <!-- h2></h2-->
        <div class="std">
            
            <?php
            if ($similar_outlets) {
                foreach ($similar_outlets as $outlet) {
                    ?>
             <div class="item col-lg-4 col-md-6 col-sm-6 respl-item itemsscroll">
                                                <div class="item-inner">
                                                    <div class="w-image-box">
                                                        <span class="hover-background"></span>
                                                        <div class="item-image">
                                                            <a href="<?= Yii::app()->request->baseUrl ?>/outlet-<?php echo $outlet->slug; ?>" title="<?=$outlet->title?>" class="product-image"><img src="<?= Yii::app()->request->baseUrl ?>/media/outlet/<?php echo $outlet->image; ?>" alt="<?=$partner->title?>"  style="width:317px  !important;height:317px !important;" ></a>
                                                           
                                                           
<div class="quick-btns">

 </div>
                                                            
                                                              
                                                              <?php
                                                                   if($outlet->discount > 0){
                                                                       ?>
                                                                    
                                                                      <span class="disc">Discount: <?php echo Yii::app()->params['currency'] ; ?>    <?=$outlet->discount?></span>
                                                                    
                                                                    <?php
                                                                   }
                                                                   ?>
                                                                    
                                                         
                                                            <p>
                                                                <?= substr($outlet->intro, 0, 50)?>
                                                            </p>
                                                        </div>
                                                    </div>
                                                    <div class="item-info">
                                                        <div class="item-title">
                                                          <?php echo $outlet->title; ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

              <?php
                }
            } else {
                ?>
            <div> <?php echo Yii::t('default', 'No items found.');?></div>
            <?php
              }
            ?>
            
        </div>
    </div>
                                                                    
                                                                    
                                                                    
<div id="menus" class="tab-pane fade">
   <!-- h2></h2-->
   <div class="std">
       
       
       
          <?php
        
if ($menus) {
  foreach ($menus as $menu) { ?>
         <div class="ltabs-item item respl-item col-lg-4 col-md-4 col-sm-6" style="-webkit-animation: slideLeft 600ms 300ms; opacity: 1;">
                                                    <div class="item-inner">
                                                        <div class="w-image-box">
                                                            <span class="hover-background">
                                                            </span>
                                                            <div class="item-image">
                                                              
                                                                    <img  style="width:200px;height:200px !important;"  alt="<?= $menu->title ?>" title="<?= $menu->title ?>" src="<?= Yii::app()->request->baseUrl ?>/media/menus/<?= $menu->image ?>">
                                                               
                                                               


                                                            </div>
                                                            <div class="label-wrapper">
                                                                <?php
                                                                if ($menu->new_deal == 1) {
                                                                    echo '<div class="new-product">
                                                        '.Yii::t('default', 'New').'
                                                  </div>';
                                                                }if ($menu->discount) {
                                                                    echo '<div class="sale-product">
                                                        '.Yii::t('default', 'Sale').'                
                                                  </div>';
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>

                                                        <div class="item-info">
                                                            <div class="item-title">
                                                               
                                                                    <?= $menu->title ?>        
                                                                
                                                            </div>
                                                            <div class="item-price">
                                                                <div class="sale-price">

                                                                    <div class="price-box">

   
                                                                        <span class="special-price">
    <span> <?php
    if($menu->discount_type==1){// %
      echo Yii::t('default', 'Discount: '). $menu->discount. ' %' ;
    }else{
         echo Yii::t('default', 'Discount: ').Yii::app()->params['currency'].' '. $menu->discount ;
    }
?>  
    </span>

   </span>
                                                                        
                                                                        
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="item-review">
                                                                <p class="no-rating">
                                                                   
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                              
                                                <?php
                                            }
                                            
} else {
                ?>
            <div> <?php echo Yii::t('default', 'No items found.');?></div>
            <?php
              }
              
   
            ?>

   </div>
</div>

                                                                    
 <div id="menu_items" class="tab-pane fade">
   <!-- h2></h2-->
   <div class="std">
         <?php
if ($menu_items) {
foreach ($menu_items as $menu_item) {
   $item= $menu_item->item ;
   ?>
       
        <div class="ltabs-item item respl-item col-lg-4 col-md-4 col-sm-6" style="-webkit-animation: slideLeft 600ms 300ms; opacity: 1;">
            
                                                        <div class="item-inner">
                                                            <div class="w-image-box">
                                                                <span class="hover-background">
                                                                </span>
                                                                <div class="item-image">
                                                                   
                                                                        <img style="width:200px;height:200px !important;"  alt="<?= $menu->title ?>"   alt="<?= $item->title ?>" title="<?= $item->title ?>" src="<?= Yii::app()->request->baseUrl ?>/media/items/<?= $item->image ?>">
                                                                        
                                                                                                                                                                          </div>
                                                                <div class="label-wrapper">
                                                                    <?php
                                                                    /*
                                                                    if ($item->new_deal == 1) {
                                                                        echo '<div class="new-product">
                                                        New
                                                  </div>';
                                                                    }if ($item->discount_amount) {
                                                                        echo '<div class="sale-product">
                                                        Sale                
                                                  </div>';
                                                                    }
                                                                    */
                                                                    ?>
                                                                </div>
                                                            </div>

                                                            <div class="item-info">
                                                                <div class="item-title">
                                                                   
                                                                        <?= $item->title ?>        
                                                                   
                                                                </div>
                                                                <div class="item-price">
                                                                    <div class="sale-price">

                                                                        <div class="price-box">

                                                                            <span class="special-price">
<!--                                                                                <span class="special-price" id="product-price-443">$ <?php  echo $item->price ; ?></span>-->
                                                                                <?php
                                                                                /*
                                                                                if ($item->discount_amount) {
                                                                                    if ($item->discount_type == 1) {
                                                                                        echo '<span class="special-price" id="product-price-443">' . $item->discount_amount . '%</span>';
                                                                                    } else {
                                                                                        echo '<span class="special-price" id="product-price-443">' . $item->discount_amount . '</span>';
                                                                                    }
                                                                                }
                                                                                */
                                                                                ?>
                                                                            </span>
                                                                        </div>
                                                                    </div>
                                                                </div>
<!--                                                                <div class="item-review">
                                                                    <p class="no-rating">
                                                                       
                                                                            Be the first to review this product
                                                                        
                                                                    </p>
                                                                </div>-->
                                                            </div>
                                                        </div>
                                                    </div>
       
       
       <?php
     }
} else {
                ?>
            <div> <?php echo Yii::t('default', 'No items found.');?></div>
            <?php
              }
            ?>

   </div>
</div>   
                                                                    
                                                                    
                                                                    
            
                                                                    
                                                                    


                                                                    
                                                                    
                                                                    

                                                                </div>
                                                            </div>  
                                                            <div style="clear:both;"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--added to fix layout hussam-->
                </div>
            </div>
        </div>   
    </div>
    <div class="bottom-wrapper">
    </div>
    </div>
    <script>
        function countReviews() {
            $('#pagesform').submit();
        }

        function like(review) {
            $.ajax({
                type: "post",
                url: "<?= Yii::app()->getBaseUrl(true); ?>/home/likeReview",
                data: {id: review},
                success: function(data) {
                    location.reload();
                }
            });
        }
        function dislike(review) {
            $.ajax({
                type: "post",
                url: "<?= Yii::app()->getBaseUrl(true); ?>/home/dislikeReview",
                data: {id: review},
                success: function(data) {
                    location.reload();
                }
            });
        }
    </script>
