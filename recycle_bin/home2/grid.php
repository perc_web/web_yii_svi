  <!-- BEGIN: content -->
      <div id="yt_content" class="yt-content wrap">
        <div class="yt-content-inner">
          <div class="main-full">
              <?php
              if($type == 'search'){
                  $this->renderPartial('results', array('partners' => $partners, 'scroll_items' => $scroll_items));
              }else{
                  $this->renderPartial('GridContent', array('partners'=>$partners, 'scroll_partners' => $scroll_partners, 'categories' => $categories));
              }
              ?>
            <!--added to fix layout hussam-->
          </div>
        </div>
      </div>
    </div>
    <div class="bottom-wrapper">
    </div>
  </div>
  <script>
    function insertParam(key, value)
    {
        key = encodeURI(key);
        value = encodeURI(value);

        var kvp = document.location.search.substr(1).split('&');

        var i = kvp.length;
        var x;
        while (i--)
        {
            x = kvp[i].split('=');

            if (x[0] == key)
            {
                x[1] = value;
                kvp[i] = x.join('=');
                break;
            }
        }

        if (i < 0) {
            kvp[kvp.length] = [key, value].join('=');
        }

        //this will reload the page, it's likely better to store this until finished
        document.location.search = kvp.join('&');
    }
</script>
