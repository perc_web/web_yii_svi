<div class="container">
                <div class="row">
                  <div class="col-md-8 most-popular">
                    <div class="title-footer">
                      <h2><?php echo Yii::t('default', 'More Information');?></h2>
                      <div class="content-footer">
                        <div class="row">
                          <div class="col-md-3 col-sm-6">
                            <h3></h3>
                            <ul class="list-links">
                              
                              <li>
                                <a title="<?php echo Yii::t('default', 'Karbonn Mobile');?>" href="#"><?php echo Yii::t('default', 'Terms &amp; Conditions');?></a>
                              </li>
                            </ul>
                          </div>
                          <div class="col-md-3 col-sm-6">
                            <h3></h3>
                            <ul class="list-links">
                              <li>
                                <a title="<?php echo Yii::t('default', 'Camera Bag');?>" href="#"><?php echo Yii::t('default', 'Advertise');?></a>
                              </li>
                            </ul>
                          </div>
                          <div class="col-md-3 col-sm-6 clear-left">
                            <h3></h3>
                            <ul class="list-links">
       
                              
                              <li>
                                <a title="<?php echo Yii::t('default', 'Notebook');?>" href="#"><?php echo Yii::t('default', 'Join As Partner');?></a>
                              </li>
                            </ul>
                          </div>
                          <div class="col-md-3 col-sm-6">
                            <h3></h3>
                            <ul class="list-links">
 
                              <li>
                                <a title="<?php echo Yii::t('default', 'LED TV');?>" href="#"><?php echo Yii::t('default', 'Careers');?></a>
                              </li>
                            </ul>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="col-md-4">
                    <div class="row">
                      
                      

                      <div class="col-md-12 payment-wrap" style="padding-top:0px">
                        <div class="title-footer">
                          <h2>
                            <?php echo Yii::t('default', 'We Accept');?>
                          </h2>
                        </div>
                        <div class="content-footer">
                          <ul class="list-payment">
                            <li title="<?php echo Yii::t('default', 'Payment 1');?>">
                              <img src="<?=Yii::app()->request->baseUrl?>/media/wysiwyg/payment-1.png" width = "62" height = "64"  alt="<?php echo Yii::t('default', 'Payment 1');?>">
                            </li>
                            <li title="<?php echo Yii::t('default', 'Payment 2');?>">
                              <img src="<?=Yii::app()->request->baseUrl?>/media/wysiwyg/payment-2.png" alt="<?php echo Yii::t('default', 'Payment 2');?>">
                            </li>
                            <li title="<?php echo Yii::t('default', 'Payment 4');?>">
                              <img src="<?=Yii::app()->request->baseUrl?>/media/wysiwyg/payment-4.png" alt="<?php echo Yii::t('default', 'Payment 4');?>">
                            </li>
                          </ul>
                        </div>
                      </div>

                    </div>
                  </div>
                </div>
              </div>
              